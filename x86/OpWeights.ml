open PrepassSchedulingOracleDeps;;

let get_opweights () : opweights =
{
  pipelined_resource_bounds = [||];
  nr_non_pipelined_units = 0;
  latency_of_op = (fun _ _ -> 0);
  resources_of_op = (fun _ _ -> [||]);
  non_pipelined_resources_of_op = (fun _ _ -> [||]);
  latency_of_load = (fun _ _ _ _ -> 0);
  resources_of_load = (fun _ _ _ _ -> [||]);
  resources_of_store = (fun _ _ _ -> [||]);
  resources_of_cond = (fun _ _ -> [||]);
  latency_of_call = (fun _ _ -> 0);
  resources_of_call = (fun _ _ -> [||]);
  resources_of_builtin = (fun _ -> [||])
}

