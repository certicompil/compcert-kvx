(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*          Xavier Leroy, INRIA Paris-Rocquencourt                     *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the INRIA Non-Commercial License Agreement.     *)
(*                                                                     *)
(* *********************************************************************)

(** Auxiliary functions on machine registers *)

let is_scratch_register s =  s = "R14"  || s = "r14"
                                              
let class_of_type = function
  | AST.Tint | AST.Tlong -> 0
  | AST.Tfloat | AST.Tsingle -> 1
  | AST.Tany32 | AST.Tany64 -> assert false

(* number of available registers per class *)
let nr_regs = [| 12; 16 |]
