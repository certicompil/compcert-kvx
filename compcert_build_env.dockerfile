FROM debian:stable-20210408
LABEL maintainer="David.Monniaux@univ-grenoble-alpes.fr"
RUN apt-get update && apt-get upgrade -y && apt-get -y install gcc-powerpc-linux-gnu gcc-powerpc64-linux-gnu gcc-riscv64-linux-gnu gcc-arm-linux-gnueabi gcc-arm-linux-gnueabihf  gcc-aarch64-linux-gnu qemu-user opam
RUN adduser --gecos "Application user" appuser
USER appuser
RUN opam init --disable-sandboxing && opam switch create 4.11.2+flambda && eval $(opam config env) && opam pin -y add -n coq 8.12.2 && opam install -y menhir ocamlbuild coq
