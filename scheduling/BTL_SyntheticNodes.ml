open Maps
open BTL
open BTLtypes
open BTLcommonaux

let compute_surrounding_blocks btl entry =
  apply_over_function btl (fun n ibf -> ibf.binfo.predecessors <- []);
  List.iter
    (fun (n, ibf) ->
      let succs = successors_block ibf in
      ibf.binfo.successors <- succs;
      List.iter
        (fun s ->
          let ibf = pget s btl in
          ibf.binfo.predecessors <- n :: ibf.binfo.predecessors)
        succs)
    (PTree.elements btl)

let eliminate_synthetic_nodes code =
  let new_code = ref code in
  List.iter
    (fun (n, ibf) ->
      match ibf.entry with
      | BF (Bgoto s, iinfo) ->
          let new_ib = Bseq (Bnop (Some (def_iinfo ())), BF (Bgoto s, iinfo)) in
          let new_ibf = mk_ibinfo new_ib ibf.binfo in
          new_code := PTree.set n new_ibf !new_code
      | _ -> ())
    (PTree.elements code);
  !new_code
