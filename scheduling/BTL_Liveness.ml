open BTL
open Maps
open Registers
open Lattice
open Kildall
open BTLtypes
open BTLcommonaux
open RTLcommonaux
open DebugPrint

let transfer btl need_dce pc after =
  let open Liveness in
  let ibf = pget pc btl in
  let rec transfer_iblock ib after =
    match ib with
    | Bnop _ | BF (Bgoto _, _) -> after
    | Bop (_, args, res, iinfo) | Bload (_, _, _, args, _, res, iinfo) ->
        if Regset.mem res after then (
          iinfo.visited <- false;
          reg_list_live args (Regset.remove res after))
        else (
          need_dce := true;
          iinfo.visited <- true;
          after)
    | Bstore (_, _, args, _, src, _) -> reg_list_live args (Regset.add src after)
    | Bseq (Bcond (_, args, ib1, Bnop None, _), ib2) ->
        reg_list_live args
          (Regset.union (transfer_iblock ib1 after) (transfer_iblock ib2 after))
    | Bseq (ib1, ib2) -> transfer_iblock ib2 after |> transfer_iblock ib1
    | BF (Bcall (_, ros, args, res, _), _) ->
        reg_list_live args (reg_sum_live ros (Regset.remove res after))
    | BF (Btailcall (_, ros, args), _) ->
        reg_list_live args (reg_sum_live ros Regset.empty)
    | BF (Bbuiltin (_, args, res, _), _) ->
        reg_list_live
          (AST.params_of_builtin_args args)
          (reg_list_dead (AST.params_of_builtin_res res) after)
    | BF (Bjumptable (arg, _), _) -> Regset.add arg after
    | BF (Breturn optarg, _) -> reg_option_live optarg Regset.empty
    | _ ->
        failwith
          "BTL_Liveness.analyze_liveness.transfer: unsupported BTL format"
  in
  transfer_iblock ibf.entry after

module RegsetLat = LFSet (Regset)
module DS = Backward_Dataflow_Solver (RegsetLat) (NodeSetBackward)

let analyze_liveness_btl btl =
  let need_dce = ref false in
  apply_over_function btl (tf_ibf_ib reset_visited_ib_rec);
  let liveouts =
    get_some @@ DS.fixpoint btl successors_block (transfer btl need_dce)
  in
  let liveness =
    PTree.map
      (fun n _ ->
        let lo = PMap.get n liveouts in
        transfer btl need_dce n lo)
      btl
  in
  (liveness, !need_dce)

let get_outputs liveness n succs =
  let list_input_regs =
    List.map (fun n -> get_some @@ PTree.get n liveness) succs
  in
  List.fold_left Regset.union Regset.empty list_input_regs

let apply_liveness_info liveness n ibf =
  let liveness = liveness in
  let inputs = pget n liveness
  and soutput = get_outputs liveness n (successors_block ibf) in
  ibf.binfo.input_regs <- inputs;
  ibf.binfo.s_output_regs <- soutput

let update_liveness_btl btl =
  let liveness, need_dce = analyze_liveness_btl btl in
  apply_over_function btl (apply_liveness_info liveness);
  need_dce

let compute_liveins n ibf btl =
  let rec traverse_rec ib =
    match ib with
    | Bseq (ib1, ib2) ->
        traverse_rec ib1;
        traverse_rec ib2
    | BF (Bgoto succ, iinfo)
    | BF (Bcall (_, _, _, _, succ), iinfo)
    | BF (Bbuiltin (_, _, _, succ), iinfo) ->
        let lives = (get_some @@ PTree.get succ btl).binfo.input_regs in
        iinfo.liveins <- lives
    | BF (Bjumptable (_, tbl), iinfo) ->
        List.iter
          (fun ex ->
            let lives = (get_some @@ PTree.get ex btl).binfo.input_regs in
            iinfo.liveins <- Regset.union iinfo.liveins lives)
          tbl
    | Bcond (_, _, BF (Bgoto succ, _), Bnop None, iinfo) -> (
        match iinfo.opt_info with
        | Some predb ->
            assert (predb = false);
            let lives = (get_some @@ PTree.get succ btl).binfo.input_regs in
            iinfo.liveins <- lives
        | None -> ())
    | _ -> ()
  in
  traverse_rec ibf.entry

let get_liveins = function
  | BF (_, iinfo)
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, _, iinfo)
  | Bcond (_, _, _, _, iinfo) ->
      iinfo.liveins
  | _ -> failwith "get_liveins: Bnop None, invalid iblock"

let btl_simple_dead_code_elimination btl =
  let count_dce = ref 0 in
  let new_code = ref btl in
  let rec block_simple_dce ib =
    match ib with
    | Bseq (ib1, ib2) -> Bseq (block_simple_dce ib1, block_simple_dce ib2)
    | Bcond (cond, args, ib1, ib2, iinfo) ->
        Bcond (cond, args, block_simple_dce ib1, block_simple_dce ib2, iinfo)
    | Bop (_, _, _, iinfo) | Bload (_, _, _, _, _, _, iinfo) ->
        if iinfo.visited then (
          count_dce := !count_dce + 1;
          Bnop (Some (def_iinfo ())))
        else ib
    | _ -> ib
  in
  List.iter
    (fun (n, ibf) ->
      let old_count = !count_dce in
      let ib = block_simple_dce ibf.entry in
      if !count_dce > old_count then
        let ibf' = mk_ibinfo ib ibf.binfo in
        new_code := PTree.set n ibf' !new_code)
    (PTree.elements btl);
  debug "btl_simple_dead_code_elimination: dce made %d operations\n" !count_dce;
  !new_code
