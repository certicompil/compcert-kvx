(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*              David Monniaux, CNRS, VERIMAG                          *)
(*                                                                     *)
(* *********************************************************************)

Require Import FunInd.
Require Import Coqlib Maps Integers Floats Lattice Kildall.
Require Import Compopts AST Linking.
Require Import Values Memory Globalenvs Builtins Events.
Require Import Registers Op BTL.
Require Import ValueDomain ValueAOp Liveness.
Require List.
Require ValueAnalysis.
Require MultiFixpoint.
Require Kildall.

Module DS := MultiFixpoint.Solver(Kildall.NodeSetForward)(VA).
(** * The dataflow analysis *)

Notation areg := ValueAnalysis.areg.
Notation aregs := ValueAnalysis.aregs.

Fixpoint ae_set_multiple (regs : list reg) (avals : list aval) (ae : aenv): aenv :=
  match regs, avals with
  | (r::regs0), (av::avals0) =>
      ae_set_multiple regs0 avals0 (AE.set r av ae)
  | nil, _ | _, nil => ae
  end.

Definition transfer_condition (cond: condition) (regs: list reg) (ae : aenv) : aenv :=
  ae_set_multiple regs (filter_static_condition cond (aregs ae regs)) ae.
                        
(** Analysis of function calls.  We treat specially the case where
  neither the arguments nor the global variables point within the
  stack frame of the current function.  In this case, no pointer
  within the stack frame escapes during the call. *)

Inductive binary_tree (A : Type) :=
| binary_tree_leaf : A -> binary_tree A
| binary_tree_split : binary_tree A -> binary_tree A -> binary_tree A.
Arguments binary_tree_leaf {A}.
Arguments binary_tree_split {A}.

Fixpoint binary_tree_fold {A B : Type} (f : A -> B -> B)
         (tr : binary_tree A) (b0 : B) :=
  match tr with
  | binary_tree_leaf a => f a b0
  | binary_tree_split l r =>
      binary_tree_fold f l (binary_tree_fold f r b0)
  end.

Fixpoint apply_at_leaves {A B : Type} (f : A -> binary_tree B)
         (tree : binary_tree A) :=
  match tree with
  | binary_tree_leaf a => f a
  | binary_tree_split l r =>
    binary_tree_split (apply_at_leaves f l) (apply_at_leaves f r)
  end.

Fixpoint match_tree {A : Type} (P : A -> Prop) (tr : binary_tree A) :=
  match tr with
  | binary_tree_leaf l => P l
  | binary_tree_split l r =>
      match_tree P l \/ match_tree P r
  end.

Definition outcome :=  (VA.t * (option final))%type.
Definition outcomes := binary_tree outcome.

Definition apply_on_outcomes (f : aenv -> amem -> outcomes):=
  apply_at_leaves
  (fun (oc : outcome) =>
     let (s, final) := oc in
     match s with
     | VA.Bot => binary_tree_leaf oc
     | VA.State ae am =>
         match final with
         | Some _ => binary_tree_leaf oc
         | None => f ae am
         end
     end).

Fixpoint transfer_iblock (rm: romem) (ib : iblock) (ae: aenv) (am: amem) :
  outcomes :=
  match ib with
  | BF fi iinfo => binary_tree_leaf ((VA.State ae am), (Some fi))
  | Bnop iinfo => binary_tree_leaf ((VA.State ae am), None)
  | Bop op args res iinfo =>
      let a := eval_static_operation op (aregs ae args) in
      binary_tree_leaf ((VA.State (AE.set res a ae) am), None)
  | Bload TRAP chunk addr args _ dst iinfo =>
      let a := loadv chunk rm am
                     (eval_static_addressing addr (aregs ae args)) in
      binary_tree_leaf ((VA.State (AE.set dst a ae) am), None)
  | Bload NOTRAP chunk addr args _ dst iinfo =>
      binary_tree_leaf ((VA.State (AE.set dst Vtop ae) am), None)
  | Bstore chunk addr args _ src iinfo =>
      let am' := storev chunk am
                        (eval_static_addressing addr (aregs ae args))
                        (areg ae src) in
      binary_tree_leaf ((VA.State ae am'), None)
  | Bseq b1 b2 =>
      apply_on_outcomes (transfer_iblock rm b2)
                        (transfer_iblock rm b1 ae am)
  | Bcond cond args ifso ifnot iinfo =>
      match eval_static_condition cond (aregs ae args) with
      | Bnone => binary_tree_leaf (VA.Bot, None)
      | (Just b | Maybe b) =>
          transfer_iblock rm (if b then ifso else ifnot) ae am
      | Btop =>
          binary_tree_split
            (transfer_iblock rm ifso
              (transfer_condition cond args ae) am)
            (transfer_iblock rm ifnot
              (transfer_condition (negate_condition cond) args ae) am)
      end
  end.
      
Definition final_outcome := (exit * VA.t)%type.
Definition final_outcomes := binary_tree (list final_outcome).

Fixpoint mapi_rec (x : Z.t) {A B : Type} (f : Z.t -> A -> B) (l:list A) : list B :=
    match l with
      | nil => nil
      | a :: t => (f x a) :: (mapi_rec (Z.succ x) f t)
    end.

Definition mapi := @mapi_rec 0%Z.
Arguments mapi {A} {B}.

Lemma list_nth_z_mapi_rec:
  forall {A B : Type} l (f : Z -> A -> B) z z0 (x : A)
         (NTH : (list_nth_z l z) = Some x),
    list_nth_z (mapi_rec z0 f l) z = Some (f (z0 + z)%Z x).
Proof.
  induction l; cbn; intros.
  discriminate.
  destruct (zeq z 0) as [ZERO | OTHER].
  { subst z.
    rewrite Z.add_0_r.
    congruence.
  }
  rewrite IHl with (x := x) by assumption.
  f_equal. f_equal.
  lia.
Qed.

Lemma list_nth_z_mapi:
  forall {A B : Type} l (f : Z -> A -> B) z (x : A)
         (NTH : (list_nth_z l z) = Some x),
    list_nth_z (mapi f l) z = Some (f z x).
Proof.
  unfold mapi.
  intros.
  erewrite list_nth_z_mapi_rec by eassumption.
  rewrite Z.add_0_l.
  reflexivity.
Qed.

Definition transfer_final (rm: romem) (fi : final) (ae: aenv) (am: amem)
          : list final_outcome :=
  match fi with
  | Bgoto exit => (exit, VA.State ae am) :: nil
  | Breturn arg => nil
  | Bcall sig reg_ident args res exit =>
      (exit, (ValueAnalysis.transfer_call ae am args res)) :: nil
  | Btailcall sig reg_ident args => nil
  | Bbuiltin ef args res exit =>
      (exit, (ValueAnalysis.transfer_builtin ae am rm ef args res)) :: nil
  | Bjumptable arg exits =>
      mapi (fun i exit =>
      (exit, VA.State (AE.set arg (I (Int.repr i)) ae) am)) exits
  end.

Definition transfer_block (rm: romem) (ib : iblock) (ae: aenv) (am: amem):
  final_outcomes :=
  apply_at_leaves
    (fun (oc : outcome) =>
       let (s, ofi) := oc in
       binary_tree_leaf
         (match ofi with
          | Some fi =>
              match s with
              | VA.Bot => nil
              | VA.State ae am => transfer_final rm fi ae am
              end
          | None => nil
          end))
    (transfer_iblock rm ib ae am).

Lemma romatch_storev
     : forall (bc : block_classification) (chunk : memory_chunk) 
         (m : mem) (ptr v : val) 
         (m' : mem) (rm : romem),
       Mem.storev chunk m ptr v = Some m' ->
       romatch bc m rm -> romatch bc m' rm.
Proof.
  intros.
  unfold Mem.storev in *.
  destruct ptr; try discriminate.
  eapply romatch_store; eauto.
Qed.

Hint Resolve romatch_storev : va.

Fixpoint flatten_outcomes_rec
           { T : Type} (t : binary_tree (list T)) (already : list T) : list T :=
  match t with
  | binary_tree_leaf l => List.rev_append l already
  | binary_tree_split l r =>
      flatten_outcomes_rec l (flatten_outcomes_rec r already)
  end.

Lemma In_rev_append:
  forall {T} l1 l2 x, In (x : T) (List.rev_append l1 l2) <-> In x l1 \/ In x l2.
Proof.
  induction l1; intros; cbn.
  tauto.
  split; intro IN.
  { apply IHl1 in IN.
    cbn in IN.
    tauto.
  }
  pose proof (IHl1 (a :: l2) x) as Z.
  cbn in Z.
  tauto.
Qed.

Lemma apply_match_tree:
  forall {A B : Type} (P : A -> Prop) (Q : B -> Prop) (f : A -> binary_tree B)
         (MAP : forall x, P x -> match_tree Q (f x))
         tr
         (EX : match_tree P tr),
    match_tree Q (apply_at_leaves f tr).
Proof.
  induction tr; cbn; intros; intuition.
Qed.
    
Lemma In_binary_tree_flatten_outcomes_rec :
  forall { T : Type } 
         (t : binary_tree (list T))
         (already : list T)
         (x : T),
      In x (flatten_outcomes_rec t already) <->
         match_tree (In x) t \/ In x already.
Proof.
  induction t; cbn; intros.
  { apply In_rev_append. }
  rewrite IHt1.
  rewrite IHt2.
  tauto.
Qed.

Definition flatten_outcomes
           { T : Type} (t : binary_tree (list T)) :=
  flatten_outcomes_rec t nil.

Lemma In_binary_tree_flatten_outcomes :
  forall { T : Type }
         (t : binary_tree (list T))
         (x : T),
      In x (flatten_outcomes t) <->
         match_tree (In x) t.
Proof.
  intros.
  unfold flatten_outcomes.
  rewrite In_binary_tree_flatten_outcomes_rec.
  cbn.
  tauto.
Qed.

Definition flat_transfer_block rm ibi ae am :=
   flatten_outcomes (transfer_block rm ibi.(entry) ae am).

Definition transfer (rm: romem) (co : code) (n : positive) (va : VA.t) :
  list (positive * VA.t)%type :=
  match va with
  | VA.Bot => nil
  | VA.State ae am =>
      match PTree.get n co with
      | Some ibi => flat_transfer_block rm ibi ae am
      | None => nil
      end
  end.

Definition va_top := VA.State AE.top mtop.

Definition analyze (rm : romem) (f : function) :=
  let entry := VA.State (einit_regs f.(fn_params)) ValueAnalysis.mfunction_entry in
  match
    DS.solution_opt (transfer rm f.(fn_code))
                    ((f.(fn_entrypoint), entry)::nil) with
  | Some solution => solution
  | None => PMap.init va_top
  end.


Lemma transfer_strict:
   forall rm co n,
     (transfer rm co n VA.bot) = nil.
Proof.
  reflexivity.
Qed.

Lemma ge_va_top: forall s', VA.ge va_top s'.
Proof.
  unfold va_top, VA.ge.
  intros.
  destruct s'. constructor.
  split. { apply AE.ge_top. }
  intros.
  eapply mmatch_top'; eauto.
Qed.

Lemma analyze_successor:
  forall f n ae am ibi n' s' rm
  (ANALYZE : (analyze rm f)!!n = VA.State ae am)
  (AT : f.(fn_code)!n = Some ibi)
  (IN : In (n', s') (flat_transfer_block rm ibi ae am)),
  VA.ge (analyze rm f)!!n' s'.
Proof.
  unfold analyze; intros.
  set (entry := VA.State (einit_regs f.(fn_params)) ValueAnalysis.mfunction_entry) in *.
  destruct DS.solution_opt eqn:SOL; cycle 1.
  { rewrite PMap.gi.
    apply ge_va_top.
  }
  pose proof (DS.solution_stable _ (transfer_strict _ _) _ _ SOL) as STABLE.
  apply STABLE with (n := n).
  unfold transfer.
  rewrite ANALYZE.
  rewrite AT.
  assumption.
Qed.

Section LOCAL.
Variable bc: block_classification.
Variable ge: genv.
Variable GENV : genv_match bc ge.
Variable sp: block.
Hypothesis STACK: bc sp = BCstack.

Lemma analyze_succ:
  forall e m f n ae am ibi n' ae' am' rm
  (ANALYZE : (analyze rm f)!!n = VA.State ae am)
  (AT : f.(fn_code)!n = Some ibi)
  (IN : In (n', VA.State ae' am') (flat_transfer_block rm ibi ae am))
  (EMATCH : ematch bc e ae')
  (MMATCH : mmatch bc m am'),
  exists ae'' am'',
     (analyze rm f)!!n' = VA.State ae'' am''
  /\ ematch bc e ae''
  /\ mmatch bc m am''.
Proof.
  intros.
  pose proof (analyze_successor _ _ _ _ _ _ _ _ ANALYZE AT IN) as SUCC.
  unfold VA.ge in SUCC.
  destruct ((analyze rm f) # n') as [ | ae'' am''] eqn:ANALYZE'.
  contradiction.
  destruct SUCC as [ESUCC MSUCC].
  exists ae''. exists am''.
  split. reflexivity. split.
  - eapply ematch_ge; eauto.
  - eauto.
Qed.

Lemma ematch_restrict:
  forall bc e ae (av : aval) r
         (VMATCH : vmatch bc (e # r) av)
         (EMATCH : ematch bc e ae),
    ematch bc e (AE.set r av ae).
Proof.
  unfold ematch.
  intros.
  assert (ae <> AE.bot) as NOT_BOT.
  { intro BOT.
    pose proof (EMATCH r) as EMATCH_r.
    rewrite BOT in EMATCH_r.
    rewrite AE.get_bot in EMATCH_r.
    inv EMATCH_r.
  }
  assert ( ~ AVal.eq av AVal.bot) as NOT_BOT2.
  { intro BOT.
    rewrite BOT in VMATCH.
    inv VMATCH.
  }
  rewrite (AE.gsspec r r0 NOT_BOT NOT_BOT2).
  destruct (peq r0 r).
  { subst r0.
    assumption.
  }
  apply EMATCH.
Qed.

Lemma ematch_restrict_multiple:
  forall regs avals e ae 
         (ARG_MATCH : list_forall2 (vmatch bc) (e ## regs) avals)
         (EMATCH : ematch bc e ae),
    (ematch bc e (ae_set_multiple regs avals ae)).
Proof.
  induction regs; cbn; intros.
  assumption.
  inv ARG_MATCH.
  apply IHregs.
  assumption.
  apply ematch_restrict; assumption.
Qed.

Lemma transfer_condition_sound:
  forall cond regs e m ae
         (EMATCH : ematch bc e ae)
         (COND : eval_condition cond (e ## regs) m = Some true),
    (ematch bc e (transfer_condition cond regs ae)).
Proof.
  intros.
  unfold transfer_condition.
  apply ematch_restrict_multiple. 2: assumption.
  apply filter_static_condition_sound with (m := m). 2: assumption.
  apply ValueAnalysis.aregs_sound.
  assumption.
Qed.

Definition match_outcomes bc e m (ofi : option final) :=
  match_tree
    (fun l =>
       match l with
       | ((VA.State ae am), ofi') =>
           ematch bc e ae /\
             mmatch bc m am /\
             ofi = ofi'
       | (VA.Bot, _) => False
       end).

Definition In_tree {T : Type} (l0 : T) : binary_tree T -> Prop :=
  match_tree (fun l => l = l0).

Lemma match_outcomes_iff_exists:
  forall bc e m ofi tr,
    match_outcomes bc e m ofi tr <->
      exists ae am,
        In_tree ((VA.State ae am), ofi) tr /\ ematch bc e ae /\ mmatch bc m am.
Proof.
  induction tr; cbn.
  { destruct a as (v & ofi').
    destruct v.
    { split. tauto.
      intros (ae & am & EQ & EM & MM).
      congruence.
    }
    split.
    { intros (EM & MM & OFI).
      exists ae. exists am.
      subst.
      auto.
    }
    intros (ae' & am' & EQ & EM & MM).
    inv EQ.
    auto.
  }
  rewrite IHtr1. rewrite IHtr2.
  clear IHtr1. clear IHtr2.
  intuition trivial.
  - destruct H0 as (ae & am & IN & EM & MM).
    exists ae. exists am.
    tauto.
  - destruct H0 as (ae & am & IN & EM & MM).
    exists ae. exists am.
    tauto.
  - destruct H as (ae & am & IN & EM & MM).
    destruct IN.
    1: left.
    2: right.
    all: exists ae; exists am; tauto.
Qed.

Lemma match_apply_on_outcomes1 : forall
    bc oc1 f2 e1 m1 fi1
    (MATCH1 : match_outcomes bc e1 m1 (Some fi1) oc1),
    match_outcomes bc e1 m1 (Some fi1) (apply_on_outcomes f2 oc1).
Proof.
  induction oc1; cbn; intros.
  - destruct a as (s1, ofi1).
    destruct s1 as [ | ae1 am1].
    contradiction.
    destruct MATCH1 as (EMATCH1 & MMATCH1 & OFI1).
    subst ofi1.
    cbn. auto.
  - destruct MATCH1 as [MATCH1_LEFT | MATCH1_RIGHT].
    + left. apply IHoc1_1 with (e1 := e1) (m1 := m1); auto.
    + right. apply IHoc1_2 with (e1 := e1) (m1 := m1); auto.
Qed.

Lemma match_apply_on_outcomes2 : forall
    bc oc1 f2 e1 m1 e2 m2 ofi2
    (MATCH1 : match_outcomes bc e1 m1 None oc1)
    (MATCH2 :
      forall ae1 am1,
        ematch bc e1 ae1 -> mmatch bc m1 am1 ->
      match_outcomes bc e2 m2 ofi2 (f2 ae1 am1)),                    
    match_outcomes bc e2 m2 ofi2 (apply_on_outcomes f2 oc1).
Proof.
  induction oc1; cbn; intros.
  - destruct a as (s1, ofi1).
    destruct s1 as [ | ae1 am1].
    contradiction.
    destruct MATCH1 as (EMATCH1 & MMATCH1 & OFI1).
    subst ofi1.
    auto.
  - destruct MATCH1 as [MATCH1_LEFT | MATCH1_RIGHT].
    + left. apply IHoc1_1 with (e1 := e1) (m1 := m1); auto.
    + right. apply IHoc1_2 with (e1 := e1) (m1 := m1); auto.
Qed.

Lemma iblock_istep_romatch :
  forall ib rm e m e' m' ofi
    (ROMATCH : romatch bc m rm)
    (STEP : iblock_istep ge (Vptr sp Ptrofs.zero) None e m ib e' m' ofi),
    romatch bc m' rm.
Proof.
  induction ib; cbn; intros; inv STEP; eauto with va.
  destruct b; eauto with va.
Qed.
       
Lemma transfer_iblock_correct :
  forall ib rm ae am e m e' m' ofi
    (EMATCH : ematch bc e ae)
    (MMATCH : mmatch bc m am)
    (ROMATCH : romatch bc m rm)
    (STEP : iblock_istep ge  (Vptr sp Ptrofs.zero) None e m ib e' m' ofi),
    match_outcomes bc e' m' ofi (transfer_iblock rm ib ae am).
Proof.
  induction ib; cbn; intros; inv STEP.
  - auto.
  - auto.
  - (* Bop *)
    rename m' into m.
    split. 2: auto; fail.
    apply ematch_update; auto.
    apply (eval_static_operation_sound bc ge GENV sp STACK op (e ## args) m v (aregs ae args));
    eauto with va.
  - (* Bload *)
    destruct trap; cbn.
    + split. 2: auto; fail.
      rename m' into m.
      inv LOAD.
      apply ematch_update; auto.
      eapply loadv_sound; try eassumption.
      apply eval_static_addressing_sound with (ge := ge) (sp := sp) (vargs := (e ## args)); eauto with va.
    + split. 2: auto; fail.
      rename m' into m.
      inv LOAD.
      * apply ematch_update; auto.
        eapply vmatch_top.
        eapply loadv_sound; try eassumption.
        apply eval_static_addressing_sound with (ge := ge) (sp := sp) (vargs := (e ## args)); eauto with va.
      * apply ematch_update; auto.
        constructor.
  - (* Bstore *)
    split. auto; fail.
    split. 2: auto; fail.
    rename e' into e.
    eapply storev_sound; eauto.
    apply eval_static_addressing_sound with (ge := ge) (sp := sp) (vargs := (e ## args)); eauto with va.

  - (* Bseq: final on first part *)
    apply match_apply_on_outcomes1.
    apply IHib1 with (e := e) (m := m); auto.
    
  - (* Bseq *)
    apply match_apply_on_outcomes2 with (e1 := rs1) (m1 := m1).
    + apply IHib1 with (e := e) (m := m); auto.
    + intros.
      apply IHib2 with (e := rs1) (m := m1); auto.
      eapply iblock_istep_romatch; eassumption.
  - (* Bcond *)
    assert (list_forall2 (vmatch bc) e ## args (aregs ae args)) as ARGS by auto with va.
    pose proof (eval_static_condition_sound bc cond (e ## args) m (aregs ae args) ARGS) as COND.
    destruct (eval_static_condition cond (aregs ae args)); inv COND.
    + congruence.
    + replace b0 with b by congruence.
      destruct b.
      * apply IHib1 with (e := e) (m := m); auto.
      * apply IHib2 with (e := e) (m := m); auto.
    + congruence.
    + replace b0 with b by congruence.
      destruct b.
      * apply IHib1 with (e := e) (m := m); auto.
      * apply IHib2 with (e := e) (m := m); auto.
    + cbn.
      destruct b.
      * left.
        apply IHib1 with (e := e) (m := m).
        apply transfer_condition_sound with (m := m).
        all: assumption.
      * right.
        assert (eval_condition (negate_condition cond) e ## args m = Some true) as COND'.
        { rewrite eval_negate_condition.
          rewrite EVAL.
          reflexivity.
        }
        apply IHib2 with (e := e) (m := m).
        apply transfer_condition_sound with (m := m).
        all: assumption.
Qed.

Definition transfer_final_nobuiltin_correct:
  forall fi stack f rm ae am e m _bc0 _bc1 tr pc' e' m'
    (EMATCH : ematch bc e ae)
    (MMATCH : mmatch bc m am)
    (ROMATCH : romatch bc m rm)
    (STEP : final_step ge stack f (Vptr sp Ptrofs.zero) e m _bc0 fi tr
                       (State stack f (Vptr sp Ptrofs.zero) pc' e' m' _bc1))
    (NOBUILTIN: match fi with
             | Bbuiltin _ _ _ _ => False
             | _ => True
             end),
    exists ae' am',
    In (pc', VA.State ae' am') (transfer_final rm fi ae am) /\
    ematch bc e' ae' /\
      mmatch bc m' am'.
Proof.
  destruct fi; intros; cbn; inv STEP.
  - (* goto *)
    exists ae. exists am. auto.
  - (* builtin *)
    contradiction.
  - (* jumptable *)
    eexists. exists am.
    split.
    { apply list_nth_z_in with (n := Int.unsigned n).
      erewrite list_nth_z_mapi by eassumption.
      reflexivity.
    }
    split. 2: assumption.
    apply ematch_restrict. 2: assumption.
    rewrite H3.
    rewrite Int.repr_unsigned.
    constructor.
Qed.
End LOCAL.

Section SOUNDNESS.

Variable prog: program.
Variable ge: genv.

Let rm := ValueAnalysis.romem_for prog.

(** Properties of the dataflow solution. *)

Lemma analyze_entrypoint:
  forall rm f vl m bc,
  (forall v, In v vl -> vmatch bc v (Ifptr Nonstack)) ->
  mmatch bc m ValueAnalysis.mfunction_entry ->
  exists ae am,
     (analyze rm f)!!(fn_entrypoint f) = VA.State ae am
  /\ ematch bc (RTL.init_regs vl (fn_params f)) ae
  /\ mmatch bc m am.
Proof.
  intros.
  unfold analyze.
  set (entry := VA.State (einit_regs f.(fn_params)) ValueAnalysis.mfunction_entry).
  destruct DS.solution_opt as [res|] eqn:FIX.
  - assert (A: VA.ge res!!(fn_entrypoint f) entry).
    { eapply DS.solution_includes_initial. eassumption.
      constructor. reflexivity.
    }
    destruct (res!!(fn_entrypoint f)) as [ | ae am ]; cbn in A.
    contradiction.
    destruct A as [A1 A2].
    exists ae, am.
    split. auto.
    split. eapply ematch_ge; eauto. apply ematch_init; auto.
    auto.
  - exists AE.top, mtop.
    split. apply PMap.gi.
    split. apply ematch_ge with (einit_regs (fn_params f)).
    apply ematch_init; auto. apply AE.ge_top.
    eapply mmatch_top'; eauto.
Qed.

(** * Soundness proof *)

(* This comes from ValueAnalysis.v but we have different types, this seems difficult to factor *)

Inductive sound_stack: block_classification -> list stackframe -> mem -> block -> Prop :=
  | sound_stack_nil: forall bc m bound,
      sound_stack bc nil m bound
  | sound_stack_public_call:
      forall (bc: block_classification) res f sp pc e stk m bound bc' bound' ae
        (STK: sound_stack bc' stk m sp)
        (INCR: Ple bound' bound)
        (BELOW: bc_below bc' bound')
        (SP: bc sp = BCother)
        (SP': bc' sp = BCstack)
        (SAME: forall b, Plt b bound' -> b <> sp -> bc b = bc' b)
        (GE: genv_match bc' ge)
        (AN: VA.ge (analyze rm f)!!pc (VA.State (AE.set res Vtop ae) ValueAnalysis.mafter_public_call))
        (EM: ematch bc' e ae),
      sound_stack bc (Stackframe res f (Vptr sp Ptrofs.zero) pc e :: stk) m bound
  | sound_stack_private_call:
     forall (bc: block_classification) res f sp pc e stk m bound bc' bound' ae am
        (STK: sound_stack bc' stk m sp)
        (INCR: Ple bound' bound)
        (BELOW: bc_below bc' bound')
        (SP: bc sp = BCinvalid)
        (SP': bc' sp = BCstack)
        (SAME: forall b, Plt b bound' -> b <> sp -> bc b = bc' b)
        (GE: genv_match bc' ge)
        (AN: VA.ge (analyze rm f)!!pc (VA.State (AE.set res (Ifptr Nonstack) ae) (ValueAnalysis.mafter_private_call am)))
        (EM: ematch bc' e ae)
        (CONTENTS: bmatch bc' m sp am.(am_stack)),
      sound_stack bc (Stackframe res f (Vptr sp Ptrofs.zero) pc e :: stk) m bound.

Inductive sound_state_base (bc : block_classification) : state -> Prop :=
  | sound_regular_state:
      forall s f sp pc e m _bc ae am
        (STK: sound_stack bc s m sp)
        (AN: (analyze rm f)!!pc = VA.State ae am)
        (EM: ematch bc e ae)
        (RO: romatch bc m rm)
        (MM: mmatch bc m am)
        (GE: genv_match bc ge)
        (SP: bc sp = BCstack),
      sound_state_base bc (State s f (Vptr sp Ptrofs.zero) pc e m _bc)
  | sound_call_state:
      forall s fd args m _bc
        (STK: sound_stack bc s m (Mem.nextblock m))
        (ARGS: forall v, In v args -> vmatch bc v Vtop)
        (RO: romatch bc m rm)
        (MM: mmatch bc m mtop)
        (GE: genv_match bc ge)
        (NOSTK: ValueAnalysis.bc_nostack bc),
      sound_state_base bc (Callstate s fd args m _bc)
  | sound_return_state:
      forall s v m _bc
        (STK: sound_stack bc s m (Mem.nextblock m))
        (RES: vmatch bc v Vtop)
        (RO: romatch bc m rm)
        (MM: mmatch bc m mtop)
        (GE: genv_match bc ge)
        (NOSTK: ValueAnalysis.bc_nostack bc),
      sound_state_base bc (Returnstate s v m _bc).
(** Properties of the [sound_stack] invariant on call stacks. *)

Lemma sound_stack_ext:
  forall m' bc stk m bound,
  sound_stack bc stk m bound ->
  (forall b ofs n bytes,
       Plt b bound -> bc b = BCinvalid -> n >= 0 ->
       Mem.loadbytes m' b ofs n = Some bytes ->
       Mem.loadbytes m b ofs n = Some bytes) ->
  sound_stack bc stk m' bound.
Proof.
  induction 1; intros INV.
- constructor.
- assert (Plt sp bound') by eauto with va.
  eapply sound_stack_public_call; eauto. apply IHsound_stack; intros.
  apply INV. extlia. rewrite SAME; auto with ordered_type. extlia. auto. auto.
- assert (Plt sp bound') by eauto with va.
  eapply sound_stack_private_call; eauto. apply IHsound_stack; intros.
  apply INV. extlia. rewrite SAME; auto with ordered_type. extlia. auto. auto.
  apply bmatch_ext with m; auto. intros. apply INV. extlia. auto. auto. auto.
Qed.

Lemma sound_stack_inv:
  forall m' bc stk m bound,
  sound_stack bc stk m bound ->
  (forall b ofs n, Plt b bound -> bc b = BCinvalid -> n >= 0 -> Mem.loadbytes m' b ofs n = Mem.loadbytes m b ofs n) ->
  sound_stack bc stk m' bound.
Proof.
  intros. eapply sound_stack_ext; eauto. intros. rewrite <- H0; auto.
Qed.

Lemma match_not_invalid:
  forall bc b ofs x,
    (vmatch bc (Vptr b ofs) x) -> bc b <> BCinvalid.
Proof.
  intros.
  assert (A: pmatch bc b ofs Ptop).
  { inv H; eapply pmatch_top'; eauto. }
  inv A.
  assumption.
Qed.  

Lemma sound_stack_store:
  forall chunk m b ofs v m' (bc : block_classification) stk bound
    (BC : (bc b) <> BCinvalid)
    (STORE : Mem.store chunk m b ofs v = Some m'),
  sound_stack bc stk m bound ->
  sound_stack bc stk m' bound.
Proof.
  intros.
  intros. apply sound_stack_inv with m; auto.
  intros.
  eapply Mem.loadbytes_store_other. eassumption.
  left.
  congruence.
Qed.

Lemma sound_stack_storev:
  forall chunk m addr v m' bc aaddr stk bound,
  Mem.storev chunk m addr v = Some m' ->
  vmatch bc addr aaddr ->
  sound_stack bc stk m bound ->
  sound_stack bc stk m' bound.
Proof.
  intros. destruct addr; simpl in H; try discriminate.
  eapply sound_stack_store.
  { eapply match_not_invalid. exact H0. }
  exact H. assumption.
Qed.

Lemma sound_stack_storebytes:
  forall m b ofs bytes m' bc aaddr stk bound,
  Mem.storebytes m b (Ptrofs.unsigned ofs) bytes = Some m' ->
  vmatch bc (Vptr b ofs) aaddr ->
  sound_stack bc stk m bound ->
  sound_stack bc stk m' bound.
Proof.
  intros. apply sound_stack_inv with m; auto.
  assert (A: pmatch bc b ofs Ptop).
  { inv H0; eapply pmatch_top'; eauto. }
  inv A.
  intros. eapply Mem.loadbytes_storebytes_other; eauto. left; congruence.
Qed.

Lemma sound_stack_free:
  forall m b lo hi m' bc stk bound,
  Mem.free m b lo hi = Some m' ->
  sound_stack bc stk m bound ->
  sound_stack bc stk m' bound.
Proof.
  intros. eapply sound_stack_ext; eauto. intros.
  eapply Mem.loadbytes_free_2; eauto.
Qed.

Lemma sound_stack_new_bound:
  forall bc stk m bound bound',
  sound_stack bc stk m bound ->
  Ple bound bound' ->
  sound_stack bc stk m bound'.
Proof.
  intros. inv H.
- constructor.
- eapply sound_stack_public_call with (bound' := bound'0); eauto. extlia.
- eapply sound_stack_private_call with (bound' := bound'0); eauto. extlia.
Qed.

Lemma sound_stack_exten:
  forall bc stk m bound (bc1: block_classification),
  sound_stack bc stk m bound ->
  (forall b, Plt b bound -> bc1 b = bc b) ->
  sound_stack bc1 stk m bound.
Proof.
  intros. inv H.
- constructor.
- assert (Plt sp bound') by eauto with va.
  eapply sound_stack_public_call; eauto.
  rewrite H0; auto. extlia.
  intros. rewrite H0; auto. extlia.
- assert (Plt sp bound') by eauto with va.
  eapply sound_stack_private_call; eauto.
  rewrite H0; auto. extlia.
  intros. rewrite H0; auto. extlia.
Qed.

Lemma sound_stack_istep:
  forall bc (GE : genv_match bc ge) ib  fin stack ae am rs m rs' m' sp
         (EM : ematch bc rs ae)
         (MM : mmatch bc m am)
         (RM: romatch bc m rm)
         (SOUND : sound_stack bc stack m sp)
         (ISTEP : iblock_istep ge (Vptr sp Ptrofs.zero) None rs m 
                               ib rs' m' fin)
         (BC : bc sp = BCstack),
    sound_stack bc stack m' sp.
Proof.
  induction ib; intros; inv ISTEP; trivial.
  - exploit eval_static_addressing_sound; eauto with va.
    intro VM.
    eapply sound_stack_storev; eauto.
  - eapply IHib1 in SOUND; eassumption.
  - exploit IHib1; eauto.
    intro.
    exploit (transfer_iblock_correct bc ge GE sp BC ib1); eauto.
    intro OUTCOMES.
    rewrite match_outcomes_iff_exists in OUTCOMES.
    destruct OUTCOMES as (ae1 & am1 & IN & EM1 & MM1).
    exploit (IHib2 fin stack ae1 am1); eauto.
    apply (iblock_istep_romatch bc ge sp ib1 rm rs m rs1 m1 None RM EXEC1).
  - destruct b; eauto.
Qed.

Lemma sound_succ_state0:
  forall bc pc ae am ibi ae' am'  s f sp pc' e' m' _bc
  (ANALYZE : (analyze rm f)!!pc = VA.State ae am)
  (AT : f.(fn_code)!pc = Some ibi)
  (IN : In (pc', VA.State ae' am') (flat_transfer_block rm ibi ae am))
  (EMATCH : ematch bc e' ae')
  (MMATCH : mmatch bc m' am')
  (ROMATCH : romatch bc m' rm)
  (GENV : genv_match bc ge)
  (BC : bc sp = BCstack)
  (SOUND : sound_stack bc s m' sp),
  sound_state_base bc (State s f (Vptr sp Ptrofs.zero) pc' e' m' _bc).
Proof.
  intros. exploit analyze_succ; eauto. intros (ae'' & am'' & AN & EM & MM).
  econstructor; eauto.
Qed.

Lemma sound_succ_state:
  forall bc pc ae am ibi ae' am'  s f sp pc' e' m' _bc
  (ANALYZE : (analyze rm f)!!pc = VA.State ae am)
  (AT : f.(fn_code)!pc = Some ibi)
  (IN : In (pc', VA.State ae' am') (flat_transfer_block rm ibi ae am))
  (EMATCH : ematch bc e' ae')
  (MMATCH : mmatch bc m' am')
  (ROMATCH : romatch bc m' rm)
  (GENV : genv_match bc ge)
  (BC : bc sp = BCstack)
  (SOUND : sound_stack bc s m' sp),
  exists bc, sound_state_base bc (State s f (Vptr sp Ptrofs.zero) pc' e' m' _bc).
Proof.
  intros. exists bc; eapply sound_succ_state0; eassumption.
Qed.

Lemma propagate_to_final: forall ib fin ae am rm ae' am' x
    (MID : match_tree      
          (fun l : VA.t' * option final => l = (VA.State ae' am', Some fin))
          (transfer_iblock rm (entry ib) ae am))
    (END : In x (transfer_final rm fin ae' am')),
    In x (flat_transfer_block rm ib ae am).
Proof.
  intros.
  unfold flat_transfer_block.
  rewrite In_binary_tree_flatten_outcomes.
  unfold transfer_block.
  apply apply_match_tree with (P := (fun l : VA.t' * option final => l = (VA.State ae' am', Some fin))).
  intros.
  subst. exact END.
  assumption.
Qed.

Definition final_preserv_bc (f : final) : bool :=
  match f with
  | Bgoto _ | Bjumptable _ _ => true
  | _ => false
  end.

Lemma sound_step_base_exec_iblock stack f sp pc rs m _bc trace bc ib rs' m' fin st'
  (SOUND: sound_state_base bc (State stack f sp pc rs m _bc))
  (PC: (fn_code f) ! pc = Some ib)
  (ISTEP: iblock_istep ge sp None rs m (entry ib) rs' m' (Some fin))
  (FINAL: final_step ge stack f sp rs' m' _bc fin trace st'):
  if final_preserv_bc fin
  then sound_state_base bc st'
  else exists bc', sound_state_base bc' st'.
Proof.
  inv SOUND.
  pose proof (transfer_iblock_correct bc ge GE sp0 SP (entry ib) rm ae am rs m rs' m' (Some fin) EM MM RO ISTEP)
          as MATCH_ISTEP.
  rewrite match_outcomes_iff_exists in MATCH_ISTEP.
  destruct MATCH_ISTEP as (ae' & am' & IN' & EM' & MM').
  unfold In_tree in IN'.
  pose proof (iblock_istep_romatch bc ge sp0 (entry ib) rm rs m rs' m' (Some fin) RO ISTEP) as RO'.
  inversion FINAL; simpl; try rename bc' into _bc'.
  all: try assert (match fin with
            | Bbuiltin _ _ _ _ => False
                   | _ => True
                   end) as NOBUILTIN by (subst fin; trivial).
  + (* goto *)
    subst st'; destruct (transfer_final_nobuiltin_correct bc ge sp0 fin stack f rm ae' am' rs' m' _ _ trace _ rs' m' EM' MM' RO' FINAL NOBUILTIN) as (ae'' & am'' & IN'' & EM'' & AM'').
    apply propagate_to_final with (ib := ib) (ae := ae) (am := am) in IN''.
    2: assumption.
    eapply sound_succ_state0; eauto.
    apply sound_stack_istep with (ib := entry ib) (fin := Some fin) (ae := ae) (am := am) (rs := rs) (m := m) (rs' := rs'); auto.

  + (* return *)
    inv H.
    exploit (ValueAnalysis.anonymize_stack ge); eauto.
    intros (bc' & A & B & C & D & E & F & G).
    exists bc'.
    apply sound_return_state; auto.
    erewrite Mem.nextblock_free by eauto.
    apply sound_stack_new_bound with stk.
    apply sound_stack_exten with bc.
    eapply sound_stack_free; eauto.
    apply sound_stack_istep with (ib := entry ib) (fin := Some (Breturn or)) (ae := ae) (am := am) (rs := rs) (m := m) (rs' := rs'); auto.

    intros. apply C. apply Plt_ne; auto.
    apply Plt_Ple. eapply mmatch_below; eauto with va.
    destruct or; simpl. eapply D; eauto. constructor.
    eapply romatch_free; eauto.
    eapply mmatch_free; eauto.
  
  + (* call *)
    subst.
    eapply propagate_to_final with (ib := ib) (ae := ae) (am := am) in IN'; cycle 1.
    { cbn. left. reflexivity. }
    subst.
    destruct (pincl (am_nonstack am') Nonstack &&
                forallb (fun av => vpincl av Nonstack) (aregs ae' args)) eqn:NOLEAK.
    * (* private call *)
      unfold ValueAnalysis.transfer_call, ValueAnalysis.analyze_call in IN'.
      rewrite NOLEAK in IN'.
      InvBooleans.
      exploit analyze_successor; eauto.
      intro SUCC.
      exploit (ValueAnalysis.hide_stack ge); eauto.
      { apply pincl_ge; auto. }
     intros (bc' & A & B & C & D & E & F & G).
     exists bc'; apply sound_call_state; auto.
      ++ eapply sound_stack_private_call with
           (bound' := Mem.nextblock m') (bc' := bc); eauto.
        ** apply sound_stack_istep with (ib := entry ib) (fin := Some (Bcall (funsig fd) ros args res pc')) (ae := ae) (am := am) (rs := rs) (m := m) (rs' := rs'); auto.
        ** apply Ple_refl.
        ** eapply mmatch_below; eauto.
        ** eapply mmatch_stack; eauto.
      ++ intros.
         exploit list_in_map_inv; eauto.
         intros (r & P & Q). subst v.
         apply D with (areg ae' r).
         apply vpincl_ge.
         rewrite forallb_forall in H1.
         apply H1. apply in_map. auto.
         auto with va.
    * (* public call *)
      unfold ValueAnalysis.transfer_call, ValueAnalysis.analyze_call in IN'.
      rewrite NOLEAK in IN'.
      InvBooleans.
      exploit analyze_successor; eauto.
      intro SUCC.
      exploit (ValueAnalysis.anonymize_stack ge); eauto.
      intros (bc' & A & B & C & D & E & F & G).
      exists bc'; apply sound_call_state; auto.
      ++ eapply sound_stack_public_call with (bound' := Mem.nextblock m') (bc' := bc); eauto.
         ** apply sound_stack_istep with (ib := entry ib) (fin := Some (Bcall (funsig fd) ros args res pc')) (ae := ae) (am := am) (rs := rs) (m := m) (rs' := rs'); auto.
         ** apply Ple_refl.
         ** eapply mmatch_below; eauto.
      ++ intros.
         exploit list_in_map_inv; eauto.
         intros (r & P & Q).
         subst v.
         apply D with (areg ae' r).
         auto with va.

  + (* tailcall *)
    inv H1.
    exploit (ValueAnalysis.anonymize_stack ge); eauto.
    intros (bc' & A & B & C & D & E & F & G).
    exists bc'; apply sound_call_state; auto.
    { erewrite Mem.nextblock_free by eauto.
      apply sound_stack_new_bound with stk.
      apply sound_stack_exten with bc.
      eapply sound_stack_free; eauto.
      {
        apply sound_stack_istep with (ib := entry ib) (fin := Some (Btailcall (funsig fd) ros args)) (ae := ae) (am := am) (rs := rs) (m := m) (rs' := rs'); auto.
      }
      intros. apply C. apply Plt_ne; auto.
      apply Plt_Ple. eapply mmatch_below; eauto. congruence.
    }
    intros. exploit list_in_map_inv; eauto. intros (r & P & Q). subst v.
    { apply D with (areg ae' r). auto with va. }
    { eapply romatch_free; eauto. }
    eapply mmatch_free; eauto.

  + (* builtin *)
    subst.
    assert (SPVALID: Plt sp0 (Mem.nextblock m')) by (eapply mmatch_below; eauto with va).
    eapply propagate_to_final with (ib := ib) (ae := ae) (am := am) in IN'; cycle 1.
    { cbn. left. reflexivity. }
    subst.
    set (aargs := map (ValueAnalysis.abuiltin_arg ae' am' rm) args) in *.
    assert (ARGS: list_forall2 (vmatch bc) vargs aargs) by (eapply ValueAnalysis.abuiltin_args_sound; eauto).
    destruct (pincl (am_nonstack am') Nonstack &&
                forallb (fun av => vpincl av Nonstack) aargs)
             eqn: NOLEAK.
    * (* private builtin call *)
       exploit (ValueAnalysis.hide_stack ge); eauto.
       { InvBooleans. rewrite forallb_forall in H2. apply pincl_ge; auto. }
       intros (bc1 & A & B & C & D & E & F & G).
       
       exploit (ValueAnalysis.external_call_match ge); eauto.
       { InvBooleans. rewrite forallb_forall in H2.
         intros. exploit list_forall2_in_left; eauto.
         intros (av & U & V).
         eapply D; eauto with va.
         apply vpincl_ge.
         apply H2; auto.
       }
       intros (bc2 & J & K & L & M & N & O & P & Q).
       
       exploit (ValueAnalysis.return_from_private_call ge bc bc2); eauto.
       { eapply mmatch_below; eauto. }
       { rewrite K; auto. }
       { intros. rewrite K; auto. rewrite C; auto. }
       { apply bmatch_inv with m'. eapply mmatch_stack; eauto.
         intros. apply Q; auto.
       }
       { eapply external_call_nextblock; eauto. }
       intros (bc3 & U & V & W & X & Y & Z & AA).
       
       destruct ef.

      all: unfold ValueAnalysis.transfer_builtin, ValueAnalysis.transfer_builtin_default, ValueAnalysis.analyze_call in IN'; fold aargs in IN';
        try rewrite NOLEAK in IN'.

      ++  eapply sound_succ_state with (bc := bc3); eauto.
         { apply ValueAnalysis.set_builtin_res_sound; eauto. }
         { apply sound_stack_exten with bc.
           apply sound_stack_inv with m'.
           { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
           intros. apply Q. red. eapply Plt_trans; eauto.
           rewrite C; auto with ordered_type.
           exact AA.
         }
      ++ (* builtin function *)
        destruct (lookup_builtin_function name sg) as [bf|] eqn:LK; auto.
        destruct (ValueAnalysis.eval_static_builtin_function ae' am' rm bf args) as [av|] eqn:ES; auto.
        { cbn in H0. red in H0. rewrite LK in H0. inv H0.
          eapply sound_succ_state; eauto. simpl; auto.
          apply ValueAnalysis.set_builtin_res_sound; auto.
          eapply ValueAnalysis.eval_static_builtin_function_sound; eauto.
          eapply sound_stack_istep with (m := m) (rs := rs); eauto.
        }
        { cbn in H0. red in H0. rewrite LK in H0. inv H0.
          eapply sound_succ_state; eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. }
          apply sound_stack_exten with (bc := bc).
          { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          assumption.
        }
        eapply sound_succ_state with (bc := bc3); eauto.
        { apply ValueAnalysis.set_builtin_res_sound; auto. }
        apply sound_stack_exten with bc.
        { apply sound_stack_inv with m'.
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          intros. apply Q.
          { red. eapply Plt_trans; eauto. }
          rewrite C. assumption.
          auto with ordered_type.
        }
        exact AA.
      ++ (* runtime *)
        eapply sound_succ_state with (bc := bc3); eauto.
         { apply ValueAnalysis.set_builtin_res_sound; eauto. }
         { apply sound_stack_exten with bc.
           apply sound_stack_inv with m'.
           { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
           intros. apply Q. red. eapply Plt_trans; eauto.
           rewrite C; auto with ordered_type.
           exact AA.
         }
      ++ (* volatile load *)
        inv H0. destruct args as [ | addr l]; subst aargs; inv ARGS.
        destruct l; cycle 1.
        { cbn in H6. inv H6. }
        clear H6.
        eapply sound_succ_state; eauto; cycle 1.
        { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
        apply ValueAnalysis.set_builtin_res_sound; auto.
        inv H1.
        ** (* true volatile access *)
          assert (VV: vmatch bc v (Ifptr Glob)).
          { inv H3; simpl in *; constructor.
            econstructor. eapply GE; eauto. }
          destruct (va_strict tt). apply vmatch_lub_r. apply vnormalize_sound. auto.
          apply vnormalize_sound. eapply vmatch_ge; eauto. constructor. constructor.
        ** (* normal memory access *)
          pose proof (loadv_sound bc chunk m'0 (Vptr b ofs) _ rm am' _ H2 RO' MM' H4) as LOAD.
          destruct (va_strict tt).
          { apply vmatch_lub_l. auto. }
          { eapply vnormalize_cast; eauto. eapply vmatch_top; eauto. }
          
      ++ (* volatile store *)
        inv H0. inv H. inv H5. inv H6.
        exploit (ValueAnalysis.abuiltin_arg_sound ge).
        exact EM'. exact RO'. exact MM'. assumption. exact SP. exact H4.
        intro MATCH_v.
        exploit (ValueAnalysis.abuiltin_arg_sound ge).
        exact EM'. exact RO'. exact MM'. assumption. exact SP. exact H3.
        intro MATCH_addr.

        inv H1.
        ** (* true volatile access *)
          eapply sound_succ_state with (bc := bc); eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. constructor. }
          apply mmatch_lub_l; auto.
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }        
        ** (* normal memory access *)
          eapply sound_succ_state with (bc := bc); eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. constructor. }
          { apply mmatch_lub_r.
            eapply storev_sound; eauto. auto. }
          { eapply romatch_store; eauto. }
          eapply sound_stack_store; eauto.
          { eapply match_not_invalid. exact MATCH_v. }
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          
      ++ (* malloc *)
        inv H0.
        eapply sound_succ_state with (bc := bc3); eauto.
        { apply ValueAnalysis.set_builtin_res_sound; auto. }
        apply sound_stack_exten with bc.
        { apply sound_stack_inv with m'.
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          intros. apply Q.
          { red. eapply Plt_trans; eauto. }
          rewrite C. assumption.
          auto with ordered_type.
        }
        exact AA.
        
      ++ (* free *)
        inv H0.
        { eapply sound_succ_state; eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. }
          { eapply sound_stack_free.
            eassumption.
            apply sound_stack_exten with (bc := bc). 2: assumption.
            eapply sound_stack_istep with (m := m) (rs := rs); eauto.
          }
        }
        { eapply sound_succ_state; eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. }
          { apply sound_stack_exten with (bc := bc). 2: assumption.
            eapply sound_stack_istep with (m := m) (rs := rs); eauto.
          }
        }
        
      ++ (* memcpy *)
        inv H0. inv H. inv H12. inv H13.
         exploit (ValueAnalysis.abuiltin_arg_sound ge).
        exact EM'. exact RO'. exact MM'. assumption. eassumption.
        exact H11.
        intro MATCH1.
        exploit (ValueAnalysis.abuiltin_arg_sound ge).
        exact EM'. exact RO'. exact MM'. assumption. eassumption.
        exact H10.
        intro MATCH2.
        eapply sound_succ_state with (bc := bc); eauto.
        { apply ValueAnalysis.set_builtin_res_sound; eauto.
          constructor. }
        { eapply storebytes_sound; eauto.
          apply match_aptr_of_aval; auto.
          eapply Mem.loadbytes_length; eauto.
          intros. eapply loadbytes_sound; eauto.
          apply match_aptr_of_aval; auto.
        }
        eapply romatch_storebytes; eauto.
        eapply sound_stack_storebytes; eauto.
        eapply sound_stack_istep with (m := m) (rs := rs); eauto.
      ++ (* annot *)
        inv H0.
        eapply sound_succ_state; eauto; cycle 1.
        { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
        apply ValueAnalysis.set_builtin_res_sound; auto. constructor.
      ++ (* annot_val *)
        inv H0.
        destruct args.
        { eapply sound_succ_state; eauto; cycle 1.
          { apply sound_stack_exten with (bc := bc). 2: assumption.
            eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          apply ValueAnalysis.set_builtin_res_sound; auto.
        }
        destruct args; cycle 1.
        { eapply sound_succ_state; eauto; cycle 1.
          { apply sound_stack_exten with (bc := bc). 2: assumption.
            eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          apply ValueAnalysis.set_builtin_res_sound; auto.
        }
        { eapply sound_succ_state; eauto; cycle 1.
          { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          inv ARGS.
          apply ValueAnalysis.set_builtin_res_sound; auto.
        }
      ++ (* inline_asm *)
         eapply sound_succ_state with (bc := bc3); eauto.
        { apply ValueAnalysis.set_builtin_res_sound; auto. }
        apply sound_stack_exten with bc.
        { apply sound_stack_inv with m'.
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          intros. apply Q.
          { red. eapply Plt_trans; eauto. }
          rewrite C. assumption.
          auto with ordered_type.
        }
        exact AA.

      ++ (* debug *)
        inv H0.
        eapply sound_succ_state; eauto; cycle 1.
        { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
        apply ValueAnalysis.set_builtin_res_sound; auto. constructor.
        
      ++ (* profiling *)
        inv H0.
        eapply sound_succ_state; eauto; cycle 1.
        { apply sound_stack_exten with (bc := bc). 2: assumption.
          eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
        apply ValueAnalysis.set_builtin_res_sound; auto.

    * (* public builtin call *)
      exploit (ValueAnalysis.anonymize_stack ge); eauto.
      intros (bc1 & A & B & C & D & E & F & G).
      exploit (ValueAnalysis.external_call_match ge); eauto.
      { intros.
        exploit list_forall2_in_left; eauto.
        intros (av & U & V). eapply D; eauto with va.
      }
      intros (bc2 & J & K & L & M & N & O & P & Q).
      exploit (ValueAnalysis.return_from_public_call ge bc bc2); eauto.
      { eapply mmatch_below; eauto. }
      { rewrite K; auto. }
      { intros. rewrite K; auto. rewrite C; auto. }
      { eapply external_call_nextblock; eauto. }
      intros (bc3 & U & V & W & X & Y & Z & AA).

      destruct ef.
      all: unfold ValueAnalysis.transfer_builtin, ValueAnalysis.transfer_builtin_default, ValueAnalysis.analyze_call in IN'; fold aargs in IN';
        try rewrite NOLEAK in IN'.

      ++ eapply sound_succ_state with (bc := bc3); eauto.
         { apply ValueAnalysis.set_builtin_res_sound; eauto. }
         { apply sound_stack_exten with bc.
           apply sound_stack_inv with m'.
           { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
           intros. apply Q. red. eapply Plt_trans; eauto.
           rewrite C; auto with ordered_type.
           exact AA.
         }
      ++ (* builtin function *)
        destruct (lookup_builtin_function name sg) as [bf|] eqn:LK; auto.
        destruct (ValueAnalysis.eval_static_builtin_function ae' am' rm bf args) as [av|] eqn:ES; auto.
        { cbn in H0. red in H0. rewrite LK in H0. inv H0.
          eapply sound_succ_state; eauto. simpl; auto.
          apply ValueAnalysis.set_builtin_res_sound; auto.
          eapply ValueAnalysis.eval_static_builtin_function_sound; eauto.
          eapply sound_stack_istep with (m := m) (rs := rs); eauto.
        }
        { cbn in H0. red in H0. rewrite LK in H0. inv H0.
          eapply sound_succ_state; eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. }
          apply sound_stack_exten with (bc := bc).
          { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          assumption.
        }
        eapply sound_succ_state with (bc := bc3); eauto.
        { apply ValueAnalysis.set_builtin_res_sound; auto. }
        apply sound_stack_exten with bc.
        { apply sound_stack_inv with m'.
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          intros. apply Q.
          { red. eapply Plt_trans; eauto. }
          rewrite C. assumption.
          auto with ordered_type.
        }
        exact AA.
      ++ (* runtime *)
        eapply sound_succ_state with (bc := bc3); eauto.
         { apply ValueAnalysis.set_builtin_res_sound; eauto. }
         { apply sound_stack_exten with bc.
           apply sound_stack_inv with m'.
           { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
           intros. apply Q. red. eapply Plt_trans; eauto.
           rewrite C; auto with ordered_type.
           exact AA.
         }
      ++ (* volatile load *)
        inv H0. destruct args as [ | addr l]; subst aargs; inv ARGS.
        destruct l; cycle 1.
        { cbn in H6. inv H6. }
        clear H6.
        eapply sound_succ_state; eauto; cycle 1.
        { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
        apply ValueAnalysis.set_builtin_res_sound; auto.
        inv H1.
        ** (* true volatile access *)
          assert (VV: vmatch bc v (Ifptr Glob)).
          { inv H3; simpl in *; constructor.
            econstructor. eapply GE; eauto. }
          destruct (va_strict tt). apply vmatch_lub_r. apply vnormalize_sound. auto.
          apply vnormalize_sound. eapply vmatch_ge; eauto. constructor. constructor.
        ** (* normal memory access *)
          pose proof (loadv_sound bc chunk m'0 (Vptr b ofs) _ rm am' _ H2 RO' MM' H4) as LOAD.
          destruct (va_strict tt).
          { apply vmatch_lub_l. auto. }
          { eapply vnormalize_cast; eauto. eapply vmatch_top; eauto. }
          
      ++ (* volatile store *)
        inv H0. inv H. inv H5. inv H6.
        exploit (ValueAnalysis.abuiltin_arg_sound ge).
        exact EM'. exact RO'. exact MM'. assumption. exact SP. exact H4.
        intro MATCH_v.
        exploit (ValueAnalysis.abuiltin_arg_sound ge).
        exact EM'. exact RO'. exact MM'. assumption. exact SP. exact H3.
        intro MATCH_addr.

        inv H1.
        ** (* true volatile access *)
          eapply sound_succ_state with (bc := bc); eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. constructor. }
          apply mmatch_lub_l; auto.
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }        
        ** (* normal memory access *)
          eapply sound_succ_state with (bc := bc); eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. constructor. }
          { apply mmatch_lub_r.
            eapply storev_sound; eauto. auto. }
          { eapply romatch_store; eauto. }
          eapply sound_stack_store; eauto.
          { eapply match_not_invalid. exact MATCH_v. }
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          
      ++ (* malloc *)
        inv H0.
        eapply sound_succ_state with (bc := bc3); eauto.
        { apply ValueAnalysis.set_builtin_res_sound; auto. }
        apply sound_stack_exten with bc.
        { apply sound_stack_inv with m'.
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          intros. apply Q.
          { red. eapply Plt_trans; eauto. }
          rewrite C. assumption.
          auto with ordered_type.
        }
        exact AA.
        
      ++ (* free *)
        inv H0.
        { eapply sound_succ_state; eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. }
          { eapply sound_stack_free.
            eassumption.
            apply sound_stack_exten with (bc := bc). 2: assumption.
            eapply sound_stack_istep with (m := m) (rs := rs); eauto.
          }
        }
        { eapply sound_succ_state; eauto.
          { apply ValueAnalysis.set_builtin_res_sound; auto. }
          { apply sound_stack_exten with (bc := bc). 2: assumption.
            eapply sound_stack_istep with (m := m) (rs := rs); eauto.
          }
        }
        
      ++ (* memcpy *)
        inv H0. inv H. inv H12. inv H13.
         exploit (ValueAnalysis.abuiltin_arg_sound ge).
        exact EM'. exact RO'. exact MM'. assumption. eassumption.
        exact H11.
        intro MATCH1.
        exploit (ValueAnalysis.abuiltin_arg_sound ge).
        exact EM'. exact RO'. exact MM'. assumption. eassumption.
        exact H10.
        intro MATCH2.
        eapply sound_succ_state with (bc := bc); eauto.
        { apply ValueAnalysis.set_builtin_res_sound; eauto.
          constructor. }
        { eapply storebytes_sound; eauto.
          apply match_aptr_of_aval; auto.
          eapply Mem.loadbytes_length; eauto.
          intros. eapply loadbytes_sound; eauto.
          apply match_aptr_of_aval; auto.
        }
        eapply romatch_storebytes; eauto.
        eapply sound_stack_storebytes; eauto.
        eapply sound_stack_istep with (m := m) (rs := rs); eauto.
      ++ (* annot *)
        inv H0.
        eapply sound_succ_state; eauto; cycle 1.
        { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
        apply ValueAnalysis.set_builtin_res_sound; auto. constructor.
      ++ (* annot_val *)
        inv H0.
        destruct args.
        { eapply sound_succ_state; eauto; cycle 1.
          { apply sound_stack_exten with (bc := bc). 2: assumption.
            eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          apply ValueAnalysis.set_builtin_res_sound; auto.
        }
        destruct args; cycle 1.
        { eapply sound_succ_state; eauto; cycle 1.
          { apply sound_stack_exten with (bc := bc). 2: assumption.
            eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          apply ValueAnalysis.set_builtin_res_sound; auto.
        }
        { eapply sound_succ_state; eauto; cycle 1.
          { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          inv ARGS.
          apply ValueAnalysis.set_builtin_res_sound; auto.
        }
      ++ (* inline_asm *)
         eapply sound_succ_state with (bc := bc3); eauto.
        { apply ValueAnalysis.set_builtin_res_sound; auto. }
        apply sound_stack_exten with bc.
        { apply sound_stack_inv with m'.
          {  eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
          intros. apply Q.
          { red. eapply Plt_trans; eauto. }
          rewrite C. assumption.
          auto with ordered_type.
        }
        exact AA.

      ++ (* debug *)
        inv H0.
        eapply sound_succ_state; eauto; cycle 1.
        { eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
        apply ValueAnalysis.set_builtin_res_sound; auto. constructor.
        
      ++ (* profiling *)
        inv H0.
        eapply sound_succ_state; eauto; cycle 1.
        { apply sound_stack_exten with (bc := bc). 2: assumption.
          eapply sound_stack_istep with (m := m) (rs := rs); eauto. }
        apply ValueAnalysis.set_builtin_res_sound; auto.
        
  + (* jumptable *)
    subst st'; destruct (transfer_final_nobuiltin_correct bc ge sp0 fin stack f rm ae' am' rs' m' _ _ trace _ rs' m' EM' MM' RO' FINAL NOBUILTIN) as (ae'' & am'' & IN'' & EM'' & AM'').
    apply propagate_to_final with (ib := ib) (ae := ae) (am := am) in IN''.
    2: assumption.
    eapply sound_succ_state0; eauto.
    apply sound_stack_istep with (ib := entry ib) (fin := Some fin) (ae := ae) (am := am) (rs := rs) (m := m) (rs' := rs'); auto.
Qed.


Theorem sound_step_base:
  forall st t st' bc,
    BTL.step false ge st t st' ->
    sound_state_base bc st ->
    exists bc', sound_state_base bc' st'.
Proof.
  induction 1; rename bc0 into _bc; try rename bc' into _bc'; intro SOUND; inversion SOUND.
  - (* exec_iblock *)
    case STEP as (rs' & m' & fin & ISTEP & FINAL).
    exploit sound_step_base_exec_iblock; eauto.
    case final_preserv_bc; eauto.
  - (* internal call *)
    exploit (ValueAnalysis.allocate_stack ge); eauto.
    intros (bc' & A & B & C & D & E & F & G).
    exists bc'.
    exploit (analyze_entrypoint rm f args m' bc'); eauto.
    intros (ae & am & AN & EM & MM').
    econstructor; eauto.
    erewrite Mem.alloc_result by eauto.
    apply sound_stack_exten with bc; auto.
    apply sound_stack_inv with m; auto.
    intros. eapply Mem.loadbytes_alloc_unchanged; eauto.
    intros. apply F. erewrite Mem.alloc_result by eauto. auto.

   - (* external call *) 
    exploit (ValueAnalysis.external_call_match ge); eauto with va.
    intros (bc' & A & B & C & D & E & F & G & K).
    exists bc'.
    econstructor; eauto.
    apply sound_stack_new_bound with (Mem.nextblock m).
    apply sound_stack_exten with bc; auto.
    apply sound_stack_inv with m; auto.
    eapply external_call_nextblock; eauto.

  - (* return *)
    inv STK.
    + (* from public call *)
      exploit (ValueAnalysis.return_from_public_call ge); eauto.
      intros; rewrite SAME; auto.
      intros (bc1 & A & B & C & D & E & F & G).
      destruct (analyze rm f)#pc as [ |ae' am'] eqn:EQ; simpl in AN; try contradiction. destruct AN as [A1 A2].
      exists bc1; eapply sound_regular_state; eauto.
      apply sound_stack_exten with bc'; auto.
      eapply ematch_ge; eauto. apply ematch_update. auto. auto.
    + (* from private call *)
      exploit (ValueAnalysis.return_from_private_call ge); eauto.
      intros; rewrite SAME; auto.
      intros (bc1 & A & B & C & D & E & F & G).
      destruct (analyze rm f)#pc as [ |ae' am'] eqn:EQ; simpl in AN; try contradiction. destruct AN as [A1 A2].
      exists bc1; eapply sound_regular_state; eauto.
      apply sound_stack_exten with bc'; auto.
      eapply ematch_ge; eauto. apply ematch_update. auto. auto.
Qed.
End SOUNDNESS.

Section INITIAL2.
Variable prog: program.

Let ge := Genv.globalenv prog.

Lemma romem_for_consistent_2:
  forall (cunit : program), linkorder cunit prog ->
  ValueAnalysis.romem_consistent (prog_defmap prog) (ValueAnalysis.romem_for cunit).
Proof.
  intros; red; intros.
  exploit (ValueAnalysis.romem_for_consistent cunit); eauto. intros (v & DM & RO & VO & DEFN & AB).
  destruct (prog_defmap_linkorder _ _ _ _ H DM) as (gd & P & Q).
  assert (gd = Gvar v).
  { inv Q. inv H2. simpl in *. f_equal. f_equal.
    destruct info1, info2; auto.
    inv H3; auto; discriminate. }
  subst gd. exists v; auto.
Qed.
  
Theorem initial_mem_matches:
  forall m
  (INIT : Genv.init_mem prog = Some m),
  exists bc,
     genv_match bc ge
  /\ bc_below bc (Mem.nextblock m)
  /\ ValueAnalysis.bc_nostack bc
  /\ (forall cunit, linkorder cunit prog -> romatch bc m (ValueAnalysis.romem_for cunit))
  /\ (forall b, Mem.valid_block m b -> bc b <> BCinvalid).
Proof.
  intros.
  exploit ValueAnalysis.initial_block_classification; eauto. intros (bc & GE & BELOW & NOSTACK & INV & VALID).
  exists bc; ValueAnalysis.splitall; auto.
  intros.
  assert (A: ValueAnalysis.initial_mem_match bc m ge).
  { eapply (ValueAnalysis.alloc_globals_match prog bc GE) with (m := Mem.empty); auto.
    red. unfold Genv.find_symbol; simpl; intros.
    rewrite ValueAnalysis.genv_find_empty in H1.
    discriminate.
  }
  assert (B: ValueAnalysis.romem_consistent (prog_defmap prog) (ValueAnalysis.romem_for cunit))
      by (apply romem_for_consistent_2; auto).
  red; intros.
  exploit B; eauto. intros (v & DM & RO & NVOL & DEFN & EQ).
  rewrite Genv.find_def_symbol in DM. destruct DM as (b1 & FS & FD).
  rewrite <- Genv.find_var_info_iff in FD.
  assert (b1 = b).
  { rewrite INV with (b:=b) in FS by auto.
    congruence.
  }
  subst b1.
  split. subst ab. apply ValueAnalysis.store_init_data_list_summary. constructor.
  split. subst ab. eapply A; eauto.
  exploit Genv.init_mem_characterization; eauto.
  intros (P & Q & R).
  intros; red; intros. exploit Q; eauto. intros [U V].
  unfold Genv.perm_globvar in V; rewrite RO, NVOL in V. inv V.
Qed.

Inductive sound_state: state -> Prop :=
  | sound_state_intro: forall st,
      (forall cunit, linkorder cunit prog -> exists bc, sound_state_base cunit ge bc st) ->
      sound_state st.

Theorem sound_step:
  forall st t st', step false ge st t st' -> sound_state st -> sound_state st'.
Proof.
  intros. inv H0. constructor; intros.
  ecase H1 as [bc]; eauto.
  eapply sound_step_base; eauto.
Qed.

Remark sound_state_inv:
  forall st cunit,
  sound_state st -> linkorder cunit prog -> exists bc, sound_state_base cunit ge bc st.
Proof.
  intros. inv H. eauto.
Qed.

Require Import Axioms.

Theorem sound_initial:
  forall st, initial_state prog st -> sound_state st.
Proof.
  destruct 1.
  exploit initial_mem_matches; eauto.
  intros (bc & GE & BELOW & NOSTACK & RM & VALID).
  constructor; intros. exists bc; apply sound_call_state.
- constructor.
- simpl; tauto.
- apply RM; auto.
- apply mmatch_inj_top with m0.
  replace (inj_of_bc bc) with (Mem.flat_inj (Mem.nextblock m0)).
  eapply Genv.initmem_inject; eauto.
  symmetry; apply extensionality; unfold Mem.flat_inj; intros x.
  destruct (plt x (Mem.nextblock m0)).
  apply inj_of_bc_valid; auto.
  unfold inj_of_bc. erewrite bc_below_invalid; eauto.
- exact GE.
- exact NOSTACK.
Qed.
End INITIAL2.
