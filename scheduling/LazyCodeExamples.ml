open Op
open BTL
open RTLcommonaux
open BTLcommonaux
open Maps
open Camlcoq
open! Integers
(*open DebugPrint*)
(*open StrengthRedCore*)

(* variables:
 * a = 1
 * b = 2
 * i = 3
 * x = 4 (const 10)
 * m = 5
 * l = 6
 * c = 7
*)
let example_sr =
  let ex = ref PTree.empty in
  let ib1 = mk_ibinfo (Bseq (Bop (Olongconst (Camlcoq.coqint_of_camlint64 10L), [], i2p 4, def_iinfo()),
                             BF (Bgoto (i2p 2), def_iinfo()))) (def_binfo()) in
  let ib2 = mk_ibinfo (Bseq (Bcond (Ccompimm (Cgt, Camlcoq.coqint_of_camlint 0l), [ i2p 1 ], BF (Bgoto (i2p 5), def_iinfo()), Bnop None, def_iinfo()),
                             BF (Bgoto (i2p 3), def_iinfo()))) (def_binfo()) in
  let ib3 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 2), def_iinfo()))) (def_binfo()) in
  let ib4 = mk_ibinfo (Bseq (Bop (Oaddlimm (Camlcoq.coqint_of_camlint64 1L), [ i2p 3 ], i2p 3, def_iinfo()),
                             BF (Bgoto (i2p 7), def_iinfo()))) (def_binfo()) in
  let ib5 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 8), def_iinfo()))) (def_binfo()) in
  let ib6 = mk_ibinfo (Bseq (Bop (Oaddlimm (Camlcoq.coqint_of_camlint64 5L), [ i2p 3 ], i2p 3, def_iinfo()),
                              BF (Bgoto (i2p 9), def_iinfo()))) (def_binfo()) in
  let ib7 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 11), def_iinfo()))) (def_binfo()) in
  let ib8 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 11), def_iinfo()))) (def_binfo()) in
  let ib9 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 11), def_iinfo()))) (def_binfo()) in
  let ib10 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                              BF (Bgoto (i2p 4), def_iinfo()))) (def_binfo()) in
  let ib11 = mk_ibinfo (Bseq (Bcond (Ccompimm (Cgt, Camlcoq.coqint_of_camlint 7l), [ i2p 2 ], BF (Bgoto (i2p 23), def_iinfo()), Bnop None, def_iinfo()),
                              BF (Bgoto (i2p 14), def_iinfo()))) (def_binfo()) in
  let ib12 = mk_ibinfo (Bseq (Bop (Oaddlimm (Camlcoq.coqint_of_camlint64 2L), [ i2p 3 ], i2p 3, def_iinfo()),
                              BF (Bgoto (i2p 6), def_iinfo()))) (def_binfo()) in
  let ib13 = mk_ibinfo (Bseq (Bop (Omull, [ i2p 3; i2p 4 ], i2p 1, def_iinfo()),
                        Bseq (Bcond (Ccompimm (Clt, Camlcoq.coqint_of_camlint 100l), [ i2p 1 ], BF (Bgoto (i2p 16), def_iinfo()), Bnop None, def_iinfo()),
                              BF (Bgoto (i2p 17), def_iinfo())))) (def_binfo()) in
  let ib14 = mk_ibinfo (Bseq (Bop (Omull, [ i2p 3; i2p 4 ], i2p 2, def_iinfo()),
                              BF (Bgoto (i2p 17), def_iinfo()))) (def_binfo()) in
  let ib15 = mk_ibinfo (Bseq (Bop (Oaddlimm (Camlcoq.coqint_of_camlint64 1L), [ i2p 3 ], i2p 3, def_iinfo()),
                              BF (Bgoto (i2p 12), def_iinfo()))) (def_binfo()) in
  let ib16 = mk_ibinfo (Bseq (Bop (Odivl, [ i2p 5; i2p 6 ], i2p 3, def_iinfo()),
                              BF (Bgoto (i2p 10), def_iinfo()))) (def_binfo()) in
  let ib17 = mk_ibinfo (Bseq (Bop (Omull, [ i2p 5; i2p 6 ], i2p 5, def_iinfo()),
                        Bseq (Bcond (Ccompimm (Ceq, Camlcoq.coqint_of_camlint 50l), [ i2p 2 ], BF (Bgoto (i2p 18), def_iinfo()), Bnop None, def_iinfo()),
                              BF (Bgoto (i2p 24), def_iinfo())))) (def_binfo()) in
  let ib18 = mk_ibinfo (Bseq (Bop (Oaddlimm (Camlcoq.coqint_of_camlint64 3L), [ i2p 3 ], i2p 3, def_iinfo()),
                              BF (Bgoto (i2p 4), def_iinfo()))) (def_binfo()) in
  let ib19 = mk_ibinfo (Bseq (Bop (Omull, [ i2p 5; i2p 6 ], i2p 3, def_iinfo()),
                              BF (Bgoto (i2p 20), def_iinfo()))) (def_binfo()) in
  let ib20 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                              BF (Bgoto (i2p 21), def_iinfo()))) (def_binfo()) in
  let ib21 = mk_ibinfo (Bseq (Bop (Omull, [ i2p 3; i2p 4 ], i2p 7, def_iinfo()),
                              BF (Bgoto (i2p 22), def_iinfo()))) (def_binfo()) in
  let ib22 = mk_ibinfo (BF (Breturn None, def_iinfo())) (def_binfo()) in
  let ib23 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                              BF (Bgoto (i2p 13), def_iinfo()))) (def_binfo()) in
  let ib24 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                              BF (Bgoto (i2p 25), def_iinfo()))) (def_binfo()) in
  let ib25 = mk_ibinfo (Bseq (Bcond (Ccompimm (Clt, Camlcoq.coqint_of_camlint 25l), [ i2p 2 ], BF (Bgoto (i2p 19), def_iinfo()), Bnop None, def_iinfo()),
                              BF (Bgoto (i2p 15), def_iinfo()))) (def_binfo()) in
  ex := PTree.set (i2p 1) ib1 !ex |> PTree.set (i2p 2) ib2 |> PTree.set (i2p 3) ib3 |>
        PTree.set (i2p 4) ib4 |> PTree.set (i2p 5) ib5 |> PTree.set (i2p 6) ib6 |>
        PTree.set (i2p 7) ib7 |> PTree.set (i2p 8) ib8 |> PTree.set (i2p 9) ib9 |>
        PTree.set (i2p 10) ib10 |> PTree.set (i2p 11) ib11 |> PTree.set (i2p 12) ib12 |>
        PTree.set (i2p 13) ib13 |> PTree.set (i2p 14) ib14 |> PTree.set (i2p 15) ib15 |>
        PTree.set (i2p 16) ib16 |> PTree.set (i2p 17) ib17 |> PTree.set (i2p 18) ib18 |>
        PTree.set (i2p 19) ib19 |> PTree.set (i2p 20) ib20 |> PTree.set (i2p 21) ib21 |>
        PTree.set (i2p 22) ib22 |> PTree.set (i2p 23) ib23 |> PTree.set (i2p 24) ib24 |>
        PTree.set (i2p 25) ib25;
  (!ex, (i2p 1))

let example_motion =
  let ex = ref PTree.empty in
  let ib1 = mk_ibinfo (Bseq (Bcond (Ccompimm (Cgt, Z.of_sint 0), [ i2p 1 ], BF (Bgoto (i2p 2), def_iinfo()), Bnop None, def_iinfo()),
                             BF (Bgoto (i2p 3), def_iinfo()))) (def_binfo()) in
  let ib2 = mk_ibinfo (Bseq (Bop (Oadd, [ i2p 2; i2p 3 ], i2p 5, def_iinfo()),
                       Bseq (Bop (Omove, [ i2p 1 ], i2p 2, def_iinfo()),
                       Bseq (Bop (Oadd, [ i2p 2; i2p 3 ], i2p 4, def_iinfo()),
                             BF (Bgoto (i2p 4), def_iinfo()))))) (def_binfo()) in
  let ib3 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 4), def_iinfo()))) (def_binfo()) in
  let ib4 = mk_ibinfo (Bseq (Bcond (Ccompimm (Clt, Z.of_sint 100), [ i2p 4 ], BF (Bgoto (i2p 5), def_iinfo()), Bnop None, def_iinfo()),
                             BF (Bgoto (i2p 6), def_iinfo()))) (def_binfo()) in
  let ib5 = mk_ibinfo (Bseq (Bcond (Ccompimm (Clt, Z.of_sint 20), [ i2p 4 ],
                                    BF (Bgoto (i2p 7), def_iinfo()), Bnop None, def_iinfo()), BF (Bgoto (i2p 8), def_iinfo()))) (def_binfo()) in
  let ib6 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 19), def_iinfo()))) (def_binfo()) in
  let ib7 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 9), def_iinfo()))) (def_binfo()) in
  let ib8 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 12), def_iinfo()))) (def_binfo()) in
  let ib9 = mk_ibinfo (Bseq (Bcond (Ccomp Ceq, [ i2p 4; i2p 5 ], BF (Bgoto (i2p 10), def_iinfo()), Bnop None, def_iinfo()),
                             BF (Bgoto (i2p 11), def_iinfo()))) (def_binfo()) in
  let ib10 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                             BF (Bgoto (i2p 13), def_iinfo()))) (def_binfo()) in
  let ib11 = mk_ibinfo (Bseq (Bop (Oadd, [ i2p 2; i2p 3 ], i2p 5, def_iinfo()),
                              BF (Bgoto (i2p 9), def_iinfo()))) (def_binfo()) in
  let ib12 = mk_ibinfo (Bseq (Bcond (Ccomp Clt, [ i2p 5; i2p 1 ], BF (Bgoto (i2p 14), def_iinfo()), Bnop None, def_iinfo()),
                              BF (Bgoto (i2p 15), def_iinfo()))) (def_binfo()) in
  let ib13 = mk_ibinfo (Bseq (Bop (Oadd, [ i2p 2; i2p 3 ], i2p 6, def_iinfo()),
                        Bseq (Bop (Omove, [ i2p 1 ], i2p 2, def_iinfo()),
                              BF (Bgoto (i2p 16), def_iinfo())))) (def_binfo()) in
  let ib14 = mk_ibinfo (Bseq (Bcond (Ccomp Clt, [ i2p 2; i2p 3 ], BF (Bgoto (i2p 17), def_iinfo()), Bnop None, def_iinfo()),
                              BF (Bgoto (i2p 18), def_iinfo()))) (def_binfo()) in
  let ib15 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                              BF (Bgoto (i2p 12), def_iinfo()))) (def_binfo()) in
  let ib16 = mk_ibinfo (Bseq (Bop (Oadd, [ i2p 2; i2p 3 ], i2p 4, def_iinfo()),
                              BF (Bgoto (i2p 19), def_iinfo()))) (def_binfo()) in
  let ib17 = mk_ibinfo (Bseq (Bop (Oadd, [ i2p 2; i2p 3 ], i2p 5, def_iinfo()),
                              BF (Bgoto (i2p 13), def_iinfo()))) (def_binfo()) in
  let ib18 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                              BF (Bgoto (i2p 16), def_iinfo()))) (def_binfo()) in
  let ib19 = mk_ibinfo (Bseq (Bnop (Some (def_iinfo())),
                              BF (Breturn None, def_iinfo()))) (def_binfo()) in
  ex := PTree.set (i2p 1) ib1 !ex |> PTree.set (i2p 2) ib2 |> PTree.set (i2p 3) ib3 |>
        PTree.set (i2p 4) ib4 |> PTree.set (i2p 5) ib5 |> PTree.set (i2p 6) ib6 |>
        PTree.set (i2p 7) ib7 |> PTree.set (i2p 8) ib8 |> PTree.set (i2p 9) ib9 |>
        PTree.set (i2p 10) ib10 |> PTree.set (i2p 11) ib11 |> PTree.set (i2p 12) ib12 |>
        PTree.set (i2p 13) ib13 |> PTree.set (i2p 14) ib14 |> PTree.set (i2p 15) ib15 |>
        PTree.set (i2p 16) ib16 |> PTree.set (i2p 17) ib17 |> PTree.set (i2p 18) ib18 |>
        PTree.set (i2p 19) ib19;
  (!ex, (i2p 1))

