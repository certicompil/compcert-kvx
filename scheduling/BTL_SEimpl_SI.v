Require Import Coqlib Maps.
Require Import Classes.Equivalence.
Require Import Registers.
Require Import RTL BTL.
Require Import BTL_SEsimuref BTL_SEtheory OptionMonad.
Require Import BTL_SEimpl_prelude.

Import Notations.
Import HConsing.
Import SvalNotations.

Import ListNotations.
Local Open Scope list_scope.
Local Open Scope lazy_bool_scope.
Local Open Scope option_monad_scope.
Local Open Scope impure.


Section CSASV_apply.
  (* The goal of this section is to prove that the evaluation of an invariant expressed as a CSASV
     can be implemented by applying sequentially the assignments on a [sistate]. *)

Record sis_is_fpasv_app (sis0 sis1 : sistate) (si : fpasv) : Prop := {
  SIA_OK:  forall ctx, sis_ok ctx sis1 <-> sis_ok ctx sis0 /\ si_ok_subst sis0 si ctx;
  SIA_REG: forall r, sis1 r = match si r with
                              | Some sv => Some (sv_subst sis0.(sis_smem) sis0 sv)
                              | None    => sis0 r
                              end;
  SIA_MEM: sis1.(sis_smem) = sis0.(sis_smem);
}.

Lemma sis_is_fpasv_app_empty sis:
  sis_is_fpasv_app sis sis si_empty.
Proof.
  constructor; try reflexivity.
  intuition auto using si_ok_subst_empty.
Qed.

Lemma sis_get_ireg_ir_subst sis0 sis1 ir si sv
  (SIA : sis_is_fpasv_app sis0 sis1 si)
  (GET: sis_get_ireg sis0 sis1 ir = Some sv):
  sv = sv_subst sis0.(sis_smem) sis0 (ir_subst (ext si) ir).
Proof.
  revert GET; unfold sis_get_ireg, ir_subst.
  case force_input; simpl.
  + intros ->. reflexivity.
  + intros R1; apply SIA_REG with (r:=regof ir) in SIA; revert SIA; rewrite R1.
    unfold ext; case (si (regof ir)); simpl.
    * congruence.
    * intros <-; reflexivity.
Qed.

Lemma sis_get_ival_iv_subst sis0 sis1 iv si sv
  (SIA : sis_is_fpasv_app sis0 sis1 si)
  (GET: sis_get_ival sis0 sis1 iv = Some sv):
  sv = sv_subst sis0.(sis_smem) sis0 (iv_subst (ext si) iv).
Proof.
  revert GET; case iv as [ir|rop rargs]; simpl.
  - apply sis_get_ireg_ir_subst. apply SIA.
  - autodestruct; intros LMAP; injection 1 as <-.
    unfold rop_subst.
    assert (l_eq: l = lsv_subst sis0.(sis_smem) sis0 (lsvof (ext si) rargs)). {
      revert l LMAP. induction rargs; intro l; simpl.
      - congruence.
      - do 2 autodestruct; intros _ GET; injection 1 as <-; f_equal.
        + eapply sis_get_ireg_ir_subst; eauto.
        + eapply IHrargs; reflexivity.
    }
    rewrite l_eq; case rop; reflexivity.
Qed.

Lemma sia_set_si sis0 sis1 sis2 r iv si
  (SIA : sis_is_fpasv_app sis0 sis1 si)
  (SET : sis_set_ival r iv sis0 sis1 = Some sis2):
  sis_is_fpasv_app sis0 sis2 (si_set r (iv_subst (ext si) iv) si).
Proof.
  revert SET; unfold sis_set_ival; autodestruct; intro GET; injection 1 as <-.
  eapply sis_get_ival_iv_subst in GET as ->; eauto.
  inversion SIA; constructor; auto.
  - intros ctx; rewrite ok_set_sreg, SIA_OK0; unfold si_ok_subst; simpl.
    intuition (subst; eauto).
  - intros r0; unfold set_sreg; simpl.
    case (Pos.eq_dec r r0) as [<-|]; [rewrite si_gss|rewrite si_gso]; auto.
Qed.

Lemma sis_exec_seq sis0 sis1 sis2 si (sq : list (reg*ival))
  (SIA : sis_is_fpasv_app sis0 sis1 si)
  (SEQ : sis_set_seq sq sis0 sis1 = Some sis2):
  sis_is_fpasv_app sis0 sis2 (exec_seq sq si).
Proof.
  revert sis1 sis2 si SIA SEQ. induction sq as [|(r, iv) tl]; intros sis1 sis2 si SI1; simpl.
  - injection 1 as <-. assumption.
  - autodestruct; intros.
    eapply IHtl; eauto.
    eapply sia_set_si; eauto.
Qed.

Program Definition sis_build_alive (out : reg -> bool) (sis : sistate) : sistate :=
  {|
    sis_pre  := fun ctx => sis.(sis_pre) ctx /\ sreg_ok ctx sis;
    sis_sreg := fun r => ASSERT (out r) IN Some (ext sis.(sis_sreg) r);
    sis_smem := sis.(sis_smem);
  |}.
Next Obligation.
  rewrite sis_pre_preserved.
  unfold sreg_ok; setoid_rewrite eval_sval_preserved.
  reflexivity.
Qed.

Definition alive_list (out : list reg) (r : reg) : bool :=
  if in_dec Reg.eq r out then true else false.

Lemma sis_build_alive_ok ctx out sis:
  sis_ok ctx (sis_build_alive out sis) <-> sis_ok ctx sis.
Proof.
  unfold sis_build_alive; split; intros []; constructor; simpl in *; try tauto.
  intros r sv; autodestruct; injection 2 as <-; unfold ext.
  autodestruct; intro SISR. 2:simpl;congruence.
  eapply OK_SREG; eauto.
Qed.

Lemma tr_sis_siof sis csi sis'
  (SEQ: sis_set_seq csi.(aseq) sis sis = Some sis'):
  let sis1 := tr_sis sis (siof csi) false in
  let sis2 := sis_build_alive (alive_list (Regset.elements csi.(outputs))) sis' in
  (forall ctx, sis_ok ctx sis1 <-> sis_ok ctx sis2) /\
  (forall r, sis1.(sis_sreg) r = sis2.(sis_sreg) r) /\
  sis1.(sis_smem) = sis2.(sis_smem).
Proof.
  case (sis_exec_seq sis sis sis' si_empty csi.(aseq)) as [?]; try assumption.
    { apply sis_is_fpasv_app_empty. }
  simpl; split; [|split]; auto.
  - intros ctx. rewrite sis_build_alive_ok, SIA_OK0.
    split.
    + intros []; simpl in *; split.
      * constructor; tauto.
      * intro. apply OK_PRE.
    + intros [OK SI_OK].
      apply ok_tr_sis; unfold tr_sis_ok; auto.
  - intros r; unfold siof, si_apply; simpl.
    rewrite build_alive_spec; unfold alive_list; case in_dec as [|]. 2:reflexivity.
    unfold ext; rewrite SIA_REG0; unfold si_apply.
    autodestruct.
Qed.

End CSASV_apply.

Section TotalSistates.
  (* Operations on [sistate] that are total if all registers are live *)

Definition is_total_sistate (sis : sistate) : Prop :=
  forall (r : reg), alive (sis r).

Lemma sis_empty_total:
  is_total_sistate sis_empty.
Proof.
  intro; simpl; congruence.
Qed.

Lemma tr_sis_total sis si:
  is_total_sistate (tr_sis sis si true).
Proof.
  intro; simpl; autodestruct.
Qed.

Lemma ris_input_total ctx ris sis
  (RR: ris_refines_ok ctx ris sis)
  (INPUT: ris.(ris_input_init) = true):
  is_total_sistate sis.
Proof.
  intro r; apply ALIVE_EQ with (r:=r) in RR; rewrite <- RR.
  unfold ris_sreg_get; rewrite INPUT; autodestruct.
Qed.

Section Total2Sistate.
  Variables (sis0 sis1 : sistate).
  Hypothesis (T0 : is_total_sistate sis0) (T1 : is_total_sistate sis1).

  Lemma sis_get_ireg_total ir:
    alive (sis_get_ireg sis0 sis1 ir).
  Proof.
    unfold sis_get_ireg. case force_input; [apply T0|apply T1].
  Qed.

  Lemma sis_get_ival_total iv:
    alive (sis_get_ival sis0 sis1 iv).
  Proof.
    case iv as [ir|rop rargs]; simpl.
    - apply sis_get_ireg_total.
    - autodestruct; intros LMAP; exfalso; revert LMAP.
      induction rargs; simpl; repeat autodestruct. congruence.
      intro A; apply sis_get_ireg_total in A as [].
  Qed.

  Lemma sis_set_ival_total r iv:
    exists sis2,
      sis_set_ival r iv sis0 sis1 = Some sis2 /\
      is_total_sistate sis2.
  Proof.
    unfold sis_set_ival.
    specialize (sis_get_ival_total iv); autodestruct.
    eexists; split. reflexivity.
    intro r0; simpl; autodestruct.
  Qed.
End Total2Sistate.

Lemma sis_set_seq_total l sis0 sis1:
  is_total_sistate sis0 ->
  is_total_sistate sis1 ->
  exists sis2,
    sis_set_seq l sis0 sis1 = Some sis2 /\
    is_total_sistate sis2.
Proof.
  intros T0; revert sis1; induction l as [|(r,iv) tl]; simpl; intros; eauto.
  ecase sis_set_ival_total with (r:=r) (iv:=iv) as (sis2 & -> & T2); eauto.
Qed.
End TotalSistates.

Section SymbolicInvariants.

  Context `{HCF : HashConsingFcts}.
  Context `{HC : HashConsingHyps HCF}.
  Context `{RRULES: rrules_set}.

Section TrSingle.
  Variable (m : se_mode).

Definition hrs_sv_set r sv (hrs: ristate): ?? ristate :=
  RET (ris_sreg_set hrs (red_PTree_set r sv hrs)).

Definition hrs_ival_set (hrs hrs_old: ristate) (r: reg) (iv: ival): ?? ristate :=
  match iv with
  | Ireg ir =>
      DO sv <~ @hrs_ir_get HCF hrs hrs_old ir;;
      hrs_sv_set r sv hrs
  | Iop rop args => @hrs_rsv_set HCF RRULES m r args rop hrs hrs_old
  end.

Lemma hrs_ival_set_spec ctx sis0 sis1 hrs hrs_old r iv
  (CTXM: ctx_se_mode m ctx)
  (RR0: ris_refines_ok ctx hrs_old sis0)
  (RR1: ris_refines_ok ctx hrs     sis1):
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN
  exists sis2,
    sis_set_ival r iv sis0 sis1 = Some sis2 /\
    ris_refines ctx hrs' sis2.
Proof.
  unfold hrs_ival_set, hrs_sv_set, sis_set_ival.
  case iv as [ir|rop args]; try wlp_seq; simpl.
  - intros sv GET.
    eapply hrs_ir_get_spec in GET as [?[GET]]; eauto; rewrite GET.
    eexists; split. reflexivity.
    apply ris_set_reg_refines; auto.
    intuition; intro.
    eapply sis_get_ireg_nofail in GET; eauto using SOK.
  - intros hrs' H.
    eapply hrs_rsv_set_spec in H as [sis2 [EQ REF]]; eauto.
    exists sis2.
    revert EQ; unfold sis_set_rop; autodestruct; auto.
Qed.

Lemma hrs_ival_set_rel hrs hrs_old r iv:
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN
  ris_rel m hrs hrs'.
Proof.
  unfold hrs_ival_set, hrs_sv_set; case iv as [|]; try wlp_seq; simpl.
  - intros ? _. constructor; simpl; try reflexivity.
    all:intros until ctx; rewrite <- ris_ok_set_sreg; auto.
  - intros hrs' ?; eapply hrs_rsv_set_rel; eauto.
Qed.

Global Opaque hrs_ival_set hrs_sv_set.

(** * Applying a sequence of invariant operations *)

Fixpoint exec_seq_imp (hrs hrs_old: ristate) (l: list (reg*ival)): ?? ristate :=
  match l with
  | nil => RET hrs
  | (r,iv)::l =>
      DO hrs' <~ hrs_ival_set hrs hrs_old r iv;;
      exec_seq_imp hrs' hrs_old l
  end.

Lemma exec_seq_imp_rel hrs hrs_old l:
  WHEN exec_seq_imp hrs hrs_old l ~> hrs' THEN
  ris_rel m hrs hrs'.
Proof.
  revert hrs. induction l as [|(r, iv)]; intros; simpl; wlp_seq.
  - reflexivity.
  - intros hrs2 H2 hrs3 H3.
    apply IHl in H3; eapply hrs_ival_set_rel in H2.
    etransitivity; eassumption.
Qed.

Lemma exec_seq_imp_spec ctx sis0 sis1 hrs hrs_old l
  (CTXM: ctx_se_mode m ctx)
  (RR0: ris_refines_ok ctx hrs_old sis0)
  (RR1: ris_refines_ok ctx hrs     sis1):
  WHEN exec_seq_imp hrs hrs_old l ~> hrs' THEN
  (exists sis2,
    sis_set_seq l sis0 sis1 = Some sis2 /\
    ris_refines_ok ctx hrs' sis2)
  \/ (~ris_ok ctx hrs' /\ (forall sis2, sis_set_seq l sis0 sis1 = Some sis2 -> ~sis_ok ctx sis2)).
Proof.
  revert sis1 hrs RR1; induction l as [|(r, iv) l]; intros; simpl; wlp_seq.
  - left; eauto.
  - intros hrs2 SET hrs3 SEQ.
    eapply hrs_ival_set_spec in SET as [sis2 [-> RR2]]; eauto.
    case (ris_ok_dec ctx hrs2) as [OK|KO].
    + apply (ok_ref OK) in RR2.
      eapply IHl; eauto.
    + right; split.
      * intro; eapply exec_seq_imp_rel in SEQ; eapply RRL_OK in SEQ; eauto.
      * intros sis3 EQ3 OK3; apply KO; apply RR2.
        eapply sis_set_seq_okpreserv in EQ3; eauto.
Qed.
Global Opaque exec_seq_imp.

(** ** Building a tree from the compact invariants representation, only on live variables *)

Fixpoint build_alive_imp (loutputs: list reg) (hrs: ristate): ?? (PTree.t sval) :=
  match loutputs with
  | nil => RET (PTree.empty sval)
  | r :: tl =>
      DO sreg <~ build_alive_imp tl hrs;;
      DO hsv <~ @hrs_sreg_get HCF hrs r;;
      RET (PTree.set r hsv sreg)
  end.

Lemma build_alive_imp_spec ctx loutputs hrs sis r
  (RR: ris_refines_ok ctx hrs sis):
  WHEN build_alive_imp loutputs hrs ~> sreg THEN
  optsv_simu ctx (sreg ! r) ((build_alive (fun r' => ext sis r') loutputs) ! r).
Proof.
  induction loutputs as [|r0 tl]; simpl; wlp_seq.
  - reflexivity.
  - intros sreg REC hsv GET.
    apply  IHtl in REC.
    eapply hrs_sreg_get_spec in GET as [sv [EQ EV]]; eauto.
    rewrite !PTree.gsspec; destruct (peq r r0); [subst|].
    + unfold ext; rewrite EQ. constructor; exact EV.
    + apply REC.
Qed.

Global Opaque build_alive_imp.

(** ** Applying a [csasv] *)

Definition tr_hrs_single (hrs: ristate) (csi: csasv): ?? ristate :=
  DO hrs' <~ exec_seq_imp hrs hrs csi.(aseq);;
  DO sreg <~ build_alive_imp (Regset.elements csi.(outputs)) hrs';;
  RET (ris_sreg_set hrs' sreg).

Lemma tr_hrs_single_rel hrs csi:
  WHEN tr_hrs_single hrs csi ~> hrs' THEN
  ris_rel m hrs hrs'.
Proof.
  unfold tr_hrs_single; wlp_seq.
  intros hrs' SEQ ? _; apply exec_seq_imp_rel in SEQ as [?].
  constructor; simpl; try assumption; setoid_rewrite <- ris_ok_set_sreg; auto.
Qed.

Section TrSingleSpec.
  Variable (ctx : iblock_common_context).
  Hypothesis (CTXM : ctx_se_mode m ctx).

Lemma tr_hrs_single_spec hrs sis csi
  (RR: ris_refines_ok ctx hrs sis)
  (T : is_total_sistate sis):
  WHEN tr_hrs_single hrs csi ~> hrs' THEN
  ris_refines ctx (ris_input_false hrs') (tr_sis sis (siof csi) false).
Proof.
  unfold tr_hrs_single; wlp_seq.
  intros hrs' SEQ sreg OUT.
  eapply exec_seq_imp_spec in SEQ as [(sis1 & SEQ & RR2)|(RERR & SERR)]; eauto.
  - apply tr_sis_siof in SEQ as (EQ_OK & EQ_REG & EQ_MEM).
    inversion RR2; eapply wref; constructor.
    4,5:intro r; rewrite EQ_REG; unfold ris_sreg_get; simpl;
        eapply build_alive_imp_spec with (r := r) in OUT; eauto;
        rewrite build_alive_spec in OUT; unfold alive_list;
        inversion OUT; revert H; autodestruct; simpl; try split; congruence.
    + rewrite <- ris_input_false_ok, <- ris_ok_set_sreg. assumption.
    + rewrite EQ_OK, sis_build_alive_ok. assumption.
    + simpl; simpl in EQ_MEM. rewrite MEM_EQ, EQ_MEM; reflexivity.
  - apply ris_refines_intro_not_ok.
    + rewrite <- ris_input_false_ok, <- ris_ok_set_sreg. assumption.
    + intros TR_OK.
      ecase (sis_set_seq_total) as (sis2 & SEQ & _); eauto.
      eapply SERR. exact SEQ.
      apply tr_sis_siof in SEQ as (EQ_OK & _ & _); specialize (EQ_OK ctx).
      rewrite sis_build_alive_ok in EQ_OK; apply EQ_OK.
      apply TR_OK.
Qed.

Global Opaque tr_hrs_single.

(* The implementation of [tr_sis] with [input_init=true] can be deduced from the above properties. *)
Lemma tr_hrs_single_spec_input hrs sis csi
  (RR: ris_refines_ok ctx hrs sis)
  (INPUT: hrs.(ris_input_init) = true):
  WHEN tr_hrs_single hrs csi ~> hrs' THEN
  ris_refines ctx hrs' (tr_sis sis (siof csi) true).
Proof.
  intros hrs' TR.
  apply tr_hrs_single_rel in TR as REL; apply RRL_INPUT in REL; rewrite INPUT in REL.
  eapply tr_hrs_single_spec in TR; eauto using ris_input_total.
  inversion TR; apply ris_refines_intro;
    [|intros ROK _; apply ok_ref in TR; [|apply -> ris_input_false_ok; exact ROK];
      inversion TR; constructor].
  - rewrite <- ris_input_false_ok in OK_EQUIV; rewrite <- OK_EQUIV.
    split; intros []; constructor; simpl in *; auto.
    + intros r sv; specialize (OK_SREG r sv); revert OK_SREG; simpl; autodestruct.
    + intros r sv; autodestruct; intro csi_eq.
      * specialize (OK_SREG r sv); simpl in OK_SREG; rewrite csi_eq in OK_SREG. exact OK_SREG.
      * injection 1 as <-; simpl. congruence.
  - apply MEM_EQ.
  - intros r; unfold ris_sreg_get; simpl; repeat autodestruct; split; congruence.
  - intros r; specialize (ALIVE_EQ r); specialize (REG_EQ r); revert ALIVE_EQ REG_EQ.
    unfold ris_sreg_get; simpl; repeat autodestruct; simpl.
    + intros _ _ [_ C]; exfalso. lapply C; congruence.
    + intros _ _ _ [C _]; exfalso. lapply C; congruence.
Qed.

(* When [se_ok_check] is set, we obtain a [ris_refines_ok] refinement. *)
Theorem tr_hrs_single_correct hrs sis csi
  (RINIT: ris_input_init hrs = true)
  (SIM:   se_ok_check m = true)
  (RR:    ris_refines_ok ctx hrs sis):
  WHEN tr_hrs_single hrs csi ~> hrs' THEN
  ris_refines_ok ctx (ris_input_false hrs') (tr_sis sis (siof csi) false).
Proof.
  intros hrs' TR; intros.
  eapply ris_input_total in RINIT; eauto.
  eapply tr_hrs_single_spec in TR as RR1; eauto.
  apply tr_hrs_single_rel in TR as [?]; rewrite SIM in RRL_OK_SI.
  apply ok_ref; auto.
  rewrite <- ris_input_false_ok; apply RRL_OK_SI; eauto using ROK.
Qed.


(* Compositions of [tr_hrs_single] used to implements [sis_history], [sis_target] and [sis_source] *)

Lemma sis_history_refines (csix: invariants):
  WHEN tr_hrs_single ris_empty (history csix) ~> hrsH THEN
  ris_refines ctx hrsH (sis_history csix) /\
  ris_input_init hrsH = true.
Proof.
  intros hrsH TR.
  apply tr_hrs_single_rel in TR as REL; apply RRL_INPUT in REL as <-.
  eapply tr_hrs_single_spec_input in TR; eauto using ris_refines_ok_empty.
Qed.

Lemma sis_target_refines csix hrsH
  (RR: ris_refines ctx hrsH (sis_history csix)):
  WHEN tr_hrs_single hrsH (glue csix) ~> hrsHG THEN
  ris_refines ctx (ris_input_false hrsHG) (sis_target csix) /\
  ris_input_init hrsH = ris_input_init hrsHG /\
  (ris_ok ctx hrsHG -> ris_ok ctx hrsH).
Proof.
  intros hrsHG TR.
  split. 2:apply tr_hrs_single_rel in TR; split; apply TR.
  case (ris_ok_dec ctx hrsH) as [ROK|RKO].
  - apply (ok_ref ROK) in RR.
    eapply tr_hrs_single_spec in TR; eauto using tr_sis_total.
  - apply ris_refines_intro_not_ok.
    + intros ROK1; apply RKO.
      apply tr_hrs_single_rel in TR; apply TR.
      apply ris_input_false_ok; exact ROK1.
    + intros SOK1; apply RKO.
      apply sis_target_ok_sis_history in SOK1.
      apply RR; exact SOK1.
Qed.

Lemma sis_source_refines csix hrsH hrsHG
  (RR1: ris_refines ctx          hrsH           (sis_history csix))
  (RR2: ris_refines ctx (ris_input_false hrsHG) (sis_target  csix))
  (RINIT_EQ: ris_input_init hrsH = ris_input_init hrsHG)
  (OK_IMP: ris_ok ctx hrsHG -> ris_ok ctx hrsH):
  ris_refines ctx (ris_sreg_set hrsHG (ris_sreg hrsH)) (sis_source csix).
Proof.
  apply ris_refines_intro.
  - rewrite sis_source_ok_sis_target, <-ris_ok_set_sreg.
    rewrite ris_input_false_ok; apply RR2.
  - intros ROK SOK.
    apply ok_ref_si in RR1. 2:apply sis_source_ok_sis_history; exact SOK.
    apply ok_ref_si in RR2. 2:apply sis_source_ok_sis_target;  exact SOK.
    constructor.
    1:apply RR2.
    all:intro r; unfold ris_sreg_get; simpl; rewrite <- RINIT_EQ; apply RR1.
Qed.

End TrSingleSpec.
End TrSingle.


(** ** Combining trees of invariant (union) for jumptables *)

Definition most_defined_sv_imp (sv1 sv2: sval): ?? sval :=
  DO b <~ phys_eq sv1 sv2;;
  if b then RET sv1
  else FAILWITH "most_defined_sv_imp: no most defined symbolic value".

Lemma most_defined_sv_imp_determ: forall sv1 sv2,
  WHEN most_defined_sv_imp sv1 sv2 ~> sv3 THEN
  WHEN most_defined_sv_imp sv1 sv2 ~> sv3' THEN
  sv3 = sv3'.
Proof.
  unfold most_defined_sv_imp; wlp_simplify.
Qed.
Hint Resolve most_defined_sv_imp_determ: core.

Lemma most_defined_sv_imp_eq_and: forall a1 a2 : sval,
  WHEN PTree.f_imp_mostdef most_defined_sv_imp (Some a1) (Some a2) ~> oa
  THEN Some a1 = oa /\ Some a2 = oa.
Proof.
  wlp_simplify.
Qed.
Hint Resolve most_defined_sv_imp_eq_and: core.
Global Opaque most_defined_sv_imp.


Definition tr_sreg_union (sreg1 sreg2: PTree.t sval): ?? PTree.t sval :=
  PTree.combine_imp_mostdef most_defined_sv_imp sreg1 sreg2.

Lemma PTree_gcombine_mostdef_ok_spec [A] f (m1 m2 m3 : PTree.t A) i:
  PTree.combine_mostdef f m1 m2 = Some m3 ->
  PTree.f_mostdef f (m1 ! i) (m2 ! i) = Some (m3 ! i).
Proof.
  intro H; case PTree.f_mostdef eqn:F.
  - symmetry; f_equal. eapply PTree.gcombine_mostdef; eauto.
  - exfalso.
    case (m3 ! i) eqn:M3i.
    + eapply PTree.gcombine_mostdef_ok in H; eauto; congruence.
    + revert F; eapply PTree.gcombine_mostdef_none_rev in H as [-> ->]; eauto.
      discriminate 1.
Qed.

Definition equiv_subst_PTree ctx sis (freg sreg : PTree.t sval) : Prop :=
  forall r, option_rel (fun svf svr => sval_equiv ctx svr (sv_subst sis.(sis_smem) sis svf))
                       (freg ! r) (sreg ! r).

Lemma ris_refines_equiv_subst_PTree ctx sis hrs si
  (RINIT: ris_input_init hrs = false)
  (RR:    ris_refines_ok ctx hrs (tr_sis sis si false)):
  equiv_subst_PTree ctx sis si.(fpa_reg) hrs.(ris_sreg).
Proof.
  intros r.
  apply ris_refines_reg_simu with (r:=r) in RR; revert RR.
  simpl; unfold ris_sreg_get, si_apply; rewrite RINIT.
  do 2 autodestruct; inversion 3; constructor; auto.
Qed.

Lemma tr_sreg_union_spec ctx sis (freg1 freg2 : PTree.t sval) (sreg1 sreg2 : PTree.t sval)
  (S1: equiv_subst_PTree ctx sis freg1 sreg1)
  (S2: equiv_subst_PTree ctx sis freg2 sreg2):
  WHEN tr_sreg_union sreg1 sreg2 ~> sreg THEN
  exists freg,
    PTree.combine_mostdef (most_defined_sv ctx sis) freg1 freg2 = Some freg /\
    equiv_subst_PTree ctx sis freg sreg.
Proof.
  unfold tr_sreg_union; intros sreg RCb.
  case PTree.combine_mostdef as [freg|] eqn:SCb.
  - eexists; split. reflexivity.
    intro r.
    specialize (S1 r); specialize (S2 r).
    apply PTree_gcombine_mostdef_ok_spec with (i := r) in SCb; revert SCb; unfold PTree.f_mostdef.
    case (sreg ! r) as [y2|] eqn:Y2.
    + specialize (fun y1 Y1 => @PTree.gcombine_imp_mostdef_or_ok _ _ _ _ y1 y2 r _
                                              RCb most_defined_sv_imp_eq_and Y1 Y2) as y2_eq.
      destruct (freg1 ! r) as [x1|]; destruct (freg2 ! r) as [x2|].
      1:unfold most_defined_sv; autodestruct; intro EQ.
      all:injection 1 as <-; inversion S1; inversion S2; subst.
      all:try solve [erewrite <- y2_eq; eauto; constructor; assumption].
      * exfalso. exploit PTree.gcombine_imp_mostdef_none; eauto. congruence.
    + exploit PTree.gcombine_imp_mostdef_none_rev; eauto.
      intros (S1r & S2r); rewrite S1r in S1; rewrite S2r in S2.
      inversion S1; inversion S2; injection 1 as <-.
      constructor.
  - apply PTree.fcombine_mostdef in SCb as [].
    intros r ? ? F1r F2r; specialize (S1 r); specialize (S2 r); rewrite F1r in S1; rewrite F2r in S2.
    inversion S1; inversion S2.
    case (sreg ! r) as [y2|] eqn:Y2.
    + assert (y_eq: y = y0). {
        specialize (fun y1 Y1 => @PTree.gcombine_imp_mostdef_or_ok _ _ _ _ y1 y2 r _
                                              RCb most_defined_sv_imp_eq_and Y1 Y2) as y2_eq.
        transitivity y2; [|symmetry]; apply y2_eq; eauto.
      }
      rewrite <- y_eq in H4; rewrite H4 in H1.
      unfold most_defined_sv, symbolic_eq; rewrite <- H1.
      autodestruct; intros _; case (Values.Val.eq); simpl; congruence.
    + exploit PTree.gcombine_imp_mostdef_none_rev; eauto.
      intros (S1r & _); exfalso; rewrite S1r in S1; inversion S1.
Qed.
Global Opaque tr_sreg_union.


Definition tr_ris_union (hrsL hrsR : ristate) : ??ristate :=
  DO sreg <~ tr_sreg_union hrsL.(ris_sreg) hrsR.(ris_sreg);;
  let ok_rsval_u := hrsL.(ok_rsval) ++ hrsR.(ok_rsval) in
  RET (ris_sreg_ok_set hrsR sreg ok_rsval_u).

Lemma combine_tr_sis_ok_preserv ctx sis siL siR si
  (UNION2SI: union2_si ctx sis siL siR = Some si)
  (TRSIS_R : tr_sis_ok ctx sis siR)
  (TRSIS_L : tr_sis_ok ctx sis siL)
  :tr_sis_ok ctx sis si.
Proof.
  unfold tr_sis_ok, si_ok_subst, union2_si in *.
  destruct (union2_si_gen ctx sis siL siR); simpl in *; subst.
  destruct y as [FPAOK FPAREG].
  induction (fpa_reg siL) using PTree.tree_ind; induction (fpa_reg siR) using PTree.tree_ind; simpl.
  all:
    intros SOK sv HIN; rewrite FPAOK in HIN;
    apply in_app_or in HIN; destruct HIN; auto.
Qed.

Lemma tr_ris_union_spec ctx (siL siR : fpasv) (hrsL hrsR: ristate) sis
  (RINIT_R: ris_input_init hrsR = false)
  (RRR: ris_refines_ok ctx           hrsR         (tr_sis sis siR false))
  (RRL: ris_refines_ok ctx (ris_input_false hrsL) (tr_sis sis siL false)):
  WHEN tr_ris_union hrsL hrsR ~> hrs THEN
  exists si,
    union2_si ctx sis siL siR = Some si /\
    ris_refines_ok ctx hrs (tr_sis sis si false) /\
    ris_input_init hrs = false.
Proof.
  unfold tr_ris_union; wlp_seq; intros sreg RU.
  eapply tr_sreg_union_spec in RU as (freg & CB & EQV); cycle 1.
    { apply ris_refines_equiv_subst_PTree in RRL. exact RRL. reflexivity. }
    { apply ris_refines_equiv_subst_PTree; eauto. }
  set (u := union2_si ctx sis siL siR); specialize (eq_refl u); unfold u at 2 3, union2_si.
  case union2_si_gen as [[si|] Spec]; [|congruence]; simpl in *; subst u; intro U.
  exists si; split. reflexivity. split. 2:assumption.
  case Spec as [SI_OK SI_REG]; rewrite CB in SI_REG; injection SI_REG as SI_REG; rewrite SI_REG in EQV.
  inversion RRR; inversion RRL.
  constructor.
  4,5: intro r; simpl; unfold ris_sreg_get, si_apply; simpl; rewrite RINIT_R;
       specialize (EQV r).
  - constructor.
    + apply ROK.
    + apply Forall_app; split; [apply ROK0|apply ROK].
  - apply tr_sis_okpreserv in SOK as SIS_OK.
    apply combine_tr_sis_ok_preserv in U. 2,3:intros _; solve [apply SOK|apply SOK0].
    apply ok_tr_sis; auto.
  - apply MEM_EQ.
  - inversion EQV; split; congruence.
  - inversion EQV; simpl; auto.
Qed.
Global Opaque tr_ris_union.

(** ** Updating an hashed refined state by applying the invariant
       NOTE that unlike in theory ([si_sfv]), here we use the same function for
       every final value.
*)

Fixpoint tr_hrs_rec m (hrs: ristate) (lcsi: list csasv) {struct lcsi}: ?? ristate :=
  match lcsi with
  | nil => DO hrs' <~ tr_hrs_single m hrs csi_empty;; RET (ris_input_false hrs')
  | csi :: nil => DO hrs' <~ tr_hrs_single m hrs csi;; RET (ris_input_false hrs')
  | csi :: lcsi' =>
      DO hrsR <~ tr_hrs_rec m hrs lcsi';;
      DO hrsL <~ tr_hrs_single m hrs csi;;
      tr_ris_union hrsL hrsR
  end.

Lemma tr_hrs_rec_spec m lcsi ctx hrs sis
  (CTXM:  ctx_se_mode m ctx)
  (RINIT: ris_input_init hrs = true)
  (SIM:   se_ok_check m = true)
  (RR:    ris_refines_ok ctx hrs sis):
  WHEN tr_hrs_rec m hrs lcsi ~> hrs' THEN
  exists si,
    union_si ctx sis lcsi = Some si /\
    ris_refines_ok ctx hrs' (tr_sis sis si false) /\
    ris_input_init hrs' = false.
Proof.
  induction lcsi as [|csi lcsi']; simpl tr_hrs_rec.
    2:destruct lcsi' as [|hd1 tl1]; [|set (lcsi' := hd1 :: tl1) in *; clearbody lcsi'; clear hd1 tl1].
    all:simpl; wlp_seq.
  - intros hrs' TR.
    eapply tr_hrs_single_correct in TR; eauto.
    eexists; split. reflexivity. split. 2:reflexivity.
    eapply ris_refines_ok_morph. 2:exact TR.
    apply tr_sis_morph. apply fpa_eq_csi_si_empty.
  - intros hrs' TR.
    eapply tr_hrs_single_correct in TR; eauto.
    fold (union_si ctx sis [csi]).
    case (union_si ctx sis [csi]) as [si|] eqn:U.
    + exists si; split. reflexivity. split. 2:reflexivity.
      eapply ris_refines_ok_morph. 2:exact TR.
      apply tr_sis_morph. apply union_si_single in U. symmetry. exact U.
    + exfalso; revert U; unfold union_si, union2_si.
      destruct union2_si_gen as [[|]]; simpl in *. congruence.
      exploit PTree.fcombine_mostdef; eauto; intros.
      rewrite PTree.gEmpty in H0; discriminate.
  - intros hrsR TRR hrsL TRL hrs' U.
    apply IHlcsi' in TRR as (siR & -> & RRR & RINIT1).
    eapply tr_hrs_single_correct in TRL as RRL; eauto.
    eapply tr_ris_union_spec in U as (si & -> & RR' & RINIT2); eauto.
Qed.
Global Opaque tr_hrs_rec.


(** ** Verifying that the input state has [ris_input_init] set to [true] *)

Definition chk_input_init hrs: ?? unit :=
  if ris_input_init hrs then RET tt else FAILWITH "chk_input_init: input_init is false".

(** * Appying an union of invariants *)

Definition tr_hrs m (hrs: ristate) (lcsi: list csasv): ?? ristate :=
  DO _ <~ chk_input_init hrs;;
  tr_hrs_rec (se_mode_set_ok_check m) hrs lcsi.

Lemma tr_hrs_spec m lcsi ctx hrs sis
  (CTXM: ctx_se_mode m ctx)
  (RR:   ris_refines_ok ctx hrs sis):
  WHEN tr_hrs m hrs lcsi ~> hrs' THEN
  exists si,
    union_si ctx sis lcsi = Some si /\
    ris_refines_ok ctx hrs' (tr_sis sis si false).
Proof.
  unfold tr_hrs; wlp_simplify.
  eapply tr_hrs_rec_spec in Hexta as (si & U & RR1 & _); eauto.
Qed.
Global Opaque tr_hrs.


(** ** Applying invariant for a hashed refined state *)

Definition tr_sfv m hrs (gm_select: node -> csasv) sfv: ?? ristate :=
  match sfv with
  | Sgoto pc => tr_hrs m hrs [gm_select pc]
  | Scall sig svid args res pc =>
      if test_clobberable (gm_select pc) res then
        tr_hrs m hrs [(csi_remove res (gm_select pc))]
      else FAILWITH "ri_sfv: Scall res not clobberable"
  | Sbuiltin ef args bres pc =>
      match reg_builtin_res bres with
      | Some r =>
          if test_clobberable (gm_select pc) r then
            tr_hrs m hrs [(csi_remove r (gm_select pc))]
          else FAILWITH "ri_sfv: Sbuiltin res not clobberable"
      | None =>
          if test_csifreem (gm_select pc) then tr_hrs m hrs [gm_select pc]
          else FAILWITH "ri_sfv: Sbuiltin memory dependent"
      end
  | Stailcall sig svid args => tr_hrs m hrs []
  | Sreturn osv => tr_hrs m hrs []
  | Sjumptable sv lpc => tr_hrs m hrs (List.map (fun pc => gm_select pc) lpc)
  end.

Lemma tr_sfv_spec m ctx hrs sis gm sfv
  (CTXM: ctx_se_mode m ctx)
  (RR: ris_refines_ok ctx hrs sis):
  WHEN tr_sfv m hrs gm sfv ~> hrs' THEN
  exists si,
    si_sfv ctx sis gm sfv = Some si /\
    ris_refines_ok ctx hrs' (tr_sis sis si false).
Proof.
  unfold tr_sfv, si_sfv; repeat autodestruct;
  intros; try solve [wlp_simplify];
  intros hrs' TR;
  eapply tr_hrs_spec in TR as (si & U & RR'); eauto;
  eexists; (split; [reflexivity|]);
  apply union_si_single in U;
  (eapply ris_refines_ok_morph; [|exact RR']);
  apply tr_sis_morph; assumption.
Qed.
Global Opaque tr_sfv.


Fixpoint tr_hstate m (gm_select: node -> csasv) hst: ?? rstate :=
  match hst with
  | Sfinal hrs sfv =>
      DO hrs' <~ tr_sfv m hrs gm_select sfv;;
      RET (Sfinal hrs' sfv)
  | Scond cond args ifso ifnot =>
      DO ifso'  <~ tr_hstate m gm_select ifso;;
      DO ifnot' <~ tr_hstate m gm_select ifnot;;
      RET (Scond cond args ifso' ifnot')
  | Sabort => RET Sabort
  end.

Lemma tr_hstate_spec m rs ss ctx gm
  (CTXM: ctx_se_mode m ctx)
  (RR: rst_refines_ok ctx rs ss):
  WHEN tr_hstate m gm rs ~> rs' THEN
  rst_refines_ok ctx rs' (tr_sstate ctx gm ss).
Proof.
  revert ss RR; unfold rst_refines_ok.
  induction rs as [ris0 rfv0| |]; intros ss; simpl; inversion 1; wlp_seq.
  - intros hrs' TR. destruct st.
    eapply tr_sfv_spec in TR as (si & SI & RR1); eauto; simpl in *.
    erewrite get_soutcome_tr_sstate; eauto.
    erewrite <- si_sfv_morph, SI; eauto.
    simpl; constructor; simpl in *; auto.
  - intros ifso' TR0 ifnot' TR1; simpl.
    revert RR; case eval_scondition as [[]|].
    + intro; apply IHrs1; eauto.
    + intro; apply IHrs2; eauto.
    + inversion 1.
Qed.
Global Opaque tr_hstate.

(** Grouping together properties about the consistency
    of invariants application on a symbolic general state. *)
Record tr_hst_coherent ctx1 ctx2 (trss ss: sstate) (rst0 rst: rstate) sfv := {
  TRSS: trss = ss;
  TRSS_OK: trss_ok ctx1 ss;
  REF: rst_refines ctx2 rst ss;
  SYMB: exists sis, symb_exec_ok ctx2 ss sis sfv;
  OKIN: rst_ok_in ctx1 rst0 rst }.

Definition ctx_switch_prop ctx1 ctx2 :=
  exists ctx, (ctx1 = bcctx1 ctx) /\ (ctx2 = bcctx2 ctx).

Lemma ctx_switch_prop_refl ctx:
  ctx_switch_prop ctx ctx.
Proof.
  destruct ctx.
  unshelve eexists;cycle 1. 2:econstructor.
  unfold bcctx1, bcctx2; simpl; split; reflexivity.
  reflexivity.
Qed.

Lemma get_soutcome_preserved [S] ctx (s : pstate S):
  get_soutcome (bcctx1 ctx) s = get_soutcome (bcctx2 ctx) s.
Proof.
  induction s; simpl; try reflexivity.
  rewrite eval_scondition_preserved; do 2 autodestruct; auto.
Qed.

Lemma rst_refines_ok_preserved ctx rs ss:
  rst_refines_ok (bcctx1 ctx) rs ss <-> rst_refines_ok (bcctx2 ctx) rs ss.
Proof.
  unfold rst_refines_ok; do 2 rewrite <- get_soutcome_preserved.
  split; inversion 1; (constructor; [apply ris_refines_ok_preserved|apply sfv_simu_preserved]); assumption.
Qed.

Theorem tr_hstate_correct m hst1_E ctx1 ctx2 gm_select ss1E sis1 sfv1
  (CTXM: ctx_se_mode m ctx2)
  (SOUT: get_soutcome ctx1 ss1E = Some {| _sis := sis1; _sfv := sfv1 |})
  (SOK : sis_ok ctx1 sis1)
  (REF1E: rst_refines ctx1 hst1_E ss1E)
  (HCTX: ctx_switch_prop ctx1 ctx2):
  WHEN tr_hstate m gm_select hst1_E ~> hst1_EI THEN
  exists ss1EI,
    tr_hst_coherent ctx1 ctx2
      (tr_sstate ctx2 (fun pc => gm_select pc) ss1E) ss1EI hst1_E hst1_EI sfv1.
Proof.
  intros hst1_EI TR.
  case HCTX as (ctx & ? & ?); subst.
  eapply ok_rst_ref_si in REF1E; eauto.
  apply rst_refines_ok_preserved in REF1E as REF1E'.
  eapply tr_hstate_spec with (ctx := bcctx2 ctx) in TR; eauto.
  inversion TR as [[ris2 rfv2] [sis2 sfv2]].
  assert (sfv2 = sfv1). {
    rewrite get_soutcome_preserved in SOUT.
    eapply get_soutcome_tr_sstate in SOUT; rewrite <- H0 in SOUT; revert SOUT.
    case si_sfv as [|]; simpl; congruence.
  }
  subst sfv2.
  eexists; constructor. reflexivity.
  - eexists sis2, sfv1; split.
    + rewrite H0, get_soutcome_preserved. reflexivity.
    + apply sis_ok_preserved. apply RST_RR.
  - apply wrst_ref. exact TR.
  - unfold symb_exec_ok. exists sis2; split.
    + symmetry; exact H0.
    + apply RST_RR.
  - intros _ _ _. inversion REF1E; eexists; split. reflexivity. apply RST_RR0.
Qed.

End SymbolicInvariants.
