open Maps
open BTL
open Registers
open BTLtypes
open RTLcommonaux
open Datatypes
open BTL_Invariants

(** Ghostfields related functions *)

let undef_node = -1

let mk_iinfo _inumb _opt_info =
  {
    inumb = _inumb;
    opt_info = _opt_info;
    visited = false;
    liveins = Regset.empty;
  }

let def_iinfo () =
  {
    inumb = undef_node;
    opt_info = None;
    visited = false;
    liveins = Regset.empty;
  }

let mk_binfo _bnumb _input_regs _s_output_regs _successors _predecessors =
  {
    bnumb = _bnumb;
    visited = false;
    input_regs = _input_regs;
    s_output_regs = _s_output_regs;
    successors = _successors;
    predecessors = _predecessors;
  }

let def_binfo () =
  {
    bnumb = undef_node;
    visited = false;
    input_regs = Regset.empty;
    s_output_regs = Regset.empty;
    successors = [];
    predecessors = [];
  }

let mk_ibinfo _entry _binfo = { entry = _entry; binfo = _binfo }
let mk_finfo _typing _bb_mode = { typing = _typing; bb_mode = _bb_mode }

let mk_function _fn_info _fn_gm _fn_sig _fn_params _fn_stacksize _fn_code
    _fn_entrypoint =
  {
    fn_info = _fn_info;
    fn_gm = _fn_gm;
    fn_sig = _fn_sig;
    fn_params = _fn_params;
    fn_stacksize = _fn_stacksize;
    fn_code = _fn_code;
    fn_entrypoint = _fn_entrypoint;
  }

(** Auxiliary functions on BTL *)

let pget p t = get_some @@ PTree.get p t

let reset_visited_ibf btl =
  List.iter (fun (n, ibf) -> ibf.binfo.visited <- false) (PTree.elements btl)

let apply_over_function btl tf =
  List.iter (fun (n, ibf) -> tf n ibf) (PTree.elements btl)

let tf_ibf_ib f n ibf = f n ibf.entry

let rec reset_visited_ib_rec pc = function
  | Bseq (ib1, ib2) ->
      reset_visited_ib_rec pc ib1;
      reset_visited_ib_rec pc ib2
  | Bcond (_, _, ib1, ib2, iinfo) ->
      reset_visited_ib_rec pc ib1;
      reset_visited_ib_rec pc ib2;
      iinfo.visited <- false
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, _, iinfo)
  | BF (_, iinfo) ->
      iinfo.visited <- false
  | _ -> ()

let get_visited = function
  | Bcond (_, _, _, _, iinfo)
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, _, iinfo)
  | BF (_, iinfo) ->
      iinfo.visited
  | _ -> failwith "get_visited: invalid iblock"

let jump_visit = function
  | Bcond (_, _, _, _, iinfo)
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, _, iinfo)
  | BF (_, iinfo) ->
      if iinfo.visited then true
      else (
        iinfo.visited <- true;
        false)
  | Bseq (_, _) -> false
  | Bnop None -> true

let rec get_inumb skip_goto = function
  | BF (Bgoto s, iinfo) -> if skip_goto then p2i s else iinfo.inumb
  | BF (_, iinfo)
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, _, iinfo)
  | Bcond (_, _, _, _, iinfo) ->
      iinfo.inumb
  | Bseq (ib1, _) -> get_inumb skip_goto ib1
  | _ -> failwith "get_inumb: Bnop None, invalid iblock"

let get_inumb_succ btl skip_goto s = get_inumb skip_goto (pget s btl).entry

let rec successors_instr_btl = function
  | Bseq (ib1, ib2) | Bcond (_, _, ib1, ib2, _) ->
      successors_instr_btl ib1 @ successors_instr_btl ib2
  | BF (Bgoto s, _)
  | BF (Bcall (_, _, _, _, s), _)
  | BF (Bbuiltin (_, _, _, s), _) ->
      [ s ]
  | BF (Bjumptable (_, tbl), _) -> tbl
  | _ -> []

let successors_block ibf =
  let ib = ibf.entry in
  successors_instr_btl ib

let change_final_successor_btl old_s new_s = function
  | BF (Bgoto _, iinfo) -> BF (Bgoto new_s, iinfo)
  | BF (Bcall (s, rind, lr, dst, _), iinfo) ->
      BF (Bcall (s, rind, lr, dst, new_s), iinfo)
  | BF (Bbuiltin (ef, blr, bdst, _), iinfo) ->
      BF (Bbuiltin (ef, blr, bdst, new_s), iinfo)
  | BF (Bjumptable (arg, tbl), iinfo) ->
      let tbl' = List.map (fun s -> if s = old_s then new_s else s) tbl in
      BF (Bjumptable (arg, tbl'), iinfo)
  | _ -> failwith "change_successor_btl: no successors to change"

(** Form a list containing both sources and destination regs of a block *)
let get_regindent = function Coq_inr _ -> [] | Coq_inl r -> [ r ]

let rec get_regs_ib = function
  | Bnop _ -> []
  | Bop (_, args, dest, _) -> dest :: args
  | Bload (_, _, _, args, _, dest, _) -> dest :: args
  | Bstore (_, _, args, _, src, _) -> src :: args
  | Bcond (_, args, ib1, ib2, _) -> get_regs_ib ib1 @ get_regs_ib ib2 @ args
  | Bseq (ib1, ib2) -> get_regs_ib ib1 @ get_regs_ib ib2
  | BF (Breturn (Some r), _) -> [ r ]
  | BF (Bcall (_, t, args, dest, _), _) -> dest :: (get_regindent t @ args)
  | BF (Btailcall (_, t, args), _) -> get_regindent t @ args
  | BF (Bbuiltin (_, args, dest, _), _) ->
      AST.params_of_builtin_res dest @ AST.params_of_builtin_args args
  | BF (Bjumptable (arg, _), _) -> [ arg ]
  | _ -> []

let find_last_reg_btl elts =
  reg := 1;
  let rec find_last_reg_btl_rec = function
    | [] -> ()
    | (pc, ibf) :: k ->
        traverse_list reg (get_regs_ib ibf.entry);
        find_last_reg_btl_rec k in
  find_last_reg_btl_rec elts

let plist2IS l = OrdIS.of_list (List.map p2i l)

(** Invariants map related functions *)

let mk_ir _force_input _regof = { force_input = _force_input; regof = _regof }
let empty_csasv () = { aseq = []; outputs = Regset.empty }

let get_inv_in_gm_or_def n gm =
  match PTree.get n gm with
  | Some inv -> inv
  | None -> { history = empty_csasv (); glue = empty_csasv () }

let add_outputs_in_gm n gm outputs inv_type =
  let inv = get_inv_in_gm_or_def n gm in
  let inv =
    match inv_type with
    | Gluing ->
        {
          history = inv.history;
          glue =
            {
              aseq = inv.glue.aseq;
              outputs = Regset.union inv.glue.outputs outputs;
            };
        }
    | History ->
        {
          glue = inv.glue;
          history =
            {
              aseq = inv.history.aseq;
              outputs = Regset.union inv.history.outputs outputs;
            };
        }
  in
  PTree.set n inv gm

let add_aseq_in_gm n gm dst iv inv_type =
  let inv = get_inv_in_gm_or_def n gm in
  let inv =
    match inv_type with
    | Gluing ->
        {
          history = inv.history;
          glue =
            { aseq = (dst, iv) :: inv.glue.aseq; outputs = inv.glue.outputs };
        }
    | History ->
        {
          glue = inv.glue;
          history =
            {
              aseq = (dst, iv) :: inv.history.aseq;
              outputs = inv.history.outputs;
            };
        }
  in
  PTree.set n inv gm

let build_liveness_invariants elts gm =
  let gm = ref gm in
  List.iter
    (fun (n, ibf) -> gm := add_outputs_in_gm n !gm ibf.binfo.input_regs Gluing)
    elts;
  !gm
