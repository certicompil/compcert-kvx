Require Import Coqlib AST Values Integers Memory.
Require Import RelationClasses.
Require Import OptionMonad.
Require Import Op.
Require Import BTL BTL_SEtheory BTL_SEsimuref.
Require Import ValueDomain.

Import SvalNotations.
Import ListNotations.
Local Open Scope option_monad_scope.
Local Open Scope lazy_bool_scope.


Ltac replace_by x y tc :=
  match goal with |- context G[x] =>
      let Gx := context G[x] in
      let Gy := context G[y] in
      replace Gx with Gy; [|tc]
  end.

Section IsIndexedAddr.

  Definition is_indexed_addr (addr : addressing) (ofs : ptrofs) : Prop :=
    forall F V (genv: Globalenvs.Genv.t F V) (sp: val) (args: list val)
           (rb : Values.block) (rofs : ptrofs),
    eval_addressing genv sp addr args = Some (Vptr rb rofs) <->
    exists bofs, args = Vptr rb bofs :: nil /\ rofs = Ptrofs.add bofs ofs.

  Local Obligation Tactic := simpl; intros.
  Local Transparent Archi.ptr64.
  Program Definition test_is_indexed (addr : addressing):
    {ofs : ptrofs | is_indexed_addr addr ofs} + {True} :=
    match addr with
    | Aindexed iofs => inleft (exist _ _ _)
    | _ => inright _
    end.
  Next Obligation.
    (* Depending on the architecture, we may need to convert [iofs].
       An alternative to this test would be to define [test_is_indexed] for each architecture. *)
    solve [exact iofs
          |exact (Ptrofs.of_int   iofs)
          |exact (Ptrofs.of_int64 iofs)
          |exact (if Archi.ptr64
                  then Ptrofs.of_int64 (Int64.repr iofs)
                  else Ptrofs.of_int   (Int.repr   iofs))].
  Defined.
  Local Obligation Tactic := program_simpl.
  Solve All Obligations.
  Next Obligation.
    unfold test_is_indexed_obligation_1, is_indexed_addr, eval_addressing;
      try unfold Archi.ptr64;
      try unfold eval_addressing32, eval_addressing64;
      unfold Val.offset_ptr, Val.add, Val.addl;
      intros.
    split.
    2:intros [bofs [-> ->]]; reflexivity.
    repeat autodestruct; repeat first [injection 1 as <- <- | intro]; eauto.
  Qed.
  Local Opaque Archi.ptr64.
  Global Opaque test_is_indexed.

  Lemma is_indexed_addr_case [addr ofs] (ADDR : is_indexed_addr addr ofs)
    [F V] (genv: Globalenvs.Genv.t F V) sp args
    :
    (exists b bofs,
      eval_addressing genv sp addr args = Some (Vptr b (Ptrofs.add bofs ofs)) /\
      args = Vptr b bofs :: nil) \/
    (match eval_addressing genv sp addr args with
     | Some (Vptr b rofs) => False
     | _ => True
     end).
  Proof.
    specialize (ADDR F V genv sp args). revert ADDR.
    destruct (eval_addressing genv sp addr args) as [[]|]; simpl; eauto.
    intros [[bofs EQ] _]. reflexivity.
    case EQ as [-> ->]; eauto.
  Qed.

  Lemma is_indexed_addr_eval [addr ofs] (ADDR : is_indexed_addr addr ofs)
    [F V] (genv: Globalenvs.Genv.t F V) sp b bofs:
    eval_addressing genv sp addr (Vptr b bofs :: nil) = Some (Vptr b (Ptrofs.add bofs ofs)).
  Proof.
    eapply proj2 in ADDR. rewrite ADDR; eauto.
  Qed.

End IsIndexedAddr.

(*** Semantic justification of the rules *)

Section LemmasMemory. (* -- Some additionnal properties of Mem *)

  Lemma store_success_iff chk m b ofs v:
    Mem.store chk m b ofs v <> None <-> Mem.valid_access m chk b ofs Writable.
  Proof.
    split; intro H.
    - case Mem.store as [|] eqn:STORE. 2:congruence.
      eapply Mem.store_valid_access_3. eauto.
    - eapply Mem.valid_access_store in H as [? ->].
      congruence.
  Qed.

  Lemma store_fail_iff chk m b ofs v:
    Mem.store chk m b ofs v = None <-> ~Mem.valid_access m chk b ofs Writable.
  Proof.
    rewrite <- (not_iff_compat (store_success_iff chk m b ofs v)).
    destruct Mem.store; split; intro H1; try congruence.
    exfalso. apply H1. intro. congruence.
  Qed.
  
  Lemma load_success_iff chk m b ofs:
    Mem.load chk m b ofs <> None <-> Mem.valid_access m chk b ofs Readable.
  Proof.
    split; intro H.
    - case Mem.load as [|] eqn:LOAD. 2:congruence.
      eapply Mem.load_valid_access. eauto.
    - eapply Mem.valid_access_load in H as [? ->].
      congruence.
  Qed.

  Lemma perm_store_iff [chunk m1 b ofs v m2]:
    Mem.store chunk m1 b ofs v = Some m2 ->
    forall (b' : Values.block) (ofs' : Z) (k : perm_kind) (p : permission),
    Mem.perm m1 b' ofs' k p <-> Mem.perm m2 b' ofs' k p.
  Proof.
    split; [eapply Mem.perm_store_1|eapply Mem.perm_store_2]; eassumption.
  Qed.

  Lemma store_valid_access_iff [chunk m1 b ofs v m2]:
    Mem.store chunk m1 b ofs v = Some m2 ->
    forall (chunk' : memory_chunk) (b' : Values.block) (ofs' : Z) (p : permission),
    Mem.valid_access m1 chunk' b' ofs' p <-> Mem.valid_access m2 chunk' b' ofs' p.
  Proof.
    split; [eapply Mem.store_valid_access_1|eapply Mem.store_valid_access_2]; eassumption.
  Qed.

  Section Equiv_on.
    Variable P : Values.block -> Z -> Prop.

    Record mem_equiv_on (m0 m1 : mem) : Prop :=
      mk_mem_equiv_on {
        EQ_PERM : forall b ofs k p, Mem.perm m0 b ofs k p <-> Mem.perm m1 b ofs k p;
        EQ_CTNT : forall b ofs, P b ofs ->
                  Maps.ZMap.get ofs (Maps.PMap.get b (Mem.mem_contents m0)) =
                  Maps.ZMap.get ofs (Maps.PMap.get b (Mem.mem_contents m1))
      }.

    Lemma mem_equiv_on_valid_access [m0 m1]:
      mem_equiv_on m0 m1 ->
      forall chk b ofs p,
      Mem.valid_access m0 chk b ofs p <-> Mem.valid_access m1 chk b ofs p.
    Proof.
      intros [PM _]. intros. unfold Mem.valid_access, Mem.range_perm.
      setoid_rewrite PM.
      reflexivity.
    Qed.

    Lemma ZMap_setN_preserve_eq_on ofs vl:
      forall ofs' c0 c1,
      Maps.ZMap.get ofs c0 = Maps.ZMap.get ofs c1 ->
      Maps.ZMap.get ofs (Mem.setN vl ofs' c0) = Maps.ZMap.get ofs (Mem.setN vl ofs' c1).
    Proof.
      induction vl; simpl; intros.
      - assumption.
      - apply IHvl.
        rewrite !Maps.ZMap.gsspec.
        case Maps.ZIndexed.eq as []; auto.
    Qed.

    Lemma store_morph_equiv_on chk m0 m1 b ofs v m0' m1':
      mem_equiv_on m0 m1 ->
      Mem.store chk m0 b ofs v = Some m0' ->
      Mem.store chk m1 b ofs v = Some m1' ->
      mem_equiv_on m0' m1'.
    Proof.
      intros [PM CT] S0 S1. constructor.
      - intros.
        rewrite <- (perm_store_iff S0), <- (perm_store_iff S1).
        apply PM.
      - intros b' ofs' HP.
        apply CT in HP as CT_EQ.
        apply Mem.store_mem_contents in S0 as ->, S1 as ->.
        rewrite !Maps.PMap.gsspec.
        case peq as [<-|]. 2:apply CT_EQ.
        apply ZMap_setN_preserve_eq_on. exact CT_EQ.
    Qed.

    Lemma store_equiv_on chk m b ofs v m':
      Mem.store chk m b ofs v = Some m' ->
      (forall i : Z, ofs <= i < ofs + size_chunk chk -> ~ P b i) ->
      mem_equiv_on m m'.
    Proof.
      intros HS HP. constructor.
      - eapply perm_store_iff. eassumption.
      - intros b' ofs' P'.
        apply Mem.store_mem_contents in HS. rewrite HS.
        rewrite Maps.PMap.gsspec.
        destruct peq. 2:reflexivity. subst b'.
        symmetry. apply Mem.setN_outside.
        rewrite encode_val_length, <- size_chunk_conv.
        destruct (zlt ofs' ofs); auto.
        destruct (zlt ofs' (ofs + size_chunk chk)); auto.
        elim (HP ofs'). lia. auto.
    Qed.

    Lemma load_equiv_on m0 m1 chk b ofs:
      mem_equiv_on m0 m1 ->
      (forall i, ofs <= i < ofs + size_chunk chk -> P b i) ->
      Mem.load chk m0 b ofs = Mem.load chk m1 b ofs.
    Proof.
      intros EQ HP.
      specialize (load_success_iff chk m0 b ofs).
        rewrite (mem_equiv_on_valid_access EQ).
        rewrite <- load_success_iff.
      case (Mem.load chk m0 b ofs) as [v0|] eqn:L0, (Mem.load chk m1 b ofs) as [v1|] eqn:L1; cycle 1;
        try solve [reflexivity | intros [F0 F1]; exfalso; (apply F0 + apply F1); congruence].
      intros _; apply Mem.load_result in L0 as ->, L1 as ->; do 2 f_equal.
      apply Mem.getN_exten; rewrite <- size_chunk_conv.
      intros; apply EQ; auto.
    Qed.
  End Equiv_on.

  Global Instance mem_equiv_on_Equivalence {P}:
    Equivalence (mem_equiv_on P).
  Proof.
    constructor.
    - constructor; reflexivity.
    - intros m0 m1 H. constructor; symmetry; apply H; auto.
    - intros m0 m1 m2 H0 H1. constructor; (etransitivity; [apply H0|apply H1]); auto.
  Qed.

End LemmasMemory.

Record address := mk_addr {
  a_chunk: memory_chunk;
  a_addr:  addressing;
  a_args:  list_sval;
  a_aval:  aval
}.

Definition eval_address (ctx : iblock_common_context) (a : address): option (Values.block * ptrofs) :=
  SOME args <- eval_list_sval ctx (a_args a) IN
  SOME v    <- eval_addressing (cge ctx) (csp ctx) (a_addr a) args IN
  ASSERT (vmatch_opt_dec (ctx_bc ctx) v (a_aval a)) IN
  match v with
  | Vptr b o => Some (b, o)
  | _ => None
  end.

Section DisjointnessCriteria.

  (* Semantic definition *)

  Definition vaddr_disjoint (v0 : Values.block * ptrofs) (sz0 : Z)
                            (v1 : Values.block * ptrofs) (sz1 : Z): Prop :=
    let (b0, ofs0) := v0 in
    let (b1, ofs1) := v1 in
    b0 <> b1 \/
    Ptrofs.unsigned ofs0 + sz0 <= Ptrofs.unsigned ofs1 \/
    Ptrofs.unsigned ofs1 + sz1 <= Ptrofs.unsigned ofs0.

  Definition addr_disjoint (ctx : iblock_common_context) (a0 a1 : address) :=
    if_Some (eval_address ctx a0) (fun v0 =>
    if_Some (eval_address ctx a1) (fun v1 =>
    vaddr_disjoint v0 (size_chunk (a_chunk a0)) v1 (size_chunk (a_chunk a1)))).


  (* The syntactic criteria for the disjointness of two chunk relative to a same base.
     This should be the one used by -fprepass-alias-rel *)

  Let u := Ptrofs.unsigned.

  Definition offset_chunk_lt (ofs0 : ptrofs) (chk0 : memory_chunk)
                             (ofs1 : ptrofs) (chk1 : memory_chunk)
    := (u ofs0 + size_chunk chk0) <= u ofs1 /\
       (u ofs1 + size_chunk chk1) <= u ofs0 + Ptrofs.modulus.

  Definition offset_disjoint (ofs0 : ptrofs) (chk0 : memory_chunk)
                                   (ofs1 : ptrofs) (chk1 : memory_chunk)
    := offset_chunk_lt ofs0 chk0 ofs1 chk1 \/ offset_chunk_lt ofs1 chk1 ofs0 chk0.

  Lemma disjoint_interval_modulo (m : Z) (a b : Z) (al bl : Z) (c : Z):
    0 < m ->
    0 <= al -> 0 <= bl ->
    a + al <= b ->
    b + bl <= a + m ->
    (c + a) mod m + al <= (c + b) mod m \/ (c + b) mod m + bl <= (c + a) mod m.
  Proof.
    intros Hmpos Le0 Le1 Le2 Le3.
    rewrite !Z.mod_eq by lia.
    case (Z_le_lt_eq_dec ((c + a) / m) ((c + b) / m)) as [Wrap|NoWrap].
      { apply Z.div_le_mono; lia. }
    - right.
      assert (Le: (c + a) / m + 1 <= (c + b) / m) by lia.
      apply Zmult_le_compat_l with (p:=m) in Le; lia.
    - left. lia.
  Qed.

  Lemma offset_disjoint_computed_addr (bo : Z) ofs0 chk0 ofs1 chk1:
    offset_disjoint ofs0 chk0 ofs1 chk1 ->
    (bo + u ofs0) mod Ptrofs.modulus + size_chunk chk0 <= (bo + u ofs1) mod Ptrofs.modulus \/
    (bo + u ofs1) mod Ptrofs.modulus + size_chunk chk1 <= (bo + u ofs0) mod Ptrofs.modulus.
  Proof.
    specialize Ptrofs.modulus_pos as ?.
    specialize (size_chunk_pos chk0) as ?. specialize (size_chunk_pos chk1) as ?.
    intros [Lt|Lt]; [|apply or_comm];
      apply disjoint_interval_modulo; solve [apply Lt|lia].
  Qed.

  Lemma offset_disjoint_sound ctx a0 ofs0 a1 ofs1
    (A0: is_indexed_addr (a_addr a0) ofs0)
    (A1: is_indexed_addr (a_addr a1) ofs1)
    (B: list_sval_equiv ctx (a_args a0) (a_args a1))
    (D: offset_disjoint ofs0 (a_chunk a0) ofs1 (a_chunk a1)):
    addr_disjoint ctx a0 a1.
  Proof.
    unfold is_indexed_addr in A0, A1.
    unfold addr_disjoint, eval_address; rewrite <- B.
    repeat (autodestruct; simpl; try trivial); intros; right.
    eapply proj1 in A0, A1.
    exploit A0. eassumption.
    exploit A1. eassumption.
    intros (cofs1 & B1 & ->) (cofs0 & B0 & ->).
    replace cofs1 with cofs0 by congruence.
    unfold Ptrofs.add. rewrite !Ptrofs.unsigned_repr_eq. fold u.
    apply offset_disjoint_computed_addr; assumption.
  Qed.

  (* Static criteria using the result of the abstract analysis, -fprepass-alive-abs.
     Valid only for the strict semantics. *)

  Lemma abs_disjoint_sound ctx a0 a1
    (S: ctx_bc ctx <> None)
    (D: vdisjoint (a_aval a0) (size_chunk (a_chunk a0))
                  (a_aval a1) (size_chunk (a_chunk a1)) = true):
    addr_disjoint ctx a0 a1.
  Proof.
    unfold addr_disjoint, eval_address.
    destruct ctx_bc as [bc|]; [simpl|congruence].
    repeat (autodestruct; simpl; trivial; intro).
    eapply vdisjoint_sound; eauto.
  Qed.

End DisjointnessCriteria.

Section RewritingRules.

(* This is a particular case of rewrite_load_equiv_on + disjoint_store_smem_equiv_on *)
Lemma rewrite_load_store ctx (sm : smem) (trap : trapping_mode) (chk0 chk1 : memory_chunk)
  (addr0 addr1 : addressing) (args0 args1 : list_sval) (sv1 : sval) aaddr0 sinfo1 hc0 hc1
  (DJ : addr_disjoint ctx (mk_addr chk0 addr0 args0 aaddr0)
                          (mk_addr chk1 addr1 args1 (store_aaddr sinfo1))):
  let sm' := Sstore sm chk1 addr1 args1 sinfo1 sv1 hc0 in
  alive (eval_smem ctx sm') ->
  sval_equiv ctx
    (Sload  sm' trap chk0 addr0 args0 aaddr0 hc1)
    (fSload sm  trap chk0 addr0 args0 aaddr0).
Proof.
  simpl.
  intro MALIVE; revert MALIVE DJ.
  unfold addr_disjoint, eval_address, Mem.storev; simpl.
  do 6 (autodestruct; intro); simpl.
  case Mem.store as [m1|] eqn:STORE; [intros _|congruence].
  do 4 (autodestruct; intro); simpl; intro DJ.
  set (load1 := Mem.load _ m1 _ _). set (load0 := Mem.load _ m  _ _).
  replace load0 with load1. reflexivity.
  eapply Mem.load_store_other; eauto.
Qed.

Lemma rewrite_store_store ctx (sm : smem) (chk0 chk1 : memory_chunk)
  addr0 addr1 (args0 args1 : list_sval) (sv0 sv1 : sval) sinfo0 sinfo1 hc0 hc1
  (DJ : addr_disjoint ctx (mk_addr chk0 addr0 args0 (store_aaddr sinfo0))
                          (mk_addr chk1 addr1 args1 (store_aaddr sinfo1))):
  smem_equiv ctx
    (Sstore  (Sstore  sm chk1 addr1 args1 sinfo1 sv1 hc1) chk0 addr0 args0 sinfo0 sv0 hc0)
    (fSstore (fSstore sm chk0 addr0 args0 sinfo0 sv0    ) chk1 addr1 args1 sinfo1 sv1).
Proof.
  revert DJ; simpl; unfold addr_disjoint, eval_address, Mem.storev; simpl.
  repeat autodestruct.
  2,3:
    repeat (let H := fresh "H" in intro H; try apply store_fail_iff in H);
    try ((idtac + symmetry); apply store_fail_iff);
    eauto using Mem.store_valid_access_2.
  simpl; intros.
  eapply Mem.store_store_other; eauto.
Qed.

(* Rules to rewrite the stores under a load, possibly under other stores *)

Inductive smem_equiv_on (P : Values.block -> Z -> Prop) (ctx : iblock_common_context) (sm0 sm1 : smem)
  : Prop
  := Smem_equiv_on m0 m1
      (EVAL_M0 : eval_smem ctx sm0 = Some m0)
      (EVAL_M1 : eval_smem ctx sm1 = Some m1)
      (EQ_ON : mem_equiv_on P m0 m1).

Lemma smem_equiv_on_refl P ctx sm:
  alive (eval_smem ctx sm) ->
  smem_equiv_on P ctx sm sm.
Proof.
  case eval_smem as [m|] eqn:M. 2:congruence.
  exists m m; auto. reflexivity.
Qed.

Lemma smem_equiv_on_sym P ctx sm0 sm1:
  smem_equiv_on P ctx sm0 sm1 ->
  smem_equiv_on P ctx sm1 sm0.
Proof.
  intros []. econstructor; eauto. symmetry; assumption.
Qed.

Lemma smem_equiv_on_trans P ctx sm0 sm1 sm2:
  smem_equiv_on P ctx sm0 sm1 ->
  smem_equiv_on P ctx sm1 sm2 ->
  smem_equiv_on P ctx sm0 sm2.
Proof.
  intros [m0 m1 E0 E1 EQ0] [m1' m2 E1' E2 EQ1].
  rewrite E1 in E1'. injection E1' as ?. subst m1'.
  exists m0 m2; eauto.
  etransitivity; eassumption.
Qed.

Lemma smem_equiv_on_morph_smem_equiv P ctx sm0 sm1 sm0' sm1':
  smem_equiv_on P ctx sm0 sm1 ->
  smem_equiv ctx sm0 sm0' ->
  smem_equiv ctx sm1 sm1' ->
  smem_equiv_on P ctx sm0' sm1'.
Proof.
  intros [] E0 E1.
  exists m0 m1.
  1:rewrite <- E0. 2:rewrite <- E1.
  all:assumption.
Qed.

Inductive addr_range (ctx : iblock_common_context) (a : address)
                     (b : Values.block) (i : Z) : Prop
  := Load_range ofs
      (EVAL_ADDR: eval_address ctx a = Some (b, ofs))
      (RANGE: Ptrofs.unsigned ofs <= i < Ptrofs.unsigned ofs + size_chunk (a_chunk a)).

Lemma rewrite_load_equiv_on ctx sm0 sm1 trap chk addr sargs aaddr hc0 hc1:
  let a := mk_addr chk addr sargs aaddr in
  smem_equiv_on (addr_range ctx a) ctx sm0 sm1 ->
  sval_equiv ctx (Sload sm0 trap chk addr sargs aaddr hc0)
                 (Sload sm1 trap chk addr sargs aaddr hc1).
Proof.
  intros a [].
  specialize (eq_refl (eval_address ctx a)); set (a_v := eval_address ctx a) at 1.
  simpl; unfold a, eval_address, Mem.loadv; rewrite EVAL_M0, EVAL_M1; simpl.
  do 4 (autodestruct; intro); intro a_eval; subst a_v.
  set (load1 := Mem.load _ m1 _ _). set (load0 := Mem.load _ m0 _ _).
  replace load1 with load0. reflexivity.
  eapply load_equiv_on. eassumption.
  intros; eexists; eauto.
Qed.

Lemma apply_store_smem_equiv_on P ctx sm0 sm1 chk addr sargs sinfo sv hc0 hc1:
  smem_equiv_on P ctx sm0 sm1 ->
  let sm0' := Sstore sm0 chk addr sargs sinfo sv hc0 in
  let sm1' := Sstore sm1 chk addr sargs sinfo sv hc1 in
  alive (eval_smem ctx sm0') ->
  smem_equiv_on P ctx sm0' sm1'.
Proof.
  intros [m0 m1 M0 M1 EM] sm0' sm1'.
  case (eval_smem ctx sm0') as [m0'|] eqn:M0'. 2:congruence. intros _.
  revert M0'. simpl. unfold Mem.storev. rewrite M0.
  repeat autodestruct. intros _ EQ_SV EQ_VM EQ_AD EQ_AG M0'.
  apply Mem.store_valid_access_3 in M0' as A0.
  apply (mem_equiv_on_valid_access _ EM) in A0.
  eapply Mem.valid_access_store in A0 as [m1' M1'].
  exists m0' m1'.
  - simpl. rewrite EQ_AG, EQ_AD, EQ_VM, M0, EQ_SV. apply M0'.
  - simpl. rewrite EQ_AG, EQ_AD, EQ_VM, M1, EQ_SV. apply M1'.
  - eapply store_morph_equiv_on; eauto.
Qed.

Lemma disjoint_store_smem_equiv_on ctx a sm0 chk addr sargs sinfo sv hc
  (DJ : addr_disjoint ctx a (mk_addr chk addr sargs (store_aaddr sinfo))):
  let range := addr_range ctx a                      in
  let sm1   := Sstore sm0 chk addr sargs sinfo sv hc in
  forall (MALIVE: alive (eval_smem ctx sm1)),
  smem_equiv_on range ctx sm0 sm1.
Proof.
  intros ? ? MALIVE; revert MALIVE DJ.
  simpl; unfold addr_disjoint, Mem.storev, Val.offset_ptr, Val.addl, eval_address at 2; simpl.
  do 6 (autodestruct; intro); simpl.
  case Mem.store as [m1|] eqn:STORE; [intros _|congruence].
  eexists _ m1; eauto.
  - simpl. rewrite EQ, EQ0, EQ1, EQ2, EQ3. apply STORE.
  - eapply store_equiv_on. eassumption.
    intros i' RANGE0 [ofs EVAL_ADDR RANGE1].
    rewrite EVAL_ADDR in DJ; case DJ as [|DJ]; [congruence|].
    lia.
Qed.

End RewritingRules.

(*** Implementation of the disjointness criteria *)

Section DisjointnessTest.

(* Structural equality test on sval, ignoring the hashcodes *)

Program
Fixpoint sval_eqv_dec (v0 v1 : sval)
  : {forall ctx, sval_equiv ctx v0 v1}+{True}
  := match v0, v1 with
  | Sinput r0 _, Sinput r1 _ =>
      if Pos.eq_dec r0 r1 then left _ else right _
  | Sop o0 vs0 _, Sop o1 vs1 _ =>
      if Op.eq_operation o0 o1
      then if list_sval_eqv_dec vs0 vs1
      then left _ else right _ else right _
  | Sfoldr o0 vs0 v0 _, Sfoldr o1 vs1 v1 _ =>
      if Op.eq_operation o0 o1
      then if list_sval_eqv_dec vs0 vs1
      then if sval_eqv_dec v0 v1
      then left _ else right _ else right _ else right _
  | Sload m0 t0 c0 a0 l0 aaddr0 _, Sload m1 t1 c1 a1 l1 aaddr1 _ =>
      if smem_eqv_dec m0 m1
      then if trapping_mode_eq t0 t1
      then if chunk_eq c0 c1
      then if eq_addressing a0 a1
      then if list_sval_eqv_dec l0 l1
      then if eq_aval aaddr0 aaddr1
      then left _
      else right _ else right _ else right _ else right _ else right _ else right _
  (* | _,_ => right _ *)
  | Sinput _ _, _ | Sop _ _ _, _ | Sfoldr _ _ _ _, _| Sload _ _ _ _ _ _ _, _ => right _
  end
with list_sval_eqv_dec (l0 l1 : list_sval)
  : {forall ctx, list_sval_equiv ctx l0 l1}+{True}
  := match l0, l1 with
  | Snil _, Snil _ => left _
  | Scons v0 vs0 _, Scons v1 vs1 _ =>
      if sval_eqv_dec v0 v1
      then if list_sval_eqv_dec vs0 vs1
      then left _
      else right _ else right _
  (* | _,_ => right _ *)
  | Snil _, _ | Scons _ _ _, _ => right _
  end
with smem_eqv_dec (m0 m1 : smem)
  : {forall ctx, smem_equiv ctx m0 m1}+{True}
  := match m0, m1 with
  | Sinit _, Sinit _ => left _
  | Sstore m0' c0 a0 l0 sinfo0 r0 _, Sstore m1' c1 a1 l1 sinfo1 r1 _ =>
      if smem_eqv_dec m0' m1'
      then if chunk_eq c0 c1
      then if eq_addressing a0 a1
      then if list_sval_eqv_dec l0 l1
      then if store_info_eq_dec sinfo0 sinfo1
      then if sval_eqv_dec r0 r1
      then left _
      else right _ else right _ else right _ else right _ else right _ else right _
  (* | _,_ => right _ *)
  | Sinit _, _ | Sstore _ _ _ _ _ _ _, _ => right _
  end.
Solve All Obligations with solve [program_simpl |
  let rec intro_use := try (let H := fresh "H" in intro H; intro_use; try rewrite H)
  in simpl; intro_use; reflexivity].

(* rel *)

Program Definition test_offset_lt (ofs0 : ptrofs) (chk0 : memory_chunk)
                                  (ofs1 : ptrofs) (chk1 : memory_chunk)
  : {offset_chunk_lt ofs0 chk0 ofs1 chk1}+{True} :=
  if zle (Ptrofs.unsigned ofs0 + size_chunk chk0) (Ptrofs.unsigned ofs1)
  then if zle (Ptrofs.unsigned ofs1 + size_chunk chk1) (Ptrofs.unsigned ofs0 + Ptrofs.modulus)
  then left _ else right _ else right _.
Next Obligation. unfold offset_chunk_lt. auto. Qed.

Program Definition test_offset_disjoint (ofs0 : ptrofs) (chk0 : memory_chunk)
                                        (ofs1 : ptrofs) (chk1 : memory_chunk)
  : {offset_disjoint ofs0 chk0 ofs1 chk1}+{True} :=
  if test_offset_lt ofs0 chk0 ofs1 chk1
  then left _
  else if test_offset_lt ofs1 chk1 ofs0 chk0
  then left _ else right _.
Solve All Obligations with (unfold offset_disjoint; auto).

Program Definition test_disjoint_rel (a0 a1 : address)
  : {forall ctx, addr_disjoint ctx a0 a1}+{True} :=
  if list_sval_eqv_dec (a_args a0) (a_args a1)
  then (* FIXME: a multiple pattern match is rejected *)
    match test_is_indexed (a_addr a0) with
    | inright _   => right _
    | inleft ofs0 =>
    match test_is_indexed (a_addr a1) with
    | inright _   => right _
    | inleft ofs1 => if test_offset_disjoint ofs0 (a_chunk a0) ofs1 (a_chunk a1)
                     then left _ else right _
    end end
  else right _.
Next Obligation. eapply offset_disjoint_sound; eauto. Qed.
Global Opaque test_disjoint_rel.

(* abs *)

Definition test_disjoint_abs (a0 a1 : address)
  : {forall ctx, ctx_bc ctx <> None -> addr_disjoint ctx a0 a1}+{True}.
  case (vdisjoint (a_aval a0) (size_chunk (a_chunk a0))
                  (a_aval a1) (size_chunk (a_chunk a1))) eqn:D.
  - left; intros.
    apply abs_disjoint_sound; auto.
  - right; trivial.
Defined.
Global Opaque test_disjoint_abs.

(* combined *)

Definition test_disjoint_pre (abs : bool) ctx :=
  if abs then ctx_bc ctx <> None else True.

Program Definition test_disjoint (rel abs : bool) (a0 a1 : address)
  : {forall ctx, test_disjoint_pre abs ctx -> addr_disjoint ctx a0 a1}+{True} :=
  if (if rel then test_disjoint_rel a0 a1 else right _)
  then left _
  else if abs
    as abs' return {forall ctx, test_disjoint_pre abs' ctx -> addr_disjoint ctx a0 a1}+{True}
  then test_disjoint_abs a0 a1 
  else right _.

End DisjointnessTest.

(*** Implementation of the rewriting rules *)

Section RRImpl.

(*
   The rewriting functions are parametrized  by a disjointness test and an order on the stores.
   With only -fprepass-alias-rel, one could use more efficients versions. In particular one could stop
   the rewritings under the loads earlier.
 *)

  Variable ctx_pre : iblock_common_context -> Prop.

  Variable p_test_disjoint : forall (a0 a1 : address),
    {forall ctx : iblock_common_context, ctx_pre ctx -> addr_disjoint ctx a0 a1} + {True}.

  Variable p_store_order : forall (a0 a1 : address), bool.

  Local Obligation Tactic := try solve [program_simpl]; intros.

(*
   If we can prove that [x] and [y] are disjoints, then:
      load (store (store m x v) xs.. vs..) y = load (store m xs.. vs..) y
   When applying a [load], we normalize the memory by removing all the stores that are known to be disjoint.
*)

Program Fixpoint rewrite_stores_under_load (sm: smem) (a0 : address) {struct sm}
  : {sm' : smem |
      forall ctx, ctx_pre ctx -> alive (eval_smem ctx sm) ->
                  smem_equiv_on (addr_range ctx a0) ctx sm' sm}
  := match sm with
  | Sstore sm1 chk1 addr1 sargs1 sinfo1 sv1 hc =>
      let sm1' := rewrite_stores_under_load sm1 a0               in
      let a1   := mk_addr chk1 addr1 sargs1 (store_aaddr sinfo1) in
      if p_test_disjoint a0 a1
      then sm1'
      else fSstore sm1' chk1 addr1 sargs1 sinfo1 sv1
  | _ => sm
  end.
Next Obligation.
  intros ctx PRE MALIVE.
  eapply smem_equiv_on_trans.
  - apply (proj2_sig sm1'); auto. revert MALIVE. simpl; repeat autodestruct.
  - apply disjoint_store_smem_equiv_on; auto.
Defined.
Next Obligation.
  intros ctx PRE MALIVE.
  apply smem_equiv_on_sym. apply apply_store_smem_equiv_on.
  - apply smem_equiv_on_sym. apply (proj2_sig sm1'); auto.
    revert MALIVE; simpl; repeat autodestruct.
  - exact MALIVE.
Defined.
Next Obligation.
  intros ctx PRE MALIVE; subst.
  apply smem_equiv_on_refl. exact MALIVE.
Qed.
Global Opaque rewrite_stores_under_load.

Program Definition rewrite_load_impl (sm: smem)
  (trap : trapping_mode) (chk: memory_chunk) (addr: addressing) (sargs : list_sval) aaddr
  : {sv : sval |
      forall ctx, ctx_pre ctx -> alive (eval_smem ctx sm) ->
                  sval_equiv ctx sv (fSload sm trap chk addr sargs aaddr)}
  := fSload (rewrite_stores_under_load sm (mk_addr chk addr sargs aaddr)) trap chk addr sargs aaddr.
Next Obligation.
  intros ctx PRE MALIVE.
  case rewrite_stores_under_load as [sm' SM].
  eapply rewrite_load_equiv_on; auto.
Qed.
Global Opaque rewrite_load_impl.

(*
   If we can prove that [a0] and [a1] are disjoints, then:
     store (store m a0 v0) a1 v1 = store (store m a1 v1) a0 v0
   We use this property to normalize the representation of the memory by reordering (when they commute)
   consecutive [store] according to their order in the source program.
 *)

Program Fixpoint rewrite_store_impl (sm: smem)
  (chk: memory_chunk) (addr: addressing) (sargs : list_sval) (sinfo : store_info) (sv : sval) {struct sm}
  : option {sm' : smem | forall ctx, ctx_pre ctx -> smem_equiv ctx sm' (fSstore sm chk addr sargs sinfo sv)}
  := match sm with
  | Sstore sm1 chk1 addr1 sargs1 sinfo1 sv1 _ =>
      let a0 := mk_addr chk  addr  sargs  (store_aaddr sinfo ) in
      let a1 := mk_addr chk1 addr1 sargs1 (store_aaddr sinfo1) in
      if p_test_disjoint a0 a1
      then if Pos.ltb (store_num sinfo) (store_num sinfo1)
        (* We rewrite [store (store m a1 v1) a0 v0] into [store (store m a0 v0) a1 v1]
           only if the store on [a0] appears before the store on [a1] in the source program. *)
      then let sm2 := match rewrite_store_impl sm1 chk addr sargs sinfo sv with
                      | Some sm2 => `sm2
                      | None     => fSstore sm1 chk addr sargs sinfo sv
                      end
           in Some (fSstore sm2 chk1 addr1 sargs1 sinfo1 sv1)
      else None else None
  | _ => None
  end.
Next Obligation.
  intros ctx PRE.
  assert (SM2: smem_equiv ctx sm2 (fSstore sm1 chk addr sargs sinfo sv)). {
    case rewrite_store_impl as [[? IH]|] in sm2; simpl in sm2.
    - apply IH; auto.
    - reflexivity.
  }
  symmetry; etransitivity.
  - apply rewrite_store_store; auto.
  - simpl; rewrite SM2; reflexivity.
Defined.
Global Opaque rewrite_store_impl.

End RRImpl.


Definition rewrite_load (RRULES: rrules_set) (strict : bool) (sm: smem)
  (trap : trapping_mode) (chunk: memory_chunk) (addr: addressing) (lsv: list_sval) (aaddr: aval)
  : option sval
  := match RRULES with
  | RRschedule rel => Some (proj1_sig
        (rewrite_load_impl _ (test_disjoint rel strict) sm trap chunk addr lsv aaddr))
  | _ => None
  end.

Lemma rewrite_load_correct
   (ctx : iblock_common_context) (RRULES: rrules_set) strict sm trap chunk addr sargs aaddr rew
   (STRICT: ctx_strict_reflect (ctx_bc ctx) strict)
   (RW: rewrite_load RRULES strict sm trap chunk addr sargs aaddr = Some rew):
   let r := eval_sval ctx (fSload sm trap chunk addr sargs aaddr) in
   alive (eval_smem ctx sm) -> eval_sval ctx rew = r.
Proof.
  destruct RRULES; simpl in RW; try congruence.
  - (*Case RRschedule*)
    case rewrite_load_impl as [? H] in RW.
    injection RW as <-.
    apply H.
    unfold test_disjoint_pre. inversion STRICT; trivial; congruence.
Qed.
Global Opaque rewrite_load.

Definition rewrite_store (RRULES: rrules_set) (strict : bool) (sm: smem)
  (chk: memory_chunk) (addr: addressing) (sargs : list_sval) (sinfo : store_info) (sv : sval)
  : option smem
  := match RRULES with
  | RRschedule rel =>
      match rewrite_store_impl _ (test_disjoint rel strict) sm chk addr sargs sinfo sv with
      | Some rw => Some (proj1_sig rw)
      | None    => None
      end
  | _ => None
  end.

Lemma rewrite_store_correct
   (ctx : iblock_common_context) (RRULES: rrules_set) strict sm chunk addr sargs sinfo sv rew
   (STRICT: ctx_strict_reflect (ctx_bc ctx) strict)
   (RW: rewrite_store RRULES strict sm chunk addr sargs sinfo sv = Some rew):
   smem_equiv ctx rew (fSstore sm chunk addr sargs sinfo sv).
Proof.
  destruct RRULES; simpl in RW; try congruence.
  - (*Case RRschedule*)
    case rewrite_store_impl as [[rw H]|] in RW; try congruence.
    injection RW as <-; apply H.
    unfold test_disjoint_pre. inversion STRICT; trivial; congruence.
Qed.
Global Opaque rewrite_store.
