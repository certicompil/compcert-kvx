(** Proof of BTL Block Simulation modulo Gluing (Symbolic) Invariants.

 Simulation diagram (for the "main" lemma [iblock_step_simulation])

<<

(0) = Symbolic simulation test
(1) = Source symbolic execution (on the target frame): sexec_correct
(2) = Invariant application (history & gluing) on the source: match_sstates_correct
      --> Here the context switch from stack stk1 to stk2 happens.   
(3) = Comparing with the target symbolic execution: match_sexec_live_correct.
(4) = Coming back to concrete states: sexec_exact.

       stk1           stk1          stk2                    stk2
        f1            f1                       f2            f2
       csS +++++++++ ssS -------------------> ssI ++++++++++ csI
        |             |                        |              |
        |             |                        |              |
        |             |                        |              |
        |    (1)      |          (0)           |              |
        |             |                        |      (4)     |
        |             |                        |              |
        |             |                        |              |
        |             |                        |              |
        \/           \/                        \/            \/
       csE +++++++++ ssE --------> ssEI ===> ssIE ++++++++++ ssIE'
        \                 (2)      +    (3)   +    (4)       //
         \                         +          +             //
          -----------------------> csEI ===> csIE ======== //

  Legend:
    - cs* -> concrete states
    - ss* -> symbolic states
    - *S -> Start
    - *E -> Exec
    - *I -> Invariant
    - *EI -> Exec then Invariant
    - *IE -> Invariant then Exec
    - "--->" is the match_states (modulo invariant)
    - "++++" is sem_sstates
    - "==>" means "simu on live variables of the target"

  Remark:
   - csS = <sp,rs0,m0>

>>

*)

Require Import AST Linking Values Maps Globalenvs Smallstep Registers.
Require Import Coqlib Events Errors OptionMonad.
Require Import RTL BTL BTL_SEtheory.
Require Import BTLmatchRTL BTL_BlockSimulation.
Require Import ValueDomain.
Require BTL_ValueAnalysisAnnotate BTL_LoadNumbering.


Module BTL_BlockSimulationproof (B: BTL_BlockSimulationConfig).

Include BTL_BlockSimulation B.

Definition match_prog (p tp: program) :=
  match_program (fun cu f tf => transf_fundef (ValueAnalysis.romem_for cu) f = OK tf) eq p tp.

Lemma transf_program_match:
  forall prog tprog, transf_program prog = OK tprog -> match_prog prog tprog.
Proof.
  intros. eapply match_transform_partial_program_contextual; eauto.
Qed.

Section PRESERVATION.

Variable prog: program.
Variable tprog: program.

Hypothesis TRANSL: match_prog prog tprog.

Let pge := Genv.globalenv prog.
Let tpge := Genv.globalenv tprog.

Lemma symbols_preserved s: Genv.find_symbol tpge s = Genv.find_symbol pge s.
Proof.
  rewrite <- (Genv.find_symbol_match TRANSL). reflexivity.
Qed.

Lemma senv_preserved:
  Senv.equiv pge tpge.
Proof.
  eapply (Genv.senv_match TRANSL).
Qed.

Lemma functions_translated:
  forall (v: val) (f: fundef),
  Genv.find_funct pge v = Some f ->
  exists tf cunit, transf_fundef (ValueAnalysis.romem_for cunit) f = OK tf /\ Genv.find_funct tpge v = Some tf /\ linkorder cunit prog.
Proof.
  intros. exploit (Genv.find_funct_match TRANSL); eauto.
  intros (cu & tf & A & B & C).
  exists tf. exists cu.
  intuition eauto.
Qed.

Lemma transf_function_correct ro f f':
  transf_function ro f = OK f' -> match_function f f'.
Proof.
  unfold transf_function. intros H.
  assert (OH: transf_function ro f = OK f') by auto.
  destruct (btl_optim_oracle ro f); destruct p;
  monadInv H.
  exploit transf_function_only_liveness; intuition eauto.
  simpl in *; econstructor; eauto.
  eapply check_symbolic_simu_correct; eauto.
Qed.

Lemma transf_fundef_correct ro f f':
  transf_fundef ro f = OK f' -> match_fundef f f'.
Proof.
  intros TRANSF; destruct f; simpl; inv TRANSF.
  + monadInv H0. exploit transf_function_correct; eauto.
    intros. econstructor; eauto.
  + eapply match_External.
Qed.

Lemma function_ptr_translated:
  forall b f,
  Genv.find_funct_ptr pge b = Some f ->
  exists cunit tf, Genv.find_funct_ptr tpge b = Some tf
                   /\ transf_fundef (ValueAnalysis.romem_for cunit) f = OK tf
                   /\ linkorder cunit prog.
Proof.
  intros.
  exploit (Genv.find_funct_ptr_match TRANSL); eauto.
Qed.

Lemma function_sig_translated ro f tf: transf_fundef ro f = OK tf -> funsig tf = funsig f.
Proof.
  intros H; apply transf_fundef_correct in H; destruct H; simpl; eauto.
  symmetry; erewrite preserv_fnsig; eauto.
Qed.

Theorem transf_initial_states:
  forall s1, initial_state prog s1 ->
  exists s2, initial_state tprog s2 /\ match_states pge s1 s2.
Proof.
  intros. inv H.
  exploit function_ptr_translated; eauto. intros (cu & tf & FIND &  TRANSF & LINK).
  eexists. split.
  - econstructor; eauto.
    + intros; apply (Genv.init_mem_match TRANSL); eauto.
    + replace (prog_main tprog) with (prog_main prog). rewrite symbols_preserved. eauto.
      symmetry. eapply match_program_main. eauto.
    + erewrite function_sig_translated; eauto.
  - apply transf_fundef_correct in TRANSF. inv TRANSF;
    repeat (econstructor; eauto).
Qed.

Lemma transf_final_states s1 s2 r:
  match_states pge s1 s2 -> final_state s1 r -> final_state s2 r.
Proof.
  intros. inv H0. inv H. inv STACKS. constructor.
Qed.

Local Hint Resolve match_si_ok: core.

Section SYMBOLIC_CTX.

Variables f1 f2: function.
Variable sp0: val.
Variable rs0: regset.
Variable m0: Memory.Mem.mem.
Variable bc: block_classification.

Lemma symbols_preserved_rev s: Genv.find_symbol pge s = Genv.find_symbol tpge s.
Proof.
  symmetry; apply symbols_preserved.
Qed.

Let ctx := Sctx pge tpge symbols_preserved_rev sp0 rs0 (m0, if strict then Some bc else None).

Lemma sfind_function_preserved ros1 ros2:
  svident_simu (bcctx2 ctx) ros1 ros2 ->
  sfind_function (bcctx2 ctx) ros1 = sfind_function (bcctx2 ctx) ros2.
Proof.
  unfold sfind_function. intros [sv1 sv2 SIMU|]; simpl in *; subst; auto.
  rewrite SIMU; auto.
Qed.

Lemma sfind_function_ctx_preserved ros fd:
  sfind_function (bcctx1 ctx) ros = Some fd ->
  exists (cu : program) tf,
    sfind_function (bcctx2 ctx) ros = Some tf
    /\ transf_fundef (ValueAnalysis.romem_for cu) fd = OK tf.
Proof.
  unfold sfind_function. destruct ros; simpl.
  - rewrite eval_sval_preserved.
    repeat autodestruct. intros.
    exploit functions_translated; eauto.
    intros (tf & cu & TF & TPGE & _). intuition eauto.
  - rewrite symbols_preserved. repeat autodestruct. intros.
    exploit function_ptr_translated; eauto.
    intros (tf & cu & TF & TPGE & _). intuition eauto.
Qed.
Local Hint Constructors sem_sfval: core.
Local Hint Resolve eqlive_reg_update eqlive_reg_monotonic list_nth_z_in: core.
Local Hint Unfold symb_exec_ok: core.

Theorem match_sexec_live_correct stk2 ssEI ssIE csEI t
  (SEL: match_sexec_live (Bfctx f2 (bcctx2 ctx)) ssEI ssIE)
  (SST1: sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) stk2 bc t csEI ssEI)
  (MF: match_function f1 f2)
  :exists csIE,
    sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) stk2 bc t csIE ssIE
    /\ eqlive_states csEI csIE.
Proof.
  inversion MF. exploit sem_sstate_run; eauto.
  simpl; intros (sis1 & sfv1 & rs1 & m & SOUT1 & SIS1 & SISFRAME & SF).
  exploit sem_sis_ok; eauto. intros SOK.
  exploit SEL; simpl; eauto.
  intros (sis2 & sfv2 & SOUT2 & LIVEOK & SSIMU & SFVSIM).
  remember SIS1 as EQSFV. clear HeqEQSFV.
  eapply SFVSIM in EQSFV.
  exploit SSIMU; eauto. intros (rs2 & SIS2 & EQLIVE).
  destruct SIS1 as (PRE1 & SMEM1 & SREGS1).
  try_simplify_someHyps; intros.
  inversion EQSFV; subst; inversion SF; subst; simpl in *.
  - (* goto *)
    exploit (run_sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) ssIE); simpl; eauto.
    intros SST2. eexists; split; eauto.
    eapply eqlive_states_intro; eauto.
  - (* call *)
    erewrite sfind_function_preserved in *; eauto.
    exploit (run_sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) ssIE); eauto.
    + econstructor; eauto.
      simpl; erewrite <- ARGS; eauto.
    + intros SST2. repeat (simpl; econstructor; eauto).
  - (* tailcall *)
    erewrite sfind_function_preserved in *; eauto.
    exploit (run_sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) ssIE); eauto.
    + econstructor; eauto.
      simpl; erewrite <- ARGS; eauto.
    + intros SST2. repeat (simpl; econstructor; eauto).
  - (* builtin *)
    exploit (run_sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) ssIE); eauto.
    + econstructor; eauto.
      inv BARGS.
      eapply eval_list_builtin_sval_correct; eauto.
    + intros SST2. repeat (simpl; econstructor; eauto).
      unfold regmap_setres; autodestruct; 
      intros; try (apply eqlive_reg_update; intros); eapply eqlive_reg_monotonic; intuition eauto.
      all: apply SISFRAME; intuition subst; (inv H || inv H0); try contradiction.
 - (* jumptable *)
    exploit (run_sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) ssIE); eauto.
    + econstructor; eauto.
      simpl; rewrite <- VAL; auto.
    + intros SST2. repeat (simpl; econstructor; eauto).
      eapply eqlive_reg_monotonic; intuition eauto.
  - (* return *)
    exploit (run_sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) ssIE); eauto.
    + econstructor; eauto.
      inv OPT; eauto.
      simpl; rewrite <- SIMU; auto.
    + intros SST2. eexists; split; eauto.
      econstructor; simpl; eauto.
Qed.

Lemma trss_ok_nofail sis si
  (TRSOK : sis_ok (bcctx1 ctx) (tr_sis sis si false))
  : tr_sis_ok (bcctx1 ctx) sis si.
Proof.
  unfold tr_sis_ok.
  destruct TRSOK as [(_ & _ & OK) _ _].
  eauto.
Qed.

Local Hint Resolve test_clobberable_correct test_csifreem_correct trss_ok_nofail: core.


Lemma match_sexec_redundant_final_correct (sisE: sistate) si sfv rs m
  (LIVEOK : forall r : reg, True -> build_frame sisE r)
  (SIS : sem_sistate (bcctx1 ctx) sisE rs m)
  (SER : match_sexec_redundant (bcctx1 ctx) (Sfinal (tr_sis sisE si false) sfv) sisE)
  (TRSOKH : sis_ok (bcctx1 ctx) (tr_sis sisE si false))
  : match_si {| cge := pge; csp := sp0; crs0 := rs; cmbc := (m, if strict then Some bc else None) |} si rs.
Proof.
   exploit (tr_sis_correct sisE); eauto.
   simpl. intros (SEMH & MATCHH).
   unfold match_sexec_redundant in SER; simpl in SER.
   exploit SER; eauto. clear SER.
   intros (rs2 & SIS2 & EQLIVE1).
   eapply match_si_eqlive; eauto.
   exploit sem_sistate_determ.
   eapply SIS.
   eapply SIS2.
   intros (EQLIVE2 & _).
   eapply eqlive_reg_trans; eapply eqlive_reg_monotonic; eauto.
   unfold build_frame; simpl; intros r BF; autodestruct.
Qed.
Local Hint Resolve match_sexec_redundant_final_correct: core.

Lemma match_sexec_redundant_cond_correct ctx0 cond args b ifso ifnot sisE:
  eval_scondition ctx0 cond args = Some b ->
  match_sexec_redundant ctx0 (Scond cond args ifso ifnot) sisE <->
  match_sexec_redundant ctx0 (if b then ifso else ifnot) sisE.
Proof.
  unfold match_sexec_redundant; simpl; intros SEVAL; rewrite SEVAL; simpl.
  intuition.
Qed.

Lemma inmap_eq (A B : Type) (f : A -> B) (l : list A) (x : A) (y: B):
       In x l -> y = f x -> In y (map f l).
Proof.
  intros; subst; apply in_map; eauto.
Qed.

Theorem tr_sstate_correct stk1 t csE ssE 
  (SST: sem_sstate triv_frame (Bfctx f1 (bcctx1 ctx)) stk1 bc t csE ssE):
  forall stk2 sisE sfvE ssEI stEH
  (SOUT : get_soutcome (Bfctx f1 (bcctx1 ctx)) ssE = Some (sout sisE sfvE))
  (TRSS_H: tr_sstate (bcctx1 ctx) (fun pc => history (f2.(fn_gm) pc)) ssE = stEH)
  (TRSS_G: tr_sstate (bcctx2 ctx) (fun pc => glue (f2.(fn_gm) pc)) ssE = ssEI)
  (SER: match_sexec_redundant (Bfctx f1 (bcctx1 ctx)) stEH sisE)
  (TRSS_OKH: trss_ok (bcctx1 ctx) stEH)
  (TRSS_OKG: trss_ok (bcctx1 ctx) ssEI)
  (STACKS : list_forall2 (match_stackframes pge) stk1 stk2)
  (MF: match_function f1 f2)
  , exists csEI,
    sem_sstate sfv_frame (Bfctx f2 (bcctx2 ctx)) stk2 bc t csEI ssEI
    /\ match_states pge csE csEI.
Proof.
  unfold triv_frame in *.
  induction SST; simpl in *; intros.
  - (* Sfinal *)
    inv TRSS_G; autodestruct; destruct TRSS_OKG as (sis' & sfv' & SOUT' & TRSOK');
    simpl in SOUT'; inv SOUT'; inv SOUT.
    destruct TRSS_OKH as (sisH & sfvH & SOUT' & TRSOKH); simpl in SOUT';
    inv SFV; intros SISFV; inv SISFV; inv SOUT'.
    + (* goto *)
      exploit (tr_sis_correct sisE (fn_gm f2 pc) (bcctx1 ctx)); eauto.
      intros (SEM & MATCH).
      eexists; split.
      * eapply sem_Sfinal with (rs:=(eval_subst_si (bcctx1 ctx) sisE (fn_gm f2 pc))); simpl; eauto.
        { eapply sem_sistate_preserved; eauto. }
        unfold si_apply, build_frame; intros r; simpl; autodestruct.
      * simpl in *; econstructor; eauto.
        econstructor; eauto.
   + (* return *)
      exploit (tr_sis_correct sisE si_empty (bcctx1 ctx)); eauto.
      intros (SEM & MATCH).
      eexists; split.
      * eapply sem_Sfinal with (rs:=(eval_subst_si (bcctx1 ctx) sisE si_empty)); simpl; eauto.
        -- eapply sem_sistate_preserved; eauto.
        -- contradiction.
        -- econstructor; eauto.
           { simpl; erewrite <- preserv_fnstacksize; eauto. }
           { destruct osv; eauto. simpl; rewrite <- eval_sval_preserved; eauto. }
      * simpl; repeat (econstructor; eauto).
    + (* call *)
      generalize H2; autodestruct; inv H2.
      generalize H3; autodestruct; inv H3.
      intros EQ; simpl in SER; rewrite EQ in SER.
      exploit (tr_sis_correct sisE (csi_remove res (fn_gm f2 pc)) (bcctx1 ctx)); eauto.
      intros (SEM & MATCH).
      exploit sfind_function_ctx_preserved; eauto.
      intros (cu & tf & SF & TF). eexists; split.
      * eapply sem_Sfinal with (rs:=(eval_subst_si (bcctx1 ctx) sisE (csi_remove res (fn_gm f2 pc)))); simpl; eauto.
        -- eapply sem_sistate_preserved; eauto.
        -- unfold build_frame; intros r0; simpl; autodestruct.
           intros GIR (NEQ & GISOME) _.
           exploit csi_gro.
           2: { simpl; intro GRO; erewrite GRO in GIR; intuition auto. }
           auto.
        -- econstructor; eauto.
           apply function_sig_translated with (ro := ValueAnalysis.romem_for cu); auto.
           simpl; rewrite <- list_sval_eval_preserved; eauto.
      * apply transf_fundef_correct in TF; simpl.
        do 3 (econstructor; eauto).
        intros; eapply clobbered_compat_matchinvs_preserv; eauto.
        econstructor; eauto.
    + (* tailcall *)
      exploit (tr_sis_correct sisE si_empty (bcctx1 ctx)); eauto.
      intros (SEM & MATCH).
      exploit sfind_function_ctx_preserved; eauto.
      intros (cu & tf & SF & TF). eexists; split.
      * eapply sem_Sfinal with (rs:=(eval_subst_si (bcctx1 ctx) sisE si_empty)); simpl; eauto.
        -- eapply sem_sistate_preserved; eauto.
        -- contradiction.
        -- econstructor; eauto.
           { apply function_sig_translated with (ro := ValueAnalysis.romem_for cu); auto. }
           { simpl; erewrite <- preserv_fnstacksize; eauto. }
           { simpl; rewrite <- list_sval_eval_preserved; eauto. }
      * apply transf_fundef_correct in TF.
        simpl; repeat (econstructor; eauto).
    + (* builtin *)
      destruct (reg_builtin_res res) eqn:EQBR.
      all: generalize H2; autodestruct; inv H2; intros.
      { exploit (tr_sis_correct sisE (csi_remove r (fn_gm f2 pc)) (bcctx1 ctx)); eauto.
        intros (SEM & MATCH); eexists; split.
        * eapply sem_Sfinal with (rs:=(eval_subst_si (bcctx1 ctx) sisE (csi_remove r (fn_gm f2 pc)))); simpl; eauto.
          -- eapply sem_sistate_preserved; eauto.
          -- unfold build_frame; intros r0; simpl; autodestruct.
             intros GIR (NEQ & GISOME) _.
             specialize NEQ with r.
             exploit csi_gro.
             2: { simpl; intro GRO; erewrite GRO in GIR; intuition auto. }
             intuition auto.
          -- econstructor; eauto.
             { apply eval_builtin_sargs_preserved; eauto.
               exact senv_preserved. }
             { simpl. eapply external_call_symbols_preserved; eauto.
               exact senv_preserved. }
        * simpl.
          assert (res = BR r).
          { destruct res eqn:HBR in EQBR; inv EQBR; auto. }
            subst; econstructor; eauto.
            generalize H3; autodestruct; inv H3.
            intros EQ0; simpl in SER; rewrite EQ0 in SER.
            intros; eapply clobbered_compat_matchinvs_preserv; eauto.
            econstructor; eauto.
         }
      { exploit (tr_sis_correct sisE (fn_gm f2 pc) (bcctx1 ctx)); eauto.
        intros (SEM & MATCH); eexists; split.
        * eapply sem_Sfinal with (rs:=(eval_subst_si (bcctx1 ctx) sisE (fn_gm f2 pc))); simpl; eauto.
          -- eapply sem_sistate_preserved; eauto.
          -- unfold build_frame; intros r; simpl; autodestruct.
             intuition auto.
          -- econstructor; eauto.
             { apply eval_builtin_sargs_preserved; eauto.
               exact senv_preserved. }
             { simpl. eapply external_call_symbols_preserved; eauto.
               exact senv_preserved. }
        * generalize H3; autodestruct; inv H3.
          intros EQ0; simpl in SER; rewrite EQ0 in SER.
          destruct res; simpl in EQBR; inv EQBR; simpl in *;
          econstructor; eauto;
          eapply matchinvs_update_m; eauto;
          econstructor; eauto. }
    + (* sjumptable *)
      rename H2 into UNION_G.
      revert H3; autodestruct; intros UNION_H SOUT_H; inv SOUT_H; simpl in *.
      rewrite UNION_H in SER. exploit match_sexec_redundant_final_correct; eauto; intros MATCH_H.
      exploit (tr_sis_correct sisE f (bcctx1 ctx)); eauto.
      intros (SEM_G & MATCH_G).
      eexists; split.
      * eapply sem_Sfinal with (rs:=(eval_subst_si (bcctx1 ctx) sisE f)); simpl; eauto.
        -- eapply sem_sistate_preserved; eauto.
        -- intros r (pc0 & LIST_IN & FRAME).
           unfold build_frame in *; autodestruct; intros SIR _.
           destruct (fn_gm f2 pc0 r) eqn:EQFR; try congruence.
           eapply fpa_reg_union_si in UNION_G; eauto; [ destruct UNION_G as (sv' & CONTRA & _); congruence |].
           eapply inmap_eq; eauto.
        -- econstructor; eauto. simpl. erewrite <- eval_sval_preserved; eauto.
      * simpl in *; destruct MATCH_H as (SIOKh & MSREGh); destruct MATCH_G as (SIOKg & MSREGg).
        apply list_nth_z_in in H0. 
        repeat (econstructor; eauto).
        -- intros sv0 HIN. eapply SIOKh; eapply fpa_ok_union_si in UNION_H; eauto.
           eapply inmap_eq; eauto.
        -- eapply match_sreg_union_si_H; eauto; [ simpl; reflexivity | eapply inmap_eq; eauto ].
        -- intros sv0 HIN. eapply SIOKg; eapply fpa_ok_union_si in UNION_G; eauto.
           eapply inmap_eq; eauto.
        -- eapply (match_sreg_union_si_G
                    (map (fun pc : positive => glue (fn_gm f2 pc)) tbl)
                    ctx _ _ _ rs0 _ m0 _ _ _ sp0 _ _ symbols_preserved_rev); eauto.
           reflexivity. 
           eapply inmap_eq; eauto.
  - (* Scond *)
    inv TRSS_G; destruct TRSS_OKG as (sisG & sfvG & SOUTG & TRSOKG); inv SOUTG.
    destruct TRSS_OKH as (sisH & sfvH & SOUTH & TRSOKH); inv SOUTH.
    rewrite SEVAL in *.
    rewrite match_sexec_redundant_cond_correct in SER; eauto.
    destruct b eqn:EQB; exploit IHSST; unfold trss_ok; eauto;
    intros (csEI & SST_SO & MS_SO); eexists; split; eauto.
    all: eapply sem_Scond with (b:=b); rewrite EQB; simpl; eauto;
         rewrite <- eval_scondition_preserved; simpl; eauto.
Qed.

Lemma build_frame_history csix:
  forall r, build_frame (sis_history csix) r.
Proof.
  intros; unfold build_frame, sis_history; simpl; autodestruct.
Qed.

Theorem match_sexec_csi_correct stk1 stk2 ib1 ib2 sisE sfvE stEH ssEI (csix: invariants) rsI csE t
  (SOUT : get_soutcome (Bfctx f1 (bcctx1 ctx)) (sexec ib1 (sis_source csix)) = Some (sout sisE sfvE))
  (TRSS_H: tr_sstate (bcctx1 ctx) (fun pc => history (f2.(fn_gm) pc)) (sexec ib1 (sis_source csix)) = stEH)
  (TRSS_G: tr_sstate (bcctx2 ctx) (fun pc => glue (f2.(fn_gm) pc)) (sexec ib1 (sis_source csix)) = ssEI)
  (SER: match_sexec_redundant (Bfctx f1 (bcctx1 ctx)) stEH sisE)
  (SEL: match_sexec_live (Bfctx f2 (bcctx2 ctx)) ssEI (sexec ib2 (sis_target csix)))
  (MATCHI: match_invs (bcctx1 ctx) csix rsI)
  (TRSS_OKH: trss_ok (bcctx1 ctx) stEH)
  (TRSS_OKG: trss_ok (bcctx1 ctx) ssEI)
  (ISTEP: iblock_step strict (sge1 ctx) stk1 f1 (ssp ctx) (srs0 ctx) (sm0 ctx) bc ib1 t csE)
  (STACKS: list_forall2 (match_stackframes pge) stk1 stk2)
  (MF: match_function f1 f2)
  :exists ssIE',
    iblock_step strict (sge2 ctx) stk2 f2 (ssp ctx) rsI (sm0 ctx) bc ib2 t ssIE'
    /\ match_states pge csE ssIE'.
Proof.
  unfold match_sexec_live in SEL.
  exploit (sexec_correct strict (Bfctx f1 (bcctx1 ctx)) (sis_source csix)); simpl; eauto; try reflexivity.
  { intros; destruct MATCHI; eapply sis_source_correct; eauto. }
  { apply build_frame_history. }
  generalize SOUT TRSS_H TRSS_G; clear SOUT TRSS_H TRSS_G.
  generalize (sexec ib1 (sis_source csix)).
  intros csS SOUT TRSS_H TRSS_G SEMSS.
  intros; exploit tr_sstate_correct; eauto.
  intros (csEI & SST_sEI & MS_E_EI).
  exploit match_sexec_live_correct; eauto.
  intros (csIE & SST_sIE & EQLIVE1).
  exploit (sexec_exact strict (Bfctx f2 (bcctx2 ctx))); simpl; eauto; try reflexivity.
  { eapply sis_target_correct; eauto.
    eapply match_invs_preserved; eauto.
  }
  simpl. intros (s3 & STEP2 & EQLIVE2).
  exists s3; split; eauto.
  eapply match_states_eqlive; eauto.
  eapply eqlive_states_trans; eauto.
Qed.



End SYMBOLIC_CTX.

Local Hint Unfold symb_exec_ok: core.

Theorem iblock_step_simulation stk1 stk2 f1 f2 sp rsS rsI m bc ibf1 t csE pc
  (ISTEP: iblock_step strict pge stk1 f1 sp rsS m bc ibf1.(entry) t csE)
  (PC: (fn_code f1) ! pc = Some ibf1)
  (MF: match_function f1 f2)
  (MATCHI: match_invs (Bcctx pge sp rsS (m, if strict then Some bc else None)) (f2.(fn_gm) pc) rsI)
  (STACKS: list_forall2 (match_stackframes pge) stk1 stk2)
  :exists ibf2 ssIE',
    (fn_code f2) ! pc = Some ibf2
    /\ iblock_step strict tpge stk2 f2 sp rsI m bc ibf2.(entry) t ssIE'
    /\ match_states pge csE ssIE'.
Proof.
  exploit match_sexec_ok; eauto. 
  intros (ibf2 & PC' & SYMBS).
  pose (obc := if strict then Some bc else None).
  exploit (sexec_correct strict (Bfctx f1 (Bcctx pge sp rsS (m, obc))) (sis_source (fn_gm f2 pc)));
    simpl; eauto; try reflexivity.
  { intros; destruct MATCHI; eapply sis_source_correct; eauto. }
  { apply build_frame_history. }
  intros SEMSS. exploit sem_sstate_run; eauto.
  intros (sis & sfv & rs & m0 & SOUT & SEMSIS & TRIVFRAME & SEMSFV).
  apply sem_sis_ok in SEMSIS.
  set (ctx:=Sctx pge tpge symbols_preserved_rev sp rsS (m, obc)).
  replace ({| cge := pge; csp := sp; crs0 := rsS; cmbc := (m, obc) |}) with (bcctx1 ctx) in SOUT by auto.
  exploit SYMBS; eauto.
  { simpl; unfold obc; case strict; constructor. }
  intros (TRSS_OKH & TRSS_OKI & SER & SEL).
  exploit (match_sexec_csi_correct f1 f2 sp rsS m); repeat (eauto; simpl).
  simpl in SEL.
  intros (ssIE' & ISTEP2 & MS).
  repeat (econstructor; eauto).
Qed.

Local Hint Constructors step: core.

Theorem step_simulation s1 s1' t s2
  (STEP: step strict pge s1 t s1')
  (MS: match_states pge s1 s2)
  :exists s2',
    step strict tpge s2 t s2'
  /\ match_states pge s1' s2'.
Proof.
  destruct STEP as [stack ibf f sp n rs m bc t s PC STEP | | | ]; inv MS.
  - (* iblock *)
    simplify_someHyps. intros PC.
    exploit iblock_step_simulation; eauto.
    intros (ibf' & s2 & PC2 & STEP2 & MS2). 
    eexists; split; eauto.
  - (* function internal *)
    inversion TRANSF as [xf tf MF |]; subst.
    eexists; split.
    + econstructor. erewrite <- preserv_fnstacksize; eauto.
    + generalize (trivial_glueinv_entrypoint _ _ MF); eauto.
      generalize (trivial_histinv_entrypoint _ _ MF); eauto.
      erewrite preserv_fnparams; eauto.
      erewrite preserv_entrypoint; eauto.
      intros ONLYh ONLYg.
      econstructor; eauto.
      eapply only_liveness_match_invs; eauto.
  - (* function external *)
    inv TRANSF. eexists; split; econstructor; eauto.
    eapply external_call_symbols_preserved; eauto. apply senv_preserved.
  - (* return *)
    inv STACKS. destruct b1 as [res' f' sp' pc' rs'].
    eexists; split.
    + econstructor.
    + inv H1. econstructor; eauto.
Qed.

Theorem transf_program_correct:
  forward_simulation (sem strict prog) (sem strict tprog).
Proof.
  eapply forward_simulation_step with (match_states pge); simpl; eauto. (* lock-step with respect to match_states *)
  - eapply senv_preserved.
  - eapply transf_initial_states.
  - eapply transf_final_states.
  - intros; eapply step_simulation; eauto.
Qed.

End PRESERVATION.

End BTL_BlockSimulationproof.

(* TODO? MOVE *)
Lemma forward_simulation_refl l : forward_simulation l l.
Proof.
  apply forward_simulation_step with eq; intuition (subst; eauto).
Qed.


Module BTL_BlockSimulationCompproof (B: BTL_BlockSimulationConfig).

Include BTL_BlockSimulationComp B.
Module SimuProof := BTL_BlockSimulationproof B.

Local Open Scope linking_scope.

Definition passes :=
  (if number_load tt        then mkpass BTL_LoadNumbering.match_prog else pass_identity _) :::
  (if btl_value_analysis tt then mkpass VAannot.match_prog           else pass_identity _) :::
  mkpass SimuProof.match_prog :::
  pass_nil _.

Definition pass := compose_passes passes.

Lemma transf_program_match prog tprog:
  transf_program prog = OK tprog -> pass_match pass prog tprog.
Proof.
  unfold transf_program.
  set (p1 := if number_load () then _ else _).
  set (p2 := if btl_value_analysis () then _ else _).
  intro TR.
  unfold pass, passes; simpl.
  exists p1; split; [|exists p2; split; [|eexists; split; [|reflexivity]]].
  - unfold p1; case number_load. 2:reflexivity.
    apply BTL_LoadNumbering.transf_program_match.
  - unfold p2; case btl_value_analysis. 2:reflexivity.
    apply VAannot.transf_program_match.
  - apply SimuProof.transf_program_match; assumption.
Qed.

Section PRESERVATION.

Variable prog: program.
Variable tprog: program.

Hypothesis TRANSL: pass_match pass prog tprog.

Theorem transf_program_correct:
  forward_simulation (sem false prog) (sem false tprog).
Proof.
  revert TRANSL.
  unfold pass, passes; simpl.
  intros (p1 & P1 & p2 & P2 & p3 & P3 & ?); subst p3.
  apply compose_forward_simulations with (L2 := sem false p1); [|
  apply compose_forward_simulations with (L2 := sem (btl_value_analysis ()) p2); [|
  apply compose_forward_simulations with (L2 := sem (btl_value_analysis ()) tprog) ]].
  - revert P1; case number_load.
    + simpl. apply BTL_LoadNumbering.transf_program_correct.
    + intros ->. apply forward_simulation_refl.
  - revert P2; case btl_value_analysis.
    + simpl. apply VAannot.transf_program_correct.
    + intros ->. apply forward_simulation_refl.
  - revert P3. apply SimuProof.transf_program_correct.
  - case btl_value_analysis.
    + apply Strict_to_Lax.transf_program.
    + apply forward_simulation_refl.
Qed.

End PRESERVATION.

End BTL_BlockSimulationCompproof.
