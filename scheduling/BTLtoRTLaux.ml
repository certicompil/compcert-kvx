open Maps
open BTL
open RTL
open Camlcoq
open RTLcommonaux
open DebugPrint
open PrintBTL
open BTLcommonaux
open BTLtypes
open BTL_Renumber

let translate_function btl entry =
  let rtl_code = ref PTree.empty
  and inumb2p iinfo = P.of_int iinfo.inumb
  and get_inumb2p ib = P.of_int (get_inumb true ib)
  and get_inumb2p_succ s = P.of_int (get_inumb_succ btl true s) in
  let add_rtl inst inumb =
    rtl_code := PTree.set inumb inst !rtl_code
  in
  let rec translate_btl_block ib k =
    debug "Entering translate_btl_block...\n";
    print_btl_inst stderr ib;
    match ib with
    | Bcond (cond, lr, ib1, Bnop None, iinfo) ->
        translate_btl_block ib1 None;
        add_rtl
          (Icond
             ( cond,
               lr,
               get_inumb2p ib1,
               get_inumb2p (get_some k),
               iinfo.opt_info ))
          (inumb2p iinfo)
    | Bcond (_, _, _, _, _) -> failwith "translate_function: unsupported Bcond"
    | Bseq (ib1, ib2) ->
        translate_btl_block ib1 (Some ib2);
        translate_btl_block ib2 k
    | Bnop (Some iinfo) ->
        add_rtl (Inop (get_inumb2p (get_some k))) (inumb2p iinfo)
    | Bnop None ->
        failwith
          "translate_function: Bnop None can only be in the right child of \
           Bcond"
    | Bop (op, lr, rd, iinfo) ->
        add_rtl (Iop (op, lr, rd, get_inumb2p (get_some k))) (inumb2p iinfo)
    | Bload (trap, chk, addr, lr, _, rd, iinfo) ->
        add_rtl
          (Iload (trap, chk, addr, lr, rd, get_inumb2p (get_some k)))
          (inumb2p iinfo)
    | Bstore (chk, addr, lr, _, src, iinfo) ->
        add_rtl
          (Istore (chk, addr, lr, src, get_inumb2p (get_some k)))
          (inumb2p iinfo)
    | BF (Bcall (sign, fn, lr, rd, s), iinfo) ->
        add_rtl (Icall (sign, fn, lr, rd, get_inumb2p_succ s)) (inumb2p iinfo)
    | BF (Btailcall (sign, fn, lr), iinfo) ->
        add_rtl (Itailcall (sign, fn, lr)) (inumb2p iinfo)
    | BF (Bbuiltin (ef, lr, rd, s), iinfo) ->
        add_rtl (Ibuiltin (ef, lr, rd, get_inumb2p_succ s)) (inumb2p iinfo)
    | BF (Bjumptable (arg, tbl), iinfo) ->
        let trtbl = List.map get_inumb2p_succ tbl in
        add_rtl (Ijumptable (arg, trtbl)) (inumb2p iinfo)
    | BF (Breturn oreg, iinfo) -> add_rtl (Ireturn oreg) (inumb2p iinfo)
    | _ -> ()
  in
  List.iter
    (fun (n, ibf) ->
      let ib = ibf.entry in
      translate_btl_block ib None)
    (PTree.elements btl);
  !rtl_code

module Mapnode = Map.Make(Int)

let no_succ_inst inst =
  let null = i2p 1 in
  match inst with
  | Icond (cond, args, ifso, ifnot, info) -> Icond (cond, args, null, null, info)
  | Inop (succ) -> Inop (null)
  | Iop (op, args, dest, succ) -> Iop (op, args, dest, null)
  | Iload (trap, chunk, addr, args, dest, succ) -> Iload (trap, chunk, addr, args, dest, null)
  | Istore (chunk, addr, args, src, succ) -> Istore (chunk, addr, args, src, null)
  | Icall (sign, fn, args, dest, succ) -> Icall (sign, fn, args, dest, null)
  | Ibuiltin (ef, args, dest, succ) -> Ibuiltin (ef, args, dest, null)
  | Ijumptable (arg, tbl) -> Ijumptable (arg, [])
  | _ -> inst

module Instbl = Hashtbl.Make(struct 
    type t = RTL.instruction
    let equal i1 i2 = (no_succ_inst i1) = (no_succ_inst i2)
    let hash i = Hashtbl.hash (no_succ_inst i)
  end)

let find repm pc = 
  let rep = Mapnode.find pc repm in
  assert (rep <= pc); (* we do not need of a real union-find structure because of this invariant ! *)
  rep

let subst_inst repm inst =
  let fd = fun i -> i2p (find repm (p2i i)) in
  match inst with
  | Icond (cond, args, ifso, ifnot, info) -> Icond (cond, args, fd ifso, fd ifnot, info)
  | Inop (succ) -> Inop (fd succ)
  | Iop (op, args, dest, succ) -> Iop (op, args, dest, fd succ)
  | Iload (trap, chunk, addr, args, dest, succ) -> Iload (trap, chunk, addr, args, dest, fd succ)
  | Istore (chunk, addr, args, src, succ) -> Istore (chunk, addr, args, src, fd succ)
  | Icall (sign, fn, args, dest, succ) -> Icall (sign, fn, args, dest, fd succ)
  | Ibuiltin (ef, args, dest, succ) -> Ibuiltin (ef, args, dest, fd succ)
  | Ijumptable (arg, tbl) -> Ijumptable (arg, List.map fd tbl)
  | _ -> inst

let label cfg pc i =
  match Mapnode.find_opt pc (!cfg) with
  | None -> cfg := Mapnode.add pc i (!cfg)
  | Some i -> failwith "label: Label_already_present"

let minimize_output cfg repm =
  let cfg' = ref Mapnode.empty in
  Mapnode.iter (fun pc i -> if pc = find repm pc then label cfg' pc (subst_inst repm i))
    cfg;
  !cfg'

let init_rep repm pc =
  assert (not (Mapnode.mem pc !repm));
  repm := Mapnode.add pc pc !repm
              
let ord_add_rep repm pc rep =
  assert (rep < pc);
  assert (not (Mapnode.mem pc !repm));
  assert (Mapnode.mem rep !repm);
  repm := Mapnode.add pc rep !repm
    
let get_succ inst =
  match inst with
  | Icond (_, _, ifso, ifnot, _) -> (p2i ifso)::(p2i ifnot)::[]
  | Inop (succ)
  | Iop (_, _, _, succ)
  | Iload (_, _, _, _, _, succ)
  | Istore (_, _, _, _, succ)
  | Icall (_, _, _, _, succ)
  | Ibuiltin (_, _, _, succ) -> [p2i succ]
  | Ijumptable (arg, tbl) -> List.map p2i tbl
  | _ -> []

let first_approx cfg =
  let itbl = Instbl.create 10 in (* map: inst -> node *)
  let repm = ref Mapnode.empty in
  Mapnode.iter (fun pc i ->
      match Instbl.find_opt itbl i with
      | None -> (Instbl.add itbl i pc; init_rep repm pc)
      | Some pc' -> ord_add_rep repm pc pc')
    cfg;
  !repm

(* simplify the cfg by identifying the identical nodes  
rem: principle very similar to [first_approx] above...
*)
let simplify_id cfg =
  let itbl = Hashtbl.create 10 in
  let repm = ref Mapnode.empty in
  Mapnode.iter (fun pc i ->
      match Hashtbl.find_opt itbl i with
      | None -> (Hashtbl.add itbl i pc; init_rep repm pc)
      | Some pc' -> ord_add_rep repm pc pc')
    cfg;
  minimize_output cfg !repm, !repm

let preds cfg =
  let res = Hashtbl.create 10 in
  Mapnode.iter (fun pc i -> List.iter (fun succ -> Hashtbl.add res succ pc) (get_succ i)) cfg;
  res

type eqclass = {
    rep: int;
    mutable others: int list;
}

let eqv_of cfg repm = 
  let eqv = ref Mapnode.empty in
  Mapnode.iter (fun pc rep ->
      assert (rep <= pc);
      match Mapnode.find_opt rep !eqv with
      | Some eqc -> (
        assert (rep < pc);
        eqc.others <- pc::eqc.others)
      | None -> (
        assert (rep=pc);
        eqv := Mapnode.add rep { rep=rep; others = [] } !eqv))
    repm;
  Mapnode.filter (fun _ eqc -> eqc.others <> []) !eqv;;

type state = {
    cfg: RTL.instruction Mapnode.t;
    preds: (int, int) Hashtbl.t;
    old_repm: int Mapnode.t;
    mutable repm: int Mapnode.t;
    mutable eqv: eqclass Mapnode.t;
    mutable queue: eqclass Mapnode.t;
}

let init_state cfg =
  let cfg, old_repm = simplify_id cfg in
  let repm = first_approx cfg in
  let eqv = eqv_of cfg repm in
  { cfg = cfg;
    preds = preds cfg;
    old_repm = old_repm;
    repm = repm;
    eqv = eqv;
    queue = eqv;
  }

let init_eqc (s: state) pc =
  assert (not (Mapnode.mem pc s.eqv));
  s.repm <- Mapnode.add pc pc s.repm;
  let eqc = { rep=pc; others = [] } in
  eqc

let ord_add_eqc (s: state) pc eqc =
  assert (eqc.rep < pc);
  assert (not (Mapnode.mem pc s.eqv));
  s.repm <- Mapnode.add pc eqc.rep s.repm;
  eqc.others <- pc::eqc.others

let get_succ_repr (s: state) pc = 
  List.map (find s.repm) (get_succ (Mapnode.find pc s.cfg))

let init_succm s eqc =
  let succm = Hashtbl.create ((List.length eqc.others)+1) in  
  let succ_rep = get_succ_repr s eqc.rep in
  Hashtbl.add succm succ_rep eqc;
  succm

(* return the equivalent classes that are predecessors of [eqc] *)
let get_pred (s: state) eqc =
  let res = ref Mapnode.empty in
  List.iter (fun pc ->
      List.iter (fun pred ->
          match Mapnode.find_opt (find s.repm pred) s.eqv with
          | Some eqc2 -> res := Mapnode.add eqc2.rep eqc2 !res
          | None -> ())
        (Hashtbl.find_all s.preds pc))
    (eqc.rep::eqc.others);
  !res;;

let mark_to_visit s eqc pred fresh =
  (* add [pred] and [fresh] in the work list *)
  s.queue <- Mapnode.union (fun _ _ _ -> assert false)
               fresh
               (Mapnode.union (fun _ eqc1 eqc2 -> assert (eqc1 == eqc2); Some eqc1)
                 s.queue pred)

let next_approx (s:state) eqc = 
  let pred = get_pred s eqc in
  let fresh = ref Mapnode.empty in (* set of the created subclasses of [eqc] *)
  let succ_pairs = List.fold_left (fun acc pc -> (pc, get_succ_repr s pc)::acc) [] eqc.others in
  (* NB: succ_pairs is increasing on [pc] *)
  (* => now, we compute [fresh], update [eqc.others], and while updating [s.repm] accordingly *)
  let succm = init_succm s eqc in
  eqc.others <- [];
  List.iter (fun (pc, succ) ->
      (match Hashtbl.find_opt succm succ with
      | Some eqc' -> ord_add_eqc s pc eqc'
      | None -> (
        let eqc = init_eqc s pc in
        fresh := Mapnode.add pc eqc !fresh;
        Hashtbl.add succm succ eqc);
    ))
    succ_pairs;
  (* => now, we finish to update the global state *)
  if not (Mapnode.is_empty !fresh) then (
    (if eqc.others = [] then s.eqv <- Mapnode.remove eqc.rep s.eqv);
    fresh := Mapnode.filter (fun _ eqc2 -> eqc2.others <> []) !fresh;
    s.eqv <- Mapnode.union (fun _ _ _ -> assert false) s.eqv !fresh; 
    mark_to_visit s eqc pred !fresh (* pred, fresh and eqc need to be (re)visited *)
  )

let get_rep e repm old_repm =
  match Mapnode.find_opt (p2i e) old_repm with
  | Some e -> (match Mapnode.find_opt e repm with
    | Some e -> (i2p e)
    | None -> failwith "factorize_cfg: could not find new entry")
  | None -> failwith "factorize_cfg: could not find new entry (old)"

let factorize_cfg rtl new_entry dm =
  let cfg = PTree.fold (fun m i e -> Mapnode.add (p2i i) e m) rtl Mapnode.empty in
  let s = init_state cfg in
  while not (Mapnode.is_empty s.queue) do
    let (_, eqc) = Mapnode.find_first (fun _ -> true) s.queue in
    s.queue <- Mapnode.remove eqc.rep s.queue;
    next_approx s eqc;
  done;
  (*debug_flag := true;*)
  debug "CFG:\n";
  List.iter (if !debug_flag then PrintRTL.print_instruction stderr else fun u -> ())
    (Mapnode.fold (fun i e l -> (i, e)::l) cfg []);
  debug "s.CFG:\n";
  List.iter (if !debug_flag then PrintRTL.print_instruction stderr else fun u -> ())
    (Mapnode.fold (fun i e l -> (i, e)::l) s.cfg []);
  debug "repm:\n";
  Mapnode.iter (fun i e -> debug "%d -> %d,\n" i e) s.repm;
  let cfg' = minimize_output s.cfg s.repm in
  debug "Minimized CFG:\n";
  List.iter (if !debug_flag then PrintRTL.print_instruction stderr else fun u -> ())
    (Mapnode.fold (fun i e l -> (i, e)::l) cfg' []);
  (*debug_flag := false;*)
  let rtl = Mapnode.fold (fun i e t -> PTree.set (i2p i) e t) cfg' PTree.empty in
  (rtl, get_rep new_entry s.repm s.old_repm), (PTree.map (fun _ e -> get_rep e s.repm s.old_repm) dm)

let btl2rtl (f : BTL.coq_function) =
  let btl = f.fn_code in
  let entry = f.fn_entrypoint in
  let (btl', new_entry), dm = renumber btl entry in
  let rtl = translate_function btl' new_entry in
  let (rtl, new_entry), dm =
    if not f.fn_info.bb_mode && !Clflags.option_factorize then
      factorize_cfg rtl new_entry dm
    else
      (rtl, new_entry), dm in
  (*debug_flag := true;*)
  debug "Entry %d\n" (p2i new_entry);
  debug "BTL Code:\n";
  print_btl_code stderr btl';
  debug "RTL Code:";
  print_code rtl;
  debug "Dupmap:\n";
  print_ptree print_pint dm;
  debug "\n";
  (*debug_flag := false;*)
  ((rtl, new_entry), dm)
