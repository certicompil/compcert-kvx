# BTL examples and draft notes

### LOOP INVARIANT CODE MOTION without useless duplications

Par rapport à l'approche actuelle (Duplicate + CSE3), BTL apporte deux améliorations:

- une passe de **factorisation** de code lors de BTL -> RTL, réalisée avec le *même* vérificateur que la passe de duplication RTL -> BTL.
- on utilise notre vérificateur de simulation générique (et donc on
  peut potentiellement combiner avec ses autres features, comme
  l'analyse de liveness, la réécriture...)

#### Exemple de base

On considère le code RTL d'entrée:

    // entry = loop
    loop:
           if z > 1000 goto exit
           x = y*y
           z = z + x
           goto loop
    exit: // live variables: z, y.  (x dead!)

Pour montrer le LICM, il suffit d'insérer un bloc réduit à Inop avant la boucle.
C'est ce Inop qui va être simulé par le code déplacé.

Le BTL suivant:

    entry: goto loop
    loop:
            if z > 1000
            then:
                goto exit
            else:
                x = y*y
                z = z + x
                goto loop
    exit:

est directement simulé par:

    entry: { y:=y | z := z } 
            x = y*y
            goto loop
    loop: { y:=y | z := z | x := y*y }
            if z > 1000
            then:
                goto exit
            else:
                z = z + x
                goto loop
    exit: { y:=y | z := z }

   
#### Variation: LICM d'un calcul trappant.
 
Ci-dessus, si `x` est live dans `exit`, on peut appliquer (pendant CSE)
un renommage avec une variable pas live: ça élimine le pb...
    
Par contre, si au lieu de `x = y*y`, on a une opération qui trappe
potentiellement comme `x = y[5]`, c'est plus compliqué.  Pour s'en
sortir, il faut passer par des phases de déroulement et factorisation
de boucle lors du passage RTL<->BTL.

On considère le code RTL d'entrée:

    // entry = loop
    loop:
           if z > 1000 goto exit
           x = y[5]
           z = z + x
           goto loop
    exit: // live variables: z, y, x


(1) Sélection du code BTL, après déroulage de la 1ère itération de la boucle et loop-rotate.
   **Vérificateur** RTL <-> BTL.

    // entry = iter1
    iter1:
            if z > 1000
            then:
                goto exit
            else:
                x = y[5]
                z = z + x
                if z > 1000
                then:
                  goto exit
                else:
                  goto loop
    loop:
            x = y[5]
            z = z + x
            if z > 1000
            then:
                goto exit
            else:
                goto loop
    exit:
 

(2) BTL après CSE.
   **Vérificateur** Block simulation modulo Gluing Invariants. (GI)

    // entry = iter1
    iter1: // GI: { y:=y | z := z } 
            if z > 1000
            then:
                goto exit
            else:
                x = y[5]
                z = z + x
                if z > 1000
                then:
                   goto exit
                else:
                   goto loop
    loop: // GI: { y:=y | z := z | x := y[5] }
            z = z + x
            if z > 1000
            then:
                goto exit
            else:
                goto loop
    exit: // GI: { y:=y | z := z | x := x}


(3) Le code RTL après factorisation de la première itération est:

    // entry = iter1
    iter1:
            if z > 1000 goto exit
            x = y[5]
            goto loop
    loop:
            z = z + x
            if z > 1000 goto exit
            goto loop
    exit:

(REM: en principe la linearisation vers Linear s'occupera d'inverser la condition, non ?)

### EXAMPLE OF STRENGTH REDUCTION

Exemple initial... Voir [BTLstrengthreduction.md](BTLstrengthreduction.md) pour un roadmap plus détaillé sur cette partie...

On aimerait passer du code C1:

    init: // inputs: int *t,  int n, int s 
    int i=0;
    loop: 
        if (i >= n) goto exit;
        s += t[i];
        i += 1;
        goto loop;
    exit: // inputs: int *t,  int s
   
au code C2 (annoté avec les invariants de collage):

    init: // { t := t | n := n | s := s }
        int i=0;
        int *ti = t;
    loop: // { t := t | s := s | ti := t+i | tn := t+n }
        if (i >= n) goto exit;
        s += *ti;
        i +=1;
        ti += 1;
        goto loop;
    exit; 

**ATTENTION** l'optimisation suivante avec le code C3 est subtilement **incorrecte**:

    init: // { t := t | n := n | s := s }
    int *ti = t;
    int *tn = t+n;
    loop: // { t := t | s := s | ti := t+i | tn := t+n }
        if (ti >= tn) goto exit;
        s += *ti;
        ti += 1; // i.e. sizeof(int)
        goto loop;
    exit; // { t := t | s := s }

Cette incorrection est détectée dans la sémantique formelle par le
fait que le test `ti >= tn` introduit potentiellement des **undefined
behaviors**.

Ici il y a quand même **réel bug**: si on prend `t:=NULL` et `n:=-1`,
alors C1 et C2 ne rentrent pas dans la boucle, et il n'y a pas de
segfault.  Mais C3 fait un segfault (avec un `s += *NULL`). En effet,
la comparaison de pointeurs s'effectue dans l'arithmétique non-signée.
D'où `NULL < NULL-1` (à cause d'un débordement arithmétique).

Sur cet exemple, pour éviter les pbs, il faut **au minimum** dérouler la première
itération + loop-rotate.

Par exemple en partant du code initial, en fusionant le bloc d'entrée
de la première itération de boucle avec son unique prédécesseur (c'est
sans ça l'histoire du "best-predecessor" de la thèse de Cyril ?) puis
avec loop-rotate, on obtient:

    init: // inputs: int *t,  int n, int s 
        i=0;
        if (i >= n) goto exit;
        s += t[i];
        i += 1;
        if (i >= n) goto exit;
        goto loop;
    loop: 
        s += t[i];
        i += 1;
        if (i >= n) goto exit;
        goto loop;
    exit: // inputs: int *t,  int s

On peut donc facilement faire apparaître:

    init: // { t := t | n := n | s := s }
        if (0 >= n) goto exit; // pas clair que la propagation de constante soit utile ici ?
        i=0;
        ti = t;
        s += *ti;
        i += 1;
        ti += 1;
        if (i >= n) goto exit;
        goto loop;
    loop: // { t := t | s := s | i:=i | ti := t+i }
        s += *ti;
        i +=1;
        ti += 1;
        if (i >= n) goto exit;
       goto loop;
    exit; // { t := t | s := s }

Et replier ensuite en:

    init:
        if (0 >= n) goto exit;
        i=0;
        ti = t;
        goto loop;
    loop: 
        s += *ti;
        i +=1;
        ti += 1;
        if (i >= n) goto exit;
        goto loop;
    exit; 

Bon ici: protégé par le test `0 >= n`, on a *peut-être* le droit d'introduire le `tn := t+n` de C3 et de remplacer le `i>=n` par `ti >= tn`.
Mais c'est sans doute pas évident à prouver ! 
Les cas de wraparound semblent toujours possibles, mais ils auraient plutôt tendance à faire éviter un segfault qu'à en un ajouter un ?

Le bénéfice de l'élimination du `i` ne vaut clairement pas la complexité du raisonnement en jeu !

### Généralisation avec des invariants d'histoire (ou "history invariants").

Exemple 1:

    A: 
        goto Loop
    Loop:
        x = y*y
        if z > 100 goto B
        z = z+x
        goto Loop
    B:
        y = x+y*y   // ici: un CSE malin se souvient que x et y*y sont la même chose !
        z = z+y
        goto C
    C: // variables lives: x, y, z

est simulé par
    
    A: // { y := y | z := z }
        x0 = y * y
        goto Loop
    Loop:  // { x0 := y*y | y := y | z := z }
        if z > 100 goto B
        z = z+x0
        goto Loop
    B:  // history { x := y*y }; { x0 := x | z:=z }
        y = x0+x0
        z = z+y
        goto C
    C: // history { x := y*y }; { x0 := x | y:=y | z := z }

Ci-dessus j'ai renommé le `x` du source en `x0` dans la cible, mais ce
n'est pas obligatoire (c'est juste pour montrer qu'un tel renommage
est possible, et qu'il clarifie la preuve de raffinement).

Exemple 2

    A: 
        goto Loop
    Loop:
        x = y*y  // calcul inutile quand on est dans la boucle - on peut retarder ce calcul dans B.
        if z > 100 goto B
        x = y+1
        z = z+x
        goto Loop
    B:
        y = x+y*y
        z = z+y
        goto C
    C: // variables lives: x, y, z

est simulé par

    A: 
        x=y+1
        goto Loop
    Loop:  // { x:=y+1 | y := y | z := z }
        if z > 100 goto B
        z = z+x
        goto Loop
    B:  // history { x := y*y }; { y:=y | z:=z }
        p = y*y 
        y = p+p
        z = z+y
    C: // { y:=y | z:=z | p := x }

Rem: ci-dessus, j'ai utilisé `p` (comme prophécie), mais on
aurait pu tout aussi bien réutiliser `x` (c'était juste pour clarifier).

Cet exemple suggère que les invariants d'histoire (côté source) peuvent être interprétés comme des prophécies (côté cible) !
cf. les prophécies de [The existence of refinement mappings](https://www.sciencedirect.com/science/article/pii/030439759190224P) de Abadi and Lamport (TCS'91).

### Traps dans les GI et invariants d'histoire

On veut montrer que l'entrée suivante:

    A: 
        x = y[5]; goto B
    B: 
        z = 1; goto C
    C: 
        t = y[5]; ...
    
est simulée par:

    A: // { y := y }
        x = y[5]; goto B
    B: // { x := y[5] | y := y }
        z = 1; goto C
    C: // { x := y[5] | y := y | z := z }
        t = x; ...
        
Cette preuve n'est pas acceptée par la première version de notre théorie, car lors de l'exécution symbolique du source B, le `x := y[5]` est considéré comme trappant
En effet, dans cette première version, notre test de simulation `S; GJ =>_dom(GJ) GI; T` doit en fait aussi verifier `ok(S) => ok(GJ)` pour être correct.

Une solution pour accepter l'exemple ci-dessus est de généraliser le test  `ok(GI); S; GJ =>_dom(GJ) GI; T` en vérifiant aussi `ok(GI);S => ok(GJ)`.

En présence des invariants d'histoire, on peut donc voir conceptuellement le `ok(GI)` comme une part "implicite" de l'invariant d'histoire
(ce qui est cohérent, avec l'idée que `ok(GI)` mémorise que les calculs trappant de  `GI` n'ont pas échoué).


1. Vérif de l'invariant d'histoire:

        (HI; ok(GI); S)  =>_dom(HJ)  HJ; ok(GJ)

(pas vraiment de condition de liveness ici).

2. Vérif de l'invariant de collage:

        (HI; ok(GI); S; GJ)  =>_dom(GJ)  (HI; GI)_dom(GI); T 
        

### Règles de réécriture

L'interaction des règles de réécriture avec les invariants n'est pas encore très claire. Deux pistes:

1. Il est sans doute préférable de paramétrer BlockSimulation par le
système de réécriture (à part certaines qu'on peut fixer en dure,
comme les loads non-trappants).  Rem: en pratique, le système de
réécriture n'est introduit que dans BTL_SEimpl (mais la dépendance
remonte à BlockSimulation, car l'implem du test de simulation
symbolique aura donc le système de réécriture en paramètre).

2. Les régles de réécriture doivent-elles être appliquées à l'exécution symbolique des invariants ? 
Ça semble cohérent de faire ça (un système de réécriture est juste un façon de ramener une relation d'équivalence entre termes à de l'égalité syntaxique: il semble donc logique d'appliquer la relation d'équivalence aussi dans les invariants, sinon, ça risque de donner un résultat bizarre !)
Avec la représentation séquentielle des invariants, ça semble assez simple. 
Finalement, on fait la même chose que pour l'exécution symbolique du BTL (mais sur une séquence d'affectations symboliques au lieu d'une séquence d'affections concrète). 


### AUTRES GENERALISATIONS (What comes next ?)

Est-ce qu'il ne faut pas envisager une combinaison "execution symbolique + value analysis" ? La value analysis pourrait se faire directement sur la sémantique symbolique (car on sait que la sémantique symbolique est équivalente à la sémantique concrète). On pourrait donc faire l'abstraction en sortie de l'exécution symbolique, avec un mécanisme de mémoïsation (pour éviter les calculs redondants dus à la duplication de termes dans l'état symbolique). Intérêts: la value analysis ne se ferait que sur les registres live + on passerait d'invariants purement symboliques à des invariants combinant valeurs symboliques et valeurs abstraites.

Il faut bien sûr que l'abstraction des états symboliques satisfasse la propriété usuelle: la simulation des états abstraits implique celle des états concrets (qui seraient ici les états symboliques).

En pratique, une telle value analysis pourraient servir à montrer que certains calculs ne trappent pas (e.g. on peut donc les anticiper tranquillement), ou à identifier des termes (e.g. analyse d'alias.

## Support of SSA-optimizations

Minimum feature: extends BTL with "register renamings" at exits. This should enable to represent SSA-forms in BTL IR, more or less like in MLIR.

Maximum feature: add also a basic instruction that performs parallel renaming of registers. 
This simple feature would help to represent a very general notion of "partial SSA forms": since they could appear in the middle of a block.

In both case, such register renamings would be forbidden in BTL<->RTL matching (SSA-optimizations could only happen within BTL passes).

## Alias analysis in the symbolic simulation

A REGARDER [papier pointé par Justus](https://vbpf.github.io/assets/prevail-paper.pdf) 
