open Maps
open RTLcommonaux
open BTL
open BTLtypes
open Machine
open DebugPrint
open PrintBTL
open BTLcommonaux
open BTL_Liveness
open PrepassSchedulingOracle
open ExpansionOracle
open BTL_RegisterRenaming
open BTL_BlockOptimizeraux
open BTL_IfLiftingOracle
open LazyCodeOracle
open BTL_SyntheticNodes
module IM = Map.Make (Int)

(*
let compute_makespan_ibf ibf btl =
  let bseq, olast = flatten_blk_basics ibf in
  let seqa = Array.mapi (fun i inst -> (inst, get_liveins inst)) bseq in
  compute_greedy_makespan seqa btl
 *)

let simple_schedule n ibf btl typing va =
  let bseq, olast = flatten_blk_basics ibf in
  let seqa = Array.mapi (fun i inst -> (inst, get_liveins inst)) bseq in
  let live_regs_entry = get_live_regs_entry seqa ibf btl in
  let refcount = reference_counting seqa ibf.binfo.s_output_regs typing in
  (*debug_flag := true;*)
  debug "sched inst list:\n";
  Array.iteri
    (fun i inst ->
      debug "%d: " i;
      print_btl_inst stderr inst)
    bseq;
  (*debug_flag := false;*)
  match
    schedule_sequence seqa btl live_regs_entry typing va refcount Default
  with
  | Some (positions, s) ->
      let new_ib = apply_schedule bseq olast positions in
      let new_ibf = mk_ibinfo new_ib ibf.binfo in
      PTree.set n new_ibf btl
  | None -> btl

let turn_all_loads_nontrap n ibf btl =
  if (not !config.has_non_trapping_loads) || not !Clflags.option_fnontrap_loads
  then btl
  else
    let rec traverse_rec ib =
      match ib with
      | Bseq (ib1, ib2) -> Bseq (traverse_rec ib1, traverse_rec ib2)
      | Bload (t, a, b, c, aa, d, e) ->
          let _opt_info =
            match t with AST.TRAP -> Some true | AST.NOTRAP -> Some false
          in
          e.opt_info <- _opt_info;
          Bload (AST.NOTRAP, a, b, c, aa, d, e)
      | _ -> ib
    in
    let ib' = traverse_rec ibf.entry in
    let ibf' = mk_ibinfo ib' ibf.binfo in
    PTree.set n ibf' btl

let schedule_blk n ibf btl typing ren_map va =
  if !Clflags.option_if_lift
  then if_lift n ibf btl typing va ren_map
  else if !Clflags.option_fprepass
  then simple_schedule n ibf btl typing va
  else btl

let schedule_tf (va : bool) btl fi n ibf =
  compute_liveins n ibf btl;
  let code_nt = turn_all_loads_nontrap n ibf btl in
  let ibf_nt = get_some @@ PTree.get n code_nt in
  let code_freg, ren_map = get_freg n ibf_nt code_nt in
  let ibf_freg = get_some @@ PTree.get n code_freg in
  schedule_blk n ibf_freg code_freg fi.typing ren_map va

let rec apply_optims tf btl fi = function
  | [] -> btl
  | (n, ibf) :: blks -> apply_optims tf (tf btl fi n ibf) fi blks

let btl_expansions_oracle f =
  let btl = f.fn_code in
  let elts = PTree.elements btl in
  find_last_reg_btl elts;
  let btl_exp = apply_optims expanse btl f.fn_info elts in
  let need_dce = update_liveness_btl btl_exp in
  let btl_dce =
    if need_dce then btl_simple_dead_code_elimination btl_exp else btl_exp
  in
  let gm = build_liveness_invariants elts PTree.empty in
  ((btl_dce, f.fn_info), gm)

let btl_lazy_code_oracle f =
  let btl = f.fn_code in
  let btl_optim, gm = lazy_code_oracle btl f.fn_entrypoint in
  let btl_nosynth = eliminate_synthetic_nodes btl_optim in
  ((btl_nosynth, f.fn_info), gm)

let btl_scheduling_oracle ro f =
  let need_dce = update_liveness_btl f.fn_code in
  let btl_dce =
    if need_dce then btl_simple_dead_code_elimination f.fn_code else f.fn_code
  in
  let elts = PTree.elements btl_dce in
  find_last_reg_btl elts;
  let btl_optim = apply_optims (schedule_tf !Clflags.option_fprepass_alias_abs) btl_dce f.fn_info elts in
  let gm = build_liveness_invariants elts PTree.empty in
  if BTL_IfLiftingOracle.save_stats then BTL_IfLiftingOracle.write_stats ();
  ((btl_optim, f.fn_info), gm)

let btl_expansions_rrules () = BTL_SEsimuref.RRexpansions
let btl_expansions_value_analysis () = false

let btl_lazy_code_rrules  () = BTL_SEsimuref.RRstrength
let btl_lazy_code_value_analysis () = false

let btl_scheduling_rrules () =
    if !Clflags.option_fprepass_alias_rel || !Clflags.option_fprepass_alias_abs
    then BTL_SEsimuref.RRschedule !Clflags.option_fprepass_alias_rel
    else BTL_SEsimuref.RRnone
let btl_scheduling_value_analysis () =
    !Clflags.option_fprepass_alias_abs
