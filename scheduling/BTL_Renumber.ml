open Maps
open BTL
open RTLcommonaux
open BTLcommonaux
open BTLtypes

let recompute_inumbs btl entry =
  let last_used = ref 0 and ibf = pget entry btl in
  apply_over_function btl (tf_ibf_ib reset_visited_ib_rec);
  let ipos () =
    last_used := !last_used + 1;
    !last_used
  in
  let rec walk ib k =
    let continue () = match k with Some ib -> walk ib None | _ -> () in
    let final_case succ =
      let ib' = (pget succ btl).entry in
      walk ib' None
    in
    if jump_visit ib then ()
    else
      match ib with
      | Bseq (ib1, ib2) ->
          continue ();
          walk ib1 (Some ib2)
      | Bnop None ->
          failwith
            "recompute_inumbs: Bnop None can only be in the right child of \
             Bcond"
      | Bnop (Some iinfo)
      | Bop (_, _, _, iinfo)
      | Bload (_, _, _, _, _, _, iinfo)
      | Bstore (_, _, _, _, _, iinfo) ->
          continue ();
          iinfo.inumb <- ipos ()
      | Bcond (_, _, ib1, Bnop None, iinfo) ->
          let s1 = get_inumb false ib1 and ib2 = get_some @@ k in
          let s2 = get_inumb false ib2 in
          if s1 <= s2 then (
            walk ib1 None;
            walk ib2 None)
          else (
            walk ib2 None;
            walk ib1 None);
          iinfo.inumb <- ipos ()
      | Bcond (_, _, _, _, _) -> failwith "recompute_inumbs: unsupported Bcond"
      | BF (Bcall (_, _, _, _, s), iinfo)
      | BF (Bbuiltin (_, _, _, s), iinfo)
      | BF (Bgoto s, iinfo) ->
          final_case s;
          iinfo.inumb <- ipos ()
      | BF (Bjumptable (_, tbl), iinfo) ->
          List.iter final_case tbl;
          iinfo.inumb <- ipos ()
      | BF (Btailcall (_, _, _), iinfo) | BF (Breturn _, iinfo) ->
          iinfo.inumb <- ipos ()
  in
  walk ibf.entry None

let regenerate_btl_tree btl entry =
  let new_entry = ref entry in
  let rec renumber_iblock ib =
    let get_new_succ s = i2p (get_inumb_succ btl false s) in
    match ib with
    | BF (Bcall (sign, fn, lr, rd, s), iinfo) ->
        BF (Bcall (sign, fn, lr, rd, get_new_succ s), iinfo)
    | BF (Bbuiltin (sign, fn, lr, s), iinfo) ->
        BF (Bbuiltin (sign, fn, lr, get_new_succ s), iinfo)
    | BF (Bgoto s, iinfo) -> BF (Bgoto (get_new_succ s), iinfo)
    | BF (Bjumptable (arg, tbl), iinfo) ->
        let tbl' = List.map (fun s -> get_new_succ s) tbl in
        BF (Bjumptable (arg, tbl'), iinfo)
    | Bcond (cond, lr, ib1, ib2, iinfo) ->
        Bcond (cond, lr, renumber_iblock ib1, renumber_iblock ib2, iinfo)
    | Bseq (ib1, ib2) -> Bseq (renumber_iblock ib1, renumber_iblock ib2)
    | _ -> ib
  in
  let dm = ref PTree.empty in
  let ord_btl =
    PTree.fold
      (fun ord_btl old_n old_ibf ->
        let ib = renumber_iblock old_ibf.entry in
        let n = get_inumb false ib in
        let n_pos = i2p n in
        let bi =
          mk_binfo n old_ibf.binfo.input_regs old_ibf.binfo.s_output_regs [] []
        in
        let ibf = mk_ibinfo ib bi in
        if old_n = entry then new_entry := n_pos;
        dm := PTree.set old_n n_pos !dm;
        PTree.set n_pos ibf ord_btl)
      btl PTree.empty
  in
  ((ord_btl, !new_entry), !dm)

let renumber btl entry =
  recompute_inumbs btl entry;
  regenerate_btl_tree btl entry
