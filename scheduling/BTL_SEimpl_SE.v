Require Import Coqlib AST.
Require Import Op Registers.
Require Import BTL.
Require Import BTL_SEsimuref BTL_SEtheory OptionMonad.
Require Import BTL_SEsimplify BTL_SEsimplifyproof BTL_SEsimplifyMem BTL_SEimpl_prelude.

Import Notations.
Import HConsing.
Import SvalNotations.

Import ListNotations.
Local Open Scope list_scope.
Local Open Scope lazy_bool_scope.
Local Open Scope option_monad_scope.
Local Open Scope impure.

Local Hint Resolve substitution_rule_si_empty: sempty.

Section SymbolicExecution.

  Context `{HCF : HashConsingFcts}.
  Context `{HC : HashConsingHyps HCF}.
  Context `{RRULES: rrules_set}.

  Hint Resolve hSnil_correct: wlp.
  Hint Resolve hScons_correct: wlp.
  Hint Resolve hSstore_correct: wlp.
  Hint Resolve fsval_list_proj_correct: wlp.

(** ** Hash-consed operations on [list_sval] *)

Fixpoint hlist_sreg_get (hrs: ristate) (l: list reg): ?? list_sval :=
  match l with
  | nil => @hSnil HCF ()
  | r::l =>
    DO v   <~ @hrs_sreg_get HCF hrs r;;
    DO lsv <~ hlist_sreg_get hrs l;;
    @hScons HCF v lsv
  end.

Lemma hlist_sreg_get_spec hrs l:
  WHEN hlist_sreg_get hrs l ~> lsv THEN
  forall ctx sis
  (RR: ris_refines_ok ctx hrs sis),
  exists lsv', lmap_sv sis l = Some lsv' /\ list_sval_equiv ctx lsv lsv'.
Proof.
  induction l; simpl; [wlp_simplify|wlp_seq].
  intros v GET lsv' GETL lsv CONS; intros.
  eapply hScons_correct in CONS as ->; try reflexivity; simpl.
  eapply hrs_sreg_get_spec in GET as [? [-> ->]]; eauto.
  eapply IHl in GETL as [? [-> ->]]; eauto.
Qed.
Global Opaque hlist_sreg_get. 

(** ** Normalization and rewritings *)

Definition cbranch_expanse (prev: ristate) (cond: condition)
  (args: list reg): ?? (condition * list_sval) :=
  match rewrite_cbranches RRULES prev cond args with
  | Some (cond', vargs) =>
      DO vargs' <~ @fsval_list_proj HCF vargs;;
      RET (cond', vargs')
  | None =>
      DO vargs <~ hlist_sreg_get prev args ;;
      RET (cond, vargs)
  end.

Lemma cbranch_expanse_spec hrs c l:
  WHEN cbranch_expanse hrs c l ~> r THEN
  forall ctx sis
  (RR: ris_refines_ok ctx hrs sis),
  exists lsv, lmap_sv sis l = Some lsv /\
    eval_scondition ctx (fst r) (snd r) = eval_scondition ctx c lsv.
Proof.
  unfold cbranch_expanse.
  case rewrite_cbranches as [(cond', vargs)|] eqn:REW; wlp_seq.
  - intros vargs' PJ; intros; apply fsval_list_proj_correct in PJ.
    case lmap_sv as [lsv|] eqn:LSV.
    + exists lsv; split. reflexivity. simpl.
      eapply rewrite_cbranches_correct in REW as <-; eauto using ROK.
      unfold eval_scondition. rewrite PJ. reflexivity.
    + exfalso. eapply rewrite_cbranches_nofail in REW; eauto using ROK.
  - intros vargs GETL; intros; eapply hlist_sreg_get_spec in GETL as [lsv [? EVAL]]; eauto.
    exists lsv; simpl; unfold eval_scondition; rewrite EVAL; auto.
Qed.
Global Opaque cbranch_expanse.

(** ** Assignment of memory *)

Definition hrewrite_store (strict : bool) (sm: smem)
  (chunk: memory_chunk) (addr: addressing) (lsv: list_sval) aaddr (sv : sval)
  : ?? smem :=
  match rewrite_store RRULES strict sm chunk addr lsv aaddr sv with
  | Some fsv => @fsmem_proj HCF fsv
  | None => @hSstore HCF sm chunk addr lsv aaddr sv
  end.

(* In order to obtain [hrs_ok_store_okpreserv], we cannot assume here that [sm] evaluates without error. *)
Lemma hrewrite_store_correct strict sm chunk addr lsv aaddr sv:
  WHEN hrewrite_store strict sm chunk addr lsv aaddr sv ~> rew THEN
  forall ctx
  (STRICT: ctx_strict_reflect (ctx_bc ctx) strict),
  smem_equiv ctx rew (fSstore sm chunk addr lsv aaddr sv).
Proof.
  unfold hrewrite_store.
  case rewrite_store as [rew|] eqn:REW0; intros rew' REW ctx STRICT.
  + apply fsmem_proj_correct in REW. rewrite <- REW.
    eapply rewrite_store_correct in REW0; eauto.
  + apply hSstore_correct in REW; eauto.
Qed.
Global Opaque hrewrite_store.


Definition hrexec_store m chunk addr args aaddr src hrs: ?? ristate :=
  DO hargs <~ hlist_sreg_get hrs args;;
  DO hsrc <~ @hrs_sreg_get HCF hrs src;;
  DO hm <~ hrewrite_store (se_abs_strict m) hrs chunk addr hargs aaddr hsrc;;
  RET (rset_smem hm hrs).

Lemma hrexec_store_spec m chunk addr args aaddr src hrs:
  WHEN hrexec_store m chunk addr args aaddr src hrs ~> hrs' THEN
  forall ctx sis
  (CTXM: ctx_se_mode m ctx)
  (RR: ris_refines_ok ctx hrs sis),
  exists sis',
    sexec_store chunk addr args aaddr src sis = Some sis' /\
    ris_refines ctx hrs' sis'.
Proof.
  unfold hrexec_store, sexec_store.
  wlp_seq; intros hargs GARGS hsrc GSRC hm REW; intros.
    eapply hlist_sreg_get_spec    in GARGS as [sargs [-> EQA]]; eauto.
    eapply hrs_sreg_get_spec      in GSRC  as [ssrc  [-> EQS]]; eauto.
    eapply hrewrite_store_correct in REW.
  eexists; split. reflexivity.
  apply rset_mem_correct1; auto.
  - rewrite REW; auto. simpl; intros ->. repeat autodestruct.
  - rewrite REW; auto. simpl. erewrite MEM_EQ, EQA, EQS; eauto.
Qed.

Lemma hrs_ok_store_okpreserv m ctx chunk addr args aaddr src hrs
  (CTXM: ctx_se_mode m ctx):
  WHEN hrexec_store m chunk addr args aaddr src hrs ~> rst THEN
  ris_ok ctx rst -> ris_ok ctx hrs.
Proof.
  unfold hrexec_store; wlp_seq; intros.
  exploit hrewrite_store_correct; eauto. intros MEM_EQ.
  rewrite ok_rset_mem in H2 by (rewrite MEM_EQ; simpl; intros ->; repeat autodestruct).
  apply H2.
Qed.
Global Opaque hrexec_store.

(** ** Execution of builtins *)

Fixpoint hbuiltin_arg (hrs: ristate) (arg : builtin_arg reg): ?? builtin_arg sval := 
  match arg with
  | BA r => 
         DO v <~ @hrs_sreg_get HCF hrs r;;
         RET (BA v)
  | BA_int n => RET (BA_int n)
  | BA_long n => RET (BA_long n)
  | BA_float f0 => RET (BA_float f0)
  | BA_single s => RET (BA_single s)
  | BA_loadstack chunk ptr => RET (BA_loadstack chunk ptr)
  | BA_addrstack ptr => RET (BA_addrstack ptr)
  | BA_loadglobal chunk id ptr => RET (BA_loadglobal chunk id ptr)
  | BA_addrglobal id ptr => RET (BA_addrglobal id ptr)
  | BA_splitlong ba1 ba2 => 
    DO v1 <~ hbuiltin_arg hrs ba1;;
    DO v2 <~ hbuiltin_arg hrs ba2;;
    RET (BA_splitlong v1 v2)
  | BA_addptr ba1 ba2 => 
    DO v1 <~ hbuiltin_arg hrs ba1;;
    DO v2 <~ hbuiltin_arg hrs ba2;;
    RET (BA_addptr v1 v2)
  end.

Lemma hbuiltin_arg_spec hrs arg:
  WHEN hbuiltin_arg hrs arg ~> hargs THEN
  forall ctx sis
  (RR: ris_refines_ok ctx hrs sis),
  exists sarg,
    map_builtin_arg_opt sis arg = Some sarg /\
    eval_builtin_sval ctx hargs = eval_builtin_sval ctx sarg.
Proof.
  unfold eval_builtin_sval.
  induction arg; try solve [wlp_simplify; inv SARG; eauto]; simpl; wlp_seq; simpl.
  1  :intros v V; intros. eapply hrs_sreg_get_spec in V as [? [-> ->]]; eauto.
  all:intros v1 V1 v2 V2; intros;
      eapply IHarg1 in V1 as [? [-> ->]]; eauto;
      eapply IHarg2 in V2 as [? [-> ->]]; eauto.
Qed.
Global Opaque hbuiltin_arg.


Fixpoint hbuiltin_args hrs (args: list (builtin_arg reg)): ?? list (builtin_arg sval) :=
  match args with
  | nil => RET nil
  | a::l =>
    DO ha <~ hbuiltin_arg hrs a;;
    DO hl <~ hbuiltin_args hrs l;;
    RET (ha::hl)
    end.

Lemma hbuiltin_args_spec hrs args:
  WHEN hbuiltin_args hrs args ~> hargs THEN
  forall ctx sis
  (RR: ris_refines_ok ctx hrs sis),
  exists sargs,
    bamap_opt (map_builtin_arg_opt sis) args = Some sargs /\
    bargs_simu ctx hargs sargs.
Proof.
  unfold bamap_opt, bargs_simu, eval_list_builtin_sval; induction args; simpl; wlp_seq; eauto.
  intros ha HA hl HL; intros; simpl.
  eapply hbuiltin_arg_spec in HA as [? [-> ->]]; eauto.
  eapply IHargs in HL as [? [-> ->]]; eauto.
Qed.
Global Opaque hbuiltin_args.


(** ** Execution of final instructions *)

Definition hsum_left_optmap {A B C} (f: A -> ?? B) (x: A + C): ?? (B + C) :=
  match x with
  | inl r => DO hr <~ f r;; RET (inl hr)
  | inr s => RET (inr s)
  end.

Lemma hsum_left_optmap_spec hrs (ros: reg + ident):
  WHEN hsum_left_optmap (@hrs_sreg_get HCF hrs) ros ~> hsi THEN forall ctx (sis: sistate)
  (RR: ris_refines_ok ctx hrs sis),
  exists svi,
    sum_left_optmap sis ros = Some svi /\
    svident_simu ctx hsi svi.
Proof.
  case ros as [r|]; simpl; wlp_seq.
  - intros hr HR; intros.
    eapply hrs_sreg_get_spec in HR as [? [->]]; eauto.
    repeat econstructor; auto.
  - repeat econstructor.
Qed.
Global Opaque hsum_left_optmap.


Definition hrexec_final_sfv (i: final) hrs: ?? sfval := 
  match i with
  | Bgoto pc => RET (Sgoto pc)
  | Bcall sig ros args res pc => 
      DO svos <~ hsum_left_optmap (@hrs_sreg_get HCF hrs) ros;;
      DO sargs <~ hlist_sreg_get hrs args;;
      RET (Scall sig svos sargs res pc)
  | Btailcall sig ros args =>
      DO svos <~ hsum_left_optmap (@hrs_sreg_get HCF hrs) ros;;
      DO sargs <~ hlist_sreg_get hrs args;;
      RET (Stailcall sig svos sargs)
  | Bbuiltin ef args res pc =>
      DO sargs <~ hbuiltin_args hrs args;;
      RET (Sbuiltin ef sargs res pc)
  | Breturn or => 
      match or with
      | Some r => DO hr <~ @hrs_sreg_get HCF hrs r;; RET (Sreturn (Some hr))
      | None => RET (Sreturn None)
      end
  | Bjumptable reg tbl =>
      DO sv <~ @hrs_sreg_get HCF hrs reg;;
      RET (Sjumptable sv tbl)
  end.

Lemma hrexec_final_sfv_spec fi hrs:
  WHEN hrexec_final_sfv fi hrs ~> sfv THEN
  forall ctx sis
  (RR: ris_refines_ok ctx hrs sis),
  exists sfv',
    sexec_final_sfv fi sis = Some sfv' /\
    sfv_simu ctx sfv sfv'.
Proof.
  destruct fi; simpl. 2:destruct res. all:wlp_seq.
  1,3:intros.
  3,7:intros sv SV; intros;
        eapply hrs_sreg_get_spec in SV as [? [->]]; eauto.
  5,6:intros svos SVOS sargs SARGS; intros;
        eapply hsum_left_optmap_spec in SVOS as [? [->]]; eauto;
        eapply hlist_sreg_get_spec in SARGS as [? [->]]; eauto.
  7:  intros sargs SARGS; intros;
        eapply hbuiltin_args_spec in SARGS as [? [->]]; eauto.
  all:repeat econstructor; auto.
Qed.
Global Opaque hrexec_final_sfv.


(** ** Symbolic execution of the whole block *)

Fixpoint hrexec_rec (m : se_mode) ib hrs (k: ristate -> ?? rstate): ?? rstate := 
  match ib with
  | BF fin _ =>
      DO sfv <~ hrexec_final_sfv fin hrs;;
      RET (Sfinal hrs sfv)
  (* basic instructions *)
  | Bnop _ => k hrs
  | Bop op args dst _ =>
      DO next <~ @hrs_rsv_set_input HCF RRULES m dst args (Rop op) hrs;;
      k next
  | Bload trap chunk addr args aaddr dst _ =>
      DO next <~ @hrs_rsv_set_input HCF RRULES m dst args (Rload trap chunk addr aaddr) hrs;;
      k next
  | Bstore chunk addr args aaddr src _ =>
      DO next <~ hrexec_store m chunk addr args aaddr src hrs;;
      k next
 (* composed instructions *)
  | Bseq ib1 ib2 =>
      hrexec_rec m ib1 hrs (fun hrs2 => hrexec_rec m ib2 hrs2 k)
  | Bcond cond args ifso ifnot _ =>
      DO res <~ cbranch_expanse hrs cond args;;
      let (cond, vargs) := res in
      DO ifso  <~ hrexec_rec m ifso hrs k;;
      DO ifnot <~ hrexec_rec m ifnot hrs k;;
      RET (Scond cond vargs ifso ifnot)
  end
  .

Definition hrexec m ib hrinit :=
  hrexec_rec m ib hrinit (fun _ => RET Sabort).


Lemma hrexec_rec_okpreserv m ctx ib
  (CTXM: ctx_se_mode m ctx):
  forall k
  (CONT: forall hrs lhrs rfv,
    WHEN k hrs ~> rst THEN
    get_soutcome ctx rst = Some (sout lhrs rfv) ->
    ris_ok ctx lhrs ->
    ris_ok ctx hrs)
  hrs lhrs rfv,
  WHEN hrexec_rec m ib hrs k ~> rst THEN forall
  (ROUT: get_soutcome ctx rst = Some (sout lhrs rfv))
  (ROK: ris_ok ctx lhrs),
  ris_ok ctx hrs.
Proof.
  induction ib; simpl; try (wlp_simplify; try_simplify_someHyps; fail).
  - (* Bop *)
    wlp_simplify. exploit (@hrs_rsv_set_input_okpreserv HCF); eauto.
  - (* Bload *)
    wlp_simplify. exploit (@hrs_rsv_set_input_okpreserv HCF); eauto.
  - (* Bstore *)
    wlp_simplify; exploit hrs_ok_store_okpreserv; eauto.
  - (* Bcond *)
    wlp_simplify; revert ROUT; simplify_option.
Qed.

Lemma hrexec_rec_spec m ctx ib hrs sis
  (CTXM: ctx_se_mode m ctx):
  forall rk k
  (STRICT_R: forall hrs out,
    WHEN rk hrs ~> rst THEN
    get_soutcome ctx rst = Some out ->
    ris_ok ctx out.(_sis) ->
    ris_ok ctx hrs)
  (STRICT_S: forall sis out,
    get_soutcome ctx (k sis) = Some out ->
    sis_ok ctx out.(_sis) ->
    sis_ok ctx sis)
  (CONT: forall hrs sis,
    ris_refines ctx hrs sis ->
    WHEN rk hrs ~> rst THEN
    rst_refines ctx rst (k sis))
  (RR: ris_refines ctx hrs sis),
  WHEN hrexec_rec m ib hrs rk ~> rst THEN
  rst_refines ctx rst (sexec_rec ib sis k).
Proof.
  pattern ib, hrs, sis; match goal with |- ?G ib hrs sis => set (Goal := G) end; revert ib hrs sis.
  assert (INTRO: forall ib hrs sis, (ris_refines_ok ctx hrs sis -> Goal ib hrs sis) -> Goal ib hrs sis). {
    (* We handle here the case when [ris_ok ctx hrs] does not hold (<=> [sis_ok ctx sis] does not hold).
       We use the strictness of [hrexec_rec] and [sexec_rec]. *)
    intros ib hrs sis C_OK rk k ? ? ? RR;
    case (ris_ok_dec ctx hrs) as [ROK|RKO].
    - apply (ok_ref ROK) in RR; apply C_OK; auto.
    - intros rst EXEC; apply ROUT_KO_intro.
      + intros [?] OUT OK; apply RKO.
        eapply hrexec_rec_okpreserv in EXEC; eauto.
        intros; apply STRICT_R.
      + intros [?] OUT OK; apply RKO; apply RR.
        apply sexec_rec_okpreserv in OUT; eauto.
  }

  induction ib;
      intros hrs sis; apply INTRO; clear INTRO;
      intros RR rk k ? ? ? _;
      simpl; try wlp_seq; auto.
  - (* BF *)
    intros sfv SFV.
    eapply hrexec_final_sfv_spec in SFV as (sfv' & -> & SIMU); eauto.
    apply wrst_ref; constructor; simpl; auto.
  - (* Bop *)
    intros next SET rst K; unfold sexec_op.
    eapply hrs_rsv_set_input_spec in SET as (sis1 & -> & RR1); eauto.
    eapply CONT; eauto.
  - (* Bload *)
    intros next SET rst K; unfold sexec_load.
    eapply hrs_rsv_set_input_spec in SET as (sis1 & -> & RR1); eauto.
    eapply CONT; eauto.
  - (* Bstore *)
    intros next STORE rst K.
    eapply hrexec_store_spec in STORE as (sis1 & -> & RR1); eauto.
    eapply CONT; eauto.
  - (* Bseq *)
    eapply IHib1; eauto.
    + intros hrs1 [?]; apply hrexec_rec_okpreserv; auto.
      intros; apply STRICT_R.
    + intros sis1 [?]; apply sexec_rec_okpreserv.
      intros ? ? ?; apply STRICT_S.
    + intros; apply IHib2; auto.
  - (* Bcond *)
    intros (cond0, vargs) BRANCH; wlp_seq;
    intros ifso0 RECso ifnot0 RECnot.
    eapply cbranch_expanse_spec in BRANCH as (clsv & -> & CEVAL); eauto; simpl in CEVAL.
    unfold rst_refines; simpl; rewrite CEVAL.
    case eval_scondition as [[|]|] in |-*.
    + eapply IHib1; eauto.
    + eapply IHib2; eauto.
    + apply rout_refines_None.
Qed.
Global Opaque hrexec_rec.

Theorem hrexec_spec m ctx ib hrs sis
  (CTXM: ctx_se_mode m ctx)
  (RR: ris_refines ctx hrs sis):
  WHEN hrexec m ib hrs ~> rst THEN
  rst_refines ctx rst (sexec ib sis).
Proof.
  apply hrexec_rec_spec;
  auto; simpl; intros; try wlp_seq; simpl; try congruence.
  apply rout_refines_None.
Qed.
Global Opaque hrexec.

Lemma hrexec_correct1 m ctx ib sis sfv sinit hrinit
  (CTXM: ctx_se_mode m ctx):
  WHEN hrexec m ib hrinit ~> rst THEN forall
  (SOUT: get_soutcome ctx (sexec ib sinit) = Some (sout sis sfv))
  (SOK: sis_ok ctx sis)
  (RR: ris_refines ctx hrinit sinit),
  rst_refines ctx rst (sexec ib sinit).
Proof.
  intros rst H; intros; eapply hrexec_spec in H; eauto.
Qed.

Lemma hrexec_correct2 m ctx ib hrs rfv sinit hrinit
  (CTXM: ctx_se_mode m ctx):
  WHEN hrexec m ib hrinit ~> rst THEN forall
  (ROUT: get_soutcome ctx rst = Some (sout hrs rfv))
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrinit sinit),
  rst_refines ctx rst (sexec ib sinit).
Proof.
  intros rst H; intros; eapply hrexec_spec in H; eauto.
Qed.

End SymbolicExecution.
