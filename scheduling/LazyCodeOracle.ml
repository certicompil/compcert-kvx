open RTLcommonaux
open BTLcommonaux
open Maps
open Camlcoq
open Op
open AST
open PrintBTL
open Printf
open! BTL
open BTLtypes
open BTL_Liveness
open Registers
open LazyCodeCore
open LazyCodeBackend
open BTL_Invariants
open BTL_SyntheticNodes

(** Detection, construction and insertion of candidates *)

(** If the [ckey] already exist, we check if the candidate was already
    seen in block [pc], ifso, we add it to the offset set,
    ifnot, we create a singleton. *)
let insert_cand ckey offset pc memdep args =
  let ipc = p2i pc in
  match Hashtbl.find_opt candidates ckey with
  | None -> Hashtbl.add candidates ckey (mk_cand ipc offset memdep args)
  | Some old_cand ->
      old_cand.lhs <-
        (match IM.find_opt ipc old_cand.lhs with
        | Some loff -> IM.add ipc (offset $+ loff) old_cand.lhs
        | None -> IM.add ipc ~$offset old_cand.lhs)

(** Compare both [cand] and [old_cand] according to their position,
    update the [lhs] field of the earlier,
    and and erase the last one by emptying [lhs]. *)
let erase_latest_cand ckey cand old_cand lhs =
  let new_max = IM.max_binding cand.lhs |> fst
  and old_max = IM.max_binding old_cand.lhs |> fst in
  if new_max >= old_max then (
    Hashtbl.replace candidates ckey cand;
    old_cand.lhs <- IM.empty;
    cand.lhs <- lhs)
  else (
    Hashtbl.replace candidates ckey old_cand;
    cand.lhs <- IM.empty;
    old_cand.lhs <- lhs)

(** Merge an eventually existing candidate at [ckey] with [cand],
    and erase the latest one. *)
let merge_cand ckey cand =
  llog 3 "MERGE\n";
  print_cand stderr (ckey, cand);
  let merge_lhs pc os1 os2 =
    match (os1, os2) with
    | Some soff1, Some soff2 -> Some (soff1 $|$ soff2)
    | Some soff, None | None, Some soff -> Some soff
    | None, None -> None
  in
  match Hashtbl.find_opt candidates ckey with
  | None -> Hashtbl.add candidates ckey cand
  | Some old_cand ->
      let lhs = IM.merge merge_lhs old_cand.lhs cand.lhs in
      erase_latest_cand ckey cand old_cand lhs

(** Build a candidate from a btl operation *)
let make_op_ckey offset pc trap op args dst =
  match is_sr_candidate_op op args (Some (pc, dst)) with
  | Some csr ->
      insert_cand csr offset pc (op_depends_on_memory op) args;
      stat_incr stat_nb_cand_sr
  | None ->
      (match trap with
      | TRAP -> stat_incr stat_nb_cand_trap
      | NOTRAP -> stat_incr stat_nb_cand_notrap);
      insert_cand
        (CCM (CMop (trap, op, args)))
        offset pc (op_depends_on_memory op) args

(* Build a candidate from a btl load *)
let make_load_ckey offset pc trap chk addr args aaddr =
  stat_incr stat_nb_cand_trap;
  insert_cand (CCM (CMload (trap, chk, addr, args, aaddr))) offset pc true args

(** Detect constants over the whole function (never erased)
    A constant that is defined multiple times is still a constant,
    and in this case we keep the oldest (former) one. *)
let detect_constants elts =
  let pt = ref PTree.empty in
  let detect_constants_aux o_op_pc dst =
    match (o_op_pc, PTree.get dst !pt) with
    | Some (op, pc), Some (op', pc') ->
        if op <> op' then Hashtbl.remove constants dst
        else if pc < pc' then Hashtbl.replace constants dst (op', pc')
    | None, Some (op', pc') -> Hashtbl.remove constants dst
    | Some (op, pc), None ->
        if is_constant_op op then (
          Hashtbl.add constants dst (op, pc);
          pt := PTree.set dst (op, pc) !pt)
    | None, None -> ()
  in
  let rec detect_constants_rec pc = function
    | Bop (op, _, dst, _) -> detect_constants_aux (Some (op, pc)) dst
    | Bload (_, _, _, _, _, dst, _) | BF (Bcall (_, _, _, dst, _), _) ->
        detect_constants_aux None dst
    | BF (Bbuiltin (_, _, bdst, _), _) ->
        List.iter (detect_constants_aux None) (AST.params_of_builtin_res bdst)
    | Bseq (ib1, ib2) | Bcond (_, _, ib1, ib2, _) ->
        detect_constants_rec pc ib1;
        detect_constants_rec pc ib2
    | _ -> ()
  in
  List.sort (fun a b -> P.compare (fst b) (fst a)) elts
  |> List.iter (fun (n, ibf) -> tf_ibf_ib detect_constants_rec n ibf)

(** Detect candidates (transfer function to be used with apply_over_function) *)
let detect_candidates pc ibf =
  let offset = ref 0 in
  let rec detect_candidates_rec offset pc ib =
    offset := !offset + 1;
    match ib with
    | Bop (Omove, [ a ], dst, _) -> (
        match Hashtbl.find_opt affine_int64 (pc, a) with
        | Some aff -> Hashtbl.replace affine_int64 (pc, dst) aff
        | None -> ())
    | Bop (op, args, dst, _) ->
        let is_trap = is_trapping_op op in
        if candidate_op_filter op && ((is_trap && !ok_trap) || not is_trap) then
          let trap = if is_trap then TRAP else NOTRAP in
          make_op_ckey !offset pc trap op args dst
    | Bload (trap, chk, addr, args, aaddr, dst, _) ->
        if !ok_trap then make_load_ckey !offset pc trap chk addr args aaddr
    | Bseq (ib1, ib2) | Bcond (_, _, ib1, ib2, _) ->
        detect_candidates_rec offset pc ib1;
        detect_candidates_rec offset pc ib2
    | _ -> ()
  in
  tf_ibf_ib (detect_candidates_rec offset) pc ibf

(** Match and extraction functions for candidates *)

let match_op_ckey op args = function
  | CCM (CMop (_, op', args')) | CSR (_, op', args') -> op = op' && args = args'
  | _ -> false

let match_load_ckey trap chk addr args = function
  | CCM (CMload (trap', chk', addr', args', _)) ->
      (* TODO? compare aaddr *)
      trap = trap' && chk = chk' && addr = addr' && args = args'
  | _ -> false

let extract_ckey_args = function
  | CCM (CMop (_, _, args)) | CCM (CMload (_, _, _, args, _)) | CSR (_, _, args) ->
      args

(** Local analysis part *)

(** Initialize local and non-recursive predicates *)
let initialize_nonrec_predicates card ckey cand =
  let transp = Bitv.create card (default_predicate_value P_transp)
  and ncomp = Bitv.create card (default_predicate_value P_ncomp)
  and xcomp = Bitv.create card (default_predicate_value P_xcomp)
  and nearliest = Bitv.create card (default_predicate_value P_nearliest)
  and x_earliest = Bitv.create card (default_predicate_value P_xearliest)
  and nlatest = Bitv.create card (default_predicate_value P_nlatest)
  and xlatest = Bitv.create card (default_predicate_value P_xlatest) in
  set_predicate_cand cand transp P_transp;
  set_predicate_cand cand ncomp P_ncomp;
  set_predicate_cand cand xcomp P_xcomp;
  set_predicate_cand cand nearliest P_nearliest;
  set_predicate_cand cand x_earliest P_xearliest;
  set_predicate_cand cand nlatest P_nlatest;
  set_predicate_cand cand xlatest P_xlatest;
  if is_sr_ckey ckey then
    let injured = Bitv.create card (default_predicate_value P_injured) in
    set_predicate_cand cand injured P_injured

type bb_tf_event_t = E_matchN | E_matchX | E_smash | E_injure | E_end

(** This function performs an analysis over a single basic-block [ib].
    It has currently two main purposes:
  - Computing the transp, injure, ncomp, and xcomp predicates (which are local)
  - Computing insertion points
    A transfer function [tf] must be given as an argument, with the
    following expected signature:
    tf : (offset: int) -> (border: int) -> (first_smash: int) -> bb_tf_event_t -> unit
    where:
  - [offset]: current position in the block;
    it is equal to the number of BTL instruction at the end
  - [border]: indicates the point separating the entry part from the exit part
  - [first_smash]: contains the offset of the first instruction smashing the
    given candidate dependencies
  - [bb_tf_event_t]: sum type given to [tf] to indicate one of five cases:
    {ul
    {- E_matchN: we matched the candidate in the entry part}
    {- E_matchX: we matched the candidate in the exit part}
    {- E_smash: one of the candidate dependencies was smashed}
    {- E_injure: one of the candidate dependencies was injured}
    {- E_end: we reached the end of the basic-block}} *)
let bb_analysis ckey cand tf ib =
  let offset = ref 0 and border = ref 0 and first_smash = ref 0 in
  let inc_offset () = offset := !offset + 1
  and cargs = extract_ckey_args ckey in
  let is_smashing dst w_mem =
    (cand.memdep && w_mem) || List.exists (P.( = ) dst) cargs
  and update_smashing e =
    border := !offset;
    if !first_smash = 0 then first_smash := !offset;
    tf !offset !border !first_smash e
  in
  let apply_tf () =
    if !offset <= !first_smash || !border = 0 then
      tf !offset !border !first_smash E_matchN
    else if !border <> 0 && !offset > !border then
      tf !offset !border !first_smash E_matchX
  in
  let rec find_exit_part_border ib =
    inc_offset ();
    match ib with
    | Bop (op, args, dst, _) ->
        if is_smashing dst false then
          if
            is_sr_ckey ckey
            && match_injuring_op op args dst cargs |> Option.is_some
          then update_smashing E_injure
          else update_smashing E_smash;
        if match_op_ckey op args ckey then apply_tf ()
    | Bload (trap, chk, addr, args, _, dst, _) ->
        if is_smashing dst false then update_smashing E_smash;
        if match_load_ckey trap chk addr args ckey then apply_tf ()
    | Bstore (_, _, _, _, _, _) -> if cand.memdep then update_smashing E_smash
    | BF (Bcall (_, _, _, dst, _), _) ->
        if is_smashing dst true then update_smashing E_smash
    | BF (Bbuiltin (_, _, bdst, _), _) ->
        if
          cand.memdep
          || List.exists
               (fun dst -> is_smashing dst false)
               (AST.params_of_builtin_res bdst)
        then update_smashing E_smash
    | Bseq (ib1, ib2) ->
        find_exit_part_border ib1;
        find_exit_part_border ib2
    | Bcond (_, _, _, _, _) -> offset := !offset - 2
    | _ -> ()
  in
  find_exit_part_border ib;
  tf !offset !border !first_smash E_end

(** Computes transp, injure, ncomp and xcomp predicates *)
let local_predicates_for_cand blk2id ckey cand pc ib =
  let blkid = pget pc blk2id
  and transp_bv = get_predicate_cand cand P_transp
  and xcomp_bv = get_predicate_cand cand P_xcomp
  and ncomp_bv = get_predicate_cand cand P_ncomp
  and injured_bv = cand.state.injured in
  let tf _ _ _ = function
    | E_matchN -> Bitv.set ncomp_bv blkid true
    | E_matchX -> Bitv.set xcomp_bv blkid true
    | E_smash ->
        Bitv.set transp_bv blkid false;
        Bitv.set xcomp_bv blkid false
    | E_injure ->
        Bitv.set xcomp_bv blkid false;
        Bitv.set (get_some injured_bv) blkid true
    | E_end -> ()
  in
  bb_analysis ckey cand tf ib

(** Init non-recursive predicates and computes local ones for a whole function *)
let init_and_local_analysis btl blk_tools ckey cand =
  check_time ();
  initialize_nonrec_predicates blk_tools.card ckey cand;
  apply_over_function btl
    (tf_ibf_ib (local_predicates_for_cand blk_tools.blk2id ckey cand))

(** Predicates transfer functions (to be used with round-robin algorithm) *)

(** D_Safe transfer function
    sols.(0) should be xdsafe and sols.(1) should be ndsafe *)
let d_safe_tf blk2id succs_list cand pc bitvi sols =
  llog 3 "D_Safe analyzing %d\n" (p2i pc);
  let xdsafe =
    let xcomp = get_predicate_value cand bitvi P_xcomp in
    if xcomp then xcomp (*else if List.length succs_list = 0 then false*)
    else
      List.for_all
        (fun succ ->
          let ndsafe = Bitv.get sols.(1) (pget succ blk2id) in
          llog 3 "ndsafe for succ %d is %b\n" (p2i succ) ndsafe;
          ndsafe)
        succs_list
  in
  llog 3 "xdsafe is %b\n" xdsafe;
  let ndsafe =
    let ncomp = get_predicate_value cand bitvi P_ncomp in
    if ncomp then ncomp
    else
      let transp = get_predicate_value cand bitvi P_transp in
      llog 3 "transp is %b and xdsafe is %b\n" transp xdsafe;
      transp && xdsafe
  in
  llog 3 "ndsafe is %b\n" ndsafe;
  [| xdsafe; ndsafe |]

(** U_Safe transfer function
    sols.(0) should be nusafe and sols.(1) should be xusafe *)
let u_safe_tf blk2id preds_list cand pc bitvi sols =
  llog 3 "U_Safe analyzing %d\n" (p2i pc);
  let nusafe =
    if List.length preds_list = 0 then false
    else
      List.for_all
        (fun pred ->
          let predi = pget pred blk2id in
          let xcomp = get_predicate_value cand predi P_xcomp in
          let xusafe = Bitv.get sols.(1) predi in
          xcomp || xusafe)
        preds_list
  in
  llog 3 "nusafe is %b\n" nusafe;
  let xusafe =
    let transp = get_predicate_value cand bitvi P_transp in
    let ncomp = get_predicate_value cand bitvi P_ncomp in
    transp && (ncomp || nusafe)
  in
  llog 3 "xusafe is %b\n" xusafe;
  [| nusafe; xusafe |]

(** Delayed transfer function
    sols.(0) should be ndelayed and sols.(1) should be xdelayed *)
let delayed_tf blk2id preds_list cand pc bitvi sols =
  llog 3 "Delayed analyzing %d\n" (p2i pc);
  let ndelayed =
    let nearliest = get_predicate_value cand bitvi P_nearliest in
    if nearliest then nearliest
    else if List.length preds_list = 0 then false
    else
      List.for_all
        (fun pred ->
          let predi = pget pred blk2id in
          let xcomp = get_predicate_value cand predi P_xcomp in
          let xdelayed = Bitv.get sols.(1) predi in
          llog 3 "xcomp is %b and xdelayed is %b\n" xcomp xdelayed;
          (not xcomp) && xdelayed)
        preds_list
  in
  llog 3 "ndelayed is %b\n" ndelayed;
  let xdelayed =
    let x_earliest = get_predicate_value cand bitvi P_xearliest in
    if x_earliest then x_earliest
    else if not ndelayed then ndelayed
    else
      let ncomp = get_predicate_value cand bitvi P_ncomp in
      not ncomp
  in
  llog 3 "xdelayed is %b\n" xdelayed;
  [| ndelayed; xdelayed |]

(** Isolated transfer function
    sols.(0) should be xisolated and sols.(1) should be nisolated *)
let isolated_tf blk2id succs_list cand pc bitvi sols =
  llog 3 "Isolated analyzing %d\n" (p2i pc);
  let xisolated =
    List.for_all
      (fun succ ->
        let succi = pget succ blk2id in
        let nearliest = get_predicate_value cand succi P_nearliest in
        if nearliest then nearliest
        else
          let ncomp = get_predicate_value cand succi P_ncomp in
          if ncomp then not ncomp else Bitv.get sols.(1) succi)
      succs_list
  in
  llog 3 "xisolated is %b\n" xisolated;
  let nisolated =
    if xisolated then xisolated else get_predicate_value cand bitvi P_xearliest
  in
  llog 3 "nisolated is %b\n" nisolated;
  [| xisolated; nisolated |]

(** Critical transfer function
    sols.(0) should be xcritical and sols.(1) should be ncritical *)
let critical_tf blk2id succs_list cand pc bitvi sols =
  llog 3 "Critical analyzing %d\n" (p2i pc);
  let xcritical =
    (not (get_predicate_value cand bitvi P_xcomp))
    && List.exists
         (fun succ ->
           let succi = pget succ blk2id in
           Bitv.get sols.(1) succi)
         succs_list
  in
  llog 3 "xcritical is %b\n" xcritical;
  let ncritical =
    let transp =
      get_predicate_value cand bitvi P_transp
      && not (get_predicate_value cand bitvi P_injured)
    in
    (not (get_predicate_value cand bitvi P_ncomp)) && ((not transp) || xcritical)
  in
  llog 3 "ncritical is %b\n" ncritical;
  [| xcritical; ncritical |]

(** Subst-Crit transfer function
    sols.(0) should be nsubstcrit and sols.(1) should be xsubstcrit *)
let substcrit_tf blk2id preds_list cand pc bitvi sols =
  llog 3 "Subst-crit analyzing %d\n" (p2i pc);
  let nsubstcrit =
    let ncritinsert = get_predicate_value cand bitvi P_ncritinsert in
    if ncritinsert then ncritinsert
    else
      List.exists
        (fun pred ->
          let predi = pget pred blk2id in
          let xcomp = get_predicate_value cand predi P_xcomp in
          let xsubstcrit = Bitv.get sols.(1) predi in
          llog 3 "xcomp is %b and xsubstcrit is %b\n" xcomp xsubstcrit;
          (not xcomp) && xsubstcrit)
        preds_list
  in
  llog 3 "nsubstcrit is %b\n" nsubstcrit;
  let xsubstcrit =
    let xcritinsert = get_predicate_value cand bitvi P_xcritinsert in
    if xcritinsert then xcritinsert
    else if not nsubstcrit then nsubstcrit
    else
      let ncomp = get_predicate_value cand bitvi P_ncomp in
      not ncomp
  in
  llog 3 "xsubstcrit is %b\n" xsubstcrit;
  [| nsubstcrit; xsubstcrit |]

(** Update transfer function with two equations
sols.(0) should be xupdate and sols.(1) should be nupdate
let update_tf blk2id succs_list cand pc bitvi sols =
  llog 3 "Update analyzing %d\n" (p2i pc);
  let xupdate =
    let xcomp = get_predicate_value cand bitvi P_xcomp in
    if xcomp then xcomp
    else
      List.exists
        (fun succ ->
          let succi = pget succ blk2id in
          let ninsert = get_predicate_value cand succi P_ninsert in
          let nupdate = Bitv.get sols.(1) succi in
          llog 3 "ninsert is %b and nupdate is %b\n" ninsert nupdate;
          (not ninsert) && nupdate)
        succs_list
  in
  llog 3 "xupdate is %b\n" xupdate;
  let nupdate =
    let ncomp = get_predicate_value cand bitvi P_ncomp in
    if ncomp then ncomp
    else
      let xinsert = get_predicate_value cand bitvi P_xinsert in
      if xinsert then not xinsert else xupdate
  in
  llog 3 "nupdate is %b\n" nupdate;
  [| xupdate; nupdate |]
    *)

(** Update transfer function
    sols contains the single equation *)
let update_tf blk2id succs_list cand pc bitvi sols =
  let ncomp = get_predicate_value cand bitvi P_ncomp
  and xcomp = get_predicate_value cand bitvi P_xcomp in
  let update =
    if ncomp || xcomp then true
    else
      List.exists
        (fun succ ->
          let succi = pget succ blk2id in
          let ninsert = get_predicate_value cand succi P_ninsert in
          let xinsert = get_predicate_value cand succi P_xinsert in
          let update = Bitv.get sols.(0) succi in
          (not ninsert) && (not xinsert) && update)
        succs_list
  in
  [| update |]

(** Dataflow analysis part *)

(** Main dataflow function: a unidirectionnal round-robin worklist (in [wl])
    based dataflow algorithm taking a transfer function [tf] as parameter,
    and working on a single candidate [cand] at a time. The [predicates] parameter
    is an array containing the system of equations to evaluate. *)
let round_robin btl blk_tools tf predicates cand =
  (* The worklist is made from CFG nodes *)
  let wl = ref blk_tools.wl
  and visited = Bitv.create blk_tools.card false
  (* The output will be a bitvector *)
  and is_fwd = is_fwd_predicate predicates.(0) in
  let sols =
    Array.map
      (fun predicate ->
        Bitv.create blk_tools.card (default_predicate_value predicate))
      predicates
  in
  (* Pick function depends on forward/backward analysis: on a renumbered code,
     for forward analysis, we select the max elt first,
     and for backward, the min *)
  let pick = if is_fwd then OrdIS.max_elt_opt else OrdIS.min_elt_opt in
  (* Try to converge on a fixpoint *)
  let rec fixpoint () =
    match pick !wl with
    | None -> ()
    | Some ipc ->
        llog 3 "fixpoint()\n";
        check_time ();
        let pc = i2p ipc in
        let bitvi = pget pc blk_tools.blk2id in
        wl := OrdIS.remove ipc !wl;
        let ibf = pget pc btl in
        let succs_list = ibf.binfo.successors
        and preds_list = ibf.binfo.predecessors in
        let alist = if is_fwd then preds_list else succs_list in
        let old_outs = Array.map (fun sol -> Bitv.get sol bitvi) sols in
        (* Applying the transfer function *)
        let outs = tf blk_tools.blk2id alist cand pc bitvi sols in
        (* This condition tests if something changed *)
        if
          Array.exists2 (fun a b -> a <> b) outs old_outs
          || not (Bitv.get visited bitvi)
        then (
          llog 3 "update!\n";
          Bitv.set visited bitvi true;
          let nlist = if is_fwd then succs_list else preds_list in
          Array.iteri
            (fun i out ->
              llog 3 "set i=%d out=%b\n" i out;
              Bitv.set sols.(i) bitvi out)
            outs;
          wl := !wl $|$ plist2IS nlist);
        fixpoint ()
  in
  fixpoint ();
  sols

(** Computation of predicates from dataflow equations *)

let compute_earliestness btl blk_tools cand =
  OrdIS.iter
    (fun ipc ->
      let pc = i2p ipc in
      let bitvi = pget pc blk_tools.blk2id in
      let nearliest =
        let ndsafe = get_predicate_value cand bitvi P_ndsafe in
        if not ndsafe then ndsafe
        else
          let preds = (pget pc btl).binfo.predecessors in
          List.for_all
            (fun pred ->
              let predi = pget pred blk_tools.blk2id in
              let xdsafe = get_predicate_value cand predi P_xdsafe in
              let xusafe = get_predicate_value cand predi P_xusafe in
              not (xdsafe || xusafe))
            preds
      in
      if nearliest then set_predicate_value cand bitvi P_nearliest nearliest;
      let x_earliest =
        let xdsafe = get_predicate_value cand bitvi P_xdsafe in
        let transp = get_predicate_value cand bitvi P_transp in
        xdsafe && not transp
      in
      if x_earliest then set_predicate_value cand bitvi P_xearliest x_earliest)
    blk_tools.wl

let compute_latestness btl blk_tools cand =
  OrdIS.iter
    (fun ipc ->
      let pc = i2p ipc in
      let bitvi = pget pc blk_tools.blk2id in
      let nlatest =
        let ndelayed = get_predicate_value cand bitvi P_ndelayed in
        if not ndelayed then ndelayed
        else
          let ncomp = get_predicate_value cand bitvi P_ncomp in
          ndelayed && ncomp
      in
      if nlatest then set_predicate_value cand bitvi P_nlatest nlatest;
      let xlatest =
        let xdelayed = get_predicate_value cand bitvi P_xdelayed in
        if not xdelayed then xdelayed
        else
          let xcomp = get_predicate_value cand bitvi P_xcomp in
          if xcomp then xcomp
          else
            let succs = (pget pc btl).binfo.successors in
            List.exists
              (fun succ ->
                let ndelayed =
                  get_predicate_value cand
                    (pget succ blk_tools.blk2id)
                    P_ndelayed
                in
                not ndelayed)
              succs
      in
      if xlatest then set_predicate_value cand bitvi P_xlatest xlatest)
    blk_tools.wl

(** Insertion points predicates and related functions *)

(* Compute critical insertion points:
 - P_ncritinsert = P_ninsert /\ P_ncritical
 - P_xcritinsert = P_xinsert /\ P_xcritical *)
let compute_critical_insertion cand =
  let ncritinsert_bv =
    Bitv.bw_and
      (get_predicate_cand cand P_ninsert)
      (get_predicate_cand cand P_ncritical)
  and xcritinsert_bv =
    Bitv.bw_and
      (get_predicate_cand cand P_xinsert)
      (get_predicate_cand cand P_xcritical)
  in
  set_predicate_cand cand ncritinsert_bv P_ncritinsert;
  set_predicate_cand cand xcritinsert_bv P_xcritinsert

(** Set insertion and replacement points in the candidate record *)
let set_lcm_targets cand ninsert_bv xinsert_bv nreplace_bv xreplace_bv =
  set_predicate_cand cand ninsert_bv P_ninsert;
  set_predicate_cand cand xinsert_bv P_xinsert;
  set_predicate_cand cand nreplace_bv P_nreplace;
  set_predicate_cand cand xreplace_bv P_xreplace

(** Push critical insertion points forward to the first non critical but
    substitution critical point, in the direction of the control-flow. *)
let find_substcrit_targets btl blk_tools cand =
  let ninsert_bv = get_predicate_cand cand P_ninsert
  and xinsert_bv = get_predicate_cand cand P_xinsert
  and ncritical_bv = get_predicate_cand cand P_ncritical
  (* TODO I am not sure about this algorithm, should we push insertion
   * targets forward a node with non-critial but substitution-critical end part? *)
  and xcritical_bv = get_predicate_cand cand P_xcritical
  and nsubstcrit_bv = get_predicate_cand cand P_nsubstcrit
  and xsubstcrit_bv = get_predicate_cand cand P_xsubstcrit
  and ncritinsert_bv = get_predicate_cand cand P_ncritinsert
  and xcritinsert_bv = get_predicate_cand cand P_xcritinsert in
  let rec find_substcrit_targets_rec pc =
    let ibf = pget pc btl in
    ibf.binfo.visited <- true;
    let succs_list = ibf.binfo.successors in
    List.iter
      (fun succ ->
        let succi = pget succ blk_tools.blk2id in
        (* XXX can do a stack overflow here?! *)
        llog 3 "iterate over succ=%d\n" (p2i succ);
        if (not (Bitv.get ncritical_bv succi)) && Bitv.get nsubstcrit_bv succi
        then (
          llog 3 "not ncrit && nsubstcrit\n";
          Bitv.set ninsert_bv succi true)
        else if
          (not (Bitv.get xcritical_bv succi)) && Bitv.get xsubstcrit_bv succi
        then (
          llog 3 "not xcrit && xsubstcrit\n";
          Bitv.set xinsert_bv succi true)
        else if not (pget succ btl).binfo.visited then (
          llog 3 "recursive call\n";
          find_substcrit_targets_rec succ))
      succs_list
  in
  let find_substcrit_targets_gen ins_bv ins_crit_bv =
    reset_visited_ibf btl;
    Bitv.iteri_true
      (fun bitvi ->
        Bitv.set ins_bv bitvi false;
        let pc = IM.find bitvi blk_tools.id2blk in
        llog 3 "will call find_substcrit_targets_rec for pc=%d\n" (p2i pc);
        find_substcrit_targets_rec pc)
      ins_crit_bv
  in
  if not (Bitv.all_zeros ncritinsert_bv) then (
    llog 3 "find_substcrit_targets_gen for N\n";
    find_substcrit_targets_gen ninsert_bv ncritinsert_bv);
  if not (Bitv.all_zeros xcritinsert_bv) then (
    llog 3 "find_substcrit_targets_gen for X\n";
    find_substcrit_targets_gen xinsert_bv xcritinsert_bv);
  set_predicate_cand cand ninsert_bv P_ninsert;
  set_predicate_cand cand xinsert_bv P_xinsert

(** Non-trapping insertion points: Insert(n) = Latest(n) /\ not Isolated(n) *)
let compute_lcm_targets_notrap cand =
  llog 2 "compute_lcm_targets_notrap\n";
  let nlatest_bv = get_predicate_cand cand P_nlatest
  and xlatest_bv = get_predicate_cand cand P_xlatest
  and nisolated_bv = get_predicate_cand cand P_nisolated
  and xisolated_bv = get_predicate_cand cand P_xisolated
  and ncomp_bv = get_predicate_cand cand P_ncomp
  and xcomp_bv = get_predicate_cand cand P_xcomp in
  let ninsert_bv = Bitv.bw_and nlatest_bv (Bitv.bw_not nisolated_bv)
  and xinsert_bv = Bitv.bw_and xlatest_bv (Bitv.bw_not xisolated_bv)
  and nreplace_bv =
    Bitv.bw_and ncomp_bv (Bitv.bw_not (Bitv.bw_and nlatest_bv nisolated_bv))
  and xreplace_bv =
    Bitv.bw_and xcomp_bv (Bitv.bw_not (Bitv.bw_and xlatest_bv xisolated_bv))
  in
  set_lcm_targets cand ninsert_bv xinsert_bv nreplace_bv xreplace_bv

(** Trapping insertion points:
  - First computes a set P = {pc_p | N-Comp(n) /\ N-Up-Safe(n) }
  - For each pc_p, build the set I = {pc_i | pc_i != pc_p /\
    (N-Comp(pc_i) \/ X-Comp(pc_i)) /\ exists (p : path),
    p = (pc_i, pc_p) /\ forall n, n in p /\ n > pc_i /\ n < pc_p -> Transp(n)
    Then, it applies the below rules:
    |I| > 0 -> N-Replace(pc_p) <- true
    X-Comp(pc_p) = true -> X-Insert(pc_p) <- true /\ X-replace(pc_p) <- true
    For each pc_i,
    {ul
    {- N-Comp(pc_i) -> N-Insert(pc_i) <- true /\ N-Replace(pc_i) <- true}
    {- X-Comp(pc_i) -> X-Insert(pc_i) <- true /\ X-Replace(pc_i) <- true}} *)
let compute_lcm_targets_trap btl entry blk_tools cand =
  llog 2 "compute_lcm_targets_trap\n";
  List.iter (fun (_, ibf) -> ibf.binfo.visited <- false) (PTree.elements btl);
  let transp_bv = get_predicate_cand cand P_transp
  and ncomp_bv = get_predicate_cand cand P_ncomp
  and xcomp_bv = get_predicate_cand cand P_xcomp
  and nusafe_bv = get_predicate_cand cand P_nusafe
  and ninsert_bv = Bitv.create blk_tools.card false
  and xinsert_bv = Bitv.create blk_tools.card false
  and nreplace_bv = Bitv.create blk_tools.card false
  and xreplace_bv = Bitv.create blk_tools.card false in
  let lhs_s =
    Bitv.foldi_left
      (fun ps i b -> if b then p2i (IM.find i blk_tools.id2blk) $+ ps else ps)
      OrdIS.empty
      (Bitv.bw_or ncomp_bv xcomp_bv)
  in
  let get_ins_rep_bvs b =
    if b then (ninsert_bv, nreplace_bv) else (xinsert_bv, xreplace_bv)
  and pot_replace = ref OrdIS.empty in
  let find_ir_points_to target =
    let ptarget = i2p target in
    OrdIS.filter
      (fun ipc ->
        llog 3 "filter function with ipc %d\n" ipc;
        let visited = ref OrdIS.empty in
        let rec reachable_from isfst = function
          | [] -> false
          | s :: l ->
              let si = p2i s in
              llog 3 "reachable_from %b %d\n" isfst si;
              let seen = si $? !visited in
              if isfst && s = ptarget then false
              else if s = ptarget then true
              else if seen then reachable_from false l
              else (
                visited := si $+ !visited;
                llog 3 "else reach\n";
                let transp = Bitv.get transp_bv (pget s blk_tools.blk2id) in
                if (not isfst) && (si $? lhs_s || not transp) then
                  reachable_from false l
                else
                  let succs = (pget s btl).binfo.successors in
                  reachable_from false (succs @ l))
        in
        let res = reachable_from true [ i2p ipc ] in
        llog 3 "res is %b\n" res;
        res)
      lhs_s
  in
  let rec visit_code_fwd pc =
    let ibf = pget pc btl in
    if ibf.binfo.visited then ()
    else (
      ibf.binfo.visited <- true;
      let bitvi = pget pc blk_tools.blk2id in
      let ncomp = Bitv.get ncomp_bv bitvi
      and nusafe = Bitv.get nusafe_bv bitvi in
      if ncomp && nusafe then
        pot_replace := p2i pc $+ !pot_replace;
      List.iter (fun s -> visit_code_fwd s) ibf.binfo.successors)
  in
  visit_code_fwd entry;
  OrdIS.iter (fun i -> llog 3 "%d " i) !pot_replace;
  llog 3 "will compute ir_points...\n";
  OrdIS.iter
    (fun pot_rep ->
      llog 3 "for pot rep %d\n" pot_rep;
      let ir_points = find_ir_points_to pot_rep in
      (if OrdIS.cardinal ir_points > 0 then
       let bitvi = pget (i2p pot_rep) blk_tools.blk2id in
       Bitv.set nreplace_bv bitvi true;
       if Bitv.get xcomp_bv bitvi then (
       Bitv.set xinsert_bv bitvi true;
       Bitv.set xreplace_bv bitvi true)
      );
      OrdIS.iter
        (fun irpt ->
          let bitvi = pget (i2p irpt) blk_tools.blk2id in
          let xcomp = Bitv.get xcomp_bv bitvi in
          let ins_bv, rep_bv = get_ins_rep_bvs (not xcomp) in
          Bitv.set ins_bv bitvi true;
          Bitv.set rep_bv bitvi true)
        ir_points)
    !pot_replace;
  set_lcm_targets cand ninsert_bv xinsert_bv nreplace_bv xreplace_bv

(** Main dataflow analysis *)
let dataflow_analysis btl entry blk_tools ckey cand =
  let sr_ckey = is_sr_ckey ckey in
  let sr_mul_ckey = is_sr_mul_ckey ckey in
  let dsafe_preds = [| P_xdsafe; P_ndsafe |] in
  let dsafe_sols = round_robin btl blk_tools d_safe_tf dsafe_preds cand in
  set_predicates_array cand dsafe_preds dsafe_sols;

  let usafe_preds = [| P_nusafe; P_xusafe |] in
  let usafe_sols = round_robin btl blk_tools u_safe_tf usafe_preds cand in
  set_predicates_array cand usafe_preds usafe_sols;

  compute_earliestness btl blk_tools cand;

  let delayed_preds = [| P_ndelayed; P_xdelayed |] in
  let delayed_sols = round_robin btl blk_tools delayed_tf delayed_preds cand in
  set_predicates_array cand delayed_preds delayed_sols;

  compute_latestness btl blk_tools cand;

  let isolated_preds = [| P_xisolated; P_nisolated |] in
  let isolated_sols =
    round_robin btl blk_tools isolated_tf isolated_preds cand
  in
  set_predicates_array cand isolated_preds isolated_sols;

  let need_sr_preds = sr_mul_ckey && is_really_sr_cand cand in
  (if need_sr_preds then
   let critical_preds = [| P_xcritical; P_ncritical |] in
   let critical_sols =
     round_robin btl blk_tools critical_tf critical_preds cand
   in
   set_predicates_array cand critical_preds critical_sols);

  if not (is_trapping_ckey ckey) then compute_lcm_targets_notrap cand
  else (
    assert (not sr_ckey);
    compute_lcm_targets_trap btl entry blk_tools cand);
  if need_sr_preds then (
    compute_critical_insertion cand;
    let substcrit_preds = [| P_nsubstcrit; P_xsubstcrit |] in
    let substcrit_sols =
      round_robin btl blk_tools substcrit_tf substcrit_preds cand
    in
    set_predicates_array cand substcrit_preds substcrit_sols;
    find_substcrit_targets btl blk_tools cand);
  if sr_ckey then
    let update_preds = [| P_update |] in
    let update_sols = round_robin btl blk_tools update_tf update_preds cand in
    set_predicates_array cand update_preds update_sols

(** Rewriting part (modifying the CFG once analysis is done) *)

(** Insert [inst] in [ib] at [offset_target], using [offset] as iterator *)
let insert_node inst offset_target ib =
  let offset = ref 0 in
  let rec insert_node_rec ib =
    offset := !offset + 1;
    llog 3 "insert_node: new offset is %d, offset_target is %d and inst is "
      !offset offset_target;
    if log_lvl >= 3 then print_btl_inst stderr ib;
    llog 3 "\n";
    if !offset = offset_target then Bseq (inst, ib)
    else if !offset < offset_target then
      match ib with
      | Bseq (ib1, ib2) ->
          let ib1' = insert_node_rec ib1 and ib2' = insert_node_rec ib2 in
          Bseq (ib1', ib2')
      | _ -> ib
    else ib
  in
  insert_node_rec ib

let commit_new_block btl blk_target ib ibf =
  let ibf' = mk_ibinfo ib ibf.binfo in
  (* Commit modification to the code *)
  btl := PTree.set blk_target ibf' !btl

let update_args_ckey ckey args =
  llog 3 "cand args were updated!\n";
  match ckey with
  | CCM (CMop (trap, op, _)) -> CCM (CMop (trap, op, args))
  | CCM (CMload (trap, chk, addr, _, aaddr)) -> CCM (CMload (trap, chk, addr, args, aaddr))
  | CSR (sr_type, op, _) -> CSR (sr_type, op, args)

let mark_updated_ckey ckey nargs =
  match Hashtbl.find_opt candidates ckey with
  | Some cand ->
      cand.updated_args <- Some nargs;
      let uckey = update_args_ckey ckey nargs in
      Hashtbl.remove candidates ckey;
      merge_cand uckey cand
  | None -> ()

let mark_updated_op op args nargs =
  let ckey =
    match is_sr_candidate_op op args None with
    | Some csr -> csr
    | None ->
        let trap = if is_trapping_op op then TRAP else NOTRAP in
        CCM (CMop (trap, op, args))
  in
  mark_updated_ckey ckey nargs

let mark_updated_load trap chk addr args nargs aaddr =
  let ckey = CCM (CMload (trap, chk, addr, args, aaddr)) in
  mark_updated_ckey ckey nargs

let subst_arg old_a new_a arg = if arg = old_a then new_a else arg
let subst_arg_list old_a new_a args = List.map (subst_arg old_a new_a) args

(** Forward substitution of auxiliary variables, and candidate
    replacement procedure. This function takes the target 
    block [pc], the target offset [offset_target], and recurses through the
    basic-block [ib]. It bases its behavior on the comparison between the
    current offset [offset] and [offset_target]:
  - if [offset] < [offset_target]: continue and increment [offset];
  - if [offset] = [offset_target]: ensure a match between the current
    instruction and the candidate to replace, replace it by a nop
    (rather than directly by a move), and continue. At this point, we also
    update the tables of affine values and constants by copying the previous
    mapping (if existing) to a new one bound to the auxiliary variable of the
    candidate. The algorithm saves the original destination of the replaced
    instruction;
  - if [offset] > [offset_target]: (meaning the candidate was already replaced
    by a nop), there are two possible subcases:
    {ul
    {- A final case when either (i) the auxiliary variable or the original
       destination of the candidate is rewritten; (ii) the current instruction
       is another occurrence of the candidate or an injuring operation;
       (iii) we are reaching the end of the block.
       If so, we insert the move from the auxiliary variable and stop
       the substitution algorithm;}
    {- A recursive case otherwise, where we substitute the previously saved
       original destination in the [updated_args] field of the current
       candidate by its auxiliary variable.}} *)
let forward_replace pc offset_target ckey vaux ib =
  llog 2 "forward_replace: offset_target is %d\n" offset_target;
  let offset = ref 0
  and orig_dst = ref None
  and cargs = extract_ckey_args ckey
  and stop = ref false in
  let tsubst_arg arg = subst_arg (get_some !orig_dst) vaux arg in
  let tsubst_arg_list args = subst_arg_list (get_some !orig_dst) vaux args in
  let build_move () = Bop (Omove, [ vaux ], get_some !orig_dst, def_iinfo ())
  and build_bnop () = Bnop (Some (def_iinfo ())) in
  let rec forward_replace_rec ib =
    offset := !offset + 1;
    llog 3 "forward_replace_rec: offset is %d\n" !offset;
    let replace_mode = compare !offset offset_target in
    match ib with
    | Bop (op, args, dst, iinfo) ->
        let matched = match_op_ckey op args ckey && dst <> vaux in
        if !stop then ib
        else if Option.is_none !orig_dst then
          if matched && replace_mode = 0 then (
            orig_dst := Some dst;
            llog 2 "Replaced (%d, %d) with (%d, %d)!\n" (p2i pc) (p2i dst)
              (p2i pc) (p2i vaux);
            if is_constant_reg dst then
              Hashtbl.find constants dst |> Hashtbl.add constants vaux;
            (if is_affine_op op then
             match Hashtbl.find_opt affine_int64 (pc, dst) with
             | Some aff ->
                 (*print_affine_form stderr (Camlcoq.camlint64_of_coqint, aff);*)
                 Hashtbl.replace affine_int64 (pc, vaux) aff
             | None -> ());
            build_bnop ())
          else ib
        else if
          dst <> vaux
          && dst <> get_some !orig_dst
          && (Option.is_none @@ match_injuring_op op args dst cargs)
          && not matched
        then (
          let nargs = tsubst_arg_list args in
          if nargs <> args then mark_updated_op op args nargs;
          Bop (op, nargs, tsubst_arg dst, iinfo))
        else (
          stop := true;
          Bseq (build_move (), ib))
    | Bload (trap, chk, addr, args, aaddr, dst, iinfo) ->
        let matched = match_load_ckey trap chk addr args ckey && dst <> vaux in
        if !stop then ib
        else if Option.is_none !orig_dst then
          if matched && replace_mode = 0 then (
            orig_dst := Some dst;
            build_bnop ())
          else ib
        else if dst <> vaux && dst <> get_some !orig_dst && not matched then (
          let nargs = tsubst_arg_list args in
          if nargs <> args then mark_updated_load trap chk addr args nargs aaddr;
          Bload (trap, chk, addr, nargs, aaddr, tsubst_arg dst, iinfo))
        else (
          stop := true;
          Bseq (build_move (), ib))
    | _ -> (
        if !stop || (Option.is_none !orig_dst && replace_mode > 0) then ib
        else
          match ib with
          | Bstore (chk, addr, args, aaddr, src, iinfo) ->
              if Option.is_some !orig_dst then
                Bstore (chk, addr, tsubst_arg_list args, aaddr, tsubst_arg src, iinfo)
              else ib
          | Bseq (Bcond (_, _, _, _, _), ib2) -> Bseq (build_move (), ib)
          | Bseq (ib1, ib2) ->
              let ib1' = forward_replace_rec ib1
              and ib2' = forward_replace_rec ib2 in
              Bseq (ib1', ib2')
          | BF (_, _) -> Bseq (build_move (), ib)
          | _ -> ib)
  in
  forward_replace_rec ib

(** Compute the insertion point for a given basic-block and candidate *)
let compute_insertion_point ckey cand ib is_entry =
  let insertion_point_N = ref 0 and insertion_point_X = ref 0 in
  let tf offset border first_smash = function
    | E_matchN -> if !insertion_point_N = 0 then insertion_point_N := offset
    | E_matchX -> insertion_point_X := offset
    | E_end ->
        if !insertion_point_N = 0 then
          if first_smash > 0 then insertion_point_N := first_smash
          else insertion_point_N := offset;
        if !insertion_point_X = 0 then
          if border <> 0 then insertion_point_X := border + 1
          else insertion_point_X := offset
    | _ -> ()
  in
  bb_analysis ckey cand tf ib;
  if is_entry then !insertion_point_N else !insertion_point_X

(** Translate a virtual candidate to a concrete BTL instruction *)
let build_inst_from_ckey vaux ckey =
  match ckey with
  | CCM (CMop (_, op, args)) | CSR (_, op, args) ->
      Bop (op, args, vaux, def_iinfo ())
  | CCM (CMload (trap, chk, addr, args, aaddr)) ->
      Bload (trap, chk, addr, args, aaddr, vaux, def_iinfo ())

let strength_reduce_update blk_tools replace_pts pc ckey cand ib =
  let vaux = get_some cand.vaux in
  let was_reduced = ref false in
  let cargs = extract_ckey_args ckey in
  let len =
    if not @@ is_sr_mul_ckey ckey then
      List.length @@ get_some @@ cand.updated_args
    else 1
  in
  llog 2 "len is %d\n" len;
  let rec strength_reduce_update_rec ib =
    match ib with
    | Bop (op, args, dst, iinfo) ->
        llog 2 "dst is %d, vaux is %d\n" (p2i dst) (p2i vaux);
        if dst <> vaux && not !was_reduced then
          match match_injuring_op op args dst cargs with
          | Some (iarg, _) ->
              print_affine_map affine_int64 Camlcoq.camlint64_of_coqint;
              (* TODO improve stats *)
              stat_incr stat_nb_update_sr;
              if is_sr_mul_ckey ckey then (
                let vinj = aff_inj_const_int64 (pc, iarg) in
                let const = extract_ckey_const ckey in
                llog 2
                  "strength_reduce_update_rec: mul; update iarg %d vinj %Ld \
                   const %Ld\n"
                  (p2i iarg)
                  (Camlcoq.camlint64_of_coqint vinj)
                  (Camlcoq.camlint64_of_coqint const);
                was_reduced := true;
                blk_tools.sr_vauxs <- PTree.set vaux iarg blk_tools.sr_vauxs;
                aff_add1_int64_uset pc vaux vaux vinj true;
                (*List.iter*)
                (*(fun pt ->*)
                (*llog 2*)
                (*"add_or_nothing (pt, iarg)=(%d, %d) to replace with %Ld\n"*)
                (*(p2i pt) (p2i iarg)*)
                (*(Camlcoq.camlint64_of_coqint vinj);*)
                (*if*)
                (*Hashtbl.find_opt affine_int64 (pt, iarg) |> Option.is_none*)
                (*then*)
                (*let vinj =*)
                (*vinj |> Camlcoq.camlint64_of_coqint |> Int64.abs*)
                (*|> Camlcoq.coqint_of_camlint64*)
                (*in*)
                (*aff_mul_int64_uset pt iarg iarg vinj)*)
                (*replace_pts;*)
                Bseq
                  ( Bop
                      ( mk_sr_update_op (Integers.Int64.mul const vinj),
                        [ vaux ],
                        vaux,
                        def_iinfo () ),
                    ib ))
              else if not (is_sr_update_op op) then ib
              else (
                llog 2 "add_may_sr iarg %d\n" (p2i iarg);
                let vinj =
                  match PTree.get dst blk_tools.sr_vauxs with
                  | Some iarg ->
                      let old_vinj = aff_inj_const_int64 (pc, iarg) in
                      llog 2 "add_sr iarg %d old_vinj %Ld\n" (p2i iarg)
                        (Camlcoq.camlint64_of_coqint old_vinj);
                      (*let is_neg = Integers.Int64.lt old_vinj Integers.Int64.zero in*)
                      (*let sign =*)
                      (*if is_neg then Camlcoq.coqint_of_camlint64 (-1L)*)
                      (*else Camlcoq.coqint_of_camlint64 1L*)
                      (*in*)
                      blk_tools.sr_vauxs <-
                        PTree.set vaux iarg blk_tools.sr_vauxs;
                      Integers.Int64.mul old_vinj
                        (aff_acc_inj_int64 (List.hd replace_pts, vaux) iarg)
                  | None -> Integers.Int64.one
                in
                llog 2 "hd replace_pts %d and vinj is %Ld\n"
                  (p2i (List.hd replace_pts))
                  (Camlcoq.camlint64_of_coqint vinj);
                was_reduced := true;
                let op =
                  llog 2 "strength_reduce_update_rec: add; vinj %Ld\n"
                    (Camlcoq.camlint64_of_coqint vinj);
                  mk_sr_update_op vinj
                in
                Bseq (ib, Bop (op, [ vaux ], vaux, def_iinfo ())))
          | None -> ib
        else ib
    | Bseq (ib1, ib2) ->
        let ib1' = strength_reduce_update_rec ib1
        and ib2' = strength_reduce_update_rec ib2 in
        Bseq (ib1', ib2')
    | _ -> ib
  in
  let ib = strength_reduce_update_rec ib in
  cand.was_reduced <- !was_reduced;
  (*Printf.eprintf "LG_WR\n";*)
  ib

(** Applies code motion using the information in [insert_pred] and
    [replace_pred] *)
let code_motion btl id2blk ckey cand insert_pred replace_pred is_entry =
  Bitv.iteri
    (fun i insert ->
      let replace = Bitv.get replace_pred i in
      if insert || replace then (
        let blk_target = IM.find i id2blk in
        let ibf = pget blk_target !btl in
        let ib = ref ibf.entry in
        let insertion_point =
          ref (compute_insertion_point ckey cand !ib is_entry)
        in
        if insert then (
          stat_code_motion is_entry ckey true;
          llog 2
            "code_motion_gen: insert with i is %d, blk_target is %d, \
             insertion_point is %d\n"
            i (p2i blk_target) !insertion_point;
          let inst = build_inst_from_ckey (get_some cand.vaux) ckey in
          ib := insert_node inst !insertion_point !ib;
          insertion_point := !insertion_point + 2);
        if replace then (
          stat_code_motion is_entry ckey false;
          llog 2
            "code_motion_gen: replace with i is %d, blk_target is %d, \
             insertion_point is %d\n"
            i (p2i blk_target) !insertion_point;
          ib :=
            forward_replace blk_target !insertion_point ckey
              (get_some cand.vaux) !ib);
        commit_new_block btl blk_target !ib ibf))
    insert_pred

let strength_reduction btl blk_tools initial_pos ckey cand =
  let update_bv = get_predicate_cand cand P_update in
  let injured_bv = get_predicate_cand cand P_injured in
  Bitv.iteri_true
    (fun i ->
      let blk_target = IM.find i blk_tools.id2blk in
      let ibf = pget blk_target !btl in
      llog 2 "sr insupd for id=%d which is blk=%d\n" i (p2i blk_target);
      let ib =
        strength_reduce_update blk_tools initial_pos blk_target ckey cand
          ibf.entry
      in
      commit_new_block btl blk_target ib ibf)
    (Bitv.bw_and update_bv injured_bv)

let rewrite_graph btl_ref blk_tools ckey cand =
  let ninsert = get_predicate_cand cand P_ninsert
  and xinsert = get_predicate_cand cand P_xinsert
  and nreplace = get_predicate_cand cand P_nreplace
  and xreplace = get_predicate_cand cand P_xreplace in
  let ok_insert = not (Bitv.all_zeros ninsert && Bitv.all_zeros xinsert) in
  if ok_insert then (
    let _vaux = r2pi () in
    cand.vaux <- Some _vaux;
    llog 2 "code_motion() for ninsert\n";
    code_motion btl_ref blk_tools.id2blk ckey cand ninsert nreplace true;
    llog 2 "code_motion() for xinsert\n";
    code_motion btl_ref blk_tools.id2blk ckey cand xinsert xreplace false;
    if is_sr_ckey ckey then
      let replace_pts =
        Bitv.foldi_right
          (fun i b l -> if b then IM.find i blk_tools.id2blk :: l else l)
          (Bitv.bw_or nreplace xreplace)
          []
      in
      if is_really_sr_cand cand then
        strength_reduction btl_ref blk_tools replace_pts ckey cand)

(** Invariant construction part *)

let build_ival_from_ckey input ckey =
  match ckey with
  | CCM (CMop (_, op, args)) | CSR (_, op, args) ->
      Iop (Rop op, List.map (mk_ir input) args)
  | CCM (CMload (trap, chk, addr, args, aaddr)) ->
      Iop (Rload (trap, chk, addr, aaddr), List.map (mk_ir input) args)

(** Build a liveness bitvector for an auxiliary variable [vaux] *)
(* TODO we do not need this?
   let liveness_pred_for_vaux btl card vaux blk2id =
     let live_bv = Bitv.create card false in
     apply_over_function btl (fun n ibf ->
         let bitvi = pget n blk2id in
         let is_live = Regset.mem vaux ibf.binfo.input_regs in
         Bitv.set live_bv bitvi is_live);
     live_bv
*)

(** Build all the needed invariants for LCT:
for each candidate in the list, with a defined vaux:
  pp = (N-REPLACE /\ not N-INSERT) \/ (not N-ISOLATED /\ not N-DELAYED)
  if the candidate is trapping:
    pp = pp_op /\ N-U-SAFE
  for blk_pc in CFG
    if blk_pc <> entry && candidate is a SRmul && candidate is not immediate SR op && candidate was reduced:
      r = constant_reg of candidate
      cop, cpc = obtain the constant operation and first appearance block pc
      if blk_pc < cpc && r <= last_reg (ie is not a fresh variable) && (blk_pc in pp || r live in blk_pc)
        add an history invariant based on the constant operation, along with a corresponding liveness entry.
    if blk_pc in pp:
      add a gluing invariant based on the candidate operation in ckey *)
let build_invariants btl gm entry blk_tools lcandidates =
  let gm = ref gm in
  List.iter
    (fun (ckey, cand) ->
      llog 2 "invariants for candidate ";
      print_cand stderr (ckey, cand);
      llog 2 "\n";
      match cand.vaux with
      | Some vaux ->
          let ninsert = get_predicate_cand cand P_ninsert
          and nreplace = get_predicate_cand cand P_nreplace
          (*and xreplace = get_predicate_cand cand P_xreplace*)
          (*and ncomp = get_predicate_cand cand P_ncomp*)
          and nisolated = get_predicate_cand cand P_nisolated
          and ndelayed = get_predicate_cand cand P_ndelayed
          and nusafe = get_predicate_cand cand P_nusafe in
          let n_not_delayed_nor_isolated =
            Bitv.bw_and (Bitv.bw_not nisolated) (Bitv.bw_not ndelayed)
          (*and x_not_delayed_nor_isolated =*)
          (*Bitv.bw_and (Bitv.bw_not xisolated) (Bitv.bw_not xdelayed)*)
          and n_replace_and_not_insert =
            Bitv.bw_and nreplace (Bitv.bw_not ninsert)
            (*and x_replace_and_not_insert =*)
            (*Bitv.bw_and xreplace (Bitv.bw_not xinsert)*)
            (*and n_replace_or_x_replace = Bitv.bw_or nreplace xreplace*)
          in
          let preserv_pts_op =
            (*Bitv.bw_or*)
            Bitv.bw_or n_replace_and_not_insert n_not_delayed_nor_isolated
            (*(Bitv.bw_or x_replace_and_not_insert x_not_delayed_nor_isolated)*)
          in
          let preserv_pts =
            if is_trapping_ckey ckey then
              (*let vlive =*)
              (*liveness_pred_for_vaux btl blk_tools.card vaux blk_tools.blk2id*)
              (*in*)
              (*let vlive_or_replace = Bitv.bw_or vlive nreplace in*)
              (*Bitv.bw_and vlive_or_replace preserv_pts_op*)
              Bitv.bw_and nusafe preserv_pts_op
              (*Bitv.bw_and preservation_points (Bitv.bw_or ncomp nusafe)*)
            else preserv_pts_op
          in
          Bitv.iteri
            (fun i b ->
              (* TODO add something to remember where (which block) the constant has been seen for the first time?
               * Normally the above suggestion should be useless thanks to the transparency analysis... *)
              let blk_target = IM.find i blk_tools.id2blk in
              if blk_target <> entry then (
                let inputs = (pget blk_target btl).binfo.input_regs in
                (match ckey with
                | CSR (SRmul, op, args) ->
                    if (not (is_immediate_sr_op op)) && cand.was_reduced then (
                      let r = List.find is_constant_reg args in
                      let constop, cpc = Hashtbl.find constants r in
                      llog 3
                        "want to add a constant in blk_target %d and cpc %d\n"
                        (p2i blk_target) (p2i cpc);
                      if
                        P.( < ) blk_target cpc
                        && P.( <= ) r blk_tools.last_reg
                        && (b || Regset.mem r inputs)
                      then (
                        let hiv = Iop (Rop constop, []) in
                        gm := add_aseq_in_gm blk_target !gm r hiv History;
                        gm :=
                          add_outputs_in_gm blk_target !gm (Regset.singleton r)
                            History))
                | _ -> ());
                if b then
                  (*(if Option.is_some cand.updated_args && Bitv.get nusafe i then
                    let move_target =
                      List.find
                        (fun a -> not (PSet.contains blk_tools.sr_vauxs a))
                        (extract_ckey_args ckey)
                    in
                    if Regset.mem move_target inputs then
                      let giv = Iop (Rop Omove, [ mk_ir false vaux ]) in
                      gm := add_aseq_in_gm blk_target !gm move_target giv Gluing);*)
                  let input = false in
                  (*let input = (not @@ is_sr_mul_ckey ckey) && Bitv.get nusafe i in*)
                  (*let input = not (is_sr_ckey ckey && Bitv.get nusafe i) in*)
                  let giv = build_ival_from_ckey input ckey in
                  gm := add_aseq_in_gm blk_target !gm vaux giv Gluing))
            preserv_pts
      | None -> ())
    lcandidates;
  !gm

(** Main part *)

(** This function builds two mappings from block ids to bitvector ids
    and its reverse, to facilitate bitvector manipulation *)
let identify_blocks btl =
  let last_id = ref 0 in
  let blk2id =
    List.fold_left
      (fun pm (n, ibf) ->
        let pm' = PTree.set n !last_id pm in
        last_id := !last_id + 1;
        pm')
      PTree.empty (PTree.elements btl)
  in
  let id2blk = PTree.fold (fun pm k v -> IM.add v k pm) blk2id IM.empty in
  (blk2id, id2blk)

let initialize () =
  Hashtbl.clear constants;
  Hashtbl.clear candidates;
  Hashtbl.clear affine_int64;
  tstart := Unix.gettimeofday ();
  tcounter := 0;
  ok_trap := !Clflags.option_flct_trap;
  let open Machine in
  ok_sr := !Clflags.option_flct_sr && !config.name = "rv64";
  tlimit := !Clflags.option_flct_tlimit;
  nlimit := !Clflags.option_flct_nlimit;
  reset_stats ()

let prepare_data btl entry =
  let elts = PTree.elements btl in
  find_last_node elts;
  find_last_reg_btl elts;
  llog 1 "compute surrounding blocks\n";
  compute_surrounding_blocks btl entry;
  llog 1 "identify_blocks...\n";
  let _blk2id, _id2blk = identify_blocks btl in
  let btl_prep, gm =
    if !ok_sr then (
      llog 1 "detect_constants...\n";
      detect_constants elts;
      if log_lvl >= 2 then print_constants ();
      (*simplify_trivial_ops btl _id2blk*)
      (btl, PTree.empty))
    else (btl, PTree.empty)
  in
  llog 1 "detect_candidates...\n";
  apply_over_function btl_prep detect_candidates;
  filter_candidates ();
  llog 1 "sort candidates...\n";
  let lcandidates =
    Hashtbl.fold (fun k v acc -> (k, v) :: acc) candidates []
    |> List.sort (fun (k1, v1) (k2, v2) ->
           let imk_to_ordis im =
             IM.fold (fun blkid _ ps -> blkid $+ ps) im OrdIS.empty
           in
           let blks1 = imk_to_ordis v1.lhs and blks2 = imk_to_ordis v2.lhs in
           let common_blks = blks1 $&$ blks2 in
           if OrdIS.cardinal common_blks = 0 then
             compare
               (fst @@ IM.max_binding v2.lhs)
               (fst @@ IM.max_binding v1.lhs)
           else
             let max_common_blks = !$+common_blks in
             compare
               !$-(IM.find max_common_blks v1.lhs)
               !$-(IM.find max_common_blks v2.lhs))
  in
  llog 1 "Printing ordered candidates\n";
  List.iter
    (fun tup ->
      print_cand stderr tup;
      llog 2 "\n")
    lcandidates;
  llog 1 "making elts map...\n";
  let keys = List.map (fun t -> fst t) elts in
  llog 1 "making worklist...\n";
  let _wl = plist2IS keys in
  llog 1 "making cardinal...\n";
  let _card = OrdIS.cardinal _wl in
  let blk_tools =
    {
      blk2id = _blk2id;
      id2blk = _id2blk;
      wl = _wl;
      card = _card;
      last_reg = r2p ();
      sr_vauxs = PTree.empty;
    }
  in
  (btl_prep, gm, blk_tools, lcandidates)

(** The function runs over a basic-block for an SRadd candidate with [op] and
    [args], and for its sr-arguments [sr_args]. It find every original
    sr_variable by looking for moves, and it returns true if either
  - it exist an lcm argument in the SRadd (so neither input nor sr);
  - a sr_variable or an original one is read with [sr_var_read];
  - a non-sr-argument is written by a non-affine operation before
    [offset_target] with [non_affine_write].
    The function ignores sr moves, sr update instructions,
    and the candidate itself. *)
let is_incompatible_sradd op args sr_args ibf last_reg offset_target =
  let offset = ref 0 and ps_dsts = ref OrdIS.empty in
  let ps_args = plist2IS args and ps_sr = plist2IS sr_args in
  let lmem args ps = List.exists (fun a -> p2i a $? ps) args in
  let sr_var_read args = lmem args !ps_dsts || lmem args ps_sr
  and non_affine_write dst =
    let ldst = [ dst ] in
    !offset < offset_target && (not (lmem ldst ps_sr)) && lmem ldst ps_args
  in
  (* If at least one of the arguments is an lcm one, we do not have to analyze
   * the block and we can already be sure to downgrade *)
  if OrdIS.exists (fun a -> p2i last_reg < a && not (a $? ps_sr)) ps_args then
    true
  else
    let rec get_read_sr_result_rec ib =
      offset := !offset + 1;
      match ib with
      | Bop (Omove, [ arg ], dst, iinfo) ->
          (* This condition recognize a sr-move *)
          if p2i arg $? ps_sr && iinfo.inumb = -1 then (
            ps_dsts := p2i dst $+ !ps_dsts;
            false)
          else sr_var_read [ arg ]
      | Bop (op', args', dst, iinfo) ->
          (* The first conditional is true if the operation
           * is not affine but write one of the non-sr arguments; and the second
           * if it is neither the candidate itself, nor an update update operation. *)
          if (not (is_affine_op op')) && non_affine_write dst then true
          else if
            (op <> op' || args <> args')
            && ((not (is_sr_update_op op')) || List.hd args' <> dst)
          then sr_var_read args'
          else false
      | Bstore (_, _, args', _, _, _)
      | Bcond (_, args', _, _, _)
      | BF (Btailcall (_, _, args'), _) ->
          sr_var_read args'
      | Bload (_, _, _, args', _, dst, _) | BF (Bcall (_, _, args', dst, _), _) ->
          non_affine_write dst || sr_var_read args'
      | BF (Bjumptable (r, _), _) | BF (Breturn (Some r), _) ->
          sr_var_read [ r ]
      | BF (Bbuiltin (_, bargs, bdst, _), _) ->
          AST.params_of_builtin_res bdst |> List.exists non_affine_write
          || AST.params_of_builtin_args bargs |> sr_var_read
      | Bseq (ib1, ib2) ->
          get_read_sr_result_rec ib1 || get_read_sr_result_rec ib2
      | _ -> false
    in
    get_read_sr_result_rec ibf.entry

(** Downgrading an SRadd candidate to a LCM one. Cases of downgrading:
  - If none of the arguments are strength-reduction auxiliary variables
  - If, for at least one of the blocks where the candidate is present:
    {ul
    {- One of the sr-arguments is live after (union of live variables
       of the successors)} TODO sure we need that?
    {- One of the arguments is read in the block}}
*)
let may_downgrade_ckey btl blk_tools ckey cand =
  match ckey with
  | CSR (SRadd, op, args) ->
      (* [sr_args] are arguments which were previously strength-reduced *)
      let sr_args =
        List.find_all
          (fun a -> PTree.get a blk_tools.sr_vauxs |> Option.is_some)
          args
      in
      (*
      let must_downgrade =
        (* If there is zero sr-argument, we downgrade *)
        List.length sr_args = 0
        || IM.exists (* If a single block is incompatible *)
             (fun ipc soff ->
               let pc = i2p ipc in
               let ibf = pget pc btl in
               (* Union of live variables of every successor *)
               let live_after =
                 List.fold_right
                   (fun succ la ->
                     Regset.union la (pget succ btl).binfo.input_regs)
                   ibf.binfo.successors Regset.empty
               in
               (* If at least one sr-argument is live after, downgrade!
                * The below command takes the original arguments of the
                * candidate to test the liveness, but to be sure that the
                * argument is also an sr one, we compare it with the
                * updated args (it must not be found) *)
               let ko_dead =
                 List.exists
                   (fun a ->
                     Regset.mem a live_after
                     && List.for_all (fun a' -> not @@ P.( = ) a a') args)
                   cand.orig_args
               in
               ko_dead
               || OrdIS.exists
                    (is_incompatible_sradd op args sr_args ibf
                       blk_tools.last_reg)
                    soff)
             cand.lhs
      in*)
      let must_downgrade = List.length sr_args = 0 in
      if not must_downgrade then ckey
      else
        let uckey = CCM (CMop (NOTRAP, op, args)) in
        Hashtbl.remove candidates ckey;
        merge_cand uckey cand;
        uckey
  | _ -> ckey

let update_ckey btl blk_tools ckey cand =
  let ckey =
    match cand.updated_args with
    | Some args -> update_args_ckey ckey args
    | None -> ckey
  in
  may_downgrade_ckey btl blk_tools ckey cand

let rec lazy_optim btl_ref entry blk_tools = function
  | [] -> []
  | (ckey, cand) :: l ->
      if IM.cardinal cand.lhs = 0 then lazy_optim btl_ref entry blk_tools l
      else (
        llog 1 "lazy_optim\n";
        let ckey = update_ckey !btl_ref blk_tools ckey cand in
        llog 2 "cand: ";
        print_cand stderr (ckey, cand);
        llog 1 "\nStart init_and_local_analysis...\n";
        init_and_local_analysis !btl_ref blk_tools ckey cand;
        llog 1 "Start dataflow_analysis...\n";
        dataflow_analysis !btl_ref entry blk_tools ckey cand;
        llog 1 "Start rewrite_graph...\n";
        rewrite_graph btl_ref blk_tools ckey cand;
        (ckey, cand) :: lazy_optim btl_ref entry blk_tools l)

(** Launch according to cli args *)
let lazy_code_oracle btl entry =
  (*let open LazyCodeExamples in*)
  (*let btl, entry = example_sr in*)
  initialize ();
  (*llog 1 "Pre-analyze liveness\n";*)
  (*let _ = if !ok_sr then update_liveness_btl btl else false in*)
  llog 1 "Start Lazy Code Oracle...\n";
  let btl_prep, gm, blk_tools, lcandidates = prepare_data btl entry in
  let btl_optim, lcandidates, ok_lazy_optim =
    let btl_ref = ref btl_prep in
    try
      let lcandidates = lazy_optim btl_ref entry blk_tools lcandidates in
      (!btl_ref, lcandidates, true)
    with TimeLimitReached ->
      llog 1 "TimeLimitReached exception! LCM aborted!\n";
      (btl, lcandidates, false)
  in
  llog 1 "Start build_invariants...\n";
  let need_dce = update_liveness_btl btl_optim in
  let btl_dce =
    if need_dce then btl_simple_dead_code_elimination btl_optim else btl_optim
  in
  let gm_seq =
    if ok_lazy_optim then
      build_invariants btl_dce gm entry blk_tools (List.rev lcandidates)
    else PTree.empty
  in
  let gm_full = build_liveness_invariants (PTree.elements btl_dce) gm_seq in

  (*let open DebugPrint in*)
  (*debug_flag := true;*)
  if btl != btl_dce then (
    print_predicates blk_tools.wl blk_tools.blk2id;
    llog 2 "entry is %d\n" (p2i entry);
    llog 2 "btl orig code\n";
    print_btl_code stderr btl;
    llog 2 "trans code (lazy_optim + simple dce)\n";
    print_btl_code stderr btl_dce;
    print_gluemap gm_full);

  (*debug_flag := false;*)
  stat_lct_exec_time := Unix.gettimeofday () -. !tstart;
  if save_stats then write_stats ();
  if log_lvl >= 1 then
    eprintf "Stats: %a%a" fprint_stats_header () fprint_stats ();
  llog 1 "End Lazy Code Oracle...\n";
  (btl_dce, gm_full)
