Require Import Coqlib AST Registers Maps.
Require Import Op OptionMonad.
Require Import BTL_SEtheory BTL_SEsimuref.
Require Import BTL_SEsimplify BTL_SEsimplifyproof.
Require Import BTL_SEsimplifyMem.
Import ValueDomain.
Import Notations.
Import HConsing.
Import SvalNotations.
Import ListNotations.

Local Open Scope list_scope.
Local Open Scope option_monad_scope.
Local Open Scope impure.

(** Debug printer *)
Definition XDEBUG {A} (x:A) (k: A -> ?? pstring): ?? unit := RET tt. (* TO REMOVE DEBUG INFO *)
(*Definition XDEBUG {A} (x:A) (k: A -> ?? pstring): ?? unit := DO s <~ k x;; println ("DEBUG simu_check:" +; s). (* TO INSERT DEBUG INFO *)*)

Definition DEBUG (s: pstring): ?? unit := XDEBUG tt (fun _ => RET s).

(** * Implementation of Data-structure use in Hash-consing *)

(** Now, we build the hash-Cons value from a "hash_eq".

  Informal specification: 
    [hash_eq] must be consistent with the "hashed" constructors defined above.

  We expect that hashinfo values in the code of these "hashed" constructors verify:
    (hash_eq (hdata x) (hdata y) ~> true) <-> (hcodes x)=(hcodes y)
*)

Definition sval_hash_eq (sv1 sv2: sval): ?? bool :=
  match sv1, sv2 with
  | Sinput r1 _, Sinput r2 _ => struct_eq r1 r2 (* NB: really need a struct_eq here ? *)
  | Sop op1 lsv1 _, Sop op2 lsv2 _  =>
     DO b1 <~ phys_eq lsv1 lsv2;;
     if b1
     then struct_eq op1 op2 (* NB: really need a struct_eq here ? *)
     else RET false
  | Sload sm1 trap1 chk1 addr1 lsv1 aaddr1 _, Sload sm2 trap2 chk2 addr2 lsv2 aaddr2 _ =>
     DO b1 <~ phys_eq lsv1 lsv2;;
     DO b2 <~ phys_eq sm1 sm2;;
     DO b3 <~ struct_eq trap1 trap2;;
     DO b4 <~ struct_eq chk1 chk2;;
     if b1 && b2 && b3 && b4
     then if eq_aval aaddr1 aaddr2
     then struct_eq addr1 addr2
     else RET false else RET false
  | Sfoldr op1 lsv1 sv0_1 _, Sfoldr op2 lsv2 sv0_2 _ =>
      DO b1 <~ phys_eq lsv1 lsv2;;
      if b1 then
        DO b2 <~ phys_eq sv0_1 sv0_2;;
        if b2 then
          struct_eq op1 op2
        else RET false
      else RET false
  | _,_ => RET false
  end.

Lemma and_true_split a b: a && b = true <-> a = true /\ b = true.
Proof.
  destruct a; simpl; intuition.
Qed.

Lemma sval_hash_eq_correct x y:
  WHEN sval_hash_eq x y ~> b THEN 
  b = true -> sval_set_hid x unknown_hid = sval_set_hid y unknown_hid.
Proof.
  destruct x, y; wlp_simplify; try ( rewrite !and_true_split in * ); intuition; subst; try congruence.
Qed.
Global Opaque sval_hash_eq.
Global Hint Resolve sval_hash_eq_correct: wlp.

Definition list_sval_hash_eq (lsv1 lsv2: list_sval): ?? bool :=
  match lsv1, lsv2 with
  | Snil _, Snil _ => RET true
  | Scons sv1 lsv1' _, Scons sv2 lsv2' _  =>
     DO b <~ phys_eq lsv1' lsv2';;
     if b 
     then phys_eq sv1 sv2
     else RET false
  | _,_ => RET false
  end.

Lemma list_sval_hash_eq_correct x y:
  WHEN list_sval_hash_eq x y ~> b THEN 
  b = true -> list_sval_set_hid x unknown_hid = list_sval_set_hid y unknown_hid.
Proof.
  destruct x, y; wlp_simplify; try ( rewrite !and_true_split in * ); intuition; subst; try congruence.
Qed.
Global Opaque list_sval_hash_eq.
Global Hint Resolve list_sval_hash_eq_correct: wlp.

Definition smem_hash_eq (sm1 sm2: smem): ?? bool :=
  match sm1, sm2 with
  | Sinit _, Sinit _ => RET true
  | Sstore sm1 chk1 addr1 lsv1 aaddr1 sv1 _, Sstore sm2 chk2 addr2 lsv2 aaddr2 sv2 _ =>
     DO b1 <~ phys_eq lsv1 lsv2;;
     DO b2 <~ phys_eq sm1 sm2;;
     DO b3 <~ phys_eq sv1 sv2;;
     DO b4 <~ struct_eq chk1 chk2;;
     if b1 && b2 && b3 && b4
     then if store_info_eq_dec aaddr1 aaddr2
     then struct_eq addr1 addr2
     else RET false else RET false
  | _,_ => RET false
  end.

Lemma smem_hash_eq_correct x y:
  WHEN smem_hash_eq x y ~> b THEN 
  b = true -> smem_set_hid x unknown_hid = smem_set_hid y unknown_hid.
Proof.
  destruct x, y; wlp_simplify; try ( rewrite !and_true_split in * ); intuition; subst; try congruence.
Qed.
Global Opaque smem_hash_eq.
Global Hint Resolve smem_hash_eq_correct: wlp.

Definition hSVAL: hashP sval := {| hash_eq := sval_hash_eq; get_hid:=sval_get_hid; set_hid:=sval_set_hid |}. 
Definition hLSVAL: hashP list_sval := {| hash_eq := list_sval_hash_eq; get_hid:= list_sval_get_hid; set_hid:= list_sval_set_hid |}.
Definition hSMEM: hashP smem := {| hash_eq := smem_hash_eq; get_hid:= smem_get_hid; set_hid:= smem_set_hid |}.

Program Definition mk_hash_params: Dict.hash_params sval :=
 {|
    Dict.test_eq := phys_eq;
    Dict.hashing := fun (sv: sval) => RET (sval_get_hid sv);
    Dict.log := fun sv =>
         DO sv_name <~ string_of_hashcode (sval_get_hid sv);;
         println ("unexpected undef behavior of hashcode:" +; (CamlStr sv_name)) |}.
Next Obligation.
  wlp_simplify.
Qed.

(** * Implementation of symbolic execution *)
Record HashConsingFcts :=
  {
    hC_sval: hashinfo sval -> ?? sval;
    hC_list_sval: hashinfo list_sval -> ?? list_sval;
    hC_smem: hashinfo smem -> ?? smem
  }.

Class HashConsingHyps HCF :=
  {
    hC_sval_correct: forall s,
      WHEN HCF.(hC_sval) s ~> s' THEN forall ctx,
      sval_equiv ctx (hdata s) s';

    hC_list_sval_correct: forall lh,
      WHEN HCF.(hC_list_sval) lh ~> lh' THEN forall ctx,
      list_sval_equiv ctx (hdata lh) lh';

    hC_smem_correct: forall hm,
      WHEN HCF.(hC_smem) hm ~> hm' THEN forall ctx,
      smem_equiv ctx (hdata hm) hm'
  }.

Global Hint Resolve hC_sval_correct hC_list_sval_correct hC_smem_correct: wlp.

(* Useful Ltac when an impure lemma conclude to a contradiction. *)
Ltac wlp_exploit hint := try intros; exploit hint; eauto; try contradiction.

Section SymbolicCommon.

  Context `{HCF : HashConsingFcts}.
  Context `{HC : HashConsingHyps HCF}.
  Context `{RRULES: rrules_set}.

(** Hash consing of symbolic values *)

Definition reg_hcode := 1.
Definition op_hcode := 2.
Definition foldr_hcode := 3.
Definition load_hcode := 4.
Definition undef_code := 5.

Definition hSinput_hcodes (r: reg) :=
  DO hc <~ hash reg_hcode;;
  DO sv <~ hash r;;
  RET [hc;sv].
Extraction Inline hSinput_hcodes.

Definition hSop_hcodes (op:operation) (lsv: list_sval) :=
  DO hc <~ hash op_hcode;;
  DO sv <~ hash op;;
  RET [hc;sv;list_sval_get_hid lsv].
Extraction Inline hSop_hcodes.

Definition hSfoldr_hcodes (op: operation) (lsv: list_sval) (sv0: sval) :=
  DO hc <~ hash foldr_hcode;;
  DO sv <~ hash op;;
  RET [hc;sv;list_sval_get_hid lsv; sval_get_hid sv0].
Extraction Inline hSfoldr_hcodes.

Definition hSload_hcodes (sm: smem) (trap: trapping_mode) (chunk: memory_chunk)
  (addr: addressing) (lsv: list_sval):=
  DO hc  <~ hash load_hcode;;
  DO sv1 <~ hash trap;;
  DO sv2 <~ hash chunk;;
  DO sv3 <~ hash addr;;
  RET [hc; smem_get_hid sm; sv1; sv2; sv3; list_sval_get_hid lsv].
Extraction Inline hSload_hcodes.

Definition hSstore_hcodes (sm: smem) (chunk: memory_chunk)
  (addr: addressing) (lsv: list_sval) (srce: sval) :=
  DO sv1 <~ hash chunk;;
  DO sv2 <~ hash addr;;
  RET [smem_get_hid sm; sv1; sv2; list_sval_get_hid lsv; sval_get_hid srce].
Extraction Inline hSstore_hcodes.

Definition hSnil (_: unit): ?? list_sval :=
  hC_list_sval HCF {| hdata := Snil unknown_hid; hcodes := nil |}.

Lemma hSnil_correct:
  WHEN hSnil() ~> sv THEN forall ctx,
  list_sval_equiv ctx sv (Snil unknown_hid).
Proof.
  unfold hSnil; wlp_simplify.
Qed.
Global Opaque hSnil.
Hint Resolve hSnil_correct: wlp.

Definition hScons (sv: sval) (lsv: list_sval): ?? list_sval :=
  hC_list_sval HCF
  {| hdata := Scons sv lsv unknown_hid; hcodes := [sval_get_hid sv; list_sval_get_hid lsv] |}.

Lemma hScons_correct sv1 lsv1:
  WHEN hScons sv1 lsv1 ~> lsv1' THEN forall ctx sv2 lsv2
  (ESVEQ: sval_equiv ctx sv1 sv2)
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2),
  list_sval_equiv ctx lsv1' (Scons sv2 lsv2 unknown_hid).
Proof.
  unfold hScons; wlp_simplify.
  rewrite <- ESVEQ, <- ELSVEQ.
  auto.
Qed.
Global Opaque hScons.
Hint Resolve hScons_correct: wlp.

Definition hSinput (r:reg): ?? sval :=
  DO sv <~ hSinput_hcodes r;;
  hC_sval HCF {| hdata:=Sinput r unknown_hid; hcodes :=sv; |}.

Lemma hSinput_correct r:
  WHEN hSinput r ~> sv THEN forall ctx,
  sval_equiv ctx sv (Sinput r unknown_hid).
Proof.
  wlp_simplify.
Qed.
Global Opaque hSinput.
Hint Resolve hSinput_correct: wlp.
  
Definition hSop (op:operation) (lsv: list_sval): ?? sval :=
  DO sv <~ hSop_hcodes op lsv;;
  hC_sval HCF {| hdata:=Sop op lsv unknown_hid; hcodes :=sv |}.

Lemma hSop_fSop_correct op lsv:
  WHEN hSop op lsv ~> sv THEN forall ctx,
  sval_equiv ctx sv (fSop op lsv).
Proof.
  wlp_simplify.
Qed.
Global Opaque hSop.
Hint Resolve hSop_fSop_correct: wlp_raw.

Lemma hSop_correct op lsv1:
  WHEN hSop op lsv1 ~> sv THEN forall ctx lsv2
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2),
  sval_equiv ctx sv (Sop op lsv2 unknown_hid).
Proof.
  wlp_xsimplify ltac:(intuition eauto with wlp wlp_raw).
  rewrite <- ELSVEQ. erewrite H; eauto.
Qed.
Hint Resolve hSop_correct: wlp.

Definition hSfoldr (op: operation) (lsv: list_sval) (sv0: sval): ?? sval :=
  DO sv <~ hSfoldr_hcodes op lsv sv0;;
  hC_sval HCF {| hdata:=Sfoldr op lsv sv0 unknown_hid; hcodes := sv |}.

Lemma hSfoldr_correct op lsv1 sv0_1:
  WHEN hSfoldr op lsv1 sv0_1 ~> sv THEN forall ctx lsv2 sv0_2
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2)
  (ESVEQ: sval_equiv ctx sv0_1 sv0_2),
  sval_equiv ctx sv (Sfoldr op lsv2 sv0_2 unknown_hid).
Proof.
  wlp_simplify.
  rewrite <- ELSVEQ, <- ESVEQ.
  auto.
Qed.
Global Opaque hSfoldr.
Hint Resolve hSfoldr_correct: wlp.
  
Definition hSload (sm: smem) (trap: trapping_mode) (chunk: memory_chunk)
  (addr: addressing) (lsv: list_sval) aaddr: ?? sval :=
  DO sv <~ hSload_hcodes sm trap chunk addr lsv;;
  hC_sval HCF {| hdata := Sload sm trap chunk addr lsv aaddr unknown_hid; hcodes := sv |}.

Lemma hSload_correct sm1 trap chunk addr lsv1 aaddr:
  WHEN hSload sm1 trap chunk addr lsv1 aaddr ~> sv THEN forall ctx lsv2 sm2
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2)
  (EMEQ: smem_equiv ctx sm1 sm2),
  sval_equiv ctx sv (Sload sm2 trap chunk addr lsv2 aaddr unknown_hid).
Proof.
  wlp_simplify.
  rewrite <- ELSVEQ, <- EMEQ.
  auto.
Qed.
Global Opaque hSload.
Hint Resolve hSload_correct: wlp.

Definition hSstore (sm: smem) (chunk: memory_chunk)
  (addr: addressing) (lsv: list_sval) aaddr (srce: sval): ?? smem :=
  DO sv <~ hSstore_hcodes sm chunk addr lsv srce;;
  hC_smem HCF {| hdata := Sstore sm chunk addr lsv aaddr srce unknown_hid; hcodes := sv |}.

Lemma hSstore_correct sm1 chunk addr lsv1 aaddr sv1:
  WHEN hSstore sm1 chunk addr lsv1 aaddr sv1 ~> sm1' THEN forall ctx lsv2 sm2 sv2
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2)
  (EMEQ: smem_equiv ctx sm1 sm2)
  (ESVEQ: sval_equiv ctx sv1 sv2),
  smem_equiv ctx sm1' (Sstore sm2 chunk addr lsv2 aaddr sv2 unknown_hid).
Proof.
  wlp_simplify.
  rewrite <- ELSVEQ, <- EMEQ, <- ESVEQ.
  auto.
Qed.
Global Opaque hSstore.
Hint Resolve hSstore_correct: wlp.

(* Convert a "fake" hash-consed term into a "real" hash-consed term *)

Fixpoint fsval_proj sv: ?? sval :=
  match sv with
  | Sinput r hc => 
      DO b <~ phys_eq hc unknown_hid;;
      if b then hSinput r (* was not yet really hash-consed *)
      else RET sv (* already hash-consed *)
  | Sop op hl hc => 
      DO b <~ phys_eq hc unknown_hid;;
      if b then (* was not yet really hash-consed *)
        DO hl' <~ fsval_list_proj hl;;
        hSop op hl'
      else RET sv (* already hash-consed *)
  | Sfoldr op hl hv hc =>
      DO b <~ phys_eq hc unknown_hid;;
      if b then (* was not yet really hash-consed *)
        DO hl' <~ fsval_list_proj hl;;
        DO hv' <~ fsval_proj hv;;
        hSfoldr op hl' hv'
      else RET sv (* already hash-consed *)
  | Sload hm t chk addr hl aaddr hc =>
      DO b <~ phys_eq hc unknown_hid;;
      if b then (* was not yet really hash-consed *)
        DO hm' <~ fsmem_proj hm;;
        DO hl' <~ fsval_list_proj hl;;
        hSload hm' t chk addr hl' aaddr
      else RET sv (* already hash-consed *)
  end
with fsval_list_proj sl: ?? list_sval :=
  match sl with
  | Snil hc => 
      DO b <~ phys_eq hc unknown_hid;;
      if b then hSnil() (* was not yet really hash-consed *)
      else RET sl (* already hash-consed *)
  | Scons sv hl hc => 
      DO b <~ phys_eq hc unknown_hid;;
      if b then (* was not yet really hash-consed *)
        DO sv' <~ fsval_proj sv;;
        DO hl' <~ fsval_list_proj hl;;
        hScons sv' hl' 
      else RET sl (* already hash-consed *)
  end
with fsmem_proj sm: ??smem :=
  match sm with
  | Sinit hc =>
      (* There should be only one Sinit, created in ris_empty, we do not hash-cons it *)
      RET sm
  | Sstore sm1 chk addr hl aaddr hv hc =>
      DO b <~ phys_eq hc unknown_hid;;
      if b then (* was not yet really hash-consed *)
        DO sm1' <~ fsmem_proj sm1;;
        DO hl'  <~ fsval_list_proj hl;;
        DO hv'  <~ fsval_proj hv;;
        hSstore sm1' chk addr hl' aaddr hv'
      else RET sm (* already hash-consed *)
  end.

Lemma fsval_proj_correct sv:
  WHEN fsval_proj sv ~> sv' THEN forall ctx,
  sval_equiv ctx sv sv'.
Proof.
  induction sv using sval_mut
  with (P0 := fun lsv =>
        WHEN fsval_list_proj lsv ~> lsv' THEN forall ctx,
          eval_list_sval ctx lsv = eval_list_sval ctx lsv')
        (P1 := fun sm =>
        WHEN fsmem_proj sm ~> sm' THEN forall ctx,
           smem_equiv ctx sm sm');
     try (wlp_simplify; tauto).
  - (* Sop *)
    wlp_xsimplify ltac:(intuition eauto with wlp_raw wlp).
    rewrite H, H0; auto.
  - (* Sfoldr *)
    wlp_xsimplify ltac:(intuition eauto with wlp_raw wlp).
    rewrite H, H0; auto.
    erewrite H1; eauto.
  - (* Sload *)
    wlp_xsimplify ltac:(intuition eauto).
    rewrite H, H0.
    exploit hSload_correct; eauto.
  - (* Scons *)
    wlp_simplify; erewrite H0, H1; eauto.
  - (* Sstore *)
    wlp_xsimplify ltac:(intuition eauto).
    rewrite H, H0, H1.
    exploit hSstore_correct; eauto.
Qed.
Global Opaque fsval_proj.
Hint Resolve fsval_proj_correct: wlp.

Lemma fsval_list_proj_correct lsv:
  WHEN fsval_list_proj lsv ~> lsv' THEN forall ctx,
  list_sval_equiv ctx lsv lsv'.
Proof.
  induction lsv; wlp_simplify.
  erewrite H0, H1; eauto.
Qed.
Global Opaque fsval_list_proj.
Hint Resolve fsval_list_proj_correct: wlp.

Lemma fsmem_proj_correct sm:
  WHEN fsmem_proj sm ~> sm' THEN forall ctx,
  smem_equiv ctx sm sm'.
Proof.
  induction sm; wlp_simplify.
  erewrite H0, H1, H2; eauto.
Qed.
Global Opaque fsmem_proj.
Hint Resolve fsmem_proj_correct: wlp.

(** ** Access of registers *)

(** rem: this is a defensive check to assert
    that every value is already hash-consed. *)
(*
Definition is_hash_consed sv: ?? bool :=
  match sv with
  | Sinput _ hc
  | Sop _ _ hc
  | Sload _ _ _ _ _ hc =>
      DO b <~ phys_eq hc unknown_hid;;
      RET (negb b)
  end.
 *)

Definition hrs_sreg_get (hrs: ristate) r: ?? sval :=
   match hrs.(ris_sreg)!r with
   | None => if ris_input_init hrs then hSinput r
             else FAILWITH "hrs_sreg_get: dead variable"
   | Some sv =>
       (* DO is_hc <~ is_hash_consed sv;;*) RET sv
   end.

Lemma hrs_sreg_get_spec_ris hrs r:
  WHEN hrs_sreg_get hrs r ~> hsv THEN
  exists sv, hrs r = Some sv /\
    forall ctx, sval_equiv ctx hsv sv.
Proof.
  unfold hrs_sreg_get, ris_sreg_get.
  repeat autodestruct; wlp_simplify.
Qed.
Global Opaque hrs_sreg_get.

Lemma hrs_sreg_get_spec hrs r:
  WHEN hrs_sreg_get hrs r ~> hsv THEN
  forall ctx sis
  (RR: ris_refines_ok ctx hrs sis),
  exists sv, sis r = Some sv /\ sval_equiv ctx hsv sv.
Proof.
  intros hsv H; intros. apply hrs_sreg_get_spec_ris in H as [? [H ->]].
  eapply ris_refines_reg_some in H as [? [? ->]]; eauto.
Qed.

(** ** Hash-consed operations on [ireg] *)

(* [hrs_ir_get hrs hrs_old ir] implements [ir_subst_si sis si ir] when:
   - [hrs_old] refines [sis]
   - [hrs] is the obtained by applying the substitution [si] to [sis].
     This second condition is not unnecessary if [force_input ir]
     (since [hrs_ir_get] will only depend on [hrs_old]). *)

Definition hrs_ir_get hrs hrs_old ir: ?? sval :=
  if force_input ir
  then hrs_sreg_get hrs_old (regof ir)
  else hrs_sreg_get hrs     (regof ir).

Lemma hrs_ir_get_spec hrs hrs_old ir:
  WHEN hrs_ir_get hrs hrs_old ir ~> s THEN
  forall ctx sis0 sis1
  (RR0: ris_refines_ok ctx hrs_old sis0)
  (RR1: ris_refines_ok ctx hrs     sis1),
  exists sv, sis_get_ireg sis0 sis1 ir = Some sv /\ sval_equiv ctx s sv.
Proof.
  unfold hrs_ir_get, sis_get_ireg.
  case force_input; intros s GET; intros; eapply hrs_sreg_get_spec in GET; eauto.
Qed.

Fixpoint hrs_lir_get hrs hrs_old (lir: list ireg): ?? ((list sval) * list_sval) :=
  match lir with
  | nil => DO hlsv <~ hSnil();;
      RET (nil, hlsv)
  | ir::lir =>
      DO lsv_hlsv <~ hrs_lir_get hrs hrs_old lir;;
      DO sv       <~ hrs_ir_get hrs hrs_old ir;;
      let (lsv, hlsv) := lsv_hlsv in
      DO hlsv     <~ hScons sv hlsv;;
      RET ((sv :: lsv), hlsv)
  end.

(* The two lists returned by [hrs_lir_get] are equivalents: *)

Definition list_sval_equiv_list ctx (hls : list_sval) (ls : list sval) : Prop
  := eval_list_sval ctx hls = map_opt (eval_sval ctx) ls.

Lemma hrs_lir_get_equiv_list hrs hrs_old lir:
  WHEN hrs_lir_get hrs hrs_old lir ~> ls_hls THEN forall ctx,
  list_sval_equiv_list ctx (snd ls_hls) (fst ls_hls).
Proof.
  induction lir; simpl.
  - wlp_simplify.
  - wlp_seq; intros (ls0, hls0) REC sv _; apply IHlir in REC; simpl in REC.
    wlp_seq; intros hlsv HSCONS ctx; apply hScons_correct in HSCONS; simpl.
    unfold list_sval_equiv_list.
    erewrite HSCONS. 2,3:reflexivity.
    simpl. rewrite REC. reflexivity.
Qed.

(* Alternative definition of [list_sval_equiv_list] using lmap_sv: *)

Lemma lmap_sv_Some (ls : list sval):
  {hls : list_sval | lmap_sv (fun sv : sval => Some sv) ls = Some hls /\
                     forall ctx, list_sval_equiv_list ctx hls ls}.
Proof.
  induction ls as [|x xs [ys [IH0 IH1]]]; eexists; split; try reflexivity.
  - simpl. rewrite IH0. reflexivity.
  - intro ctx. unfold list_sval_equiv_list. simpl. rewrite IH1. reflexivity.
Qed.

Lemma list_sval_equiv_list_iff_lmap_sv {ctx hls ls}:
  list_sval_equiv_list ctx hls ls <->
  (exists hls', lmap_sv (fun sv => Some sv) ls = Some hls' /\ list_sval_equiv ctx hls hls').
Proof.
  split.
  - intros EV.
    case (lmap_sv_Some ls) as [hls' [? EV']].
    exists hls'. split. assumption.
    rewrite EV, EV'. reflexivity.
  - intros [hls' [EQ EV]]. revert hls hls' EQ EV.
    induction ls; simpl; unfold list_sval_equiv_list.
    + injection 1 as <-. simpl. auto.
    + autodestruct. injection 2 as <-. intros ->.
      simpl. erewrite IHls; reflexivity.
Qed.

Lemma hrs_lir_get_spec [hrs hrs_old: ristate] [lir]
  :WHEN hrs_lir_get hrs hrs_old lir ~> lsv_hlsv THEN
  forall ctx (sis0 sis1: sistate)
  (RR0: ris_refines_ok ctx hrs_old sis0)
  (RR1: ris_refines_ok ctx hrs     sis1),
  exists lsv',
    lmap_sv (sis_get_ireg sis0 sis1) lir = Some lsv' /\
    list_sval_equiv ctx (snd lsv_hlsv) lsv'.
Proof.
  induction lir; simpl.
  - wlp_simplify.
  - wlp_seq; intros (lsv0, hlsv0) REC sv Hsv; simpl; wlp_seq; intros hlsv Hhlsv; intros; simpl.
    eapply hScons_correct in Hhlsv as ->; try reflexivity; simpl.
    eapply IHlir in REC as [? [-> ->]]; eauto.
    eapply hrs_ir_get_spec in Hsv as [? [-> ->]]; eauto.
Qed.

Global Opaque hrs_ir_get hrs_lir_get.

(** ** Assignment of registers *)

Fixpoint lsv_in_imp (sv: sval) lsv :=
  match lsv with
  | nil => RET false
  | sv'::lsv =>
      DO b <~ phys_eq sv sv';;
      if b then RET true
      else lsv_in_imp sv lsv
  end.

Lemma lsv_in_imp_correct lsv: forall sv,
  WHEN lsv_in_imp sv lsv ~> b THEN b = true ->
  In sv lsv.
Proof.
  induction lsv; wlp_simplify; congruence.
Qed.
Local Hint Resolve lsv_in_imp_correct: wlp.


Section Rewriting.

  Variable strict : bool.

Definition hrewrite_load (sm: smem)
  (trap : trapping_mode) (chunk: memory_chunk) (addr: addressing) (lsv: list_sval) aaddr
  : ?? sval :=
  match rewrite_load RRULES strict sm trap chunk addr lsv aaddr with
  | Some fsv => fsval_proj fsv
  | None => hSload sm trap chunk addr lsv aaddr
  end.

Lemma hrewrite_load_correct sm trap chunk addr lsv aaddr:
  WHEN hrewrite_load sm trap chunk addr lsv aaddr ~> rew THEN
  forall ctx,
  ctx_strict_reflect (ctx_bc ctx) strict ->
  alive (eval_smem ctx sm) -> sval_equiv ctx rew (fSload sm trap chunk addr lsv aaddr).
Proof.
  unfold hrewrite_load.
  case rewrite_load as [rew|] eqn:REW0; intros rew' REW ctx STRICT MALIVE.
  + apply fsval_proj_correct in REW. rewrite <- REW.
    eapply rewrite_load_correct in REW0; eauto.
  + apply hSload_correct in REW; eauto.
Qed.
Global Opaque hrewrite_load.


(** Build a symbolic value representing the application of a [root_op].
    This symbolic value can be used to detect the failure of the expression.
    Hence the simplifications done here must be correct even if the application of
    the operator fails. *)

Definition root_happly (rsv: root_op) (lsv: list_sval) (sm: smem): ?? sval :=
  match rsv with
  | Rop op => hSop op lsv
  | Rload trap chunk addr aaddr => hrewrite_load sm trap chunk addr lsv aaddr
  end.

Definition root_happly_spec rop lsv sm:
  WHEN root_happly rop lsv sm ~> sv THEN
  forall ctx
  (STRICT: ctx_strict_reflect (ctx_bc ctx) strict)
  (MALIVE: alive (eval_smem ctx sm)),
  sval_equiv ctx sv (root_apply rop lsv sm).
Proof.
  unfold root_happly; case rop as [|].
  - intros sv Op  ? _ _. eapply hSop_correct in Op as ->; reflexivity.
  - intros sv REW ? ? ?. apply hrewrite_load_correct in REW as ->; auto.
Qed.
Global Opaque root_happly.


Definition fst_lhsv_imp lhsv :=
  match lhsv with
  | Scons sv (Snil _) _ => RET sv
  | _ => FAILWITH "fst_lhsv_imp: move args bug - should never occur"
  end.

Lemma fst_lhsv_imp_correct (lhsv: list_sval): forall ctx v,
  WHEN fst_lhsv_imp lhsv ~> sv THEN forall
  (HEVAL: eval_list_sval ctx lhsv = Some [v]),
  eval_sval ctx sv = Some v.
Proof.
  unfold fst_lhsv_imp; case lhsv as [|? [|]]; wlp_simplify.
  revert HEVAL; autodestruct.
Qed.
Global Opaque fst_lhsv_imp.

(** Simplify a symbolic value before assignment to a register *)

Definition simplify (rsv: root_op) (lsv: list sval) (lhsv: list_sval) (sm: smem): ?? sval :=
  match rsv with
  | Rop op =>
      match is_move_operation op lsv with
      | Some arg => fst_lhsv_imp lhsv
      | None =>
        match rewrite_ops RRULES op lsv with
        | Some fsv => fsval_proj fsv
        | None => hSop op lhsv
        end
      end
  | Rload _ chunk addr aaddr =>
      hrewrite_load sm NOTRAP chunk addr lhsv aaddr
  end.

Lemma simplify_spec ctx rsv lsv lhsv sm
  (STRICT: ctx_strict_reflect (ctx_bc ctx) strict)
  (LSV: list_sval_equiv_list ctx lhsv lsv):
  let sv0 := root_apply rsv lhsv sm in
  WHEN simplify rsv lsv lhsv sm ~> sv1 THEN forall
  (* The simplification only has to be correct if the original expression evaluates without error. *)
  (SEVAL: eval_sval ctx sv0 <> None),
  sval_equiv ctx sv0 sv1.
Proof.
  intros sv0 sv1.
  destruct rsv as [op|trap chunk addr].
  - unfold simplify.
    destruct is_move_operation eqn:HMOVE.
    + intros MV. subst sv0. simpl.
      apply is_move_operation_correct in HMOVE as [? ?]. subst.
      rewrite LSV. simpl.
      autodestruct. intros SEVAL _.
      eapply fst_lhsv_imp_correct in MV. rewrite LSV in MV. simpl in MV. rewrite SEVAL in MV.
      symmetry. apply MV; reflexivity.
    + case rewrite_ops as [rew|] eqn:REW.
      * intros PROJ. apply fsval_proj_correct in PROJ as <-.
        simpl. autodestruct. intros.
        apply list_sval_equiv_list_iff_lmap_sv in LSV as [lhsv' [? LSV']].
        eapply rewrite_ops_correct in REW. rewrite <-REW. reflexivity.
        eassumption. reflexivity. rewrite <- LSV'. assumption.
      * intros H _. apply hSop_correct in H. symmetry. eapply H. reflexivity.
  - unfold simplify.
    intros REW SEVAL. apply hrewrite_load_correct in REW. symmetry.
    rewrite REW; revert SEVAL; simpl; repeat autodestruct.
Qed.
Global Opaque simplify.

End Rewriting.

Definition hrs_add_okval (m : se_mode) (hrs : ristate) (osv : option sval) : ?? list sval :=
  match osv with
  | Some sv =>
      if se_ok_check m then (
         DO bin <~ lsv_in_imp sv (ok_rsval hrs);;
         if bin then RET (ok_rsval hrs)
         else FAILWITH "hrs_rsv_set: adding potential failure"
      ) else
         RET (sv :: ok_rsval hrs)
  | None => RET (ok_rsval hrs)
  end.

Lemma hrs_add_okval_correct ctx m hrs osv:
  WHEN hrs_add_okval m hrs osv ~> ok' THEN
  let P := alive_sval ctx in
  Forall P ok' <-> Forall P (ok_rsval hrs) /\ if_Some osv P.
Proof.
  unfold hrs_add_okval; repeat autodestruct; intros; simpl.
  - wlp_simplify. eapply Forall_forall in H; eauto.
  - wlp_seq. rewrite Forall_cons_iff. tauto.
  - wlp_simplify.
Qed.

Lemma hrs_add_okval_ok_check ctx m hrs sv
  (SI: se_ok_check m = true):
  WHEN hrs_add_okval m hrs sv ~> ok' THEN
  let P := alive_sval ctx in
  Forall P (ok_rsval hrs) -> Forall P ok'.
Proof.
  unfold hrs_add_okval; rewrite SI; simpl; wlp_simplify.
Qed.
Global Opaque hrs_add_okval.


(* NOTE: We use [root_happly] when we need a simplification that preserves errors and [simplify]
         when we are only interested in the case where the expression evaluates without errors.
         With the current rewriting rules, this results in two calls to [hrewrite_load] that could be factorized. *)
Definition hrs_rsv_ok_clauses (m : se_mode) (rsv : root_op) (args : list_sval) (emem : smem): ?? option sval :=
  if may_trap (se_abs_strict m) rsv (lsv_length args)
  then DO sv <~ root_happly (se_abs_strict m) rsv args emem;;
       RET (Some sv)
  else RET None.

Lemma hrs_rsv_ok_clauses_correct ctx m rsv args emem
  (CTXM:  ctx_se_mode m ctx)
  (EMEM:  alive (eval_smem ctx emem))
  (EARGS: alive (eval_list_sval ctx args)):
  WHEN hrs_rsv_ok_clauses m rsv args emem ~> osv THEN
  alive (eval_sval ctx (root_apply rsv args emem)) <-> if_Some osv (alive_sval ctx).
Proof.
  unfold hrs_rsv_ok_clauses.
  case may_trap eqn:MT; wlp_seq.
  - intros sv SV; simpl; unfold alive_sval.
    eapply root_happly_spec in SV as ->; auto.
    reflexivity.
  - split; [simpl; trivial | intros _].
    case eval_list_sval eqn:EARGS_eq in EARGS. 2:congruence.
    eapply may_trap_correct; eauto.
    apply eval_list_sval_length in EARGS_eq; congruence.
Qed.
Global Opaque hrs_rsv_ok_clauses.


Definition hrs_rsv_set (m : se_mode) r (lir: list ireg) rsv (hrs hrs_old: ristate): ?? ristate :=
  DO lsv_hlsv <~ hrs_lir_get hrs hrs_old lir;;
  let (lsv, hlsv) := lsv_hlsv  in
  let emem := ris_smem hrs_old in
  DO osv  <~ hrs_rsv_ok_clauses m rsv hlsv emem;;
  DO ok'  <~ hrs_add_okval m hrs osv;;
  DO simp <~ simplify (se_abs_strict m) rsv lsv hlsv emem;;
  RET (ris_sreg_ok_set hrs (red_PTree_set r simp hrs) ok').

Lemma ris_set_reg_refines ctx hrs sis r sv0 sv1 rok
  (RR:    ris_refines_ok ctx hrs sis)
  (EQV:   alive (eval_sval ctx sv1) -> sval_equiv ctx sv1 sv0)
  (OK_EQ: Forall (alive_sval ctx) rok <->
          Forall (alive_sval ctx) (ok_rsval hrs) /\ alive_sval ctx sv1):
  let hrs1 := ris_sreg_ok_set hrs (red_PTree_set r sv0 hrs) rok in
  let sis1 := set_sreg r sv1 sis in
  ris_refines ctx hrs1 sis1.
Proof.
  apply ris_refines_intro; [|intros ROK SOK; constructor].
  - transitivity (Forall (alive_sval ctx) rok).
    + rewrite ok_set_sreg, OK_EQ by apply RR; unfold alive_sval.
      intuition (apply RR).
    + apply ROK in RR as [?]; split; [constructor|intros [?]]; tauto.
  - apply RR.
  - intro r0; rewrite ris_sreg_set_access, red_PTree_set_refines_none by auto; simpl.
    case Pos.eq_dec as [|]. 2:apply RR.
    split; discriminate 1.
  - intro r0; rewrite ris_sreg_set_access.
    erewrite eval_osv_morph by apply red_PTree_set_spec; simpl.
    case Pos.eq_dec as [|]. 2:apply RR. simpl.
    symmetry; apply EQV.
    apply ok_set_sreg in SOK as [_ OK]; exact OK.
Qed.

Lemma hrs_rsv_set_spec m ctx sis0 sis1 r args rop hrs hrs_old
  (CTXM:  ctx_se_mode m ctx)
  (RR0: ris_refines_ok ctx hrs_old sis0)
  (RR1: ris_refines_ok ctx hrs     sis1):
  WHEN hrs_rsv_set m r args rop hrs hrs_old ~> hrs' THEN
  exists sis2,
    sis_set_rop r rop args sis0 sis1 = Some sis2 /\
    ris_refines ctx hrs' sis2.
Proof.
  unfold hrs_rsv_set, sis_set_rop.
  wlp_seq; intros (lsv, hlsv) GET.
  wlp_seq; intros ok_cs CS s_ok_cs ACS simp SMP.
  apply hrs_lir_get_spec in GET as GET'; ecase GET' as (lsv' & GET_lmap & EQ_lsv);
    eauto; rewrite GET_lmap; simpl in EQ_lsv; clear GET'.
  assert (REQ: eval_sval ctx (root_apply rop hlsv (ris_smem hrs_old))
             = eval_sval ctx (root_apply rop lsv' (sis_smem sis0)))
    by (apply root_apply_morph_equiv; [exact EQ_lsv|apply RR0]).
  eexists; split. reflexivity.
  eapply hrs_rsv_ok_clauses_correct in CS; eauto; cycle 1.
    { apply RR0. }
    { rewrite EQ_lsv; eapply sis_get_ireg_lmap_nofail; eauto; eauto using SOK. }
  eapply hrs_add_okval_correct in ACS; simpl in ACS; erewrite <- CS in ACS.
  apply ris_set_reg_refines; auto.
  - rewrite <- REQ; intro.
    apply hrs_lir_get_equiv_list in GET.
    eapply simplify_spec in SMP as <-; eauto.
  - rewrite ACS, REQ; reflexivity.
Qed.


Lemma hrs_rsv_set_rel m dest args rop hrs hrs_old:
  WHEN hrs_rsv_set m dest args rop hrs hrs_old ~> rst THEN
  ris_rel m hrs rst.
Proof.
  wlp_simplify; constructor; simpl; try reflexivity; [|intro SI]; intro ctx;
  intros [?]; constructor; simpl in *; try assumption.
  - eapply hrs_add_okval_correct in Hexta1; apply Hexta1 in OK_RREG; tauto.
  - eapply hrs_add_okval_ok_check in Hexta1; auto.
Qed.

Lemma hrs_rsv_set_okpreserv m dest args rop hrs hrs_old:
  WHEN hrs_rsv_set m dest args rop hrs hrs_old ~> rst THEN
  forall ctx,
  ris_ok ctx rst -> ris_ok ctx hrs.
Proof.
  intros rst H. apply hrs_rsv_set_rel in H. apply H.
Qed.
Global Opaque hrs_rsv_set.

(* Case when [hrs=hrs_old] *)

Definition hrs_rsv_set_input m r (args: list reg) rsv hrs :=
  hrs_rsv_set m r (ir_input_of args) rsv hrs hrs.

Lemma sis_set_rop_input r rop lr sis:
  sis_set_rop r rop (ir_input_of lr) sis sis
  = SOME args <- lmap_sv sis lr IN
    Some (set_sreg r (root_apply rop args (sis_smem sis)) sis).
Proof.
  unfold sis_set_rop.
  replace (lmap_sv (sis_get_ireg sis sis) (ir_input_of lr)) with (lmap_sv sis lr). reflexivity.
  induction lr.
  - reflexivity.
  - simpl; rewrite IHlr; reflexivity.
Qed.

Lemma hrs_rsv_set_input_spec m ctx sis r lr rsv hrs
  (CTXM:  ctx_se_mode m ctx)
  (RR: ris_refines_ok ctx hrs sis):
  WHEN hrs_rsv_set_input m r lr rsv hrs ~> hrs' THEN
  exists args,
    lmap_sv sis lr = Some args /\
    ris_refines ctx hrs' (set_sreg r (root_apply rsv args (sis_smem sis)) sis).
Proof.
  intros hrs' SET.
  eapply hrs_rsv_set_spec in SET as (sv & ROP & RR'); eauto.
  rewrite sis_set_rop_input in ROP; revert ROP.
  autodestruct; intros _; injection 1 as <-; eauto.
Qed.

Lemma hrs_rsv_set_input_okpreserv m dest args rop hrs:
  WHEN hrs_rsv_set_input m dest args rop hrs ~> rst THEN
  forall ctx,
  ris_ok ctx rst -> ris_ok ctx hrs.
Proof.
  apply hrs_rsv_set_okpreserv.
Qed.

Global Opaque hrs_rsv_set_input.

End SymbolicCommon.
