Require Import Coqlib Maps Integers Smallstep OptionMonad.
Require Import Op AST BTL_Invariants BTL Values Registers Memory Globalenvs Linking.
Require Import ValueDomain ValueAOp BTL_ValueAnalysis.
Require ValueAnalysis.

Module Type BTL_VAannotConfig.
  (* If this option is set, the pass will annotate Vtop on non-reachable points
     and instead of non-pointer aval (there is an error if the program reach those addresses). *)
  Parameter conservative_bot : bool.
End BTL_VAannotConfig.

Module BTL_VAannot (C : BTL_VAannotConfig).

Definition c_Vbot : aval :=
  if C.conservative_bot then Vtop else Vbot.

Definition c_aaddr (a : aval) : aval :=
  if C.conservative_bot
  then match a with
       | Vbot | I _ | L _ | F _ | FS _ => Vtop
       | Uns p _ | Sgn p _ | Ptr p | Ifptr p =>
           match p with
           | Pbot => Vtop
           | _ => a
           end
       end
  else a.

Lemma c_addr_correct bc v (a: aval):
  vmatch bc v a ->
  vmatch bc v (c_aaddr a).
Proof.
  unfold c_aaddr; repeat autodestruct; eauto using vmatch_top.
Qed.


Fixpoint transf_iblock_bot (ib : iblock) :=
  match ib with
  | BF _ _ | Bnop _ | Bop _ _ _ _ => ib
  | Bload trap chunk addr args _ dst iinfo =>
      Bload trap chunk addr args c_Vbot dst iinfo
  | Bstore chunk addr args sinfo src iinfo =>
      Bstore chunk addr args (store_info_set_aaddr sinfo c_Vbot) src iinfo
  | Bseq b1 b2 =>
      Bseq (transf_iblock_bot b1) (transf_iblock_bot b2)
  | Bcond cond args ifso ifnot iinfo =>
      Bcond cond args (transf_iblock_bot ifso) (transf_iblock_bot ifnot) iinfo
  end.

Fixpoint transf_iblock (rm: romem) (ib : iblock) (ae: aenv) (am: amem) {struct ib}:
  iblock * VA.t :=
  match ib with
  | BF fi iinfo =>
      (ib, VA.Bot)
  | Bnop iinfo =>
      (ib, VA.State ae am)
  | Bop op args res iinfo =>
      let a := eval_static_operation op (aregs ae args) in
      (ib, VA.State (AE.set res a ae) am)
  | Bload trap chunk addr args _ dst iinfo =>
      let aaddr := eval_static_addressing addr (aregs ae args) in
      let v     := match trap with
                   | TRAP   => loadv chunk rm am aaddr
                   | NOTRAP => Vtop
                   end
      in
      (Bload trap chunk addr args (c_aaddr aaddr) dst iinfo,
       VA.State (AE.set dst v ae) am)
  | Bstore chunk addr args sinfo src iinfo =>
      let aaddr := eval_static_addressing addr (aregs ae args) in
      let am'   := storev chunk am aaddr (areg ae src)         in
      (Bstore chunk addr args (store_info_set_aaddr sinfo (c_aaddr aaddr)) src iinfo,
       VA.State ae am')
  | Bseq b1 b2 =>
      let (b1', s1) := transf_iblock rm b1 ae am in
      match s1 with
      | VA.State ae' am' =>
          let (b2', s2) := transf_iblock rm b2 ae' am' in
          (Bseq b1' b2', s2)
      | VA.Bot =>
          (Bseq b1' (transf_iblock_bot b2), VA.Bot)
      end
  | Bcond cond args ifso ifnot iinfo =>
      let (ifso',  s1) :=
        transf_iblock rm ifso  (transfer_condition                   cond  args ae) am in
      let (ifnot', s2) :=
        transf_iblock rm ifnot (transfer_condition (negate_condition cond) args ae) am in
      (Bcond cond args ifso' ifnot' iinfo,
       VA.lub s1 s2)
       (* On a superblock, one of the branches should leave the superblock, so s1 or s2 is VBot. *)
  end.

Definition transf_function (rm : romem) (f : function) : BTL.function :=
  let an    := analyze rm f in
  let code' := PTree.map (fun i ib =>
                    let ib' := match an !! i with
                               | VA.State ae am => fst (transf_iblock rm (entry ib) ae am)
                               | VA.Bot => transf_iblock_bot (entry ib)
                               end in
                    mk_ibinfo ib' (binfo ib))
                  (fn_code f) in
  BTL.mkfunction (fn_sig f) (fn_params f) (fn_stacksize f) code' (fn_entrypoint f) (fn_gm f) (fn_info f).

Definition transf_fundef (rm : romem) (f : fundef) : fundef :=
  AST.transf_fundef (transf_function rm) f.

Definition transf_program (p : program) : BTL.program :=
  AST.transform_program (transf_fundef (ValueAnalysis.romem_for p)) p.


Definition match_prog (p tp: program) :=
  match_program (fun cu f tf => tf = transf_fundef (ValueAnalysis.romem_for cu) f) eq p tp.

Lemma transf_program_match p:
  match_prog p (transf_program p).
Proof.
  apply match_transform_program_contextual; reflexivity.
Qed.


(** Soundness *)

Section iblock_istep.

Variable bc: block_classification.
Variable ge0 ge1: genv.
Hypothesis GENV : genv_match bc ge0.
Variable sp: block.
Hypothesis STACK: bc sp = BCstack.
Hypothesis PRESERVED: forall s : ident, Genv.find_symbol ge1 s = Genv.find_symbol ge0 s.

Definition amatch_state e m (astate : VA.t) :=
  match astate with
  | VA.Bot => False
  | VA.State ae am => ematch bc e ae /\ mmatch bc m am
  end.

Lemma amatch_state_ge e m a a':
  amatch_state e m a ->
  VA.ge a' a ->
  amatch_state e m a'.
Proof.
  unfold amatch_state, VA.ge; destruct a, a'; intuition (eauto using ematch_ge).
Qed.

Lemma transf_iblock_simu rm ib ae am e m e' m' ofi
    (EMATCH : ematch bc e ae)
    (MMATCH : mmatch bc m am)
    (ROMATCH : romatch bc m rm)
    (STEP : iblock_istep ge0 (Vptr sp Ptrofs.zero) None e m ib e' m' ofi):
    let (ib', s') := transf_iblock rm ib ae am in
    (ofi = None -> amatch_state e' m' s') /\
    iblock_istep ge1 (Vptr sp Ptrofs.zero) (Some bc) e m ib' e' m' ofi.
Proof.
  revert ae am EMATCH MMATCH ROMATCH; induction STEP; simpl; intros. simpl.
  - (* exec_final *)
    split. discriminate 1. constructor.
  - (* exec_nop *)
    split; auto. constructor.
 - (* exec_op *)
   split.
   + split; auto.
     apply ematch_update; auto.
     apply (eval_static_operation_sound bc ge0 GENV sp STACK op (rs ## args) m v (aregs ae args));
     eauto with va.
   + constructor. erewrite eval_operation_preserved; eauto.
 - (* exec_load *)
   inversion LOAD as [? ? AEVAL|].
   + (* normal load *)
     eapply eval_static_addressing_sound in AEVAL as AMATCH; eauto with va.
     split.
     * split; auto.
       apply ematch_update; auto.
       case trap; [|eapply vmatch_top]; eapply loadv_sound; eauto.
     * constructor.
       case LOAD as [|]; [econstructor 1 | econstructor 2]; eauto;
         try setoid_rewrite eval_addressing_preserved; eauto.
       apply c_addr_correct; assumption.
   + (* default load *)
     subst; split.
     * split; auto.
       apply ematch_update; auto.
       constructor.
     * constructor; apply BTL.has_loaded_default; auto.
       intros a AEVAL _.
       erewrite eval_addressing_preserved in AEVAL; eauto.
       eapply eval_static_addressing_sound in AEVAL as AMATCH; eauto with va.
       apply LOAD0; simpl; auto.
 - (* exec_store *)
   eapply eval_static_addressing_sound in EVAL as AMATCH; eauto with va.
   split.
   + split; auto.
     eapply storev_sound; eauto.
   + econstructor.
     1:erewrite eval_addressing_preserved.
     all:eauto with va.
     apply c_addr_correct; exact AMATCH.
 - (* exec_seq_stop *)
   specialize (IHSTEP ae am).
   destruct (transf_iblock _ b1) as [b1' [|ae' am']]; [|case (transf_iblock _ b2) as [b2' s2]];
   (split; [discriminate 1|constructor; apply IHSTEP; auto]).
 - (* exec_seq_continue *)
   specialize (IHSTEP1 ae am).
   destruct (transf_iblock _ b1) as [b1' [|ae1 am1]].
     { exfalso; apply IHSTEP1; auto. }
   case IHSTEP1 as ((? & ?) & ?); auto.
   specialize (IHSTEP2 ae1 am1).
   destruct (transf_iblock _ b2) as [b2' s2].
   case IHSTEP2 as (? & ?); eauto using iblock_istep_romatch.
   split; auto.
   econstructor; eauto.
 - (* exec_cond *)
   destruct (transf_iblock _ ifso)  as [ifso'  s1] eqn:eq_so,
            (transf_iblock _ ifnot) as [ifnot' s2] eqn:eq_not.
   assert (EVAL_NOT : eval_condition (negate_condition cond) (rs ## args) m = Some (negb b))
    by (rewrite eval_negate_condition, EVAL; reflexivity).
   destruct b;
     epose proof (IH := IHSTEP _ am); rewrite ?eq_so, ?eq_not in IH;
     (case IH as (? & ?); auto; [eapply transfer_condition_sound; eauto|]);
     (split;
        [intro; eapply amatch_state_ge; eauto using VA.ge_lub_left, VA.ge_lub_right
        |econstructor; eauto ]).
Qed.

End iblock_istep.

Section program.

Variable prog: program.
Variable tprog: program.

Hypothesis TRANSL: match_prog prog tprog.

Let ge0 := Genv.globalenv prog.
Let ge1 := Genv.globalenv tprog.
Notation rmf := ValueAnalysis.romem_for.

Lemma ge_find_symbol (s : ident):
  Genv.find_symbol ge1 s = Genv.find_symbol ge0 s.
Proof.
  apply (Genv.find_symbol_match TRANSL).
Qed.

Local Hint Resolve ge_find_symbol : core.

Lemma ge_find_function (fp : reg + ident) rs f:
  find_function ge0 fp rs = Some f ->
  exists cunit,
    find_function ge1 fp rs = Some (transf_fundef (ValueAnalysis.romem_for cunit) f) /\
    linkorder cunit prog.
Proof.
  intro F0; apply (find_function_match TRANSL) in F0 as (cu & ? & ? & ? & ?); subst; eauto.
Qed.


Inductive match_stackframe: stackframe -> stackframe -> Prop :=
  | match_stackframe_intro 
      sp res f pc rs cu
      (LINK: linkorder cu prog)
      : match_stackframe (BTL.Stackframe res f sp pc rs)
                         (BTL.Stackframe res (transf_function (rmf cu) f) sp pc rs).

Definition match_stackframes : list stackframe -> list stackframe -> Prop :=
  list_forall2 match_stackframe.

Inductive match_states0 rm : state -> state -> Prop :=
  | match_states_norm stk stk' f pc sp rs m _bc bc
      (STACK: match_stackframes stk stk')
      : match_states0 rm (State stk f sp pc rs m _bc)
                         (State stk' (transf_function rm f) sp pc rs m bc)
  | match_states_call stk stk' f args m _bc bc
      (STACK: match_stackframes stk stk')
      : match_states0 rm (Callstate stk f args m _bc)
                         (Callstate stk' (transf_fundef rm f) args m bc)
  | match_states_return stk stk' v m _bc bc
      (STACK: match_stackframes stk stk')
      : match_states0 rm (Returnstate stk  v m _bc)
                         (Returnstate stk' v m  bc)
   .

Definition state_bc (s : state) : block_classification :=
  match s with
  | State _ _ _ _ _ _ bc
  | Callstate _ _ _ _ bc
  | Returnstate _ _ _ bc
      => bc
  end.

Inductive match_states (st st' : state) : Prop :=
  match_states_intro
    (cu : program) (LINK: linkorder cu prog)
    (SOUND: sound_state_base cu ge0 (state_bc st') st)
    (MATCH: match_states0 (rmf cu) st st').

Lemma transfer_step st0 t st1 st0'
  (STEP  : step false ge0 st0 t st1)
  (MATCH : match_states st0 st0')
  (OSOUND: sound_state prog st0):
  exists st1',
    step true ge1 st0' t st1' /\ match_states st1 st1'.
Proof.
  eapply sound_step in OSOUND; eauto.
  inversion STEP; try rename bc into _bc; try rename bc' into _bc'; subst; inv MATCH; inv MATCH0; simpl in *.
  2,3:exploit sound_step_base; eauto; intros [bc' SOUND'].
  - (* exec_iblock *)
    case STEP0 as (rs' & m' & fin & ISTEP & FIN).
    inversion SOUND; subst.
    cut (exists st1',
          final_step ge1 stk' (transf_function (rmf cu) f) (Vptr sp0 Ptrofs.zero) rs' m' bc fin t st1' /\
          match_states st1 st1'). {
      intros (st1' & FIN' & ?); exists st1'; repeat split; auto.
      eapply exec_iblock.
      - etransitivity. apply PTree.gmap.
        rewrite PC, AN; simpl; reflexivity.
      - simpl.
        exists rs', m', fin; split; auto.
        eapply transf_iblock_simu with (ge0 := ge0) (ge1 := ge1) in ISTEP as ISTEP'; eauto.
        destruct transf_iblock. apply ISTEP'.
    }
    exploit sound_step_base_exec_iblock; eauto; intros SOUND'.
    inversion FIN; try rename bc' into _bc'; subst; simpl in *.
    2,5:case SOUND' as [bc' SOUND'].
    1,2,6:solve [eexists; split;
                  [econstructor;eauto
                  |exists cu; [auto | simpl;apply SOUND' | constructor;auto]]].
    + (* exec_Bbuiltin *)
      eexists; split; [|exists cu; auto].
      * econstructor; eauto.
        -- eapply Events.eval_builtin_args_preserved; eauto.
        -- eapply Events.external_call_symbols_preserved; eauto.
           eapply (Genv.senv_match TRANSL).
      * exact SOUND'.
      * constructor; auto.
    (* When executing calls, [ge_find_function] gives a new [cu'] for the callee.
       We recover the soundness with respect to the analysis with [cu'] using [OSOUND]. *)
    + (* exec_Bcall *)
      apply ge_find_function in H as (cu' & ? & ?).
      clear SOUND'; apply sound_state_inv with (cunit := cu') in OSOUND as [bc' SOUND']; auto.
      eexists; split; [|exists cu'; auto].
      * econstructor. eassumption.
        case fd; reflexivity.
      * exact SOUND'.
      * repeat (constructor; auto).
    + (* exec_Btailcall *)
      apply ge_find_function in H as (cu' & ? & ?).
      clear SOUND'; apply sound_state_inv with (cunit := cu') in OSOUND as [bc' SOUND']; auto.
      eexists; split; [|exists cu'; auto].
      * econstructor; eauto.
        case fd; reflexivity.
      * exact SOUND'.
      * constructor; auto.
  
  - (* exec_function_internal *)
    eexists; split; [|exists cu; auto].
    + eapply exec_function_internal with (f:=transf_function (rmf cu) f); simpl; eauto.
    + exact SOUND'.
    + constructor; auto.

  - (* exec_function_external *)
    eexists; split; [|exists cu; auto].
    + eapply exec_function_external; auto.
      eapply Events.external_call_symbols_preserved; eauto.
      eapply (Genv.senv_match TRANSL).
    + exact SOUND'.
    + constructor; auto.

  - (* exec_return *)
    clear cu LINK SOUND.
    inversion STACK as [|? ? ? ? STACK0 STACK1].
    inv STACK0.
    apply sound_state_inv with (cunit := cu) in OSOUND as [bc' SOUND']; auto.
    eexists; split; [|exists cu; auto].
    + eapply exec_return.
    + exact SOUND'.
    + constructor; auto.
Qed.


Theorem transf_program_correct:
  forward_simulation (sem false prog) (sem true tprog).
Proof.
  eapply forward_simulation_step
    with (fun s s' => sound_state prog s /\ match_states s s');
    simpl.
  - apply (Genv.senv_match TRANSL).
  - intros ? INI.
    apply sound_initial in INI as SOUND.
    inv INI.
    exploit (Genv.find_funct_ptr_match TRANSL); eauto; intros (cu & ? & ? & ? & ?); subst.
    exploit sound_state_inv; eauto; intros [bc SOUND_b].
    eexists; repeat split.
    + econstructor.
      * eapply (Genv.init_mem_match TRANSL); eauto.
      * rewrite ge_find_symbol, (match_program_gen_main _ _ _ _ _ TRANSL). eassumption.
      * eassumption.
      * destruct f; simpl in *; assumption.
    + intros; exploit sound_state_inv; eauto.
    + exists cu; eauto. do 2 constructor.
  - intros ? ? ? [_ MATCH].
    inversion 1; subst; inv MATCH; inv MATCH0; inv STACK.
    constructor.
  - intuition.
    exploit transfer_step; eauto; intros (s2' & ? & ?).
    exists s2'; intuition.
    exploit sound_step; eauto.
Qed.

End program.

End BTL_VAannot.
