Require Import Maps.
Require Import AST.
Require Import RTL BTL_Invariants.
Require Export BTLmatchRTL.

Require Import Errors.

Module Type RTLtoBTL_TranslationOracle.

  (** External oracle that can be configured according to the translation mode *)
  Parameter rtl2btl: RTL.function -> BTL.code * node * function_info * (PTree.t node).

End RTLtoBTL_TranslationOracle.

Module RTLtoBTL_Translation (B: RTLtoBTL_TranslationOracle).

Export B.

Local Open Scope error_monad_scope.

Definition transf_function (f: RTL.function) : res BTL.function :=
  let (tctefi, dupmap) := rtl2btl f in
  let (tcte, fi) := tctefi in
  let (tc, te) := tcte in
  let f' := BTL.mkfunction (RTL.fn_sig f) (RTL.fn_params f) (RTL.fn_stacksize f) tc te gm_empty fi in
  do u <- verify_function dupmap f' f true;
  OK f'.

Definition transf_fundef (f: RTL.fundef) : res fundef :=
  transf_partial_fundef transf_function f.

Definition transf_program (p: RTL.program) : res program :=
  transform_partial_program transf_fundef p.

End RTLtoBTL_Translation.
