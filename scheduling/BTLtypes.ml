open Registers

type inst_info = {
  mutable inumb : int;
  mutable opt_info : bool option;
  mutable visited : bool;
  mutable liveins : Regset.t;
}

(** This struct is a ghost field attached to each BTL instruction
 * inumb: int field used for remembering the original numbering of CFG
 * opt_info: option bool used for various tests:
 *  - On Bcond, stores the prediction information
 *  - On Bload, stores the trapping information (if Some false, the load was
 *  initially non-trapping, and the opposite if some true)
 * visited: boolean used to mark nodes
 * liveins: set of lives registers
 *)

type block_info = {
  mutable bnumb : int;
  mutable visited : bool;
  mutable input_regs : Regset.t;
  mutable s_output_regs : Regset.t;
  mutable successors: Camlcoq.P.t list;
  mutable predecessors: Camlcoq.P.t list;
}
(** Struct attached to each BTL iblock_info type
 * bnumb: int representing the block id in the BTL CFG
 * visited:  boolean used to mark blocks
 * s_output_regs: set of output registers
 * typing: field transferring RTL typing information in BTL (used in regpres
 * scheduling)
 *)

type function_info = {
  typing : RTLtyping.regenv;
  bb_mode : bool;
}
(** Struct attached to each BTL function
 *)

module OrdIS = Set.Make (Int)
let ($?) i is = OrdIS.mem i is
let ($+) i is = OrdIS.add i is
let ($|$) is1 is2 = OrdIS.union is1 is2
let ($&$) is1 is2 = OrdIS.inter is1 is2
let (~$) i = OrdIS.singleton i
let (!$-) is = OrdIS.min_elt is
let (!$+) is = OrdIS.max_elt is

type inv_t = Gluing | History
