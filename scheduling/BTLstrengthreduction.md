# Vers une strength-reduction certifiée dans BTL (pour RISCV 64 bits)...

Le sujet étant assez complexe - on se limite à étudier le pb sur RISCV 64 bits (sachant que linux pour RISCV n'est dispo que sur 64 bits). 
Moralement, gérer aussi le RISCV 32 bits ne sera qu'une adaptation à réaliser (mais qu'on peut laisser pour après la thèse de Léo).

## Calculs en 64 bits seulement

### Exemples de motivation (traités par GCC):

Considérons le code suivant:

    long sum(long *t, unsigned long a, unsigned long n) {
      unsigned long i;
      long s = 0;
      for (i=a; i <= n; i++) {
        s += t[i];
      }
      return s;
    }

Sur RISC-V 64 bits, gcc -O1 produit:

    sum:
            bgtu    a1,a2,.L4
            slli    a5,a1,3
            add     a5,a0,a5
            li      a0,0
    .L3:
            ld      a4,0(a5)
            add     a0,a0,a4
            addi    a1,a1,1
            addi    a5,a5,8
            bgeu    a2,a1,.L3
            ret
    .L4:
            li      a0,0
            ret

Autrement dit, GCC fait un rotate puis une strength-reduction de la multiplication par 8 en addition.

Sur cet exemple, en remplaçant les `unsigned` par des `signed`, on obtient:

    sum:
            bgt     a1,a2,.L4
            slli    a1,a1,3
            add     a5,a0,a1
            addi    a0,a0,8
            slli    a2,a2,3
            add     a3,a0,a2
            li      a0,0
    .L3:
            ld      a4,0(a5)
            add     a0,a0,a4
            addi    a5,a5,8
            bne     a5,a3,.L3
            ret
    .L4:
            li      a0,0
            ret

Autrement dit GCC réduit donc à la fois, la multiplication par 8 et le test d'arrêt (en éliminant la variable de boucle `i`).
En effet, comme on est sur des signés, le `i++` du source d'origine ne peut pas déborder et l'adresse dans `a3` reste `weak_valid`.
GCC arrive à adapter ce raisonnement si on remplace le `i++` par `i+=2`.

Cette strength-reduction du test semble très compliquée à adapter à CompCert. On se limite donc à la strength-reduction qui travaille sur des unsigned (et qui marchera aussi sur les signed).

### Vers une adaptation pour CompCert (exemple initial)

Après un loop-rotate et vers le rtl de fin, on arrive à:

    sum(x3, x2, x1) {
       10:  x4 = Olongconst(0L)
        9:  x5 = x2
        8:  if (x5 >lu x1) goto 1 else goto 7 (prediction: fallthrough)
        7:  x9 = x5 <<l 3
        6:  x8 = x3 +l x9
        5:  x7 = int64[x8 + 0]
        4:  x4 = x4 +l x7
        3:  x5 = x5 +l 1
        2:  if (x5 >lu x1) goto 1 else goto 7 (prediction: fallthrough)
        1:  return x4
    }

On pourrait ensuite le transformer en:

    sum(x3, x2, x1) {
           x4 = Olongconst(0L)
           x5 = x2
           if (x5 >lu x1) goto End else goto BB1
       BB1: // invariant x5=x5 | x1=x1 | x3=x3 | x4 = x4
           x9 = x5 <<l 3
           x8 = x3 +l x9
        Loop: // invariant x8 = x3 +l x5 *l 8 | x1=x1 | x5=x5 | x4=x4
           x7 = int64[x8 + 0]
           x4 = x4 +l x7
           x5 = x5 +l 1
           x8 = x8 +l 8
           if (x5 >lu x1) goto End else goto Loop
        End:  // invariant x4=x4
           return x4
    }

- Pour la première entrée dans Loop, on aurait besoin de prouver:

        x3 +l (x5 <<l 3) = x3 +l x5 *l 8
        
- Pour le retour dans Loop, on aurait besoin de prouver:
        
        x3 +l x5 *l 8 +l 8 = x3 +l (x5 +l 1) *l 8

### Réécritures dans l'exécution symbolique:


On pourrait coder qqchose comme la tactique `ring_simplify` de Coq. Mais pour la strength-reduction ci-dessus, il me semble inutile de gérer des polynômes quelconques: on pourrait se limiter à des polynômes de degré 1 - c'est-à-dire des termes affines (ça généralise l'exemple précédent à des cas où on a plusieurs indices de boucle).

Autrement dit des termes de la forme `b + \sum_i a_i * x_i` où `b` et `a_i` sont des constantes entières 64 bits et `x_i` des noms de registres (représentant des long 64 bits).

Concrètement, on peut normaliser ces termes affines sous la forme d'une liste strictement ordonnée sur les noms de registres. 

    Oaddl (Olongconst b) (Oaddl (Omull (Olongconst a1) (Input x1)) (Oaddl (Omull (Olongconst a2) (Input x2)) ... (Omull (Olongconst an) (Input xn))))
    
Il faut définir ensuite essentiellemnt deux opérations (à coût linéaire sur les termes affines):

 - l'addition de deux termes affines
 
 - et la multiplication d'un terme affine par un immédiat long. 
 
Par exemple, le décalage à gauche d'un terme affine par un immédiat `n` est directement réécrit comme une multiplication du terme affine par `2**n`. 

## Promotion des 32 bits en 64 bits

Considérons le code suivant:

    int sum(int *t, int a, int n) {
      int i;
      int s = 0;
      for (i=a; i < n; i++) {
        s += t[i];
      }
      return s;
    }


Sur RISC-V 64 bits, le rtl.0 (par défaut) de CompCert est:

    sum(x3, x2, x1) {
       13:  x4 = Ointconst(0)
       12:  x5 = x2
       11:  nop
       10:  if (x5 <s x1) goto 9 else goto 2 (prediction: none)
        9:  x10 = long32signed(x5)
        8:  x9 = x10 <<l 2
        7:  x8 = x3 +l x9
        6:  x7 = int32[x8 + 0]
        5:  x4 = x4 + x7
        4:  x5 = x5 + 1
        3:  goto 10
        2:  x6 = x4
        1:  return x6
    }

On aimerait (naïvement) avoir une strength-reduction qui donne qqchose comme:

    sum(x3, x2, x1) {
        x4 = Ointconst(0)
        x5 = x2
        x10 = long32signed(x5)
        x9 = x10 <<l 2
        x8 = x3 +l x9
    Loop: // gluing invariant: x8 = x3 +l long32signed(x5) *l 4  | x1=x1 | x4=x4 | x5=x5
        if (x5 <s x1) goto Body else goto End
    Body: // gluing invariant: x8 = x3 +l long32signed(x5) *l 4  | x1=x1 | x4=x4 | x5=x5
        x7 = int32[x8 + 0]
        x4 = x4 + x7
        x5 = x5 + 1
        x8 = x8 +l 4
        goto Loop
    End: // x4=x4
        x6 = x4
        return x6
    }
    
Bon, mais ce n'est sans doute pas si simple, car il y a potentiellement un pb de débordement sur `x5` ici.
En effet la préservation de l'invariant demande de prouver:

    x3 +l long32signed(x5) *l 4 +l 4 = x3 +l long32signed(x5+1) *l 4

ce qui n'est vrai que si `x5+1` ne wrappe pas... 

Sur cet exemple très précis, on pourrait justifier que le débordement ne va pas se produire, parce qu'à l'entrée du `Body` on a `(x5 <s x1)` et que l'incrémentation n'est faite que de `1` (on a donc 
`(x5 <=s x1)` en sortie de `Body`).
Mais, c'est un raisonnement qu'on ne sait pas faire avec le vérificateur actuel! Ça demande une extension non-triviale (cf. ci-dessous).

Pour éviter ces problèmes, on pourrait faire une passe en amont qui
essaye de prouvoir les calculs 32 bits en 64 bits. Autrement dit
remonter, les `long32signed` en dehors des boucles.  L'intérêt d'avoir
une passe dédiée séparée, c'est que cette passe de promotion a un intérêt
en elle-même et ça simplifie (dans un premier temps) de considérer
indépendamment la promotion des 32 bits et la multiplication par des constantes.

On peut voir la promotion des 32 bits en elle-même comme une forme de
strength-reduction. Elle peut s'appuyer sur une analyse statique
d'intervalle pour donner des résultats précis (mais je ne suppose d'analyse dataflow d'intervalle ci-dessous)... 
Ci-dessous, on se place après un loop-rotate car ça simplifie à priori un peu le raisonnement (mais c'est peut-être pas complètement nécessaire ?)

### Strength-reduction du long2signed

Sur l'exemple précédent, après un loop-rotate et vers le rtl de fin, on arrive à:

    sum(x3, x2, x1) {
       11:  x4 = Ointconst(0)
       10:  x5 = x2
        9:  if (x5 >=s x1) goto 1 else goto 8 (prediction: fallthrough)
        8:  x10 = long32signed(x5)
        7:  x9 = x10 <<l 2
        6:  x8 = x3 +l x9
        5:  x7 = int32[x8 + 0]
        4:  x4 = x4 + x7
        3:  x5 = x5 + 1
        2:  if (x5 >=s x1) goto 1 else goto 8 (prediction: fallthrough)
        1:  return x4
    }

On aurait ensuite envie de le transformer en (avant d'appliquer la strength-reduction éliminant les décalages):

    sum(x3, x2, x1) {
           x4 = Ointconst(0)
           x5 = long32signed(x2)
           x1 = long32signed(x1)
           if (x5 >=ls x1) goto End else goto Loop
       Loop: // gluing invariant: x5=long32signed(x5) | x1=long32signed(x1) | x3=x3 | x4 = x4
           x9 = x5 <<l 2
           x8 = x3 +l x9
           x7 = int32[x8 + 0]
           x4 = x4 + x7
           x5 = x5 +l 1
           if (x5 >=ls x1) goto End else goto Loop
        End:  // gluing invariant: x4=x4
           return x4
    }
    
Il faut ici une réécriture des conditions `long32signed(x5) >=ls long32signed(x1)` en `x5 >=s x1`

Et, les invariants à montrer sont

- entrée dans Loop:

        long32signed(x2) = long32signed(x2)
        
- retour dans Loop:
    
        long32signed(x5) +l 1 = long32signed(x5 + 1)

Pour ça, on peut prendre une réécriture `long32signed(x5 + 1)` donne `long32signed(x5) +l 1`. Mais ça demande une garde permettant de borner `x5`.

#### Idée d'ajout de gardes dans le vérificateur BTL

Une idée est d'introduire en BTL un opérateur `guard(cond,op)`. 
Cet opérateur fait un "trap" (undefined behavior) si la `cond` est fausse, et calcule `op` sinon.
Au niveau de l'exécution symbolique, en plus d'un terme trappant écrit `Rguard cond` (donc sans paramètre `op`), on aurait un terme `Mayundef(cond,op)` qui remplace le trap par un `Vundef`.
Ainsi, une instruction BTL `r:=guard(cond,op)` serait traduite comme l'ajout d'un trap symbolique potentiel `Rguard cond` et d'une affectation symbolique `r:=Mayundef(cond,op)`.
Ça permettrait sur l'exemple d'avoir une règle de réécriture:

  `long32signed(Mayundef(x5 <s x1,x5) + 1)` donne `long32signed(Mayundef(x5 <s x1,x5)) +l 1`

L'exécution symbolique d'un `if cond S1 S2` ajouterait un `Rguard cond` dans la branche S1 et `Rguard dual-cond` dans la branche S2.
Eventuellement, pour éviter de les générer tout le temps, on pourrait faire ça uniquement dans les branches ont un tag particulier ?
Ci-dessous, pour simplifier, je suppose qu'on a un mode d'exécution par bloc: l'oracle de transformation indique pour chaque bloc si on doit générer le trap des conditions ou pas lors de l'exécution symbolique du bloc. Dans ce cas, ces trap de conditions ne sont générés que pour le bloc source.

Dans les invariants, on utilise aussi la notion `guard(cond,op)` (qui génère donc en fait un `Rguard` et un `Mayundef`).

#### Application à l'exemple initial (en enchaînant deux simulations) ####

En fait, il faut sans doute que les règles associées aux réécritures sur `guard` dépendent du contexte d'utilisation. 
Par exemple, sur l'exemple après loop-rotate, on appliquerait la génération des traps sur les conditions (au niveau source) et on aimerait alors vérifier le code cible suivant 
qui borne la variable `x5`.

    sum(x3, x2, x1) {
           x4 = Ointconst(0)
           x5 = x2
           if (x5 >=s x1) goto End else goto Loop
       Loop: 
           // gluing invariant: x5=guard(x5 <s x1, x5) | x1=x1 | x3=x3 | x4 = x4 | x5=x5
           x10 = long32signed(x5) 
           x9 = x10 <<l 2
           x8 = x3 +l x9
           x7 = int32[x8 + 0]
           x4 = x4 + x7
           x5 = guard(x5 <s x1, x5) + 1
           if (x5 >=s x1) goto End else goto Loop
        End:  // gluing invariant: x4=x4
           return x4
    }

Ici, pour la préservarion d'invariant, on aimerait avoir

- `long32signed(guard(x5 <s x1, x5)) = long32signed(x5)` 

- `guard(x5 <s x1, x5)+1 = x5+1`

- ...

Donc, pour montrer cette simulation, il faudrait donc que `r:=guard(cond,op)` soit traduite comme l'ajout d'un trap symbolique potentiel `Rguard cond` et une affectation symbolique `r:=op`.
**REM** C'est cette première simulation qui peut être généralisée avec une analyse data-flow d'intervalles...

En prenant maintenant le code ci-dessus comme code source, on pourrait montrer cette deuxième simulation:

    sum(x3, x2, x1) {
           x4 = Ointconst(0)
           x5 = long32signed(x2)
           x1 = long32signed(x1)
           if (x5 >=ls x1) goto End else goto Loop
       Loop:
           // gluing invariant: x5=long32signed(x5) | x1=long32signed(x1) | x3=x3 | x4 = x4
           x9 = x5 <<l 2
           x8 = x3 +l x9
           x7 = int32[x8 + 0]
           x4 = x4 + x7
           x5 = guard(x5 <ls x1, x5) +l 1
           if (x5 >=ls x1) goto End else goto Loop
        End:  // gluing invariant: x4=x4
           return x4
    }

Ici, pour la préservarion d'invariant, on aimerait avoir:

- `long32signed(x5) <ls long32signed(x1)` se réécrit en  `x5 <s x1`

-  `guard(x5 <s x1, long32signed(x5)) +l 1` se réécrit en `long32signed(guard(x5 <s x1, x5) + 1)`

- `long32signed(t) >=ls long32signed(x1)` se réécrit en  `t >=s x1`

Ça peut se faire avec l'interprétation du `guard` en `Mayundef` décrite plus haut... 


#### Elimination du `guard` dans les instructions BTL ####

Ça pourrait se faire dans la passe `BTL` vers `RTL` ?

## Autres mélanges 32/64 bits

Quelques exemples quand le mécanisme de promotion 32 -> 64 ne s'applique pas... 
**Ça ne semble pas primordial de gérer ces cas dans le cadre de la thèse de Léo.**
On sera déjà content si on réussit à faire le reste (voire uniquement la strength-reduction uniquement 64 bits) - on peut garder les autres idées en guise de "future works".

### Variations sur un exemple (sans tableaux - orthogonal en fait):

    int mul10(int x, int y, int n) {
      int i;
      int r = x;
      for (i=y; i < n; i++) {
        r += i * 10;
      }
      return r;
    }

Sur RISC-V 64 bits, le rtl.0 (par défaut) de CompCert est:

    mul10(x3, x2, x1) {
       13:  x4 = x3
       12:  x5 = x2
       11:  nop
       10:  if (x5 <s x1) goto 9 else goto 2 (prediction: none)
        9:  x8 = x5
        8:  x9 = x8 << 1
        7:  x10 = x8 << 3
        6:  x7 = x9 + x10
        5:  x4 = x4 + x7
        4:  x5 = x5 + 1
        3:  goto 10
        2:  x6 = x4
        1:  return x6
    }

On aimerait ici le transformer en qqchose comme:

    mul10(x3, x2, x1) {
           x4 = x3
           x5 = x2
           x8 = x5
           x9 = x8 << 1
           x10 = x8 << 3
           x7 = x9 + x10
           x11 = x7
    Loop: // gluing invariant x11=x5*10 | x4=x4 | x5 = x5
        if (x5 <s x1) goto Body else goto End
    Body: // gluing invariant x11=x5*10 | x4=x4 | x5 = x5
           x4 = x4 + x11
           x5 = x5 + 1
           x11 = x11 + 10
           goto Loop
    End :// gluing invariant x4=x4
           x6 = x4
           return x6
    }

### Interaction avec la sélection d'instruction

Déjà on remarque que le calcul `x7 = x8*10` est décomposé en:

      x9 = x8 << 1
      x10 = x8 << 3
      x7 = x9 + x10

(autrement dit, `x8*10=x8*2+x8*8`)

On peut se demander si c'est efficace et si on veut garder ça.

Si oui, il faudra que la strength-reduction travaille donc sur des combinainons de shifts par des constantes et d'additions.
Dans la suite de ces exemples, on applique une strength-reduction sur ce code.
Ça semble de toute façon intéressant pour traiter bien les tableaux (par exemple, matrices).

### Un mélange simple de long et int

Une variante de l'exemple précédent:

    long mul10(long x, int y, int n) {
        int i;
        long r = x;
        for (i=y; i < n; i++) {
            r += i * 10;
        }
        return r;
    }
  
Son code rtl.0  est:

    mul10(x3, x2, x1) {
       14:  x4 = x3
       13:  x5 = x2
       12:  nop
       11:  if (x5 <s x1) goto 10 else goto 2 (prediction: none)
       10:  x9 = x5
        9:  x10 = x9 << 1
        8:  x11 = x9 << 3
        7:  x8 = x10 + x11
        6:  x7 = long32signed(x8)
        5:  x4 = x4 +l x7
        4:  x5 = x5 + 1
        3:  goto 11
        2:  x6 = x4
        1:  return x6
    }

Le `long32signed` ne commutant de toute façon pas bien avec le reste, on aurait envie de réécrire ça en:

    mul10(x3, x2, x1) {
           x4 = x3
           x5 = x2
           x9 = x5
           x10 = x9 << 1
           x11 = x9 << 3
           x8 = x10 + x11
           x12 = x8
    Loop: // gluing invariant x12=x5*10 | x4=x4 | x5=x5
           if (x5 <s x1) goto Body else goto End
    Body:
           x7 = long32signed(x12)
           x4 = x4 +l x7
           x5 = x5 + 1
           x12 = x12 + 10
           goto Loop
    End:   x6 = x4
           return x6
    }

Autrement dit, ici, au niveau du vérificateur, on n'a pas besoin de faire de réécritures spécialies sur `long32signed`. La strength-reduction a lieu uniquement sur des `int` (il faudrait donc une variante des termes affines qui fonctionnent sur 32 bits).


### Un mélange plus complexe à justifier avec des troncatures

    int mul10(int x, long y, long n) {
        long i;
        int r = x;
        for (i=y; i < n; i++) {
            r += i * 10;
        }
        return r;
    }
  
Son code rtl.0 est:

    mul10(x3, x2, x1) {
       15:  x4 = x3
       14:  x5 = x2
       13:  nop
       12:  if (x5 <ls x1) goto 11 else goto 2 (prediction: none)
       11:  x8 = long32signed(x4)
       10:  x10 = x5
        9:  x11 = x10 <<l 1
        8:  x12 = x10 <<l 3
        7:  x9 = x11 +l x12
        6:  x7 = x8 +l x9
        5:  x4 = lowlong(x7)
        4:  x5 = x5 +l 1
        3:  goto 12
        2:  x6 = x4
        1:  return x6
    }

Idéalement, comme "lowlong" commute avec les opérations sur les "long", (en arithmétique modulo), on aurait envie d'écrire ça:

    mul10(x3, x2, x1) {
           x4 = x3
           x5 = x2
           x8 = long32signed(x4)
           x10 = x5
           x11 = x10 <<l 1
           x12 = x10 <<l 3
           x9 = x11 +l x12
    Loop:  // gluing invariant x9=x5 *l 10 | lowlong(x8)=x4 | x5=x5
           if (x5 <s x1) goto Body else goto End
    Body:
           x8 = x8 +l x9
           x5 = x5 +l 1
           x9 = x9 +l 10
           goto Loop
    End:   x6 = lowlong(x8)
           return x6
    }

**Problème** le gluing invariant ci-dessus `lowlong(x8)=x4` n'est pas syntaxiquement exprimable dans notre vérificateur (il faut une variable à gauche du "=").

En supposant qu'on sache gérer ça (ce qui demanderait une modification non-triviale du processus de vérification), on aurait à vérifier:

- 1ère entrée dans Loop:   `lowlong(long32signed(x3))=x3`

- retour dans Loop:

        lowlong(x8) = x4 => lowlong(x8 +l x5 *l 10)=lowlong(long32signed(x4) +l x5 *l 10)

C'est comment vérifier une telle implication par exécution symbolique qui n'est pas très clair... Je propose une alternative ci-dessous via les invariants d'histoire...

#### Appliquer une strength-reduction naïve en introduisant une variable "zombie".

On a l'idée d'introduire des variables auxiliaires en tant que variables "zombies" (ie. des morts qui vont être réveillés prochainement) initialisées à `Vundef`. Ensuite, on peut imaginer une passe certifiée (zombi\_wakeup) qui remplace ces `Vundef` par des expressions quelconques (mais qui n'ont pas le droit d'échouer). Le plus simple est d'ajouter une opération spéciale `zombie op` en BTL qui emballe n'importe quelle opération `op` -- pour trapper comme `op` mais en retournant `Vundef`. La passe zombi\_wakeup élimine donc juste `zombie`. La vérif de l'introduction des zombies peut se faire par exécution symbolique. Ci-dessous, `x17` est la variable zombie: elle est destinée à être l'équivalent du `x8` de la strength-reduction à la main. Elle est introduite pendant une étape de strength-reduction naïve (ou incomplète) de façon à entrelacer les calculs zombies avec ceux de la strength-reduction... 

    mul10(x3, x2, x1) {
           x4 = x3
           x5 = x2
           x10 = x5
           x11 = x10 <<l 1
           x12 = x10 <<l 3
           x9 = x11 +l x12
           // x17 = zombie (long32signed(x4))
    Loop:   // gluing invariant x9=x5 *l 10 | x17=SVundef | x4=x4 | x5=x5
           if (x5 <s x1) goto Body else goto End
    Body:
           x8 = long32signed(x4)
           x7 = x8 +l x9
           x4 = lowlong(x7)
           // x17 = zombie(x17 +l x9)
           x5 = x5 +l 1
           x9 = x9 +l 10
           goto Loop
    End:   x6 = x4
           return x6
    }

Dans l'exécution symbolique on interprète directement `zombie op` comme `SVundef` (après avoir éventuellement trapper comme op): l'invariant sur `x17` est donc triviale.

Ici, la strength-reduction n'a lieu que sur des `long`.

#### Zombi\_wakeup

C'est une passe certifiée dédiée qu'il faudra écrire.
On fait l'élimination des `zombie`.
Normalement, ça ne devrait pas être compliqué à montrer: c'est juste une simulation `lessdef` classique.

#### Elimination des variables inutiles

On élimine `x8` (remplacé par `x17`) et `x4` devenu inutile, via un invariant d'histoire.

    mul10(x3, x2, x1) {
           x4 = x3
           x5 = x2
           x10 = x5
           x11 = x10 <<l 1
           x12 = x10 <<l 3
           x9 = x11 +l x12
           x17 = long32signed(x4)
    Loop:  // history invariant x4=lowlong(x17)
           // gluing invariant x9=x9 | x17=x17 | x5=x5
           if (x5 <s x1) goto Body else goto End
    Body:
           x17 = x17 +l x9
           x5 = x5 +l 1
           x9 = x9 +l 10
           goto Loop
    End:   x6 = lowlong(x17)
           return x6
    }

Ici la partie "gluing" est essentiellement triviale.
Pour la partie "histoire",  on aura à prouver

- entrée dans Loop: `x3=lowlong(long32signed(x3))`

- retour dans Loop: `lowlong(long32signed(lowlong(x17)) +l x9)=lowlong(x17 +l x9)`

Bref, pour vérifier ça, il faut appliquer des règles de commutations des `lowlong` sur les opérations arithmétiques...  Faut voir à quel point la combinaison de toutes ces règles de réécritures est compliquée !

