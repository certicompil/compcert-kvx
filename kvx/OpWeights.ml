open Op;;
open PostpassSchedulingOracle;;
open PrepassSchedulingOracleDeps;;

module KV3 =
  struct
let resource_bounds = PostpassSchedulingOracle.resource_bounds;;
let nr_non_pipelined_units = 0;;

let rec nlist_rec x l = function
  | 0 -> l
  | n when n > 0 -> nlist_rec x (x :: l) (n-1)
  | _ -> failwith "nlist_rec";;
let nlist x n = nlist_rec x [] n;;

let bogus_register = Machregs.R0;;
let bogus_inputs n = nlist bogus_register n;;

let insns_of_op (op : operation) (nargs : int) =
  match Asmblockgen.transl_op op
          (bogus_inputs nargs) bogus_register [] with
  | Errors.Error msg -> failwith "OpWeights.insns_of_op"
  | Errors.OK insns -> insns;;

let insn_of_op op nargs =
  match insns_of_op op nargs with
  | [] -> failwith "OpWeights.insn_of_op"
  | h::_ -> h;;

let insns_of_cond (cond : condition) (nargs : int) =
  match Asmblockgen.transl_cond_op cond
           Asmvliw.GPR0 (bogus_inputs nargs) [] with
  | Errors.Error msg -> failwith "OpWeights.insns_of_cond"
  | Errors.OK insns -> insns;;

let insn_of_cond cond nargs =
  match insns_of_cond cond nargs with
  | [] -> failwith "OpWeights.insn_of_cond"
  | h::_ -> h;;

let insns_of_load trap chunk addressing (nargs : int) =
  match Asmblockgen.transl_load trap chunk addressing
          (bogus_inputs nargs) bogus_register [] with
  | Errors.Error msg -> failwith "OpWeights.insns_of_load"
  | Errors.OK insns -> insns;;

let insn_of_load trap chunk addressing nargs =
  match insns_of_load trap chunk addressing nargs with
  | [] -> failwith "OpWeights.insn_of_load"
  | h::_ -> h;;

let insns_of_store chunk addressing (nargs : int) =
  match Asmblockgen.transl_store chunk addressing
          (bogus_inputs nargs) bogus_register [] with
  | Errors.Error msg -> failwith "OpWeights.insns_of_store"
  | Errors.OK insns -> insns;;

let insn_of_store chunk addressing nargs =
  match insns_of_store chunk addressing nargs with
  | [] -> failwith "OpWeights.insn_of_store"
  | h::_ -> h;;

let latency_of_op (op : operation) (nargs : int) =
  let insn = insn_of_op op nargs in
  let record = basic_rec insn in
  let latency = real_inst_to_latency record.inst in
  latency;;

let resources_of_op (op : operation) (nargs : int) =
  let insn = insn_of_op op nargs in
  let record = basic_rec insn in
  rec_to_usage record;;
  
let non_pipelined_resources_of_op (op : operation) (nargs : int) = [| |]

let resources_of_cond (cond : condition) (nargs : int) =
  let insn = insn_of_cond cond nargs in
  let record = basic_rec insn in
  rec_to_usage record;;

let latency_of_load trap chunk (addr : addressing) (nargs : int) = 3;;
let latency_of_call _ _ = 6;;

let resources_of_load trap chunk addressing nargs =
  let insn = insn_of_load trap chunk addressing nargs in
  let record = basic_rec insn in
  rec_to_usage record;;

let resources_of_store chunk addressing nargs =
  let insn = insn_of_store chunk addressing nargs in
  let record = basic_rec insn in
  rec_to_usage record;;

let resources_of_call _ _ = resource_bounds;;
let resources_of_builtin _ = resource_bounds;;
  end;;

let get_opweights () : opweights =
  match !Clflags.option_mtune with
  | "kv3" | "" ->
     {
       pipelined_resource_bounds = KV3.resource_bounds;
       nr_non_pipelined_units = KV3.nr_non_pipelined_units;
       latency_of_op = KV3.latency_of_op;
       resources_of_op = KV3.resources_of_op;
       non_pipelined_resources_of_op = KV3.non_pipelined_resources_of_op;
       latency_of_load = KV3.latency_of_load;
       resources_of_load = KV3.resources_of_load;
       resources_of_store = KV3.resources_of_store;
       resources_of_cond = KV3.resources_of_cond;
       latency_of_call = KV3.latency_of_call;
       resources_of_call = KV3.resources_of_call;
       resources_of_builtin = KV3.resources_of_builtin
     }
  | xxx -> failwith (Printf.sprintf "unknown -mtune: %s" xxx);;
