(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*           Xavier Leroy       INRIA Paris-Rocquencourt       *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*           Cyril Six          Kalray                         *)
(*                                                             *)
(*  Copyright Kalray. Copyright VERIMAG. All rights reserved.  *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Auxiliary functions on machine registers *)

open Camlcoq
open Machregs

let register_names : (mreg, string) Hashtbl.t = Hashtbl.create 31

let _ =
  List.iter
    (fun (s, r) -> Hashtbl.add register_names r (camlstring_of_coqstring s))
    Machregs.register_names

let is_scratch_register r = false

let class_of_type = function
  | AST.Tint | AST.Tlong 
  | AST.Tfloat | AST.Tsingle -> 0
  | AST.Tany32 | AST.Tany64 -> assert false

let nr_regs = [| 59 |]
