(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*          Xavier Leroy, INRIA Paris-Rocquencourt                     *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the INRIA Non-Commercial License Agreement.     *)
(*                                                                     *)
(* *********************************************************************)

(* Command-line flags *)

let prepro_options = ref ([]: string list)
let linker_options = ref ([]: string list)
let assembler_options = ref ([]: string list)
let option_flongdouble = ref (Configuration.arch = "kvx")
let option_fstruct_passing = ref false
let option_fvararg_calls = ref true
let option_funprototyped = ref true
let option_fpacked_structs = ref false
let option_ffpu = ref true
let option_ffloatconstprop = ref 2
let option_ftailcalls = ref true
let option_fconstprop = ref true
let option_fcse = ref true
let option_fcse2 = ref false

let option_fcse3 = ref true
let option_fcse3_alias_analysis = ref true
let option_fcse3_across_calls = ref false
let option_fcse3_across_merges = ref true
let option_fcse3_glb = ref true
let option_fcse3_trivial_ops = ref false
let option_fcse3_refine = ref true
let option_fcse3_conditions = ref true
                        
let option_fredundancy = ref true

let option_fleaf = ref true
                 
(** Options relative to superblock scheduling *)
let option_fpredict = ref true (* insert static branch prediction information, and swaps ifso/ifnot branches accordingly *)
let option_ftailduplicate = ref 0 (* perform tail duplication for blocks of size n *)
let option_ftracelinearize = ref true (* uses branch prediction information to improve the linearization *)
let option_funrollsingle = ref 0 (* unroll a single iteration of innermost loops of size n *)
let option_funrollbody = ref 0 (* unroll the body of innermost loops of size n *)
let option_flooprotate = ref 0 (* rotate the innermost loops to have the condition inside the loop body *)

(* Scheduling *)
let option_mtune = ref ""

let option_fprepass = ref true
let option_fprepass_sched = ref "regpres"
let option_fprepass_alias_rel = ref true
let option_fprepass_alias_abs = ref true
let option_fprepass_alias_trivial = ref false
let option_fprepass_print_makespan = ref false

let option_fpostpass = ref true
let option_fpostpass_sched = ref "list"

(* Lazy code motion *)
let option_flct = ref true
let option_flct_trap = ref true
let option_flct_sr = ref true
let option_flct_tlimit = ref 0
let option_flct_nlimit = ref 500
let option_flct_tun = ref false
                
let option_fifconversion = ref true
let option_Obranchless = ref false
let option_falignfunctions = ref (None: int option)
let option_falignbranchtargets = ref 0
let option_faligncondbranchs = ref 0
let option_finline_asm = ref false
let option_fcommon = ref true
let option_mthumb = ref (Configuration.model = "armv7m")
let option_Osize = ref false
let option_finline = ref true
let option_finline_functions_called_once = ref true
let option_dprepro = ref false
let option_dparse = ref false
let option_dcmedium = ref false
let option_dclight = ref false
let option_dcminor = ref false
let option_drtl = ref false
let option_dltl = ref false
let option_dalloctrace = ref false
let option_dmach = ref false
let option_dasm = ref false
let option_sdump = ref false
let option_g = ref false
let option_gdwarf = ref (if Configuration.system = "diab" then 2 else 3)
let option_gdepth = ref 3
let option_o = ref (None: string option)
let option_E = ref false
let option_S = ref false
let option_c = ref false
let option_v = ref false
let option_interp = ref false
let option_small_data =
  ref (if Configuration.arch = "powerpc"
       && Configuration.abi = "eabi"
       && Configuration.system = "diab"
       then 8 else 0)
let option_small_const = ref (!option_small_data)
let option_timings = ref false
let option_std = ref "c99"
let stdlib_path = ref Configuration.stdlib_path
let use_standard_headers =  ref Configuration.has_standard_headers

let option_fglobaladdrtmp = ref false
let option_fglobaladdroffset = ref false
let option_fxsaddr = ref true  
let option_faddx = ref false
let option_fmadd = ref true
let option_div_i32 = ref "inline-fp"
let option_div_i64 = ref "inline-fp" 
let option_fcoalesce_mem = ref true
let option_fexpanse_rtlcond = ref false
let option_fexpanse_others = ref false
let option_fforward_moves = ref false
let option_reg_renaming_opt = ref "backward"
let option_reg_renaming = ref false
let option_if_lift = ref false
let option_if_lift_pol = ref "movstores"
let option_if_lift_ratio = ref 0
let option_schedule_compense = ref true
let option_factorize = ref false
let option_fmove_loop_invariants = ref false
let option_fnontrap_loads = ref true
let option_all_loads_nontrap = ref false
let option_inline_auto_threshold = ref 0
let option_profile_arcs = ref false
let option_fbranch_probabilities = ref true
let option_branch_threshold = ref 100L
let option_branch_delta = ref 0.2
let option_debug_compcert = ref 0
let option_regpres_threshold = ref 2
let option_regpres_wait_window = ref false
let option_branch_target_protection = ref true
let option_retaddr_pac = ref true
let option_aarch64_has_pac = ref false
let get_optims_expanse () = !option_fexpanse_rtlcond || !option_fexpanse_others
let get_btl_bb () = get_optims_expanse() || !option_flct
let get_btl_sb () = !option_reg_renaming || !option_if_lift || !option_fprepass
let main_function_name = ref "main"
