open RTL
open Camlcoq
open Datatypes

module OrdPS = Set.Make (struct
  let compare = P.compare

  type t = P.t
end)
(** Ordered set of positives *)

let p2i r = P.to_int r

let i2p i = P.of_int i

(** Functions to shift node ids to bitvector indices:
 * In the RTL CFG, nodes are numbered starting from 1, but in the Bitv module,
 * we start counting from 0. *)
let p2id p = p2i p - 1

let i2pi i = i2p (i + 1)

let reg = ref 1

let node = ref 1

let r2p () = P.of_int !reg

let n2p () = P.of_int !node

let r2pi () =
  reg := !reg + 1;
  r2p ()

let regd () = reg := !reg - 1

let n2pi () =
  node := !node + 1;
  n2p ()

let get_some = function
  | None -> failwith "Got None instead of Some _"
  | Some thing -> thing

let get_ok r = match r with Errors.OK x -> x | _ -> failwith "Did not get OK"

let predicted_successor = function
  | Inop n | Iop (_, _, _, n) | Iload (_, _, _, _, _, n) | Istore (_, _, _, _, n)
    ->
      Some n
  | Icall (_, _, _, _, n) | Ibuiltin (_, _, _, n) -> None
  | Icond (_, _, n1, n2, p) -> (
      match p with Some true -> Some n1 | Some false -> Some n2 | None -> None)
  | Ijumptable _ | Itailcall _ | Ireturn _ -> None

let non_predicted_successors i = function
  | None -> successors_instr i
  | Some ps -> List.filter (fun s -> s != ps) (successors_instr i)

let change_successor old_s new_s = function
  | Inop _ -> Inop new_s
  | Iop (op, args, dst, _) -> Iop (op, args, dst, new_s)
  | Iload (trap, chk, addr, lr, dst, _) ->
      Iload (trap, chk, addr, lr, dst, new_s)
  | Istore (chk, addr, lr, src, _) -> Istore (chk, addr, lr, src, new_s)
  | Icall (s, rind, lr, dst, _) -> Icall (s, rind, lr, dst, new_s)
  | Ibuiltin (ef, blr, bdst, _) -> Ibuiltin (ef, blr, bdst, new_s)
  | Icond (cond, lr, s1, s2, p) ->
      let s1' = if old_s = s1 then new_s else s1 in
      let s2' = if old_s = s2 then new_s else s2 in
      Icond (cond, lr, s1', s2', p)
  | Ijumptable (arg, tbl) ->
      let tbl' = List.map (fun s -> if s = old_s then new_s else s) tbl in
      Ijumptable (arg, tbl')
  | Itailcall _ | Ireturn _ ->
      failwith "change_successor: no successors to change"

let get_regindent = function Coq_inr _ -> [] | Coq_inl r -> [ r ]

(** Form a list containing both sources and destination regs of an instruction *)
let get_regs_inst = function
  | Inop _ -> []
  | Iop (_, args, dest, _) -> dest :: args
  | Iload (_, _, _, args, dest, _) -> dest :: args
  | Istore (_, _, args, src, _) -> src :: args
  | Icall (_, t, args, dest, _) -> dest :: (get_regindent t @ args)
  | Itailcall (_, t, args) -> get_regindent t @ args
  | Ibuiltin (_, args, dest, _) ->
      AST.params_of_builtin_res dest @ AST.params_of_builtin_args args
  | Icond (_, args, _, _, _) -> args
  | Ijumptable (arg, _) -> [ arg ]
  | Ireturn (Some r) -> [ r ]
  | _ -> []

(** Compute the last used node and reg indexs *)

let rec traverse_list var = function
  | [] -> ()
  | e :: t ->
      let e' = p2i e in
      if e' > !var then var := e';
      traverse_list var t

let rec find_last_node_rec = function
  | [] -> ()
  | (pc, i) :: k ->
      traverse_list node [ pc ];
      find_last_node_rec k

let find_last_node code =
  node := 1;
  find_last_node_rec code

let rec find_last_node_reg_rec_rtl = function
  | [] -> ()
  | (pc, i) :: k ->
      traverse_list node [ pc ];
      traverse_list reg (get_regs_inst i);
      find_last_node_reg_rec_rtl k

let find_last_node_reg_rtl code =
  node := 1;
  reg := 1;
  find_last_node_reg_rec_rtl code
