open Maps
open Camlcoq
open Registers
open RTLcommonaux

let debug_flag = ref false

let debug fmt =
  if !debug_flag then (flush stderr; flush stdout; Printf.eprintf fmt)
  else Printf.ifprintf stderr fmt

let print_ptree_bool oc pt =
  if !debug_flag then
    let elements = PTree.elements pt in
    begin
      Printf.fprintf oc "[";
      List.iter (fun (n, b) ->
        if b then Printf.fprintf oc "%d, " (P.to_int n)
      ) elements;
      Printf.fprintf oc "]\n"
    end
  else ()

let print_ptree_opint oc pt =
  if !debug_flag then
    let elements = PTree.elements pt in
    begin
      Printf.fprintf oc "[";
      List.iter (fun (n, op) ->
        match op with
        | None -> ()
        | Some p -> Printf.fprintf oc "%d -> %d, " (P.to_int n) (P.to_int p)
      ) elements;
      Printf.fprintf oc "]\n"
    end
  else ()

let print_intlist oc l =
  let rec f oc = function
  | [] -> ()
  | n::ln -> (Printf.fprintf oc "%d %a" (P.to_int n) f ln)
  in begin
    if !debug_flag then begin
      Printf.fprintf oc "[%a]" f l
    end
  end

let print_ordps oc ps =
  let seq = OrdPS.to_seq ps
  in begin
    if !debug_flag then begin
      Printf.fprintf oc "{";
      Seq.iter (fun n ->
        Printf.fprintf oc "%d " (P.to_int n)
      ) seq;
      Printf.fprintf oc "}"
    end
  end

let print_ptree_oplist oc pt =
  if !debug_flag then
    let elements = PTree.elements pt in
    begin
      Printf.fprintf oc "[";
      List.iter (fun (n, ol) ->
        match ol with
        | None -> ()
        | Some l -> Printf.fprintf oc "%d -> %a,\n" (P.to_int n) print_intlist l
      ) elements;
      Printf.fprintf oc "]\n"
    end
  else ()

(* Adapted from backend/PrintRTL.ml: print_function *)
let print_code code = let open PrintRTL in let open Printf in
  if (!debug_flag) then begin
    fprintf stdout "{\n";
    let instrs =
      List.sort
        (fun (pc1, _) (pc2, _) -> compare pc2 pc1)
        (List.rev_map
          (fun (pc, i) -> (P.to_int pc, i))
          (PTree.elements code)) in
    List.iter (print_instruction stdout) instrs;
    fprintf stdout "}"
  end

let print_ptree printer pt =
  let elements = PTree.elements pt in
  begin
    debug "[\n";
    List.iter (fun (n, elt) ->
      debug "\t%d: %a\n" (P.to_int n) printer elt
    ) elements;
    debug "]\n"
  end

let print_option_pint oc o =
  if !debug_flag then
    match o with
    | None -> Printf.fprintf oc "None"
    | Some n -> Printf.fprintf oc "Some %d" (P.to_int n)

let print_pint oc i = if !debug_flag then Printf.fprintf oc "%d" (P.to_int i) else ()

let print_regset rs = begin
  debug "[";
  List.iter (fun n -> debug "%d " (P.to_int n)) (Regset.elements rs);
  debug "]"
end

let print_ptree_regset pt = begin
  debug "[";
  List.iter (fun (n, rs) ->
    debug "\n\t";
    debug "%d: " (P.to_int n);
    print_regset rs
  ) (PTree.elements pt);
  debug "]"
end

let print_instructions insts code =
  if (!debug_flag) then begin
    debug "[ ";
    List.iter (
      fun n -> (PrintRTL.print_instruction stdout (P.to_int n, get_some @@ PTree.get n code))
    ) insts; debug "  ]"
  end

let print_arrayp arr = begin
  debug "[| ";
  Array.iter (fun n -> debug "%d, " (P.to_int n)) arr;
  debug "|]"
end
