#include <stdio.h>

int call1(
	     int call00,
	     int call01,
	     int call02,
	     int call03,
	     int call04,
	     int call05,
	     int call06,
	     int call07,
	     int call08,
	     int call09,
	     int call10,
	     int call11,
	     int call12,
	     int call13,
	     int call14,
	     int call15,
	     int call16,
	     int call17,
	     int call18,
	     int call19,
	     int call20,
	     int call21,
	     int call22,
	     int call23,
	     int call24,
	     int call25,
	     int call26,
	     int call27,
	     int call28,
	     int call29,
	     int call30,
	     int call31,
	     int call32,
	     int call33,
	     int call34,
	     int call35,
	     int call36,
	     int call37,
	     int call38,
	     int call39,
	     int call40,
	     int call41,
	     int call42,
	     int call43,
	     int call44,
	     int call45,
	     int call46,
	     int call47,
	     int call48,
	     int call49,
	     int call50,
	     int call51,
	     int call52,
	     int call53,
	     int call54,
	     int call55,
	     int call56,
	     int call57,
	     int call58,
	     int call59) {
  return     ( call00
	     + call01
	     + call02
	     + call03
	     + call04
	     + call05
	     + call06
	     + call07
	     + call08
	     + call09
	     + call10
	     + call11
	     + call12
	     + call13
	     + call14
	     + call15
	     + call16
	     + call17
	     + call18
	     + call19
	     + call20
	     + call21
	     + call22
	     + call23
	     + call24
	     + call25
	     + call26
	     + call27
	     + call28
	     + call29
	     + call30
	     + call31
	     + call32
	     + call33
	     + call34
	     + call35
	     + call36
	     + call37
	     + call38
	     + call39
	     + call40
	     + call41
	     + call42
	     + call43
	     + call44
	     + call45
	     + call46
      	     + call47
	     + call48
	     + call49
	     + call50
	     + call51
	     + call52
	     + call53
	     + call54
	     + call55
	     + call56
	     + call57
	     + call58
             + call59);
}

int call2(
	     int call00,
	     int call01,
	     int call02,
	     int call03,
	     int call04,
	     int call05,
	     int call06,
	     int call07,
	     int call08,
	     int call09,
	     int call10,
	     int call11,
	     int call12,
	     int call13,
	     int call14,
	     int call15,
	     int call16,
	     int call17,
	     int call18,
	     int call19,
	     int call20,
	     int call21,
	     int call22,
	     int call23,
	     int call24,
	     int call25,
	     int call26,
	     int call27,
	     int call28,
	     int call29,
	     int call30,
	     int call31,
	     int call32,
	     int call33,
	     int call34,
	     int call35,
	     int call36,
	     int call37,
	     int call38,
	     int call39,
	     int call40,
	     int call41,
	     int call42,
	     int call43,
	     int call44,
	     int call45,
	     int call46,
	     int call47,
	     int call48,
	     int call49,
	     int call50,
	     int call51,
	     int call52,
	     int call53,
	     int call54,
	     int call55,
	     int call56,
	     int call57,
	     int call58,
	     int call59) {
  return 10 + call1(
	     call00,
	     call01,
	     call02,
	     call03,
	     call04,
	     call05,
	     call06,
	     call07,
	     call08,
	     call09,
	     call10,
	     call11,
	     call12,
	     call13,
	     call14,
	     call15,
	     call16,
	     call17,
	     call18,
	     call19,
	     call20,
	     call21,
	     call22,
	     call23,
	     call24,
	     call25,
	     call26,
	     call27,
	     call28,
	     call29,
	     call30,
	     call31,
	     call32,
	     call33,
	     call34,
	     call35,
	     call36,
	     call37,
	     call38,
	     call39,
	     call40,
	     call41,
	     call42,
	     call43,
	     call44,
	     call45,
	     call46,
	     call47,
	     call48,
	     call49,
	     call50,
	     call51,
	     call52,
	     call53,
	     call54,
	     call55,
	     call56,
	     call57,
	     call58,
	     call59);
}

int main() {
  int x =
    call2(   0,
	     1,
	     2,
	     3,
	     4,
	     5,
	     6,
	     7,
	     8,
	     9,
	     10,
	     11,
	     12,
	     13,
	     14,
	     15,
	     16,
	     17,
	     18,
	     19,
	     20,
	     21,
	     22,
	     23,
	     24,
	     25,
	     26,
	     27,
	     28,
	     29,
	     30,
	     31,
	     32,
	     33,
	     34,
	     35,
	     36,
	     37,
	     38,
	     39,
	     40,
	     41,
	     42,
	     43,
	     44,
	     45,
	     46,
	     47,
	     48,
	     49,
	     50,
	     51,
	     52,
	     53,
	     54,
	     55,
	     56,
	     57,
	     58,
	     59);
  printf("%d\n", x);
}
