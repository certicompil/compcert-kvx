#!/bin/bash
INCLUDE=-I$COMPCERT/test/monniaux/csmith/csmith/include/csmith-2.3.0
if /usr/bin/clang $INCLUDE -O -Werror=int-conversion -Werror=format -Werror=strict-prototypes -Werror=uninitialized -Werror=implicit -Werror=return-type source.c -o source.clang.host && valgrind --exit-on-first-error=yes --error-exitcode=120 --log-file=valgrind.out ./source.clang.host > source.clang.host.out ;
then
    if gcc $INCLUDE -Werror=int-conversion -Werror=format -Werror=strict-prototypes -Werror=uninitialized -Werror=implicit -Werror=return-type -fsanitize=undefined -fsanitize=address source.c -o source.gcc+san.host && ./source.gcc+san.host > source.gcc+san.host.out ;
    then
	if grep "runtime error" source.gcc+san.host.out
	then
	    exit 66
	fi
	if cmp source.clang.host.out source.gcc+san.host.out && riscv64-linux-gnu-gcc $INCLUDE -Werror=format -Werror=uninitialized -Werror=implicit source.c -Werror=return-type -o source.gcc.target && qemu-riscv64 -L /usr/riscv64-linux-gnu source.gcc.target >& source.gcc.target.out && grep checksum source.gcc.target.out > source.gcc.target.check && diff source.clang.host.out source.gcc.target.out
	then
	    if $COMPCERT/ccomp $INCLUDE -Werror=missing-declarations -fall -fno-cse -fno-cse2 -fno-cse3 source.c -o source.ccomp.target
	    then
		qemu-riscv64 -L /usr/riscv64-linux-gnu source.ccomp.target >& source.ccomp.target.out
		if grep checksum source.ccomp.target.out > source.ccomp.target.check
		then
		    if diff	source.ccomp.target.check source.gcc.target.check
		    then exit 40
		    else exit 0
		    fi
		else
	            exit 50
		fi
	    else
		exit 2
	    fi
	else
	    exit 1
	fi
    else
	exit 4
    fi
else
    exit 5
fi
    
