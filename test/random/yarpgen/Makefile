TARGET_CCOMP=../../../ccomp
TARGET_CC=gcc
#EXECUTE=kvx-cluster -- 

all:

.SECONDARY:

ifndef YARPGEN
YARPGEN=./yarpgen
GENERATOR=yarpgen
endif

ifdef BITS
YARPGEN+=-m $(BITS)
CFLAGS+=-m$(BITS)
endif

MAX=129
PREFIX=ran%06.f

TARGET_CFLAGS=-static
CCOMPOPTS=-static
CCOMPFLAGS+= -funprototyped -fbitfields -fno-cse2 -stdlib ../../../runtime

TESTS_C=$(shell seq --format $(PREFIX)/func.c 1 $(MAX)) \
	$(shell seq --format $(PREFIX)/driver.c 1 $(MAX)) \
	$(shell seq --format $(PREFIX)/init.c 1 $(MAX)) \
	$(shell seq --format $(PREFIX)/hash.c 1 $(MAX)) \
	$(shell seq --format $(PREFIX)/check.c 1 $(MAX)) \
	$(shell seq --format $(PREFIX)/init.h 1 $(MAX))

$(TESTS_C): $(GENERATOR)

TESTS_CCOMP_TARGET_S=$(TEST_C:.c=.ccomp.target.s)
TESTS_GCC_TARGET_S=$(TEST_C:.c=.gcc.target.s)
TESTS_GCC_HOST_S=$(TEST_C:.c=.gcc.host.s)
TESTS_GCC_O3_TARGET_S=$(TEST_C:.c=.gcc_O3.target.s)
TESTS_GCC_O3_HOST_S=$(TEST_C:.c=.gcc_O3.host.s)
TESTS_CCOMP_TARGET_OUT=$(shell seq --format $(PREFIX)/example.ccomp.target.out 1 $(MAX))
TESTS_GCC_TARGET_OUT=$(shell seq --format $(PREFIX)/example.gcc.target.out 1 $(MAX))
TESTS_GCC_O3_TARGET_OUT=$(shell seq --format $(PREFIX)/example.gcc_O3.target.out 1 $(MAX))
TESTS_GCC_HOST_OUT=$(shell seq --format $(PREFIX)/example.gcc.host.out 1 $(MAX))
TESTS_GCC_O3_HOST_OUT=$(shell seq --format $(PREFIX)/example.gcc.host.out 1 $(MAX))
TESTS_CMP=$(shell seq --format $(PREFIX)/example.target.cmp 1 $(MAX))
TESTS_CMP_TARGET_HOST=$(shell seq --format $(PREFIX)/example.host_target.cmp 1 $(MAX))
TESTS_CMP_O3=$(shell seq --format $(PREFIX)/example.target.cmp_O3 1 $(MAX))

# FIXME - test000089 fails in CI in arm and armhf because of memory consumption during register allocation being too high
# Removing it from the pool
BADID:=89
TESTS_C:=$(filter-out $(shell seq --format $(PREFIX)/func.c $(BADID) $(BADID)),$(TESTS_C))
TESTS_C:=$(filter-out $(shell seq --format $(PREFIX)/driver.c $(BADID) $(BADID)),$(TESTS_C))
TESTS_C:=$(filter-out $(shell seq --format $(PREFIX)/init.c $(BADID) $(BADID)),$(TESTS_C))
TESTS_C:=$(filter-out $(shell seq --format $(PREFIX)/hash.c $(BADID) $(BADID)),$(TESTS_C))
TESTS_C:=$(filter-out $(shell seq --format $(PREFIX)/check.c $(BADID) $(BADID)),$(TESTS_C))
TESTS_C:=$(filter-out $(shell seq --format $(PREFIX)/init.h $(BADID) $(BADID)),$(TESTS_C))
TESTS_CMP:=$(filter-out $(shell seq --format $(PREFIX)/example.target.cmp $(BADID) $(BADID)),$(TESTS_CMP))
TESTS_GCC_HOST_OUT:=$(filter-out $(shell seq --format $(PREFIX)/example.gcc.host.out $(BADID) $(BADID)),$(TESTS_GCC_HOST_OUT))
TESTS_CCOMP_TARGET_OUT:=$(filter-out $(shell seq --format $(PREFIX)/example.ccomp.target.out $(BADID) $(BADID)),$(TESTS_CCOMP_TARGET_OUT))
TESTS_GCC_TARGET_OUT:=$(filter-out $(shell seq --format $(PREFIX)/example.gcc.target.out $(BADID) $(BADID)),$(TESTS_GCC_TARGET_OUT))
TESTS_GCC_O3_TARGET_OUT:=$(filter-out $(shell seq --format $(PREFIX)/example.gcc.target.out $(BADID) $(BADID)),$(TESTS_GCC_O3_TARGET_OUT))

all:	$(TESTS_CCOMP_TARGET_OUT) $(TESTS_GCC_TARGET_OUT) $(TESTS_CCOMP_TARGET_S) $(TESTS_GCC_TARGET_S) $(TESTS_CMP) $(TESTS_C)

tests_c: $(TESTS_C)

tests_s: $(TESTS_CCOMP_TARGET_S)

tests_O3: $(TESTS_CMP_O3)

%.ccomp.target.s : %.c
	$(TARGET_CCOMP) $(CCOMPOPTS) $(CCOMPFLAGS) -S -o $@ $< 2> $*.ccomp.target.log

%.gcc.target.s : %.c
	$(TARGET_CC) $(TARGET_CFLAGS) -S -o $@ $< 2> $*.gcc.target.s

%.gcc_O3.target.s : %.c
	$(TARGET_CC) $(TARGET_CFLAGS) -O3 -S -o $@ $<

%.gcc.host.s : %.c
	$(CC) $(CFLAGS) -S -o $@ $<

%.target.o : %.target.s
	$(TARGET_CC) -c -o $@ $<

%.host.o : %.host.s
	$(CC) $(CFLAGS) -c -o $@ $<

%.target.out : %.target
	$(EXECUTE) $< | tee $@

%.host.out : %.host
	./$< | tee $@

ran%/func.ccomp.target.s ran%/func.gcc.target.s ran%/func.ccomp.host.s ran%/func.gcc.host.s ran%/init.gcc.host.s : ran%/init.h

ran%/func.gcc_O3.target.s ran%/func.gcc_O3.host.s ran%/init.gcc_O3.host.s : ran%/init.h

ran%/example.ccomp.target: ran%/func.ccomp.target.o ran%/driver.ccomp.target.o  ran%/init.ccomp.target.o ran%/check.ccomp.target.o ran%/hash.ccomp.target.o
	$(TARGET_CCOMP) $(CCOMPOPTS) $(CCOMPFLAGS) $+ -o $@

ran%/example.gcc.target: ran%/func.gcc.target.o ran%/driver.gcc.target.o ran%/init.gcc.target.o ran%/check.gcc.target.o ran%/hash.gcc.target.o
	$(TARGET_CC) $(TARGET_CFLAGS) $+ -o $@

ran%/example.gcc_O3.target: ran%/func.gcc_O3.target.o ran%/driver.gcc_O3.target.o ran%/init.gcc_O3.target.o ran%/check.gcc_O3.target.o ran%/hash.gcc_O3.target.o
	$(TARGET_CC) $(TARGET_CFLAGS) $+ -o $@

ran%/example.gcc.host: ran%/func.gcc.host.o ran%/driver.gcc.host.o ran%/init.gcc.host.o ran%/check.gcc.host.o ran%/hash.gcc.host.o
	$(CC) $(CFLAGS) $+ -o $@

ran%/driver.c ran%/func.c ran%/init.c ran%/check.c ran%/hash.c ran%/init.h:
	mkdir -p ran$*
	$(YARPGEN) --seed=$* --out-dir=ran$*/ --std=c99

ran%/example.target.cmp : ran%/example.gcc.target.out ran%/example.ccomp.target.out
	cmp $+ > $@

ran%/example.target.cmp_O3 : ran%/example.gcc.target.out ran%/example.gcc_O3.target.out
	cmp $+ > $@

ran%/example.host_target.cmp : ran%/example.gcc.host.out ran%/example.ccomp.target.out
	cmp $+ > $@

yarpgen:
	curl -L -o yarpgen_v1.1.tar.gz https://github.com/intel/yarpgen/archive/v1.1.tar.gz
	tar xfz yarpgen_v1.1.tar.gz
	$(MAKE) CXX=g++ -C yarpgen-1.1
	cp yarpgen-1.1/yarpgen $@

.PHONY: all clean tests_c tests_c

clean:
	-rm -rf ran*
