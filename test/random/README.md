# Test suite with randomly generated programs
(maintained by David~Monniaux, CNRS/VERIMAG)

The subdirectories of this directory (`csmith`, `yarpgen`, `ccg`) each download and install a different generator for random C programs.

`csmith` and `yarpgen` will compile the programs with `ccomp` and `gcc` (see `../../.gitlab-ci.yml` for examples of invokation of the makefiles), execute them and check that the results match.

`ccg` will just check that compilation succeeds.
