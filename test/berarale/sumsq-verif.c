#include "stdio.h"
int main() {
    double x[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    double s = 0.;
    for (int i = 0; i < 10; i++) s += x[i]*x[i];
    printf("%f\n", s);
    return 0;
}
