int main(int len, double* x) {
    double s = 0.;
    for (int i = 0; i < len; i++) s += x[i]*x[i];
    return s;
}
