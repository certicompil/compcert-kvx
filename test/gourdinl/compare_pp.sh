ffname=$(basename $1)
fname=${ffname%.*}
nopp=$fname.nopp.s
pp=$fname.pp.s

../../ccomp -fno-coalesce-mem -fno-postpass -S $1 -o $nopp
../../ccomp -fno-coalesce-mem -fpostpass= list -S $1 -o $pp
sed -i '1,2d' $nopp
sed -i '1,2d' $pp
if cmp -s $nopp $pp; then
  echo "same!"
else
  echo "differents!"
  diff -y $nopp $pp
fi

