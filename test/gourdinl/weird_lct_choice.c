enum { a } typedef g;
typedef g b;
__inline__ b c(d) {
  if (d)
    h();
  e(d * sizeof(g));
}
__inline__ b i(f, d) {
  if (f)
    c(d);
}
void j() {
  int actno, c = i(d(), actno);
  i(e(), actno);
  for (;;)
    ;
}
