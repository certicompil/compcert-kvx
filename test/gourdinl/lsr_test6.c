long sum_slice(long n, long m) {
  long r = 0;
  for(long l = n; l < m; l++) {
    r += l * 5;
  }
  return r;
}
