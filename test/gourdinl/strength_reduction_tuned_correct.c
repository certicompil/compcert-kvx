int sum(int *t, int n) {
  int i = 0, s = 0, *ti = t;
  while (i < n) {
    s += *ti;
    i++;
    ti++;
  }
  return s;
}
