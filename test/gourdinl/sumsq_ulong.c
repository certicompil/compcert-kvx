double sumsq(double *x, unsigned long len) {
  double s = 0.0;
  for(unsigned long i =0; i < len; i++) s += x[i] * x[i];
  return s ;
}
