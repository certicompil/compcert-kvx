long foo(int x, char y, long *t) {
  int z = x / 4096;
  y = x / 256;
  t[0] = t[1] * t[2];
  if (x + z < 7) {
    if (y < 7)
      return 421 + t[0];
  }
  y = y - z;
  return x + y - t[0];
}
