int main(int x, int *y, int z) {
  int a = 0, b;
  while (a < 100) {
    x *= *y;
    if (a > 35) {
      b = z << 2;
      x += a * b;
    }
    a += x;
  }
  return a;
}
