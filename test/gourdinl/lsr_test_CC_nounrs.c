long main(long x, long y, long *p) {
  long i = 0, b = *p;
  while (i < 100) {
    x *= *p;
    if (i > 35)
      b += y * y;
    x += i * 5;
    i += 3;
  }
  return x-b;
}
