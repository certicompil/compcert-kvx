extern long x;

long bar(long a, long *y){
  long s = x;
  do {
    *y = x;
    s = s - a;
  } while (x < s);
  return x;
}
