#include <string.h>
void a(size_t b, long *bv) {
  size_t c, d;
  for (; b; d += 2)
    for (c = 0; c < 2; ++c)
      bv[d + c] = 0;
}
