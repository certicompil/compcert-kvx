extern long x;

long bar(long a, long *y){
  long s = x;
  do {
    s += x;
    *y = x+1;
  } while (x < a);
  return s;
}
