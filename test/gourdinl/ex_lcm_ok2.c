extern long x;

long bar(long a, long *y){
  long s = x, b;
  do {
    b = x;
    if (b > 10) s-=10;
    s += b;
    *y = x+1;
  } while (x < a);
  return s;
}
