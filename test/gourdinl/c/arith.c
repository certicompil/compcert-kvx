int main(int num1, int num2)
{
  int sum, sub, mult, mod;
  float div;

  /*
   * Perform all arithmetic operations
   */ 
  sum = num1 + num2;
  sub = num1 - num2;
  mult = num1 * num2;
  div = (float)num1 / num2;
  mod = num1 % num2;

  return sum + sub + mult + div + mod;
}
