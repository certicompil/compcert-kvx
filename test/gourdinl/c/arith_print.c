int main()
{
  int num1 = 2;
  int num2 = 4;
  int sum, sub, mult, mod;
  float div;

  /*
   * Perform all arithmetic operations
   */ 
  sum = num1 + num2;
  sub = num1 - num2;
  mult = num1 * num2;
  div = (float)num1 / num2;
  mod = num1 % num2;

  printf("%d", sum + sub + mult + div + mod);
  return;
}
