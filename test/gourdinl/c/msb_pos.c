/* Function to find the MSB bit position */
int main(int n)
{
  int i = 0, bit;
  while (i < 32)
  {
     bit = n & 0x80000000;
     if (bit == -0x80000000)
     {
        bit = 1;
     }

     if (bit == 1) 
       break;

      n = n << 1;
      i++;
  }
  return i;
}
