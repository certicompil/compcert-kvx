double approx(double *a) {
  double r = 1;
  if (a[0] < 1) return 1;
  while (r < a[2])
    if (r >= a[1]) r -= a[0];
    else r *= 7;
  return r;
}
