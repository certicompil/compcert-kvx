#ifndef __TYPES_H__
#define __TYPES_H__

#define uint64_t    unsigned long long
#define int64_t     signed long long

#endif // __TYPES_H__
