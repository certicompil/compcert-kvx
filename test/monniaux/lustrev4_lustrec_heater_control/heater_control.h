/* C code generated by lustrec
   Version number 1.6-@GITBRANCH@
   Code is C99 compliant
   Using (double) floating-point numbers */
   
#ifndef _HEATER_CONTROL
#define _HEATER_CONTROL

/* Imports standard library */
#include <stdint.h>
#include "arrow.h"


/* Import dependencies */

/* Types definitions */

/* Global constant (declarations, definitions are in C file) */
extern double FAILURE;
extern double TMIN;
extern double TMAX;
extern double DELTA;

/* Structs declarations */
struct not_a_sauna2_mem;
struct heater_control_mem;
struct not_a_sauna_mem;

/* Nodes declarations */
extern void not_a_sauna2_reset (struct not_a_sauna2_mem *self);

extern void not_a_sauna2_init (struct not_a_sauna2_mem *self);

extern void not_a_sauna2_clear (struct not_a_sauna2_mem *self);

extern void not_a_sauna2_step (double T, double T1, double T2, double T3,
                               _Bool Heat_on, 
                               _Bool (*ok),
                               struct not_a_sauna2_mem *self);

extern void heater_control_reset (struct heater_control_mem *self);

extern void heater_control_init (struct heater_control_mem *self);

extern void heater_control_clear (struct heater_control_mem *self);

extern void heater_control_step (double T, double T1, double T2, double T3, 
                                 _Bool (*Heat_on),
                                 struct heater_control_mem *self);

extern void not_a_sauna_reset (struct not_a_sauna_mem *self);

extern void not_a_sauna_init (struct not_a_sauna_mem *self);

extern void not_a_sauna_clear (struct not_a_sauna_mem *self);

extern void not_a_sauna_step (double T, double T1, double T2, double T3,
                              _Bool Heat_on, 
                              _Bool (*ok),
                              struct not_a_sauna_mem *self);

extern void oneoftree_step (_Bool f1, _Bool f2, _Bool f3, 
                            _Bool (*r)
                            );

extern void noneoftree_step (_Bool f1, _Bool f2, _Bool f3, 
                             _Bool (*r)
                             );

extern void alloftree_step (_Bool f1, _Bool f2, _Bool f3, 
                            _Bool (*r)
                            );

extern void abs_step (double v, 
                      double (*a)
                      );

extern void Median_step (double a, double b, double c, 
                         double (*z)
                         );

extern void Average_step (double a, double b, 
                          double (*z)
                          );

extern void min2_step (double one, double two, 
                       double (*m)
                       );

extern void max2_step (double one, double two, 
                       double (*m)
                       );


#endif

