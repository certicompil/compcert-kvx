long sumprod(int n, long *a, long *b, long s) {
  for(int i=0; i<n; i++) {
    s += a[i] * b[i];
  }
  return s;
}

#define N 100000
static long a[N], b[N];
volatile long glob_sum;

int main() {
  for(int i=0; i<N; i++) {
    a[i] = i;
    b[i] = i+1;
  }
  for(int k=0; k<10000; k++) {
    glob_sum = sumprod(N, a, b, glob_sum);
  }
  return 0;
}
