#define copy(typ, x) __builtin_copy_##typ((x), __LINE__)

int toto(int x) {
  int y= copy(int, x);
  int z= copy(int, x);
  return (y==z && x==y);
}
