aarch64-linux-gnu-gcc -O3 -ffp-contract=off -mcpu=cortex-a53+nosimd sumprod.c -o sumprod.gcc-O3.aarch64
aarch64-linux-gnu-gcc -O2 -ffp-contract=off -mcpu=cortex-a53+nosimd sumprod.c -o sumprod.gcc-O2.aarch64
aarch64-linux-gnu-gcc -O1 -ffp-contract=off -mcpu=cortex-a53+nosimd sumprod.c -o sumprod.gcc-O1.aarch64

clang -target aarch64-linux-gnu -O3 -ffp-contract=off -mcpu=cortex-a53+nosimd sumprod.c -o sumprod.clang-O3.aarch64
clang -target aarch64-linux-gnu -O2 -ffp-contract=off -mcpu=cortex-a53+nosimd sumprod.c -o sumprod.clang-O2.aarch64
clang -target aarch64-linux-gnu -O1 -ffp-contract=off -mcpu=cortex-a53+nosimd sumprod.c -o sumprod.clang-O1.aarch64

../../../ccomp -flooprotate 10 -funrollbody 10 -fif-lift sumprod.c -o sumprod.iflift.aarch64
../../../ccomp -flooprotate 10 -funrollbody 10 sumprod.c -o sumprod.unroll.aarch64
../../../ccomp -flooprotate 10 sumprod.c -o sumprod.rotate.aarch64
../../../ccomp sumprod.c -o sumprod.ccomp.aarch64

