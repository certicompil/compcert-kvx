#include <stdio.h>
#include "../clock.h"

#define N 100000

void FN(long n, double *r, double *a, double *b);
  
static double a[N], b[N], r[N];

int main() {
  for(int i=0; i<N; i++) {
    a[i] = i * 0.1;
    b[i] = (double) i*i * 0.001;
  }
  clock_prepare();
  clock_start();
  for(int j=0; j<1000; j++) {
    FN(N, r, a, b);
  }
  clock_stop();
  print_total_clock();
}
