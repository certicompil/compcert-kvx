void sum_arrays1(long n, double *r, double *a, double *b) {
  for(long i=0; i<n; i++) r[i] = a[i] + b[i];
}

void sum_arrays2(long n, double *r, double *a, double *b) {
  for(long i=n-1; i>=0; i--) r[i] = a[i] + b[i];
}

void sum_arrays3(int n, double *r, double *a, double *b) {
  for(int i=0; i<n; i++) r[i] = a[i] + b[i];
}

void sum_arrays4(int n, double *r, double *a, double *b) {
  for(int i=n-1; i>=0; i--) r[i] = a[i] + b[i];
}

void sum_arrays3o(int n, double *r, double *a, double *b) {
  for(int i=0; i<n; i++) r[i] = a[i] + b[i];
}
