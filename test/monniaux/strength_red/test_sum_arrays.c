#include <stdio.h>

#define N 100
void sum_arrays1(long n, double *r, double *a, double *b);
void sum_arrays2(long n, double *r, double *a, double *b);
void sum_arrays3(int n, double *r, double *a, double *b);
void sum_arrays4(int n, double *r, double *a, double *b);
void sum_arrays3o(int n, double *r, double *a, double *b);

void test_copy(const char *msg, int n, double *a, double *b) {
  for(int i=0; i<n; i++) {
    if (a[i] != b[i]) {
      printf("%s: error at %i, %f != %f\n", msg, i, a[i], b[i]);
    }
  }
}

int main() {
  double a[N], b[N], r1[N], r2[N], r3[N], r4[N];
  for(int i=0; i<N; i++) {
    a[i] = i * 0.1;
    b[i] = (double) i*i * 0.001;
  }
  sum_arrays1(N, r1, a, b);
  sum_arrays2(N, r2, a, b);
  sum_arrays3(N, r3, a, b);
  sum_arrays4(N, r4, a, b);
  sum_arrays3o(N, r4, a, b);
  
  test_copy("1v2", N, r1, r2);
  test_copy("1v3", N, r1, r3);
  test_copy("1v4", N, r1, r4);
  test_copy("1v3o", N, r1, r4);
}
