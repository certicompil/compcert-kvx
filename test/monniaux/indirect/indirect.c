#include <stdio.h>

typedef int unary(int x);

int add1(int x) {
  return x+1;
}

int foo(unary *f, int x) {
  return (*f)(x+1);
}

int main() {
  printf("%d\n", foo(add1, 3));
}
