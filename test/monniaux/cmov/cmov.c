#include <stdio.h>

long cmovl(int x, long y, long z) {
  return __builtin_sel(x, y, z);
}

int cmovi(int x, int y, int z) {
  return __builtin_sel(x, y, z);
}

double cmovd(int x, double y, double z) {
  return __builtin_sel(x, y, z);
}

int main() {
  printf("%ld\n", cmovl(1, 42, 65));
  printf("%ld\n", cmovl(0, 42, 65));
  printf("%d\n", cmovi(1, 42, 65));
  printf("%d\n", cmovi(0, 42, 65));
  printf("%f\n", cmovd(1, 42., 65.));
  printf("%f\n", cmovd(0, 42., 65.));
}
