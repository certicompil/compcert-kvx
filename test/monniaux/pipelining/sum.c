double somme(int n, double *t) {
  double s= 0.0;
  for(int i=0; i<n; i++) s += t[i];
  return s;
}
