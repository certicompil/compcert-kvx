#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "../cycles.h"

static uint32_t dm_random_uint32(void) {
  static uint32_t current=UINT32_C(0xDEADBEEF);
  current = ((uint64_t) current << 6) % UINT32_C(4294967291);
  return current;
}

static uint64_t dm_biased_random_uint32(void) {
  uint32_t flags = dm_random_uint32();
  uint32_t r;
  switch (flags & 15) {
  case 0:
    r = dm_random_uint32() & 0xFU;
    break;
  case 1:
    r = dm_random_uint32() & 0xFFU;
    break;
  case 2:
    r = dm_random_uint32() & 0xFFFU;
    break;
  case 3:
    r = dm_random_uint32() & 0xFFFFU;
    break;
  case 4:
    r = dm_random_uint32() & 0xFFFFFU;
    break;
  case 5:
    r = dm_random_uint32() & 0xFFFFFFU;
    break;
  case 6:
    r = dm_random_uint32() & 0xFFFFFFFU;
    break;
  case 7:
    r = dm_random_uint32() & 0x3;
    break;
  default:
    r = dm_random_uint32();
  }
  return r;
}

inline uint32_t native_udiv32(uint32_t x, uint32_t y) {
  return x/y;
}
extern uint32_t my_udiv32(uint32_t x, uint32_t y);

int main() {
  cycle_t time_me=0, time_native=0;
  cycle_count_config();
  
  for(int i=0; i<1000; i++) {    
    uint32_t x = dm_biased_random_uint32();
    uint32_t y = dm_biased_random_uint32();
    if (y == 0) continue;
    
    cycle_t cycle_a, cycle_b, cycle_c;
    
    uint32_t q1, q2;
    cycle_a = get_cycle();
    q1 = native_udiv32(x, y);
    cycle_b = get_cycle();
    q2 = my_udiv32(x, y);
    cycle_c = get_cycle();

    if(q1 != q2) {
      printf("ERREUR %u %u\n", q1, q2);
    }
    
    time_native += cycle_b - cycle_a;
    time_me += cycle_c - cycle_b;
  }

  printf("%" PRcycle "\t%" PRcycle "\n", time_native, time_me);
  
  return 0;
}
