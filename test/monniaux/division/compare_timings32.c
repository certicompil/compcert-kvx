#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include "cycles.h"

#define CHECKS(mode, quotient) \
void checks_##mode() {				\
  uint32_t checksum=UINT32_C(0),		\
    a=UINT32_C(0x1000000),			\
    b=UINT32_C(0x1000);				\
  for(int i=0; i<10000; i++) {			\
    uint32_t q = (quotient);			\
    a += UINT32_C(0x367);			\
    b += UINT32_C(0x13);			\
    checksum += q;				\
  }						\
  printf("checksum = %" PRIx32 "\n", checksum);	\
}

#define CHECKS2(mode, quotient) \
void checks2_##mode() {				\
  uint32_t checksum=UINT32_C(0),		\
    a=UINT32_C(0x1000000),			\
    b=UINT32_C(0x1000);				\
  for(int i=0; i<5000; i++) {			\
    uint32_t q = (quotient);			\
    a += UINT32_C(0x367);			\
    b += UINT32_C(0x13);			\
    checksum += q;				\
    q = (quotient);		 	        \
    a += UINT32_C(0x367);			\
    b += UINT32_C(0x13);			\
    checksum += q;				\
  }						\
  printf("checksum = %" PRIx32 "\n", checksum);	\
}

CHECKS(normal, a/b)
CHECKS(fp, __builtin_fp_udiv32(a, b))

CHECKS2(normal, a/b)
CHECKS2(fp, __builtin_fp_udiv32(a, b))
       
int main() {
  cycle_t start, stop;
  cycle_count_config();

  start = get_cycle();
  checks_normal();
  stop = get_cycle();
  printf("normal division: %" PRcycle " cycles\n", stop-start);

  start = get_cycle();
  checks_fp();
  stop = get_cycle();
  printf("fp division: %" PRcycle " cycles\n", stop-start);

  start = get_cycle();
  checks2_normal();
  stop = get_cycle();
  printf("normal division x2: %" PRcycle " cycles\n", stop-start);

  start = get_cycle();
  checks2_fp();
  stop = get_cycle();
  printf("fp division x2: %" PRcycle " cycles\n", stop-start);
}
