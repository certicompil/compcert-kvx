#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include "cycles.h"

#define CHECKS(mode, quotient) \
void checks_##mode() {				\
  uint64_t checksum=UINT64_C(0),		\
    a=UINT64_C(0x10000000000),			\
    b=UINT64_C(0x1000);				\
  for(int i=0; i<10000; i++) {			\
    uint64_t q = (quotient);			\
    a += UINT64_C(0x36667);			\
    b += UINT64_C(0x13);			\
    checksum += q;				\
  }						\
  printf("checksum = %" PRIx64 "\n", checksum);	\
}

#define CHECKS2(mode, quotient) \
void checks2_##mode() {				\
  uint64_t checksum=UINT64_C(0),		\
    a=UINT64_C(0x10000000000),			\
    b=UINT64_C(0x1000);				\
  for(int i=0; i<5000; i++) {			\
    uint64_t q = (quotient);			\
    a += UINT64_C(0x36667);			\
    b += UINT64_C(0x13);			\
    checksum += q;				\
    q = (quotient);		 	        \
    a += UINT64_C(0x36667);			\
    b += UINT64_C(0x13);			\
    checksum += q;				\
  }						\
  printf("checksum = %" PRIx64 "\n", checksum);	\
}

CHECKS(normal, a/b)
CHECKS(fp, __builtin_fp_udiv64(a, b))

CHECKS2(normal, a/b)
CHECKS2(fp, __builtin_fp_udiv64(a, b))
       
int main() {
  cycle_t start, stop;
  cycle_count_config();

  start = get_cycle();
  checks_normal();
  stop = get_cycle();
  printf("normal division: %" PRcycle " cycles\n", stop-start);

  start = get_cycle();
  checks_fp();
  stop = get_cycle();
  printf("fp division: %" PRcycle " cycles\n", stop-start);

  start = get_cycle();
  checks2_normal();
  stop = get_cycle();
  printf("normal division x2: %" PRcycle " cycles\n", stop-start);

  start = get_cycle();
  checks2_fp();
  stop = get_cycle();
  printf("fp division x2: %" PRcycle " cycles\n", stop-start);
}
