#include <assert.h>
#define copy_int(x) __builtin_copy_int((x))

int unprotected(int x, int y) {
  return x*x+y*y;
}

int badly_protected(int x, int y) {
  int x2 = x;
  int y2 = y;
  int z = x*x+y*y;
  int z2 = x2*x2+y2*y2;
  assert(z == z2);
  return z;
}

int protected(int x, int y) {
  int x2 = copy_int(x);
  int y2 = copy_int(y);
  int z = x*x+y*y;
  int z2 = x2*x2+y2*y2;
  assert(z == z2);
  return z;
}
