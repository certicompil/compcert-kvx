#include <assert.h>

struct vec {
  int x, y, z;
};;

void incr_vec(struct vec *v) {
  v -> x ++;
  v -> y ++;
  v -> z ++;
}

#define N 2000000000
int main() {
  struct vec v0 = {0, 0, 0 };
  for(int i=0; i<N; i++) {
    incr_vec(&v0);
  }
  assert(v0.x == N);
  assert(v0.y == N);
  assert(v0.z == N);
}
