#include <stdio.h>

int f(int n) {
	if (n > 0) 
		return 42;
	else
		return n;
}


int main(int argc, char *argv[]) {
    int a=1;
	float b=2.;
	int c = f(a);
	a = 3;
	int d = f(a);
	printf("%e, %d, %d, %d", b, a, c, d);
    return 0;
}
