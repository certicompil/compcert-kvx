open Op
open BTL
open BTLcommonaux
open Maps
open Camlcoq
open LazyCodeCore

(** Functions of the LCT oracle that are specific to the underlying
architecture. We only define SR-related functions here. *)

let is_constant_op op =
  match op with
  | Ointconst _ -> false (* TODO future work for now we only focus on long *)
  | Olongconst _ -> true
  | _ -> false

let get_const_from_reg r =
  match Hashtbl.find constants r with
  | Olongconst l, _ -> l
  | _ -> failwith "get_const_from_reg: unsupported constant"

let match_injuring_op op args dst cargs =
  let check_arg_dst arg =
    match List.find_opt (P.( = ) arg) cargs with
    | Some reg -> reg = dst
    | None -> false
  in
  match op with
  | Oaddimm _ -> None (* TODO future work *)
  | Oadd -> None (* TODO future work *)
  | Oaddlimm l ->
      let arg = List.hd args in
      if check_arg_dst arg then Some (arg, l) else None
  | Oaddl ->
      let a1 = List.nth args 0 and a2 = List.nth args 1 in
      let is_c1 = is_constant_reg a1 and is_c2 = is_constant_reg a2 in
      if (not is_c1) && not is_c2 then None
      else
        let ax, ac = if is_c1 then (a2, a1) else (a1, a2) in
        if check_arg_dst ax then Some (ax, get_const_from_reg ac) else None
  | _ -> None

(* Auxiliary function for candidate detection: the [sr_pattern] parameter
 * is a reference containing the current sequence, which will be either:TODO*)
let is_sr_candidate_op op args o_pc_dst =
  (*print_affine_map affine_int64 Camlcoq.camlint64_of_coqint;*)
  if !ok_sr then
    match (op, args) with
    | Omul, [ a1; a2 ] -> None (* TODO future work *)
    | Oshllimm l, [ a1 ] ->
        let l = Integers.Int64.shl Integers.Int64.one l in
        (match o_pc_dst with
        | Some (pc, dst) -> aff_mul_int64_uset pc dst a1 l false
        | _ -> ());
        Some (CSR (SRmul, op, args))
    | Omull, [ a1; a2 ] ->
        let is_c1 = is_constant_reg a1 and is_c2 = is_constant_reg a2 in
        if is_c1 || is_c2 then (
          let ax, ac = if is_c1 then (a2, a1) else (a1, a2) in
          (match o_pc_dst with
          | Some (pc, dst) ->
              aff_mul_int64_uset pc dst ax (get_const_from_reg ac) false
          | _ -> ());
          Some (CSR (SRmul, op, args)))
        else None
    | Oaddl, [ a1; a2 ] ->
        (match o_pc_dst with
        | Some (pc, dst) -> aff_add2_int64_uset pc dst a1 a2 false
        | _ -> ());
        Some (CSR (SRadd, op, args))
    | Oaddlimm l, [ a1 ] ->
        (match o_pc_dst with
        | Some (pc, dst) -> aff_add1_int64_uset pc dst a1 l true
        | _ -> ());
        None
    | _ -> None
  else None

let simplify_trivial_ops btl id2blk =
  let gm = ref PTree.empty in
  let rec simplify_trivial_ops_rec pc ib =
    match ib with
    | Bop (Omull, [ a1; a2 ], dst, iinfo) ->
        if
          Hashtbl.find_opt constants a1 |> Option.is_some
          && Hashtbl.find_opt constants a2 |> Option.is_some
        then (
          let l1 = get_const_from_reg a1 and l2 = get_const_from_reg a2 in
          radd_constant_to_gm btl gm id2blk a1 pc;
          radd_constant_to_gm btl gm id2blk a2 pc;
          Bop (Olongconst (Integers.Int64.mul l1 l2), [], dst, iinfo))
        else ib
    | Bseq (ib1, ib2) ->
        Bseq (simplify_trivial_ops_rec pc ib1, simplify_trivial_ops_rec pc ib2)
    | ib -> ib
  in
  let btl' = ref PTree.empty in
  List.iter
    (fun (pc, ibf) ->
      let ib' = simplify_trivial_ops_rec pc ibf.entry in
      let ibf' = mk_ibinfo ib' ibf.binfo in
      btl' := PTree.set pc ibf' !btl')
    (PTree.elements btl);
  (!btl', !gm)

let is_immediate_sr_op = function Oshllimm _ -> true | _ -> false

(* Extracting the constant value from a SR key. *)
let extract_ckey_const = function
  | CSR (SRmul, Oshllimm l, _) -> Integers.Int64.shl Integers.Int64.one l
  | CSR (SRmul, Omull, args) ->
      let r = List.find is_constant_reg args in
      get_const_from_reg r
  | _ -> failwith "extract_ckey_const: unsupported ckey"

(* This is the operation that we will use to insert an update in SR *)
let mk_sr_update_op l = Oaddlimm l
let is_sr_update_op = function Oaddlimm _ -> true | _ -> false

let is_affine_op = function
  | Oaddlimm _ | Oshllimm _ | Oaddl | Omull -> true
  | _ -> false
