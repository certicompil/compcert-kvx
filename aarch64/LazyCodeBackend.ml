let is_constant_op op = false

let get_const_from_reg r =
  failwith "get_const_from_reg: SR NYS"

let match_injuring_op op args dst r = None

let is_sr_candidate_op op args odst = None

let is_immediate_sr_op op = false

let extract_ckey_const ckey = failwith "extract_ckey_const: unsupported ckey"

let mk_sr_update_op l = failwith "sr_update_op: SR NYS"

let is_sr_update_op op = false

let is_affine_op op = false
