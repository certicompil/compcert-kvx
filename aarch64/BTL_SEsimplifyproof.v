Require Import Op.
Require Import BTL_SEtheory BTL_SEsimuref BTL_SEsimplify.

Import SvalNotations.

(* Main proof of simplification *)

Lemma rewrite_ops_correct RRULES ctx op lsv l hlsv fsv args: forall
   (H: rewrite_ops RRULES op lsv = Some fsv)
   (LMAP: lmap_sv (fun sv => Some sv) lsv = Some l)
   (ELSVEQ: list_sval_equiv ctx l hlsv)
   (OK: eval_list_sval ctx hlsv = Some args),
   eval_sval ctx fsv = eval_operation (cge ctx) (csp ctx) op args (cm0 ctx).
Proof.
  unfold rewrite_ops; simpl.
  intros H ? ? ?.
  congruence.
Qed.
Global Opaque rewrite_ops.

Lemma rewrite_cbranches_nofail RRULES hrs c1 l1 ctx sis c2 l2: forall
  (H: rewrite_cbranches RRULES hrs c1 l1 = Some (c2, l2))
  (REF: ris_refines ctx hrs sis)
  (OK: ris_ok ctx hrs)
  (LMAP: lmap_sv sis l1 = None),
  False.
Proof.
  unfold rewrite_cbranches, eval_scondition; simpl.
  intros H ? ?.
  congruence.
Qed.

Lemma rewrite_cbranches_correct RRULES hrs c1 l1 ctx sis c2 l2 l3: forall
  (H: rewrite_cbranches RRULES hrs c1 l1 = Some (c2, l2))
  (REF: ris_refines ctx hrs sis)
  (OK: ris_ok ctx hrs)
  (LMAP: lmap_sv sis l1 = Some l3),
  eval_scondition ctx c2 l2 =
  eval_scondition ctx c1 l3.
Proof.
  unfold rewrite_cbranches; simpl.
  intros H ? ?.
  congruence.
Qed.
Global Opaque rewrite_cbranches.
